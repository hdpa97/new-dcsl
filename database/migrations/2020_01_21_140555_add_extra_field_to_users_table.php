<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraFieldToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('npk')->nullable();
            $table->integer('role')->nullable()->default(2);
            $table->boolean('flag')->default(0);
        });

        $this->insertSuperAdmin();
        $this->insertSurveyor('00002','Brigitta','brigitta@mail.com');
        $this->insertSurveyor('00003','Indra','indra@mail.com');
        $this->insertSurveyor('00004','Sisca','sisca@mail.com');
    }

    public function down()
    {

    }

    public function insertSuperAdmin()
    {
        DB::table('users')->insert([
	        'name' => 'Super Admin',
	        'email' => 'superadmin@gmail.com', // superadmin@gmail.com
            'password' => '$2y$10$jH/tYT6.Yx.jJhCFZtSA/O4ipELhWW4lsLtMqX4ufkW4lnE2f7lGK', // Le@rning1!
            'flag' => '1',
            'remember_token' => '0vzFEJs1n8STJAcxMxOGFG0QRu8W6ngm6rzre1b57cbZYCT2Hkfm5k7sxFor',
            'created_at' => '2020-01-01 00:00:00',
            'updated_at' => '2020-01-01 00:00:00',
            'npk' => '00001',
            'role' => '1' // Super Admin
        ]);
    }

    public function insertSurveyor($npk,$nama,$email)
    {
        DB::table('users')->insert([
            'name' => $nama,
	        'email' => $email, 
            'password' => '$2y$10$jH/tYT6.Yx.jJhCFZtSA/O4ipELhWW4lsLtMqX4ufkW4lnE2f7lGK', // Le@rning1!
            'flag' => '0',
            'remember_token' => '0vzFEJs1n8STJAcxMxOGFG0QRu8W6ngm6rzre1b57cbZYCT2Hkfm5k7sxFor',
            'created_at' => '2020-01-01 00:00:00',
            'updated_at' => '2020-01-01 00:00:00',
            'npk' => $npk,
            'role' => '0' // Surveyor
        ]);
    }

}
