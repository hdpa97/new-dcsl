<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKodePengeluaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kode_pengeluaran', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_pengeluaran')->index();
            $table->string('pengeluaran')->nullable();
            $table->string('huruf_pengeluaran')->index();
            $table->boolean('flag')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kode_pengeluaran');
    }
}
