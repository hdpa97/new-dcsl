<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKodePendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kode_pendidikan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_pendidikan')->index();
            $table->string('nama_pendidikan')->nullable();
            $table->boolean('flag')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kode_pendidikan');
    }
}
