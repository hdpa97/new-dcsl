<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblLayer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_layer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_layer');
            $table->boolean('flag')->default(1);
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('last_update_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_layer');
    }
}
