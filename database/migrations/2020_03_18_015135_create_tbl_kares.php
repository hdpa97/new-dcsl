<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKares extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_kares')->nullable();
            $table->boolean('flag')->default(1);
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('last_update_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kares');
    }
}
