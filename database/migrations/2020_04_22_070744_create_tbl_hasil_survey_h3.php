<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHasilSurveyH3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_hasil_survey_h3', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_data')->unsigned()->notNullable()->index();
            $table->string('status_survey')->notNullable();
            $table->string('kode_data')->nullable();

            $table->string('T1')->nullable();
            $table->string('T1_NOTE')->nullable();
            $table->string('A1_1')->nullable();
            $table->string('A1_2')->nullable();
            $table->string('B1_1')->nullable();
            $table->string('B1_2')->nullable();
            $table->string('B1_3')->nullable();
            $table->string('B1_4')->nullable();

            $table->string('C1')->nullable();
            $table->string('C1_A')->nullable();
            $table->string('C1_B')->nullable();
            $table->string('C2')->nullable();
            $table->string('C3')->nullable();
            $table->string('C4')->nullable();
            $table->string('C5')->nullable();
            $table->string('D1')->nullable();
            $table->string('D2')->nullable();
            $table->string('D3_1')->nullable();
            $table->string('D3_2')->nullable();
            $table->string('D3_2A')->nullable();
            $table->string('D3_3')->nullable();
            $table->string('D3_3A')->nullable();
            $table->string('D3_3B')->nullable();
            $table->string('D4')->nullable();
            $table->string('D5')->nullable();
            $table->string('E1')->nullable();
            $table->string('E2')->nullable();

            $table->string('F1')->nullable();
            $table->string('G1')->nullable();
            $table->string('G2')->nullable();
            $table->string('G3')->nullable();
            $table->string('H1')->nullable();
            $table->string('H1_NOTE')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_hasil_survey_h3');
    }
}
