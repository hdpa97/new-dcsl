<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKabupaten extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kabupaten', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_kabupaten')->nullable();
            $table->boolean('flag')->default(1);
            $table->timestamps();
            $table->integer('created_by')->unsigned()->notNullable();
            $table->integer('last_update_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kabupaten');
    }
}
