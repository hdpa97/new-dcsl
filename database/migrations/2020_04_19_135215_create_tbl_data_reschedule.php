<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDataReschedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_data_reschedule', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_data')->unsigned()->notNullable()->index();
            $table->string('status_survey')->notNullable();
            $table->string('kode_data')->nullable();
            $table->date('tgl_reschedule')->nullable();
            $table->string('flag')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_data_reschedule');
    }
}
