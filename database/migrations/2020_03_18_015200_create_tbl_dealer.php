<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDealer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_dealer', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_kares');
            $table->integer('id_jaringan')->unsigned()->notNullable();
            $table->integer('id_kabupaten')->unsigned()->notNullable();
            $table->integer('id_layer')->unsigned()->notNullable();
            $table->string('kode_dealer')->nullable();
            $table->string('nama_dealer')->nullable();
            $table->string('alamat')->nullable();
            $table->string('nama_pic')->nullable();
            $table->string('no_hp_pic')->nullable();
            $table->string('email_pic')->nullable();
            $table->boolean('flag')->default(1);
            $table->timestamps();
            $table->integer('created_by')->unsigned()->notNullable();
            $table->integer('last_update_by')->unsigned()->notNullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_dealer');
    }
}
