<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHasilSurveyH2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_hasil_survey_h2', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_data')->unsigned()->notNullable()->index();
            $table->string('status_survey')->notNullable();
            $table->string('kode_data')->nullable();

            $table->string('A1')->nullable();
            $table->string('A2')->nullable();
            $table->string('A3_1')->nullable();
            $table->string('A3_2')->nullable();
            $table->string('A3_3')->nullable();
            $table->string('A3_3A')->nullable();
            $table->string('A3_3B')->nullable();
            $table->string('A3_2B')->nullable();
            $table->string('A3_2B_NOTE')->nullable();
            $table->string('A3_3C')->nullable();
            $table->string('A3_3C_NOTE')->nullable();
            $table->string('A3_4')->nullable();

            $table->string('B1_1')->nullable();
            $table->string('B1_2')->nullable();
            $table->string('C1_1')->nullable();
            $table->string('C1_2')->nullable();
            $table->string('C1_3')->nullable();
            $table->string('C1_3A')->nullable();
            $table->string('C1_4')->nullable();
            $table->string('C1_4A')->nullable();
            $table->string('C1_2A')->nullable();
            $table->string('C1_2A_NOTE')->nullable();
            $table->string('C2')->nullable();
            $table->string('C2_NOTE')->nullable();
            $table->string('C3')->nullable();
            $table->string('C3_NOTE')->nullable();
            $table->string('C1_5')->nullable();

            $table->string('D1')->nullable();
            $table->string('D2')->nullable();
            $table->string('D3_1')->nullable();
            $table->string('D3_2')->nullable();
            $table->string('D3_3')->nullable();
            $table->string('D3_3A')->nullable();
            $table->string('D4')->nullable();
            $table->string('D5')->nullable();

            $table->string('E1_1')->nullable();
            $table->string('E1_1A')->nullable();
            $table->string('E1_2')->nullable();
            $table->string('E1_3')->nullable();
            $table->string('E1_4')->nullable();
            $table->string('G1')->nullable();
            $table->string('G2')->nullable();
            $table->string('G3')->nullable();

            $table->string('H1')->nullable();
            $table->string('H2')->nullable();
            $table->string('H3')->nullable();
            $table->string('H4')->nullable();

            $table->string('I4')->nullable();
            $table->string('I4_NOTE')->nullable();
            $table->string('I5')->nullable();
            $table->string('I6')->nullable();
            $table->string('I7')->nullable();
            $table->string('I8')->nullable();
            $table->string('I8_NOTE')->nullable();
            $table->string('I9')->nullable();
            $table->string('I9_NOTE')->nullable();
            $table->string('K1')->nullable();

            $table->string('N1')->nullable();
            $table->string('N2')->nullable();
            $table->string('N3_MONTH')->nullable();
            $table->string('N3_YEARS')->nullable();
            $table->string('N3_NOTE')->nullable();
            $table->string('N4')->nullable();
            $table->string('N4_NOTE')->nullable();
            $table->string('N5')->nullable();
            $table->string('N5_NOTE')->nullable();
            $table->string('N6')->nullable();
            $table->string('N6_NOTE')->nullable();
            $table->string('N7_NOTE')->nullable();
            $table->string('N8')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_hasil_survey_h2');
    }
}
