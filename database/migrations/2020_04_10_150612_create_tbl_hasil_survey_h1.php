<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHasilSurveyH1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_hasil_survey_h1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_data')->unsigned()->notNullable()->index();
            $table->string('status_survey')->notNullable();
            $table->string('tanda_kendaraan')->nullable(); // Apakah Anda sudah memiliki STNK atau yang Anda miliki saat ini hanya Surat Jalan?
            $table->string('kepemilikan')->nullable(); // Sepeda motor ini milik pribadi atau perusahaan?
            $table->string('kode_data')->nullable();

            $table->string('A1')->nullable();
            $table->string('A2')->nullable();
            $table->string('A3_1')->nullable();
            $table->string('A3_2')->nullable();
            $table->string('A3_3')->nullable();
            $table->string('A3_4')->nullable();

            $table->string('B1_1')->nullable();
            $table->string('B1_2')->nullable();
            $table->string('B1_3')->nullable();
            $table->string('B1_3A')->nullable();
            $table->string('B1_4')->nullable();
            $table->string('B1_5')->nullable();
            $table->string('B1_5A')->nullable();
            $table->string('B3')->nullable();
            $table->string('B4')->nullable();

            $table->string('D15')->nullable();
            $table->string('E1_1')->nullable();
            $table->string('E1_2')->nullable();
            $table->string('E1_3')->nullable();
            $table->string('F2_1')->nullable();
            $table->string('F2_2')->nullable();
            $table->string('G1')->nullable();

            $table->string('H1_1')->nullable();
            $table->string('H1_2')->nullable();
            $table->string('H1_3')->nullable();
            $table->string('H1_4')->nullable();
            $table->string('H1_5')->nullable();
            $table->string('H1_5A')->nullable();
            $table->string('H1_5B')->nullable();
            $table->string('H1_5C')->nullable();
            $table->string('H1_5D')->nullable();
            $table->string('H1_5D_NOTE')->nullable();
            $table->string('H1_6')->nullable();
            $table->string('H2')->nullable();

            $table->string('H3_A1')->nullable();
            $table->string('H3_A2')->nullable();
            $table->string('H3_A3')->nullable();
            $table->string('H3_A4')->nullable();
            $table->string('H3_A5')->nullable();
            $table->string('I1_1')->nullable();
            $table->string('I1_2')->nullable();
            $table->string('J1_1')->nullable();
            $table->string('J1_2')->nullable();

            $table->string('N1')->nullable();
            $table->string('N2_MONTH')->nullable();
            $table->string('N2_YEARS')->nullable();
            $table->string('N2_NOTE')->nullable();
            $table->string('N3')->nullable();
            $table->string('N3_NOTE')->nullable();
            $table->string('N4')->nullable();
            $table->string('N4_NOTE')->nullable();
            $table->string('N5')->nullable();
            $table->string('N5_NOTE')->nullable();
            $table->string('N7')->nullable();
            $table->string('N7_NOTE')->nullable();
            $table->string('N8')->nullable();
            $table->string('N8_NOTE')->nullable();
            $table->string('N9')->nullable();
            
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_hasil_survey_h1');
    }
}
