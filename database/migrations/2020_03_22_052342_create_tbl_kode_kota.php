<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblKodeKota extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_kode_kota', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_kota')->index();
            $table->string('nama_kota');
            $table->string('kode_prov')->index();
            $table->boolean('flag')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_kode_kota');
    }
}
