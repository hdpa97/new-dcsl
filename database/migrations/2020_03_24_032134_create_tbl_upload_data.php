<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblUploadData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_upload_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_rangka')->nullable();
            $table->string('kode_mesin')->nullable();
            $table->string('nomor_mesin')->nullable();
            $table->date('tgl_cetak')->nullable();
            $table->date('tgl_mohon')->nullable();
            $table->string('nama_lengkap')->nullable();
            $table->string('alamat')->nullable();
            $table->string('kelurahan')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('kode_kota')->nullable()->index(); // fk
            $table->string('kode_prov')->nullable()->index(); // fk
            $table->string('cash_credit')->nullable();
            $table->string('kode_dealer')->nullable()->index(); // fk
            $table->string('finance_company')->nullable()->index(); // fk
            $table->string('down_payment')->nullable()->index(); // fk
            $table->string('tenor')->nullable();
            $table->string('jenis_sales')->nullable();
            $table->string('gender')->nullable();
            $table->date('tgl_lahir')->nullable();  
            $table->string('kode_agama')->nullable()->index(); // fk
            $table->string('kode_pekerjaan')->nullable()->index(); // fk
            $table->string('kode_pendidikan')->nullable()->index(); // fk
            $table->string('kode_pengeluaran')->nullable()->index(); // fk
            $table->string('sales_person')->nullable();
            $table->string('umur')->nullable(); 
            $table->string('tipe')->nullable();
            $table->string('tiga_jenis')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('no_telp')->nullable();
            $table->boolean('flag')->default(1);
            $table->string('kode_data')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_upload_data');
    }
}
