<?php

use Illuminate\Database\Seeder;

class tbl_kares_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertKares('Yogyakarta');
        $this->insertKares('Kedu');
        $this->insertKares('Banyumas');
    }

    public function insertKares($kares)
    {
        DB::table('tbl_kares')->insert([
	        'nama_kares' => $kares,
            'flag' => '1',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' =>  1,
            'last_update_by' =>  1
        ]);
    }
}
