<?php

use Illuminate\Database\Seeder;

class tbl_motor_premium_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertMotorPremium("Forza");
        $this->insertMotorPremium("CRF 250");
        $this->insertMotorPremium("MONKEY");
        $this->insertMotorPremium("PCX Hybrid");
        $this->insertMotorPremium("SH 150");
        $this->insertMotorPremium("Super Cub");
        $this->insertMotorPremium("CBR 250 Series");
    }
    
    public function insertMotorPremium($nama)
    {
        DB::table('tbl_motor_premium')->insert([
	        'nama_motor' => $nama,
            'flag' => '1',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' =>  1,
            'last_update_by' =>  1
        ]);
    }
}
