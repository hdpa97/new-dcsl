<?php

use Illuminate\Database\Seeder;

class tbl_dealer_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // H1, REGULER, YOGYAKARTA, SLEMAN
        $this->insertDealer('11505','ASTRA MOTOR KALIURANG',1,2,1,1);
        $this->insertDealer('10104','DAYA ANUGRAH MANDIRI - SLEMAN',1,2,1,1);
        // H1, REGULER, YOGYAKARTA, KOTA YOGYAKARTA
        $this->insertDealer('05560','ASTRA MOTOR TEGALREJO',1,2,1,2);
        $this->insertDealer('03552','COLOMBO PRIMA ARTHA LESTARI - MANGKUBUMI',1,2,1,2);
        $this->insertDealer('04757','NGABEAN MOTOR',1,2,1,2);
        // H1, REGULER, YOGYAKARTA, BANTUL
        $this->insertDealer('04016','ASTRA MOTOR BANTUL',1,2,1,3);
        $this->insertDealer('15331','ASTRA MOTOR BANTUL 2',1,2,1,3);
        // H1, REGULER, YOGYAKARTA, KULONPROGO
        $this->insertDealer('07312','PANDEAN BARU MOTOR - WATES',1,2,1,5);
        $this->insertDealer('13020','ASTRA MOTOR WATES',1,2,1,5);
        // H1, REGULER, YOGYAKARTA, GUNUNG KIDUL
        $this->insertDealer('15328','ASTRA MOTOR WONOSARI',1,2,1,4);
        // H1, REGULER, BANYUMAS, BANYUMAS
        $this->insertDealer('07318','SANJAYA GEMILANG - SUMPIUH',1,2,3,6);
        $this->insertDealer('13660','KOMPO MOTOR - AJIBARANG',1,2,3,6);
        // H1, REGULER, BANYUMAS, PURBALINGGA
        $this->insertDealer('10097','PACIFIC AREA JAYA',1,2,3,7);
        // H1, REGULER, KEDU, MAGELANG
        $this->insertDealer('12683','ASTRA MOTOR MAGELANG 2',1,2,2,8);
        // H1, REGULER, KEDU, TEMANGGUNG
        $this->insertDealer('04700','SAUDARA JAYA MAKMUR - PARAKAN',1,2,2,9);
        // H1, REGULER, KEDU, PURWOREJO
        $this->insertDealer('11963','ASTRA MOTOR PURWOREJO',1,2,2,10);
        // H1, REGULER, KEDU, KEBUMEN
        $this->insertDealer('13902','ASTRA MOTOR KEBUMEN 2',1,2,2,11);
        // H1, REGULER, KEDU, WONOSOBO
        $this->insertDealer('04684','MARABUNTA SAKTI',1,2,2,12);
        // H123, BIG WING, YOGYAKARTA, SLEMAN
        $this->insertDealer('00009','ASTRA MOTOR CENTER YOGYAKARTA',2,1,1,1);
        // H123, REGULER, YOGYAKARTA, SLEMAN
        $this->insertDealer('00869','TUNASJAYA MEKARARMADA - JL. MAGELANG',2,2,1,1);
        $this->insertDealer('12029','NAGAMAS SURYA DINAMIKA - PALEMGURIH',2,2,1,1);
        $this->insertDealer('07311','WAHANAARTHA RITELINDO - SLEMAN',2,2,1,1);
        $this->insertDealer('07350','SUMBER PURNAMA SAKTI - SLEMAN',2,2,1,1);
        $this->insertDealer('07348','UTAMA MOTOR - SLEMAN',2,2,1,1);
        $this->insertDealer('08458','TUNAS DWIPA MATRA - SLEMAN',2,2,1,1);
        $this->insertDealer('04009','ASTRA MOTOR GODEAN',2,2,1,1);
        $this->insertDealer('02595','ABC MOTOR - KALASAN',2,2,1,1);
    }   

    /* ID JARINGAN
        1 = H1
        2 = H123
    */

    /* ID LAYER
        1 = BIG WING
        2 = REGULER
        3 = WING
        4 = WING SATELITE
    */

    /* ID KARES
        1 = YOGYAKARTA
        2 = KEDU 
        3 = BANYUMAS
    */

    /* ID KABUPATEN
        1 = SLEMAN
        2 = KOTA YOGYAKARTA
        3 = BANTUL
        4 = GUNUNG KIDUL
        5 = KULONPROGO
        6 = BANYUMAS
        7 = PURBALINGGA
        8 = MAGELANG
        9 = TEMANGGUNG
        10 = PURWOREJO
        11 = KEBUMEN
        12 = WONOSOBO
        13 = CILACAP
        14 = BANJARNEGARA
    */
    
    public function insertDealer($kode_dealer,$nama_dealer,$id_jaringan,$id_layer,$id_kares,$id_kabupaten)
    {
        DB::table('tbl_dealer')->insert([
	        'id_kares' => $id_kares,
            'id_jaringan' => $id_jaringan,
            'id_kabupaten' => $id_kabupaten,
            'id_layer' => $id_layer,
            'kode_dealer' => $kode_dealer,
            'nama_dealer' => $nama_dealer,
            'flag' => 1,
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' =>  1,
            'last_update_by' =>  1
        ]);
    }
}
