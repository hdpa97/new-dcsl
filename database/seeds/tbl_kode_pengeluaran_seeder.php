<?php

use Illuminate\Database\Seeder;

class tbl_kode_pengeluaran_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPengeluaran("E","<Rp 900.000,-");
        $this->insertPengeluaran("D","Rp 900.001,- s/d Rp 1.250.000,-");
        $this->insertPengeluaran("C2","Rp 1.250.001,- s/d Rp 1.750.000,-");
        $this->insertPengeluaran("C1","Rp 1.750.001,- s/d Rp 2.500.000,-");
        $this->insertPengeluaran("B","Rp 2.500.001,- s/d Rp 4.000.000,-");
        $this->insertPengeluaran("A2","Rp 4.000.001.- s/d Rp 6.000.000,-");
        $this->insertPengeluaran("A1",">Rp 6.000.000,-");
    }   

    
    public function insertPengeluaran($kode,$pengeluaran)
    {
        DB::table('tbl_kode_pengeluaran')->insert([
	        'kode_pengeluaran' => $kode,
            'pengeluaran' => $pengeluaran,
            'flag' => 1
        ]);
    }
}
