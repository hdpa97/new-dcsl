<?php

use Illuminate\Database\Seeder;

class tbl_agama_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertAgama("1","Islam");
        $this->insertAgama("2","Kristen");
        $this->insertAgama("3","Katolik");
        $this->insertAgama("4","Hindu");
        $this->insertAgama("5","Budha");
        $this->insertAgama("6","Lain-lain");
    }   

    
    public function insertAgama($kode,$agama)
    {
        DB::table('tbl_kode_agama')->insert([
	        'kode_agama' => $kode,
            'nama_agama' => $agama,
            'flag' => 1
        ]);
    }
}
