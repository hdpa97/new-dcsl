<?php

use Illuminate\Database\Seeder;

class tbl_users_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertSurveyor('17563','Brigitta','brigitta@mail.com');
        $this->insertSurveyor('00003','Indra','indra@mail.com');
        $this->insertSurveyor('00004','Sisca','sisca@mail.com');
    }
    
    public function insertSurveyor($npk,$nama,$email)
    {
        DB::table('users')->insert([
            'name' => $nama,
	        'email' => $email, 
            'password' => '$2y$10$jH/tYT6.Yx.jJhCFZtSA/O4ipELhWW4lsLtMqX4ufkW4lnE2f7lGK', // Le@rning1!
            'flag' => '0',
            'remember_token' => '0vzFEJs1n8STJAcxMxOGFG0QRu8W6ngm6rzre1b57cbZYCT2Hkfm5k7sxFor',
            'created_at' => '2020-01-01 00:00:00',
            'updated_at' => '2020-01-01 00:00:00',
            'npk' => $npk,
            'role' => '0' // Surveyor
        ]);
    }
}
