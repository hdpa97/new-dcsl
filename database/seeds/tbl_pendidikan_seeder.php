<?php

use Illuminate\Database\Seeder;

class tbl_pendidikan_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPendidikan("1","Tidak Tamat SD");
        $this->insertPendidikan("2","SD");
        $this->insertPendidikan("3","SLTP/SMP");
        $this->insertPendidikan("4","SLTA/SMU");
        $this->insertPendidikan("5","Akademi/Diploma");
        $this->insertPendidikan("6","Universitas");
        $this->insertPendidikan("7","Pasca Sarjana");
    }   

    
    public function insertPendidikan($kode,$pendidikan)
    {
        DB::table('tbl_kode_pendidikan')->insert([
	        'kode_pendidikan' => $kode,
            'nama_pendidikan' => $pendidikan,
            'flag' => 1
        ]);
    }
}
