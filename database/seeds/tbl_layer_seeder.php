<?php

use Illuminate\Database\Seeder;

class tbl_layer_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertLayer('BIG WING');
        $this->insertLayer('REGULER');
        $this->insertLayer('WING');
        $this->insertLayer('WING SATELITE');
    }
    
    public function insertLayer($layer)
    {
        DB::table('tbl_layer')->insert([
	        'nama_layer' => $layer,
            'flag' => '1',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' =>  1,
            'last_update_by' =>  1
        ]);
    }
}
