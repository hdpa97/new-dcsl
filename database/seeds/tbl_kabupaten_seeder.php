<?php

use Illuminate\Database\Seeder;

class tbl_kabupaten_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertKab('Sleman');
        $this->insertKab('Kota Yogyakarta');
        $this->insertKab('Bantul');
        $this->insertKab('Gunung Kidul');
        $this->insertKab('Kulonprogo');
        $this->insertKab('Banyumas');
        $this->insertKab('Purbalingga');
        $this->insertKab('Magelang');
        $this->insertKab('Temanggung');
        $this->insertKab('Purworejo');
        $this->insertKab('Kebumen');
        $this->insertKab('Wonosobo');
        $this->insertKab('Cilacap');
        $this->insertKab('Banjarnegara');
    }

    public function insertKab($kabupaten)
    {
        DB::table('tbl_kabupaten')->insert([
	        'nama_kabupaten' => $kabupaten,
            'flag' => '1',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' =>  1,
            'last_update_by' =>  1
        ]);
    }
}
