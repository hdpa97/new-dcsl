<?php

use Illuminate\Database\Seeder;

class tbl_kode_pekerjaan_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPekerjaan("1","Pegawai Negeri");
        $this->insertPekerjaan("2","Pegawai Swasta");
        $this->insertPekerjaan("2a","Pegawai Swasta - Pertanian/ Perkebunan/ Kehutanan/ Perikanan/ Peternakan");
        $this->insertPekerjaan("2b","Pegawai Swasta - Industri");
        $this->insertPekerjaan("2c","Pegawai Swasta - Konstruksi");
        $this->insertPekerjaan("2d","Pegawai Swasta - Pertambangan");
        $this->insertPekerjaan("2e","Pegawai Swasta - Jasa");
        $this->insertPekerjaan("2f","Pegawai Swasta - Perdagangan (Retail)");
        $this->insertPekerjaan("3","Ojek");
        $this->insertPekerjaan("4","Wiraswasta/Pedagang");
        $this->insertPekerjaan("4a","Wiraswasta/Pedagang - Pertanian/ Perkebunan/ Kehutanan/ Perikanan/ Peternakan");
        $this->insertPekerjaan("4b","Wiraswasta/Pedagang - Industri");
        $this->insertPekerjaan("4c","Wiraswasta/Pedagang - Konstruksi");
        $this->insertPekerjaan("4d","Wiraswasta/Pedagang - Pertambangan");
        $this->insertPekerjaan("4e","Wiraswasta/Pedagang - Jasa");
        $this->insertPekerjaan("4f","Wiraswasta/Pedagang - Perdagangan (Retail)");
        $this->insertPekerjaan("5","Mahasiswa/Pelajar");
        $this->insertPekerjaan("6","Guru/Dosen");
        $this->insertPekerjaan("7","TNI/ Polri");
        $this->insertPekerjaan("8","Ibu Rumah Tangga");
        $this->insertPekerjaan("9","Petani/Nelayan");
        $this->insertPekerjaan("10","Profesional (Dokter, Pengacara, dll)");
        $this->insertPekerjaan("11","Lain-lain, sebutkan isi lain-lainnya, disediakan field untuk mengisi lain-lain");
        $this->insertPekerjaan("12","Dokter");
        $this->insertPekerjaan("13","Pengacara");
        $this->insertPekerjaan("14","Wartawan");
        $this->insertPekerjaan("15","Petani");
        $this->insertPekerjaan("16","Nelayan");
    }   

    
    public function insertPekerjaan($kode,$pekerjaan)
    {
        DB::table('tbl_kode_pekerjaan')->insert([
	        'kode_pekerjaan' => $kode,
            'nama_pekerjaan' => $pekerjaan,
            'flag' => 1
        ]);
    }
}
