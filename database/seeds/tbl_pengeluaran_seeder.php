<?php

use Illuminate\Database\Seeder;

class tbl_pengeluaran_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertPengeluaran("1","E","<Rp 900.000,-");
        $this->insertPengeluaran("2","D","Rp 900.001,- s/d Rp 1.250.000,-");
        $this->insertPengeluaran("3","C2","Rp 1.250.001,- s/d Rp 1.750.000,-");
        $this->insertPengeluaran("4","C1","Rp 1.750.001,- s/d Rp 2.500.000,-");
        $this->insertPengeluaran("5","B","Rp 2.500.001,- s/d Rp 4.000.000,-");
        $this->insertPengeluaran("6","A2","Rp 4.000.001.- s/d Rp 6.000.000,-");
        $this->insertPengeluaran("7","A1",">Rp 6.000.000,-");
    }   

    
    public function insertPengeluaran($kode,$huruf,$pengeluaran)
    {
        DB::table('tbl_kode_pengeluaran')->insert([
	        'kode_pengeluaran' => $kode,
            'pengeluaran' => $pengeluaran,
            'huruf_pengeluaran' => $huruf,
            'flag' => 1
        ]);
    }
}
