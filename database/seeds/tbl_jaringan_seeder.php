<?php

use Illuminate\Database\Seeder;

class tbl_jaringan_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->insertJaringan('H1');
        $this->insertJaringan('H123');
    }

    public function insertJaringan($jaringan)
    {
        DB::table('tbl_jaringan')->insert([
	        'nama_jaringan' => $jaringan,
            'flag' => '1',
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' =>  1,
            'last_update_by' =>  1
        ]);
    }
}
