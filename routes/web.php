<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/surveyor', 'HomeController@indexSurveyor')->name('home.surveyor');

Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/{id}/edit','UserController@edit')->name('users.edit');
Route::put('/users/{id}', 'UserController@update')->name('users.update');
Route::put('/user/{id}/disable', 'UserController@disable')->name('users.disable');
Route::put('/user/{id}/enable', 'UserController@enable')->name('users.enable');

Route::get('/settings', 'UserController@setting')->name('users.setting');
Route::post('/settings', 'UserController@updateProfile')->name('users.general');
Route::post('/settings/password', 'UserController@updatePassword')->name('users.password');

Route::get('/register', 'Auth\RegisterController@showRegisForm')->name('register');
Route::post('/register', 'Auth\RegisterController@store')->name('register.submit');

// KARES
Route::get('/data-kares', 'KaresController@index')->name('kares.index');
Route::post('/data-kares', 'KaresController@store')->name('kares.store');
Route::get('/data-kares/create', 'KaresController@create')->name('kares.create');
Route::get('/data-kares/{id}/edit', 'KaresController@edit')->name('kares.edit');
Route::put('/data-kares/{id}', 'KaresController@update')->name('kares.update');
Route::put('/data-kares/{id}/disable', 'KaresController@disable')->name('kares.disable');
Route::put('/data-kares/{id}/enable', 'KaresController@enable')->name('kares.enable');

// JARINGAN
Route::get('/data-jaringan', 'JaringanController@index')->name('jaringan.index');
Route::post('/data-jaringan', 'JaringanController@store')->name('jaringan.store');
Route::get('/data-jaringan/create', 'JaringanController@create')->name('jaringan.create');
Route::get('/data-jaringan/{id}/edit', 'JaringanController@edit')->name('jaringan.edit');
Route::put('/data-jaringan/{id}', 'JaringanController@update')->name('jaringan.update');
Route::put('/data-jaringan/{id}/disable', 'JaringanController@disable')->name('jaringan.disable');
Route::put('/data-jaringan/{id}/enable', 'JaringanController@enable')->name('jaringan.enable');

// LAYER
Route::get('/data-layer', 'LayerController@index')->name('layer.index');
Route::post('/data-layer', 'LayerController@store')->name('layer.store');
Route::get('/data-layer/create', 'LayerController@create')->name('layer.create');
Route::get('/data-layer/{id}/edit', 'LayerController@edit')->name('layer.edit');
Route::put('/data-layer/{id}', 'LayerController@update')->name('layer.update');
Route::put('/data-layer/{id}/disable', 'LayerController@disable')->name('layer.disable');
Route::put('/data-layer/{id}/enable', 'LayerControlle@enable')->name('layer.enable');

// KABUPATEN
Route::get('/data-kabupaten', 'KabupatenController@index')->name('kabupaten.index');
Route::post('/data-kabupaten', 'KabupatenController@store')->name('kabupaten.store');
Route::get('/data-kabupaten/create', 'KabupatenController@create')->name('kabupaten.create');
Route::get('/data-kabupaten/{id}/edit', 'KabupatenController@edit')->name('kabupaten.edit');
Route::put('/data-kabupaten/{id}', 'KabupatenController@update')->name('kabupaten.update');
Route::put('/data-kabupaten/{id}/disable', 'KabupatenController@disable')->name('kabupaten.disable');
Route::put('/data-kabupaten/{id}/enable', 'KabupatenController@enable')->name('kabupaten.enable');

// DEALER
Route::get('/data-dealer', 'DealerController@index')->name('dealer.index');
Route::post('/data-dealer', 'DealerController@store')->name('dealer.store');
Route::get('/data-dealer/fetch_krs/{idKares}', 'DealerController@fetch_kab');
Route::get('/data-dealer/create', 'DealerController@create')->name('dealer.create');
Route::get('/data-dealer/{id}/edit', 'DealerController@edit')->name('dealer.edit');
Route::put('/data-dealer/{id}', 'DealerController@update')->name('dealer.update');
Route::put('/data-dealer/{id}/disable', 'DealerController@disable')->name('dealer.disable');
Route::put('/data-dealer/{id}/enable', 'DealerController@enable')->name('dealer.enable');

// CEK
Route::get('/cek', 'CekController@index')->name('cek.index');
Route::get('/cek/fetch_krs/{idKares}', 'CekController@fetch_kab');
Route::get('/cek/fetch_kab/{idKabupaten}', 'CekController@fetch_dealer');
Route::get('/cek-detail/{kodeDealer}/h1', 'CekController@detail_h1')->name('detail.h1');
Route::get('/cek-detail/{kodeDealer}/h2', 'CekController@detail_h2')->name('detail.h2');
Route::get('/cek-detail/{kodeDealer}/h3', 'CekController@detail_h3')->name('detail.h3');

//REPORT
Route::get('/results', 'ReportController@results')->name('results.index');

Route::get('/data-scoring', 'ReportController@kepuasan')->name('kepuasan.index');
Route::get('/data-scoring/{kode}-{tipe}', 'ReportController@scoringDealer')->name('scoring.dealer');
Route::get('/detail-data/{kode}-{id}', 'ReportController@detailData')->name('detail.data');
Route::get('/demografi', 'ReportController@index')->name('demografi.index');

Route::get('/surveyor', 'ReportController@indexx')->name('surveyor.index');
Route::get('/surveyor/detail', 'ReportController@detail')->name('surveyor.detail');

Route::get('/tersurvey', 'ReportController@indexxx')->name('tersurvey.index');
Route::get('/tersurvey/fetch_jar/{idJaringan}', 'ReportController@fetch_jar');
Route::get('/tersurvey/fetch_krs/{idKares}', 'ReportController@fetch_kab');
Route::get('/tersurvey/fetch_dlr/{idKares}','ReportController@fetch_dlr');
Route::get('/tersurvey/fetch_kab/{idKabupaten}', 'ReportController@fetch_dealer');

// UPLOAD DATA
Route::get('/upload-data', 'UploadDataController@index')->name('upload-data.index');
Route::post('/upload-data', 'UploadDataController@importExcel')->name('upload-data.import.excel');

// RO DATA
Route::get('/ro-data', 'RepeatOrderController@index')->name('ro-data.index');

// FINANCE COMPANY
Route::get('/data-finance-company', 'FinanceCompanyController@index')->name('finance.company.index');
Route::get('/data-finance-company/fetch', 'FinanceCompanyController@fetch')->name('finance.company.fetch');
Route::post('/data-finance-company/add', 'FinanceCompanyController@add')->name('finance.company.add');
Route::post('/data-finance-company/update', 'FinanceCompanyController@update')->name('finance.company.update');
Route::post('/data-finance-company/delete', 'FinanceCompanyController@delete')->name('finance.company.delete');

// MOTOR PREMIUM
Route::get('/data-motor-premium', 'MotorPremiumController@index')->name('motor.premium.index');
Route::get('/data-motor-premium/fetch', 'MotorPremiumController@fetch')->name('motor.premium.fetch');
Route::post('/data-motor-premium/add', 'MotorPremiumController@add')->name('motor.premium.add');
Route::post('/data-motor-premium/update', 'MotorPremiumController@update')->name('motor.premium.update');
Route::post('/data-motor-premium/delete', 'MotorPremiumController@delete')->name('motor.premium.delete');

// AGAMA
Route::get('/data-agama', 'AgamaController@index')->name('agama.index');
Route::get('/data-agama/fetch', 'AgamaController@fetch')->name('agama.fetch');
Route::post('/data-agama/add', 'AgamaController@add')->name('agama.add');
Route::post('/data-agama/update', 'AgamaController@update')->name('agama.update');
Route::post('/data-agama/delete', 'AgamaController@delete')->name('agama.delete');

// PEKERJAAN
Route::get('/data-pekerjaan', 'PekerjaanController@index')->name('pekerjaan.index');
Route::get('/data-pekerjaan/fetch', 'PekerjaanController@fetch')->name('pekerjaan.fetch');
Route::post('/data-pekerjaan/add', 'PekerjaanController@add')->name('pekerjaan.add');
Route::post('/data-pekerjaan/update', 'PekerjaanController@update')->name('pekerjaan.update');
Route::post('/data-pekerjaan/delete', 'PekerjaanController@delete')->name('pekerjaan.delete');

// PEKERJAAN
Route::get('/data-pendidikan', 'PendidikanController@index')->name('pendidikan.index');
Route::get('/data-pendidikan/fetch', 'PendidikanController@fetch')->name('pendidikan.fetch');
Route::post('/data-pendidikan/add', 'PendidikanController@add')->name('pendidikan.add');
Route::post('/data-pendidikan/update', 'PendidikanController@update')->name('pendidikan.update');
Route::post('/data-pendidikan/delete', 'PendidikanController@delete')->name('pendidikan.delete');

// PENGELUARAN
Route::get('/data-pengeluaran', 'PengeluaranController@index')->name('pengeluaran.index');
Route::get('/data-pengeluaran/fetch', 'PengeluaranController@fetch')->name('pengeluaran.fetch');
Route::post('/data-pengeluaran/add', 'PengeluaranController@add')->name('pengeluaran.add');
Route::post('/data-pengeluaran/update', 'PengeluaranController@update')->name('pengeluaran.update');
Route::post('/data-pengeluaran/delete', 'PengeluaranController@delete')->name('pengeluaran.delete');

// SURVEY
Route::get('/survey', 'SurveyController@index')->name('survey.index');
Route::get('/detail-survey/{kodeDealer}/h1', 'SurveyController@detail_h1')->name('detail.survey.h1');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/1', 'SurveyController@panel_penjualan_page_1')->name('panel.penjualan.page.1');
Route::post('/store/page/1', 'SurveyController@store_page_1')->name('store.page.1');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/2', 'SurveyController@panel_penjualan_page_2')->name('panel.penjualan.page.2');
Route::post('/store/page/2', 'SurveyController@store_page_2')->name('store.page.2');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/3', 'SurveyController@panel_penjualan_page_3')->name('panel.penjualan.page.3');
Route::post('/store/page/3', 'SurveyController@store_page_3')->name('store.page.3');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/4', 'SurveyController@panel_penjualan_page_4')->name('panel.penjualan.page.4');
Route::post('/store/page/4', 'SurveyController@store_page_4')->name('store.page.4');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/5', 'SurveyController@panel_penjualan_page_5')->name('panel.penjualan.page.5');
Route::post('/store/page/5', 'SurveyController@store_page_5')->name('store.page.5');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/6', 'SurveyController@panel_penjualan_page_6')->name('panel.penjualan.page.6');
Route::post('/store/page/6', 'SurveyController@store_page_6')->name('store.page.6');
Route::get('/{kodeDealer}/panel-penjualan/{idData}/page/7', 'SurveyController@panel_penjualan_page_7')->name('panel.penjualan.page.7');
Route::post('/store/page/7', 'SurveyController@store_page_7')->name('store.page.7');

Route::get('/detail-survey/{kodeDealer}/h2/h3', 'SurveyController@detail_h2_h3')->name('detail.survey.h23');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/1', 'SurveyController@panel_pemeliharaan_page_1')->name('panel.pemeliharaan.page.1');
Route::post('/storee/page/1', 'SurveyController@storee_page_1')->name('storee.page.1');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/2', 'SurveyController@panel_pemeliharaan_page_2')->name('panel.pemeliharaan.page.2');
Route::post('/storee/page/2', 'SurveyController@storee_page_2')->name('storee.page.2');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/3', 'SurveyController@panel_pemeliharaan_page_3')->name('panel.pemeliharaan.page.3');
Route::post('/storee/page/3', 'SurveyController@storee_page_3')->name('storee.page.3');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/4', 'SurveyController@panel_pemeliharaan_page_4')->name('panel.pemeliharaan.page.4');
Route::post('/storee/page/4', 'SurveyController@storee_page_4')->name('storee.page.4');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/5', 'SurveyController@panel_pemeliharaan_page_5')->name('panel.pemeliharaan.page.5');
Route::post('/storee/page/5', 'SurveyController@storee_page_5')->name('storee.page.5');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/6', 'SurveyController@panel_pemeliharaan_page_6')->name('panel.pemeliharaan.page.6');
Route::post('/storee/page/6', 'SurveyController@storee_page_6')->name('storee.page.6');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/7', 'SurveyController@panel_pemeliharaan_page_7')->name('panel.pemeliharaan.page.7');
Route::post('/storee/page/7', 'SurveyController@storee_page_7')->name('storee.page.7');
Route::get('/{kodeDealer}/panel-pemeliharaan/{idData}/page/8', 'SurveyController@panel_pemeliharaan_page_8')->name('panel.pemeliharaan.page.8');
Route::post('/storee/page/8', 'SurveyController@storee_page_8')->name('storee.page.8');

Route::get('/{kodeDealer}/panel-suku-cadang/{idData}/page/1', 'SurveyController@panel_sukucadang_page_1')->name('panel.sukucadang.page.1');
Route::post('/storeee/page/1', 'SurveyController@storeee_page_1')->name('storeee.page.1');
Route::get('/{kodeDealer}/panel-suku-cadang/{idData}/page/2', 'SurveyController@panel_sukucadang_page_2')->name('panel.sukucadang.page.2');
Route::post('/storeee/page/2', 'SurveyController@storeee_page_2')->name('storeee.page.2');
Route::get('/{kodeDealer}/panel-suku-cadang/{idData}/page/3', 'SurveyController@panel_sukucadang_page_3')->name('panel.sukucadang.page.3');
Route::post('/storeee/page/3', 'SurveyController@storeee_page_3')->name('storeee.page.3');
Route::get('/{kodeDealer}/panel-suku-cadang/{idData}/page/4', 'SurveyController@panel_sukucadang_page_4')->name('panel.sukucadang.page.4');
Route::post('/storeee/page/4', 'SurveyController@storeee_page_4')->name('storeee.page.4');


Route::get('/{kodeDealer}/reschedule-survey/{kodeData}/{idData}/create', 'RescheduleController@create')->name('reschedule.survey');
Route::post('/reschedule-survey/{id}', 'RescheduleController@update')->name('reschedule.update');
Route::get('/report-survey', 'SurveyController@report')->name('survey.report');


// SURVEY REJECT
Route::get('/survey-reject', 'SurveyRejectController@index')->name('survey-reject.index');
