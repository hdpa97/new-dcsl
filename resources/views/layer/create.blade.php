@extends('layouts.sidebar')

@section('title-tab')
Add Layer
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('layer.index')}}">Data Layer</a></li>
    <li class="breadcrumb-item active">Add Layer</li>
</ol>
@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert">×</button>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Tambah Data') }}</div>

                <div class="card-body">
                    <form class="needs-validation" method="POST" action="{{ route('layer.store') }}" autocomplete="off">
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="nama_layer" class="col-md-3 col-form-label text-md-left">{{ __('Nama Layer') }}</label>

                            <div class="col-md-8">
                                <input id="nama_layer" type="text" class="form-control @error('nama_layer') is-invalid @enderror" name="nama_layer" value="{{ old('nama_layer') }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="flag" class="col-md-3 col-form-label text-md-left">{{ __('Status') }}</label>

                            <div class="col-md-8 mt-2">
                                <input name="flag" id="f1" required type="radio" value="1"><label for="f1">&nbsp;Enable</label>
                                &emsp;
                                <input name="flag" id="f2" type="radio" value="0"><label for="f2">&nbsp;Disable</label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Tambah') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection