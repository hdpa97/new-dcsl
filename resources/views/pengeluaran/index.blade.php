@extends('layouts.sidebar')

@section('title-tab')
Data Pengeluaran
@endsection
@section('button')
<a class="" onClick="return fetchData()">
    <button class="btn btn-md btn-primary pull-right">
        <i class="fas fa-plus-circle"></i>
        Tambah Data
    </button>
</a>
@endsection
@section('main-content')
<div class="card mb-4">
    <div class="card-body">
        <div id="message"></div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="dataTablePengeluaran" width="100%">
                <thead>
                    <th class="text-center" width="5%">No.</th>
                    <th class="text-center">Kode Pengeluaran</th>
                    <th class="text-center">Pengeluaran</th>
                    <th class="text-center">Huruf Pengeluaran</th>
                    <th class="text-center" width="15%">Opsi</th>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr id="{{$data->id}}">
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">
                            <span class="editSpan kd">{{$data->kode_pengeluaran}}</span>
                            <input class="editInput kd form-control input-sm" type="text" id="kode_pengeluaran{{$data->id}}" value="{{$data->kode_pengeluaran}}" style="display: none;">
                        </td>
                        <td class="text-center">
                            <span class="editSpan nm">{{$data->pengeluaran}}</span>
                            <input class="editInput nm form-control input-sm" type="text" id="pengeluaran{{$data->id}}" value="{{$data->pengeluaran}}" style="display: none;">
                        </td>
                        <td class="text-center">
                            <span class="editSpan hr">{{$data->huruf_pengeluaran}}</span>
                            <input class="editInput hr form-control input-sm" type="text" id="huruf_pengeluaran{{$data->id}}" value="{{$data->huruf_pengeluaran}}" style="display: none;">
                        </td>
                        <td class="text-center">
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-sm btn-info mr-2 editBtn" style="float: none;">edit</button>
                                <button type="button" class="btn btn-sm btn-danger deleteBtn" style="float: none;">delete</button>
                            </div>
                            <button type="button" class="btn btn-sm btn-success saveBtn" style="float: none; display: none;">Save</button>
                            <button type="button" class="btn btn-sm btn-success confirmBtn" style="float: none; display: none;">Confirm</button>
                            <button type="button" class="btn btn-sm btn-warning cancelBtn" style="float: none; display: none;">Cancel</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ csrf_field() }}
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    function fetchData() {
        $.ajax({
            url: "{{ route('pengeluaran.fetch') }}",
            dataType: "json",
            success: function(data) {
                var html = '';
                html += '<tr>';
                html += '<td></td>';
                html += '<td> <input id="addKodePengeluaran" class="form-control input-sm addInput"/></td>';
                html += '<td> <input id="addPengeluaran" class="form-control input-sm addInput"/></td>';
                html += '<td> <input id="addHurufPengeluaran" class="form-control input-sm addInput"/></td>';
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-success" id="add">Add</button>';
                html += '<button type="button" class="btn btn-sm btn-warning cancelAdd ml-2" style="float: none;">Cancel</button></td></tr>';
                for (var count = 0; count < data.length; count++) {
                    var ct = count + 1;
                    html += '<tr id="' + data[count].id + '">';
                    html += '<td class="text-center">' + ct + '</td>';
                    html += '<td class="text-center">';
                    html += '<span class="editSpan kd">' + data[count].kode_pengeluaran + '</span>';
                    html += '<input class="editInput kd form-control input-sm" type="text" id="kode_pengeluaran' + data[count].id + '" value="' + data[count].kode_pengeluaran + '" style="display: none;">'
                    html += '</td>';
                    html += '<td class="text-center">';
                    html += '<span class="editSpan nm">' + data[count].pengeluaran + '</span>';
                    html += '<input class="editInput nm form-control input-sm" type="text" id="pengeluaran' + data[count].id + '" value="' + data[count].pengeluaran + '" style="display: none;">'
                    html += '</td>';
                    html += '<td class="text-center">';
                    html += '<span class="editSpan hr">' + data[count].huruf_pengeluaran + '</span>';
                    html += '<input class="editInput nm form-control input-sm" type="text" id="huruf_pengeluaran' + data[count].id + '" value="' + data[count].huruf_pengeluaran + '" style="display: none;">'
                    html += '</td>';
                    html += '<td class="text-center">';
                    html += '<div class="btn-group btn-group-sm">';
                    html += '<button type="button" class="btn btn-sm btn-info mr-2 editBtn" style="float: none;">edit</button>';
                    html += '<button type="button" class="btn btn-sm btn-danger deleteBtn" style="float: none;">delete</button>'
                    html += '</div>';
                    html += '<button type="button" class="btn btn-sm btn-success saveBtn" style="float: none; display: none;">Save</button>';
                    html += '<button type="button" class="btn btn-sm btn-success confirmBtn" style="float: none; display: none;">Confirm</button>';
                    html += '<button type="button" class="btn btn-sm btn-warning cancelBtn ml-2" style="float: none; display: none;">Cancel</button>';
                    html += '</td>';
                }
                $('tbody').html(html);
            }
        });
    }

    $(document).ready(function() {

        // fetchData();

        var _token = $('input[name="_token"]').val();

        $(document).on('click', '#add', function() {
            var kode_pengeluaran = $('#addKodePengeluaran').val();
            var pengeluaran = $('#addPengeluaran').val();
            var huruf_pengeluaran = $('#addHurufPengeluaran').val();
            if (kode_pengeluaran || pengeluaran || huruf_pengeluaran != '') {
                if (confirm("Apakah ingin menambah data?")) {
                    $.ajax({
                        url: "{{ route('pengeluaran.add') }}",
                        method: "POST",
                        data: {
                            kode_pengeluaran: kode_pengeluaran,
                            pengeluaran: pengeluaran,
                            huruf_pengeluaran: huruf_pengeluaran,
                            _token: _token
                        },
                        success: function(data) {
                            $('#message').html(data);
                            $('#addKodePengeluaran').hide();
                            $('#addNamaPengeluaran').hide();
                            window.location.href = "{{route('pengeluaran.index')}}";
                        }
                    });
                } else {
                    window.location.href = "{{route('pengeluaran.index')}}";
                }
            } else {
                $('#message').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button>Lengkapi kolom.</div>");
            }
        });

        $(document).on('click', '.editBtn', function() {
            $(this).closest("tr").find(".editSpan").hide();
            $(this).closest("tr").find(".editInput").show();
            $(this).closest("tr").find(".editBtn").hide();
            $(this).closest("tr").find(".saveBtn").show();
            $(this).closest("tr").find(".deleteBtn").hide();
            $(this).closest("tr").find(".cancelBtn").show();
        });

        $(document).on('click', '.saveBtn', function() {
            var trObj = $(this).closest("tr");
            var id = $(this).closest("tr").attr('id');
            var kode_pengeluaran = $("#kode_pengeluaran" + id).val();
            var pengeluaran = $("#pengeluaran" + id).val();
            var huruf_pengeluaran = $("#huruf_pengeluaran" + id).val();

            $.ajax({
                url: "{{ route('pengeluaran.update') }}",
                method: "POST",
                data: {
                    kode_pengeluaran: kode_pengeluaran,
                    pengeluaran: pengeluaran,
                    huruf_pengeluaran: huruf_pengeluaran,
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $('#message').html(data);
                    trObj.find(".editSpan.kd").text(kode_pengeluaran);
                    trObj.find(".editInput.kd").text(kode_pengeluaran);
                    trObj.find(".editSpan.nm").text(pengeluaran);
                    trObj.find(".editInput.nm").text(pengeluaran);
                    trObj.find(".editSpan.hr").text(huruf_pengeluaran);
                    trObj.find(".editInput.hr").text(huruf_pengeluaran);
                    trObj.find(".editInput").hide();
                    trObj.find(".saveBtn").hide();
                    trObj.find(".editSpan").show();
                    trObj.find(".editBtn").show();
                    window.location.href = "{{route('pengeluaran.index')}}";
                }
            })

        });

        $(document).on('click', '.deleteBtn', function() {
            $(this).closest("tr").find(".deleteBtn").hide();
            $(this).closest("tr").find(".editBtn").hide();
            $(this).closest("tr").find(".confirmBtn").show();
            $(this).closest("tr").find(".cancelBtn").show();
        });

        $(document).on('click', '.confirmBtn', function() {
            var trObj = $(this).closest("tr");
            var id = $(this).closest("tr").attr('id');
            $.ajax({
                type: "POST",
                url: "{{ route('pengeluaran.delete') }}",
                dataType: "json",
                data: {
                    id: id,
                    _token: _token
                },
                success: function(res) {
                    if (res.status == "ok") {
                        $('#message').html(res.alert);
                        trObj.remove();
                        window.location.href = "{{route('pengeluaran.index')}}";
                    } else {
                        trObj.find(".confirmBtn").hide();
                        trObj.find(".deleteBtn").show();
                        $('#message').html(res.alert);
                        window.location.href = "{{route('pengeluaran.index')}}";
                    }
                },
                error: function(err) {
                    trObj.find(".confirmBtn").hide();
                    trObj.find(".deleteBtn").show();
                    $('#message').html(
                        "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button>" + err.status + " " + err.statusText + "</div>"
                    );
                }
            });
        });

        $(document).on('click', '.cancelBtn', function() {
            $(this).closest("tr").find(".editInput").hide();
            $(this).closest("tr").find(".editSpan").show();
            $(this).closest("tr").find(".confirmBtn").hide();
            $(this).closest("tr").find(".cancelBtn").hide();
            $(this).closest("tr").find(".saveBtn").hide();
            $(this).closest("tr").find(".deleteBtn").show();
            $(this).closest("tr").find(".editBtn").show();
        });

        $(document).on('click', '.cancelAdd', function() {
            var trObj = $(this).closest("tr");
            trObj.remove();
        });

    });
</script>
@endsection
