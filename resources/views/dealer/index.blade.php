@extends('layouts.sidebar')

@section('title-tab')
Data Dealer
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET" action="{{ route('dealer.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Kares</td>
                            <td class="text-left">
                                <select name="idKares" id="id_kares" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($karess as $kares)
                                    <option value="{{ $kares->id }}" @if ($request->get('idKares') == $kares->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kares->nama_kares }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Kab./Kota</td>
                            <td class="text-left">
                                <select name="idKab" id="id_kabupaten" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($kabs as $kabupaten)
                                    <option value="{{ $kabupaten->id }}" @if ($request->get('idKab') == $kabupaten->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kabupaten->nama_kabupaten }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                                <button type="submit" class="btn btn-success btn-block submit-button">
                                    <i class='fa fa-search'></i>
                                    {{ __('Show') }}
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="col-md-4 text-right">
            <a class="" href="{{route('dealer.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
        <div class="col-md-4 text-right">
            <a class="" href="{{route('dealer.create')}}">
                <button class="btn btn-md btn-primary pull-right">
                    <i class="fas fa-plus-circle"></i>
                    Tambah Dealer
                </button>
            </a>
        </div>
    </div>
</div>
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableDealer" width="340%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center">Nama Dealer System</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Jaringan</th>
                        <th class="text-center">Layer</th>
                        <th class="text-center">Kares</th>
                        <th class="text-center">Kabupaten</th>
                        <th class="text-center">Nama PIC</th>
                        <th class="text-center">No. HP PIC</th>
                        <th class="text-center">Email PIC</th>
                        <th class="text-center">Status</th>
                        <th class="text-center" width="100px">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->alamat}}</td>
                        <td class="text-center">{{$data->nama_jaringan}}</td>
                        <td class="text-center">{{$data->nama_layer}}</td>
                        <td class="text-center">{{$data->nama_kares}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_pic}}</td>
                        @php
                        if(!preg_match('/[^+0-9]/',trim($data->no_hp_pic))){
                        if(substr(trim($data->no_hp_pic), 0, 2)=='62'){
                        $hp = '08'.substr(trim($data->no_hp_pic), 1);
                        }
                        elseif(substr(trim($data->no_hp_pic), 0, 1)=='8'){
                        $hp = '08'.substr(trim($data->no_hp_pic), 1);
                        }
                        echo "<td class='text-center'>$hp</td>";
                        }else{
                        echo "<td class='text-center'>$data->no_hp_pic</td>";
                        }
                        @endphp
                        <td class="text-center">{{$data->email_pic}}</td>
                        <td class="text-center">
                            @if ($data->flag == 1)
                            <label class="label label-success">Active</label>
                            @else
                            <label class="label label-danger">No</label>
                            @endif
                        </td>
                        <td class="text-center">
                            <a class="float-left ml-2" href="{{route('dealer.edit',['id' => $data->id])}}">
                                <button class="btn btn-sm btn-info">
                                    <i>edit</i>
                                </button>
                            </a>
                            @if ($data->flag == 1)
                            <form class="text-center ml-2 delete"
                                action="{{route('dealer.disable',['id' => $data->id])}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <i>disable</i>
                                </button>
                            </form>
                            @else
                            <form class="text-center ml-2 delete"
                                action="{{route('dealer.enable',['id' => $data->id])}}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('put') }}
                                <button type="submit" class="btn btn-sm btn-success">
                                    <i>enable</i>
                                </button>
                            </form>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        document.getElementById("id_kabupaten").disabled = true;

        $('#id_kares').change(function() {
            if ($(this).val() != '') {
                var idk = $(this).val();
                document.getElementById("id_kabupaten").disabled = true;
                $.ajax({
                    url: '/data-dealer/fetch_krs/' + idk,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_kabupaten").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            $('#id_kabupaten').empty();
                            $('#id_kabupaten').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_kabupaten').append('<option value="' + result[i].id_kab + '">' + result[i].nama_kab + '</option>');
                            }
                        }
                    }
                });
            }
        });
    });
</script>
@endsection
