@extends('layouts.sidebar')

@section('title-tab')
Edit Dealer
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('dealer.index')}}">Data Dealer</a></li>
    <li class="breadcrumb-item active">Edit Dealer</li>
</ol>
@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Edit Data') }}</div>

                <div class="card-body">
                    <form class="needs-validation" method="POST" action="{{ route('dealer.update', ['id' => $data->id]) }}" autocomplete="off">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="kode_dealer" class="col-md-3 col-form-label text-md-left">{{ __('Kode Dealer') }}</label>

                            <div class="col-md-8">
                                <input id="kode_dealer" type="text" class="form-control @error('kode_dealer') is-invalid @enderror" name="kode_dealer" value="{{ old('kode_dealer') ?? $data->kode_dealer }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_dealer" class="col-md-3 col-form-label text-md-left">{{ __('Nama Dealer') }}</label>

                            <div class="col-md-8">
                                <input id="nama_dealer" type="text" class="form-control @error('nama_dealer') is-invalid @enderror" name="nama_dealer" value="{{  old('nama_dealer') ?? $data->nama_dealer}}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-3 col-form-label text-md-left">{{ __('Alamat') }}</label>

                            <div class="col-md-8">
                                <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ old('alamat') ?? $data->alamat }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="id_jaringan" class="col-md-3 col-form-label text-md-left">{{ __('Jaringan') }}</label>

                            <div class="col-md-8">
                                <select name="id_jaringan" class="custom-select" required="required">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($jaringans as $jaringan)
                                        <option value="{{ $jaringan->id }}"
                                    @if ($jaringan->id == (old('id_jaringan') ?? $data->id_jaringan))
                                        selected="selected"
                                    @endif
                                        >{{$jaringan->nama_jaringan}}</option>
                                    @endforeach
						        </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="id_layer" class="col-md-3 col-form-label text-md-left">{{ __('Layer') }}</label>

                            <div class="col-md-8">
                                <select name="id_layer" class="custom-select" required="required">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($layers as $layer)
                                        <option value="{{ $layer->id }}"
                                    @if ($layer->id == (old('id_layer') ?? $data->id_layer))
                                        selected="selected"
                                    @endif
                                        >{{$layer->nama_layer}}</option>
                                    @endforeach
						        </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="id_kares" class="col-md-3 col-form-label text-md-left">{{ __('Kares') }}</label>

                            <div class="col-md-8">
                                <select name="id_kares" class="custom-select" required="required">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($karess as $kares)
                                        <option value="{{ $kares->id }}"
                                    @if ($kares->id == (old('id_kares') ?? $data->id_kares))
                                        selected="selected"
                                    @endif
                                        >{{$kares->nama_kares}}</option>
                                    @endforeach
						        </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="id_kabupaten" class="col-md-3 col-form-label text-md-left">{{ __('Kabupaten') }}</label>

                            <div class="col-md-8">
                                <select name="id_kabupaten" class="custom-select" required="required">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($kabupatens as $kabupaten)
                                        <option value="{{ $kabupaten->id }}"
                                    @if ($kabupaten->id == (old('id_kabupaten') ?? $data->id_kabupaten))
                                        selected="selected"
                                    @endif
                                        >{{$kabupaten->nama_kabupaten}}</option>
                                    @endforeach
						        </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="nama_pic" class="col-md-3 col-form-label text-md-left">{{ __('Nama Pic') }}</label>

                            <div class="col-md-8">
                                <input id="nama_pic" type="text" class="form-control @error('nama_pic') is-invalid @enderror" name="nama_pic" value="{{ old('nama_pic') ?? $data->nama_pic }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="no_hp_pic" class="col-md-3 col-form-label text-md-left">{{ __('No HP Pic') }}</label>

                            <div class="col-md-8">
                                <input id="no_hp_pic" type="text" class="form-control @error('no_hp_pic') is-invalid @enderror" name="no_hp_pic" value="{{ old('no_hp_pic') ?? $data->no_hp_pic }}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email_pic" class="col-md-3 col-form-label text-md-left">{{ __('Email Pic') }}</label>

                            <div class="col-md-8">
                                <input id="email_pic" type="text" class="form-control @error('email_pic') is-invalid @enderror" name="email_pic" value="{{ old('email_pic') ?? $data->email_pic }}" required autofocus>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="flag" class="col-md-3 col-form-label text-md-left">{{ __('Status') }}</label>

                            <div class="col-md-8 mt-2">
                                <input name="flag" id="f1" required type="radio" value="1" @if($data->flag == 1) checked @endif><label for="f1">&nbsp;Enable</label>
                                &emsp;
                                <input name="flag" id="f2" type="radio" value="0" @if($data->flag == 0) checked @endif><label for="f2">&nbsp;Disable</label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection