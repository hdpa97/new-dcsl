@extends('layouts.sidebar')

@section('title-tab')
Tersurvey
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET"
                action="{{ route('tersurvey.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Data</td>
                            <td class="text-left">
                                <select name="tipeData" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    <option value="All" @if($request->get('tipeData') == "All") selected
                                        @elseif(!$request->get('tipeData')) selected @endif>All</option>
                                    <option value="H1" @if($request->get('tipeData') == "H1") selected @endif> H1
                                    </option>
                                    <option value="H2" @if($request->get('tipeData') == "H2") selected @endif> H2
                                    </option>
                                    <option value="H3" @if($request->get('tipeData') == "H3") selected @endif> H3
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">Kares</td>
                            <td class="text-left">
                                <select name="idKares" id="id_kares" class="custom-select"   style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($karess as $kares)
                                    <option value="{{ $kares->id }}" @if ($request->get('idKares') == $kares->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kares->nama_kares }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Kab./Kota</td>
                            <td class="text-left">
                                <select name="idKab" id="id_kabupaten" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($kabs as $kabupaten)
                                    <option value="{{ $kabupaten->id }}" @if ($request->get('idKab') == $kabupaten->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kabupaten->nama_kabupaten }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Dealer</td>
                            <td class="text-left">
                                <select name="idDlr" id="id_dealer" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($dealers as $dealer)
                                    <option value="{{ $dealer->id }}" @if ($request->get('idDlr') == $dealer->id )
                                        selected="selected"
                                        @endif
                                        > {{ $dealer->nama_dealer }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
        <div class="col-md-4">
            <table cellpadding="3" cellspacing="0" class="">
                <tbody>
                    <tr>
                        <td class="text-left">Bulan</td>
                        <td class="text-left">
                            <select name="months[]" class="custom-select2" multiple="multiple" style="width:253px;">
                            @foreach(range(1,12) as $month)
                            <option value="{{$month}}"
                            @if($request->get('months') !== null)
                                @if (in_array($month, $request->get('months'), FALSE))
                                    selected
                                @endif
                            @endif
                            >
                                {{date("M", strtotime('2020-'.$month))}}
                            </option>
                            @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun</td>
                        <td class="text-left">
                            <select name="years[]" class="custom-select2" multiple="multiple" style="width:253px;">
                                @php
                                       $firstYear = (int)date('Y') - 1;
                                    $lastYear = $firstYear + 6;
                                @endphp
                                @for($i=$firstYear;$i<=$lastYear;$i++)
                                    <option value="{{$i}}"
                                    @if($request->get('years') !== null)
                                        @if (in_array($i, $request->get('years'), FALSE))
                                            selected
                                        @endif
                                    @endif
                                    >
                                        {{$i}}
                                    </option>
                                @endfor
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-left">
                            <button type="submit" class="btn btn-success btn-block submit-button">
                                <i class='fa fa-search'></i>
                                {{ __('Show') }}
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </form>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 text-right">
            <a class="" href="{{route('tersurvey.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
    </div>
</div>
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableTersurvey" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center">Dealer</th>
                        <th class="text-center">Kares</th>
                        <th class="text-center">Kabupaten/Kota</th>
                        <th class="text-center">Target</th>
                        <th class="text-center">Tersurvey</th>
                        <th class="text-center">%Tersurvey</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getTersurvey as $data)
                    @php $t = round(($data->total / 30) * 100,2); @endphp
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->nama_kares}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">30</td>
                        <td class="text-center" @if($data->total >= 30) style="background-color:#71c765" @else
                            style="background-color:#eba98d" @endif>
                            {{$data->total}}</td>
                        <td class="text-center">{{$t}}%</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        document.getElementById("id_kabupaten").disabled = true;
        document.getElementById("id_dealer").disabled = true;
        $('#id_kares').change(function() {
            if ($(this).val() != '') {
                var idk = $(this).val();
                document.getElementById("id_dealer").disabled = true;
                document.getElementById("id_kabupaten").disabled = true;
                $.ajax({
                    url: '/tersurvey/fetch_krs/' + idk,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_kabupaten").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            $('#id_kabupaten').empty();
                            $('#id_kabupaten').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_kabupaten').append('<option value="' + result[i].id_kab + '">' + result[i].nama_kab + '</option>');
                            }
                        }
                    }
                });
            }
        });

        $('#id_kabupaten').change(function() {
            if ($(this).val() != '') {
                var idkab = $(this).val();
                document.getElementById("id_dealer").disabled = true;
                $.ajax({
                    url: '/tersurvey/fetch_kab/' + idkab,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_dealer").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_dealer').append('<option value="' + result[i].id_dlr + '">' + result[i].nama_dlr + '</option>');
                            }
                        }
                    }
                });
            }
        });
    });
</script>
<script>
   $(document).ready(function () {
         $(".custom-select2").select2({
         });
   });
</script>
@endsection
