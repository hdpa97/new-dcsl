@extends('layouts.sidebar')

@section('title-tab')
    Reschedule Survey
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                </div>
            @endif
            <div class="card">
                <div class="card-header">{{ __('Tambah Data') }}</div>
                
                <div class="card-body">
                    <form class="needs-validation" method="POST" action="{{ route('reschedule.update', ['id' => $data->id]) }}" autocomplete="off">
                        
                        {{ csrf_field() }}

                        <div class="row">
                            <label class="col-sm-3">{{ __('NAMA DEALER') }}</label>
                            <div class="col-sm-4">
                                {{$dataDealer->nama_dealer }} ({{$dataDealer->kode_dealer}})
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3">{{ __('LAYER DEALER') }}</label>
                            <div class="col-sm-4">
                                @if($dataDealer->id_layer == 1)
                                BIG WING
                                @elseif($dataDealer->id_layer == 2)
                                REGULER
                                @elseif($dataDealer->id_layer == 3)
                                WING
                                @else
                                WING SATELITE
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3">{{ __('NAMA RESPONDEN') }}</label>
                            <div class="col-sm-4">
                                {{$datas->nama_lengkap}}
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3">{{ __('STATUS SURVEY') }}</label>
                            <div class="col-sm-4">
                                {{$data->status_survey}}
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3">{{ __('RESCHEDULE') }}</label>
                            <div class="col-sm-4">
                                <input type="hidden" name="kode_data" value="{{$datas->kode_data}}">
                                <input type="hidden" name="kode_dealer" value="{{$dataDealer->kode_dealer}}">
                                <input type="date" required id="dateField" name="tgl_reschedule" class="form-control">
                            </div>
                        </div>
                        <div class="row mt-4">
                        <label class="col-sm-3"></label>
                            <div class="col-sm-4">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Tambah') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    // SET DATE MIN D+1
    var today = new Date();
    var dd = today.getDate() + 1;
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("dateField").setAttribute("min", today);
</script>
@endsection
