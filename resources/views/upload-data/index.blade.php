@extends('layouts.sidebar')

@section('title-tab')

@endsection
@section('button')
<div class="card text-left">
    <div class="card-header">
    Upload New Data
    </div>
    <div class="card-body">
        <form id="uploadForm" class="needs-validation pull-left" method="POST" action="{{ route('upload-data.import.excel') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group row">
            <label for="id_jaringan" class="col-md-2 col-form-label text-md-left">{{ __('Pilih Data') }}</label>

            <div class="col-md-3">
                <select name="kode_data" class="custom-select" required>
                    <option disabled selected value="">- Pilih</option>
                    <option value="H1"> Data H1</option>
                    <option value="H23"> Data H23</option>
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="id_jaringan" class="col-md-2 col-form-label text-md-left">{{ __('File') }}
                <button type="button" style="background-color:transparent;border:none" data-toggle="modal" data-target="#ExampleExcelModal">
                    <i class="fas fa-question-circle"></i>
                </button>
            </label>

            <div class="col-md-3">
                <input type="file" style="padding:0; border:none"class="form-control" id="file" name="file" required>
            </div>
        </div>

        
        <div class="form-group row mb-0">
            <div class="col-md-2">
            </div>
            <div class="col-md-3">
                <button type="submit" class="btn btn-success btn-block submit-button">
                    <i class='fa fa-upload'></i>
                    {{ __('Upload') }}
                </button>
            </div>
        </div>

        </form>
    </div>
</div>
@endsection
@section('breadcrumb')

@endsection

@section('main-content')

<!--ExampleExcelUploadModal-->
<div class="modal fade" id="ExampleExcelModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-width:600px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Contoh File Excel</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md 6">
                        <ol class="mt-3">
                            <li><u>Jangan membuka file</u> yang akan di upload</li>
                            <li><b>Maksimal 1000 data dalam 1 file</b></li>
                            <li>Memiliki urutan sebagai berikut
                                <ul>
                                    <li>Column 1 untuk KODE DEALER</li>
                                    <li>Column 2 untuk DEALER</li>
                                    <li>Column 3 untuk SALES PERSON</li>
                                    <li>Column 4 untuk NO. RANGKA</li>
                                    <li>Column 5 untuk KODE MESIN</li>
                                    <li>Column 6 untuk NO. MESIN</li>
                                    <li>Column 7 untuk TGL CETAK</li>
                                    <li>Column 8 untuk TGL MOHON</li>
                                    <li>Column 9 untuk NAMA LENGKAP</li>
                                    <li>Column 10 untuk ALAMAT</li>
                                    <li>Column 11 untuk KELURAHAN</li>
                                    <li>Column 12 untuk KECAMATAN</li>
                                    <li>Column 13 untuk KODE KOTA</li>
                                    <li>Column 14 untuk KODE PROVINSI</li>
                                    <li>Column 15 untuk CASH/CREDIT</li>
                                    <li>Column 16 untuk FINANCE COMPANY</li>
                                    <li>Column 17 untuk JENIS SALES</li>
                                    <li>Column 18 untuk GENDER</li>
                                    <li>Column 19 untuk TGL LAHIR</li>
                                    <li>Column 20 untuk KODE AGAMA</li>
                                    <li>Column 21 untuk KODE PEKERJAAN</li>
                                    <li>Column 22 untuk NO. HP</li>
                                    <li>Column 23 untuk NO. TELP</li>
                                    <li>Column 24 untuk UMUR</li>
                                    <li>Column 25 untuk TIPE</li>
                                    <li>Column 26 untuk 3JENIS</li>
                                    <li>Column 27 untuk DP</li>
                                    <li>Column 28 untuk TENOR</li>
                                    <li>Column 29 untuk PENGELUARAN</li>
                                    <li>Column 30 untuk PENDIDIKAN</li>
                                </ul>
                            </li>
                            <li><a href="{{URL::asset('upload/CONTOH_DATA.xlsx')}}">Download Contoh Data</a>
                        </ol>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableUploadData" width="430%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center">Nama Dealer</th>
                        <th class="text-center">Sales Person</th>
                        <th class="text-center">No. Rangka</th>
                        <th class="text-center">Kode Mesin</th>
                        <th class="text-center">No. Mesin</th>
                        <th class="text-center">Tgl. Cetak</th>
                        <th class="text-center">Tgl. Mohon</th>
                        <th class="text-center">Nama Lengkap</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Kelurahan</th>
                        <th class="text-center">Kecamatan</th>
                        <th class="text-center">Kota</th>
                        <th class="text-center">Provinsi</th>
                        <th class="text-center">Cash/Credit</th>
                        <th class="text-center">Finance Company</th>
                        <th class="text-center">Jenis Sales</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Tgl. Lahir</th>
                        <th class="text-center">Agama</th>
                        <th class="text-center">Pendidikan</th>
                        <th class="text-center" width="10%">Pekerjaan</th>
                        <th class="text-center">Pengeluaran</th>
                        <th class="text-center">No. HP</th>
                        <th class="text-center">No. Telp</th>
                        <th class="text-center">Umur</th>
                        <th class="text-center">Tipe</th>
                        <th class="text-center">3Jenis</th>
                        <th class="text-center">DP</th>
                        <th class="text-center">Tenor</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->sales_person}}</td>
                        <td class="text-center">{{$data->nomor_rangka}}</td>
                        <td class="text-center">{{$data->kode_mesin}}</td>
                        <td class="text-center">{{$data->nomor_mesin}}</td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_cetak))}}</td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_mohon))}}</td>
                        <td class="text-center">{{$data->nama_lengkap}}</td>
                        <td class="text-center">{{$data->alamat}}</td>
                        <td class="text-center">{{$data->kelurahan}}</td>
                        <td class="text-center">{{$data->kecamatan}}</td>
                        <td class="text-center">{{$data->nama_kota}}</td>
                        <td class="text-center">{{$data->nama_prov}}</td>
                        <td class="text-center">
                        @if($data->cash_credit == 1)
                            Cash
                        @else
                            Credit
                        @endif
                        </td>
                        <td class="text-center">{{$data->nama_finance_company}}</td>
                        <td class="text-center">{{$data->jenis_sales}}</td>
                        <td class="text-center">
                        @if($data->gender == 1)
                            Laki-laki
                        @else
                            Perempuan
                        @endif
                        </td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                        <td class="text-center">{{$data->nama_agama}}</td>
                        <td class="text-center">{{$data->nama_pendidikan}}</td>
                        <td class="text-center">
                        @if($data->kode_pekerjaan == 11)
                            Lain-lain
                        @else
                            {{$data->nama_pekerjaan}}
                        @endif
                        </td>
                        <td class="text-center">{{$data->pengeluaran}}</td>
                        <td class="text-center"><a href="tel:{{$data->no_hp}}">{{$data->no_hp}}</a></td>
                        <td class="text-center"><a href="tel:{{$data->no_telp}}">{{$data->no_telp}}</a></td>                        
                        <td class="text-center">{{$data->umur}}</td>
                        <td class="text-center">{{$data->tipe}}</td>
                        <td class="text-center">{{$data->tiga_jenis}}</td>
                        <td class="text-center">{{$data->down_payment}}</td>
                        <td class="text-center">{{$data->tenor}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

<style>
#input-file{
    opacity: 0;
    position: absolute;
    z-index: -1;
}
.crsr{
    cursor: pointer;
}
</style>