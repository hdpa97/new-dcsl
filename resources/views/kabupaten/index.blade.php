@extends('layouts.sidebar')

@section('title-tab')
    Data Kabupaten
@endsection
@section('button')
<a class="" href="{{route('kabupaten.create')}}">
    <button class="btn btn-md btn-primary pull-right">
        <i class="fas fa-plus-circle"></i>
        Tambah Kabupaten
    </button>
</a>
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4">   
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableKabupaten" width="100%" >
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kabupaten</th>
                        <th class="text-center">Status</th>
                        <th class="text-center" width="12%">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                        <tr>
                            <td class="text-center">{{++$i}}.</td>
                            <td class="text-center">{{$data->nama_kabupaten}}</td>
                            <td class="text-center">
                                @if ($data->flag == 1)
                                    <label class="label label-success">Active</label>
                                @else
                                <label class="label label-danger">No</label>
                                @endif
                            </td>
                            <td class="text-center">
                                <a class="float-left ml-2" href="{{route('kabupaten.edit',['id' => $data->id])}}">
                                    <button class="btn btn-sm btn-info">
                                        <i>edit</i>
                                    </button>
                                </a>
                            @if ($data->flag == 1)
                                <form class="text-center ml-2 delete" action="{{route('kabupaten.disable',['id' => $data->id])}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('put') }}
                                    <button type="submit" class="btn btn-sm btn-danger">
                                        <i>disable</i>
                                    </button>
                                </form>
                            @else
                                <form class="text-center ml-2 delete" action="{{route('kabupaten.enable',['id' => $data->id])}}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('put') }}
                                    <button type="submit" class="btn btn-sm btn-success">
                                        <i>enable</i>        
                                    </button>
                                </form>
                            @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection