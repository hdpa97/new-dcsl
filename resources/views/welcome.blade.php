<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Astra Motor Yogyakarta - Astra Motor">
    <meta name="author" content="ThemePixels">

    <title>Astra Motor Yogyakarta - Astra Motor</title>

    <!-- Vendor -->
    <link href="{{URL::asset('css/slim-clean/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('css/slim-clean/ionicons.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('css/slim-clean/slim.css')}}" rel="stylesheet" />

</head>

<body class="slim-landing">

    <div class="slim-landing-header">
        <div class="container">
            <div class="slim-landing-header-left">
                <img src="{{URL::asset('img/logo-one-heart-red-sayap.png')}}" alt="Astra Motor" style="max-height: 44px"
                    class="mr-4">
                <img src="{{URL::asset('img/img_logo_honda.png')}}" alt="Astra Motor" style="max-height: 44px">
            </div>
            <div class="slim-landing-header-right">
                <a href="#"><i
                        class="icon ion-social-facebook"></i></a>
                <a href="#"><i class="icon ion-social-twitter"></i></a>
                <a href="#"><i class="icon ion-social-instagram"></i></a>
            </div>
        </div>
    </div>

    <div class="slim-landing-headline">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h1></h1>
                    <h5>
                    </h5>
                    <a href="#" class="btn btn-slim-purchase" target="_blank">Login Now</a>
                    @if (Route::has('password.request'))
                    <a href="#" target="_blank" class="btn btn-slim-demo">Reset Password</a>
                    @endif
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <img src="#" class="img-fluid">
                </div>
            </div>
        </div>
    </div>

    <div class="slim-landing-skin">
    </div>

    <div class="slim-landing-footer">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <div class="col-md-6 text-right">

                    <img src="https://www.astramotor-md.co.id/wp-content/uploads/2017/10/logo-retail-pth.png"
                        alt="Astra Motor" style="max-height: 60px"><br />
                    <p class="card-text text-right">
                        <h5>Astra Motor Branch Office - Yogyakarta</h5>
                        <h6>Alamat : Jalan Magelang No.Km. 7,2, Mlati Beningan, Sendangadi, Kec. Mlati, Kabupaten
                            Sleman, Daerah Istimewa Yogyakarta 55285</h6>
                        <h6>Telp : 0274-868551</h6>
                    </p>

                </div>
            </div>
        </div>
    </div>

    <div class="slim-footer">
        <div class="container">
            <p>Copyright © 2020 All Rights Reserved. Astra Honda Motor</p>
            <p>Designed by: <span><i>@hinodelpa</i></span></p>
        </div><!-- container -->
    </div>
    <script src="{{URL::asset('js/slim-clean/jquery.js')}}"></script>
    <script src="{{URL::asset('js/slim-clean/popper.js')}}"></script>
    <script src="{{URL::asset('js/slim-clean/bootstrap.js')}}"></script>
</body>

</html>
