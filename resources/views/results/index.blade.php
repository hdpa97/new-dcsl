@extends('layouts.sidebar')

@section('title-tab')
Survey Results
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET" action="{{ route('results.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Data</td>
                            <td class="text-left">
                                <select name="tipeData" class="custom-select" style="width:230px;">
                                    <option value="H1" @if($request->get('tipeData') == "H1") selected
                                        @elseif(!$request->get('tipeData')) selected @endif>H1</option>
                                    <option value="H2" @if($request->get('tipeData') == "H2") selected @endif>H2
                                    </option>
                                    <option value="H3" @if($request->get('tipeData') == "H3") selected @endif> H3
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">Kares</td>
                            <td class="text-left">
                                <select name="idKares" id="id_kares" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($karess as $kares)
                                    <option value="{{ $kares->id }}" @if ($request->get('idKares') == $kares->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kares->nama_kares }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Kab./Kota</td>
                            <td class="text-left">
                                <select name="idKab" id="id_kabupaten" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($kabs as $kabupaten)
                                    <option value="{{ $kabupaten->id }}" @if ($request->get('idKab') == $kabupaten->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kabupaten->nama_kabupaten }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Dealer</td>
                            <td class="text-left">
                                <select name="idDlr" id="id_dealer" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($dealers as $dealer)
                                    <option value="{{ $dealer->id }}" @if ($request->get('idDlr') == $dealer->id )
                                        selected="selected"
                                        @endif
                                        > {{ $dealer->nama_dealer }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
        <div class="col-md-4">
            <table cellpadding="3" cellspacing="0" class="">
                <tbody>
                    <tr>
                        <td class="text-left">Bulan</td>
                        <td class="text-left">
                            <select name="months[]" class="custom-select2" multiple="multiple" style="width:253px;">
                            @foreach(range(1,12) as $month)
                            <option value="{{$month}}"
                            @if($request->get('months') !== null)
                                @if (in_array($month, $request->get('months'), FALSE))
                                    selected
                                @endif
                            @endif
                            >
                                {{date("M", strtotime('2020-'.$month))}}
                            </option>
                            @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun</td>
                        <td class="text-left">
                            <select name="years[]" class="custom-select2" multiple="multiple" style="width:253px;">
                                @php
                                       $firstYear = (int)date('Y') - 1;
                                    $lastYear = $firstYear + 6;
                                @endphp
                                @for($i=$firstYear;$i<=$lastYear;$i++)
                                    <option value="{{$i}}"
                                    @if($request->get('years') !== null)
                                        @if (in_array($i, $request->get('years'), FALSE))
                                            selected
                                        @endif
                                    @endif
                                    >
                                        {{$i}}
                                    </option>
                                @endfor
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-left">
                            <button type="submit" class="btn btn-success btn-block submit-button">
                                <i class='fa fa-search'></i>
                                {{ __('Show') }}
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </form>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 text-right">
            <a class="" href="{{route('results.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
    </div>
</div>

@if($request->get('tipeData') == 'H2')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableResults" width="800%">
                <thead>
                    <tr>
                        <th class="text-center">Dealer's Business Code</th>
                        <th class="text-center">Dealer Code</th>
                        <th class="text-center">City of Dealer</th>
                        <th class="text-center">Name of Respondent</th>
                        <th class="text-center">Phone Number</th>
                        <th class="text-center">Address</th>
                        <th class="text-center">Block</th>
                        <th class="text-center">Frame Number</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">DOB</th>
                        <th class="text-center">Age</th>
                        <th class="text-center">Age Group</th>
                        <th class="text-center">Education</th>
                        <th class="text-center">Occupation</th>
                        <th class="text-center">SES</th>
                        <th class="text-center">A1</th>
                        <th class="text-center">A2</th>
                        <th class="text-center">A3.1</th>
                        <th class="text-center">A3.2</th>
                        <th class="text-center">A3.3</th>
                        <th class="text-center">A3.3A</th>
                        <th class="text-center">A3.3B</th>
                        <th class="text-center">A3.2B</th>
                        <th class="text-center">A3.2B.NOTE</th>
                        <th class="text-center">A3.3C</th>
                        <th class="text-center">A3.3C.NOTE</th>
                        <th class="text-center">A3.4</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">C1.1</th>
                        <th class="text-center">C1.2</th>
                        <th class="text-center">C1.3</th>
                        <th class="text-center">C1.3A</th>
                        <th class="text-center">C1.4</th>
                        <th class="text-center">C1.4A</th>
                        <th class="text-center">C1.2A</th>
                        <th class="text-center">C1.2A.NOTE</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C2.NOTE</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C3.NOTE</th>
                        <th class="text-center">C1.5</th>
                        <th class="text-center">D1</th>
                        <th class="text-center">D2</th>
                        <th class="text-center">D3.1</th>
                        <th class="text-center">D3.2</th>
                        <th class="text-center">D3.3</th>
                        <th class="text-center">D3.3A</th>
                        <th class="text-center">D4</th>
                        <th class="text-center">D5</th>
                        <th class="text-center">E1.1</th>
                        <th class="text-center">E1.1A</th>
                        <th class="text-center">E1.2</th>
                        <th class="text-center">E1.3</th>
                        <th class="text-center">E1.4</th>
                        <th class="text-center">G1</th>
                        <th class="text-center">G2</th>
                        <th class="text-center">G3</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">H2</th>
                        <th class="text-center">H3</th>
                        <th class="text-center">H4</th>
                        <th class="text-center">I4</th>
                        <th class="text-center">I4_NOTE</th>
                        <th class="text-center">I5</th>
                        <th class="text-center">I6</th>
                        <th class="text-center">I7</th>
                        <th class="text-center">I8</th>
                        <th class="text-center">I8_NOTE</th>
                        <th class="text-center">I9</th>
                        <th class="text-center">I9_NOTE</th>
                        <th class="text-center">K1</th>
                        <th class="text-center">N1</th>
                        <th class="text-center">N2</th>
                        <th class="text-center">N3_MONTH</th>
                        <th class="text-center">N3_YEARS</th>
                        <th class="text-center">N3_NOTE</th>
                        <th class="text-center">N4</th>
                        <th class="text-center">N4_NOTE</th>
                        <th class="text-center">N5</th>
                        <th class="text-center">N5_NOTE</th>
                        <th class="text-center">N6</th>
                        <th class="text-center">N6_NOTE</th>
                        <th class="text-center">N7_NOTE</th>
                        <th class="text-center">N8</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getResults as $data)
                    <tr>
                        <td class="text-center" width="3%">{{$data->nama_jaringan}}</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_lengkap}}</td>
                        <td class="text-center">{{$data->no_hp}}</td>
                        <td class="text-center">{{$data->alamat}}</td>
                        <td class="text-center">{{$data->tipe}}</td>
                        <td class="text-center">{{$data->nomor_rangka}}</td>
                        <td class="text-center">
                            @if($data->gender == 1)
                            Laki-laki
                            @else
                            Perempuan
                            @endif
                        </td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                        <td class="text-center">{{$data->umur}}</td>
                        <td class="text-center">{{$data->age_group}}</td>
                        <td class="text-center">{{$data->nama_pendidikan}}</td>
                        <td class="text-center">{{$data->nama_pekerjaan}}</td>
                        <td class="text-center">{{$data->pengeluaran}}</td>
                        <td class="text-center">{{$data->a1}}</td>
                        <td class="text-center">{{$data->a2}}</td>
                        <td class="text-center">{{$data->a3_1}}</td>
                        <td class="text-center">{{$data->a3_2}}</td>
                        <td class="text-center">{{$data->a3_3}}</td>
                        <td class="text-center">{{$data->a3_3a}}</td>
                        <td class="text-center">{{$data->a3_3b}}</td>
                        <td class="text-center">{{$data->a3_2b}}</td>
                        <td class="text-center">{{$data->a3_2b_note}}</td>
                        <td class="text-center">{{$data->a3_3c}}</td>
                        <td class="text-center">{{$data->a3_3c_note}}</td>
                        <td class="text-center">{{$data->a3_4}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->c1_1}}</td>
                        <td class="text-center">{{$data->c1_2}}</td>
                        <td class="text-center">{{$data->c1_3}}</td>
                        <td class="text-center">{{$data->c1_3a}}</td>
                        <td class="text-center">{{$data->c1_4}}</td>
                        <td class="text-center">{{$data->c1_4a}}</td>
                        <td class="text-center">{{$data->c1_2a}}</td>
                        <td class="text-center">{{$data->c1_2a_note}}</td>
                        <td class="text-center">{{$data->c2}}</td>
                        <td class="text-center">{{$data->c2_note}}</td>
                        <td class="text-center">{{$data->c3}}</td>
                        <td class="text-center">{{$data->c3_note}}</td>
                        <td class="text-center">{{$data->c1_5}}</td>
                        <td class="text-center">{{$data->d1}}</td>
                        <td class="text-center">{{$data->d2}}</td>
                        <td class="text-center">{{$data->d3_1}}</td>
                        <td class="text-center">{{$data->d3_2}}</td>
                        <td class="text-center">{{$data->d3_3}}</td>
                        <td class="text-center">{{$data->d3_3a}}</td>
                        <td class="text-center">{{$data->d4}}</td>
                        <td class="text-center">{{$data->d5}}</td>
                        <td class="text-center">{{$data->e1_1}}</td>
                        <td class="text-center">{{$data->e1_1a}}</td>
                        <td class="text-center">{{$data->e1_2}}</td>
                        <td class="text-center">{{$data->e1_3}}</td>
                        <td class="text-center">{{$data->e1_4}}</td>
                        <td class="text-center">{{$data->g1}}</td>
                        <td class="text-center">{{$data->g2}}</td>
                        <td class="text-center">{{$data->g3}}</td>
                        <td class="text-center">{{$data->h1}}</td>
                        <td class="text-center">{{$data->h2}}</td>
                        <td class="text-center">{{$data->h3}}</td>
                        <td class="text-center">{{$data->h4}}</td>
                        <td class="text-center">{{$data->i4}}</td>
                        <td class="text-center">{{$data->i4_note}}</td>
                        <td class="text-center">{{$data->i5}}</td>
                        <td class="text-center">{{$data->i6}}</td>
                        <td class="text-center">{{$data->i7}}</td>
                        <td class="text-center">{{$data->i8}}</td>
                        <td class="text-center">{{$data->i8_note}}</td>
                        <td class="text-center">{{$data->i9}}</td>
                        <td class="text-center">{{$data->i9_note}}</td>
                        <td class="text-center">{{$data->k1}}</td>
                        <td class="text-center">{{$data->n1}}</td>
                        <td class="text-center">{{$data->n2}}</td>
                        <td class="text-center">{{$data->n3_month}}</td>
                        <td class="text-center">{{$data->n3_years}}</td>
                        <td class="text-center">{{$data->n3_note}}</td>
                        <td class="text-center">{{$data->n4}}</td>
                        <td class="text-center">{{$data->n4_note}}</td>
                        <td class="text-center">{{$data->n5}}</td>
                        <td class="text-center">{{$data->n5_note}}</td>
                        <td class="text-center">{{$data->n6}}</td>
                        <td class="text-center">{{$data->n6_note}}</td>
                        <td class="text-center">{{$data->n7_note}}</td>
                        <td class="text-center">{{$data->n8}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif($request->get('tipeData') == 'H3')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableResults" width="800%">
                <thead>
                    <tr>
                        <th class="text-center">Dealer's Business Code</th>
                        <th class="text-center">Dealer Code</th>
                        <th class="text-center">City of Dealer</th>
                        <th class="text-center">Name of Respondent</th>
                        <th class="text-center">Phone Number</th>
                        <th class="text-center">Address</th>
                        <th class="text-center">Block</th>
                        <th class="text-center">Frame Number</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">DOB</th>
                        <th class="text-center">Age</th>
                        <th class="text-center">Age Group</th>
                        <th class="text-center">Education</th>
                        <th class="text-center">Occupation</th>
                        <th class="text-center">SES</th>
                        <th class="text-center">T1</th>
                        <th class="text-center">T1.NOTE</th>
                        <th class="text-center">A1.1</th>
                        <th class="text-center">A1.2</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">B1.3</th>
                        <th class="text-center">B1.4</th>
                        <th class="text-center">C1</th>
                        <th class="text-center">C1.A</th>
                        <th class="text-center">C1.B</th>
                        <th class="text-center">C2</th>
                        <th class="text-center">C3</th>
                        <th class="text-center">C4</th>
                        <th class="text-center">C5</th>
                        <th class="text-center">D1</th>
                        <th class="text-center">D2</th>
                        <th class="text-center">D3.1</th>
                        <th class="text-center">D3.2</th>
                        <th class="text-center">D3.2A</th>
                        <th class="text-center">D3.3</th>
                        <th class="text-center">D3.3A</th>
                        <th class="text-center">D3.3B</th>
                        <th class="text-center">D4</th>
                        <th class="text-center">D5</th>
                        <th class="text-center">E1</th>
                        <th class="text-center">E2</th>
                        <th class="text-center">F1</th>
                        <th class="text-center">G1</th>
                        <th class="text-center">G2</th>
                        <th class="text-center">G3</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">H1.NOTE</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getResults as $data)
                    <tr>
                        <td class="text-center" width="3%">{{$data->nama_jaringan}}</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_lengkap}}</td>
                        <td class="text-center">{{$data->no_hp}}</td>
                        <td class="text-center">{{$data->alamat}}</td>
                        <td class="text-center">{{$data->tipe}}</td>
                        <td class="text-center">{{$data->nomor_rangka}}</td>
                        <td class="text-center">
                            @if($data->gender == 1)
                            Laki-laki
                            @else
                            Perempuan
                            @endif
                        </td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                        <td class="text-center">{{$data->umur}}</td>
                        <td class="text-center">{{$data->age_group}}</td>
                        <td class="text-center">{{$data->nama_pendidikan}}</td>
                        <td class="text-center">{{$data->nama_pekerjaan}}</td>
                        <td class="text-center">{{$data->pengeluaran}}</td>
                        <td class="text-center">{{$data->t1}}</td>
                        <td class="text-center">{{$data->t1_note}}</td>
                        <td class="text-center">{{$data->a1_1}}</td>
                        <td class="text-center">{{$data->a1_2}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->b1_3}}</td>
                        <td class="text-center">{{$data->b1_4}}</td>
                        <td class="text-center">{{$data->c1}}</td>
                        <td class="text-center">{{$data->c1_a}}</td>
                        <td class="text-center">{{$data->c1_b}}</td>
                        <td class="text-center">{{$data->c2}}</td>
                        <td class="text-center">{{$data->c3}}</td>
                        <td class="text-center">{{$data->c4}}</td>
                        <td class="text-center">{{$data->c5}}</td>
                        <td class="text-center">{{$data->d1}}</td>
                        <td class="text-center">{{$data->d2}}</td>
                        <td class="text-center">{{$data->d3_1}}</td>
                        <td class="text-center">{{$data->d3_2}}</td>
                        <td class="text-center">{{$data->d3_2a}}</td>
                        <td class="text-center">{{$data->d3_3}}</td>
                        <td class="text-center">{{$data->d3_3a}}</td>
                        <td class="text-center">{{$data->d3_3b}}</td>
                        <td class="text-center">{{$data->d4}}</td>
                        <td class="text-center">{{$data->d5}}</td>
                        <td class="text-center">{{$data->e1}}</td>
                        <td class="text-center">{{$data->e2}}</td>
                        <td class="text-center">{{$data->f1}}</td>
                        <td class="text-center">{{$data->g1}}</td>
                        <td class="text-center">{{$data->g2}}</td>
                        <td class="text-center">{{$data->g3}}</td>
                        <td class="text-center">{{$data->h1}}</td>
                        <td class="text-center">{{$data->h1_note}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableResults" width="800%">
                <thead>
                    <tr>
                        <th class="text-center">Dealer's Business Code</th>
                        <th class="text-center">Dealer Code</th>
                        <th class="text-center">City of Dealer</th>
                        <th class="text-center">Name of Respondent</th>
                        <th class="text-center">Phone Number</th>
                        <th class="text-center">Address</th>
                        <th class="text-center">Block</th>
                        <th class="text-center">Frame Number</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">DOB</th>
                        <th class="text-center">Age</th>
                        <th class="text-center">Age Group</th>
                        <th class="text-center">Education</th>
                        <th class="text-center">Occupation</th>
                        <th class="text-center">SES</th>
                        <th class="text-center">A1</th>
                        <th class="text-center">A2</th>
                        <th class="text-center">A3.1</th>
                        <th class="text-center">A3.2</th>
                        <th class="text-center">A3.3</th>
                        <th class="text-center">A3.4</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">B1.3</th>
                        <th class="text-center">B1.3A</th>
                        <th class="text-center">B1.4</th>
                        <th class="text-center">B1.5</th>
                        <th class="text-center">B1.5A</th>
                        <th class="text-center">B3</th>
                        <th class="text-center">B4</th>
                        <th class="text-center">D15</th>
                        <th class="text-center">E1.1</th>
                        <th class="text-center">E1.2</th>
                        <th class="text-center">E1.3</th>
                        <th class="text-center">F2.1</th>
                        <th class="text-center">F2.2</th>
                        <th class="text-center">G1</th>
                        <th class="text-center">H1.1</th>
                        <th class="text-center">H1.2</th>
                        <th class="text-center">H1.3</th>
                        <th class="text-center">H1.4</th>
                        <th class="text-center">H1.5</th>
                        <th class="text-center">H1.5A</th>
                        <th class="text-center">H1.5B</th>
                        <th class="text-center">H1.5C</th>
                        <th class="text-center">H1.5D</th>
                        <th class="text-center">H1.5D_NOTE</th>
                        <th class="text-center">H1.6</th>
                        <th class="text-center">H2</th>
                        <th class="text-center">H3.A1</th>
                        <th class="text-center">H3.A2</th>
                        <th class="text-center">H3.A3</th>
                        <th class="text-center">H3.A4</th>
                        <th class="text-center">H3.A5</th>
                        <th class="text-center">I1.1</th>
                        <th class="text-center">I1.2</th>
                        <th class="text-center">J1.1</th>
                        <th class="text-center">J1.2</th>
                        <th class="text-center">N1</th>
                        <th class="text-center">N2_MONTH</th>
                        <th class="text-center">N2_YEARS</th>
                        <th class="text-center">N2_NOTE</th>
                        <th class="text-center">N3</th>
                        <th class="text-center">N3_NOTE</th>
                        <th class="text-center">N4</th>
                        <th class="text-center">N4_NOTE</th>
                        <th class="text-center">N5</th>
                        <th class="text-center">N5_NOTE</th>
                        <th class="text-center">N7</th>
                        <th class="text-center">N7_NOTE</th>
                        <th class="text-center">N8</th>
                        <th class="text-center">N8_NOTE</th>
                        <th class="text-center">N9</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getResults as $data)
                    <tr>
                        <td class="text-center" width="3%">{{$data->nama_jaringan}}</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_lengkap}}</td>
                        <td class="text-center">{{$data->no_hp}}</td>
                        <td class="text-center">{{$data->alamat}}</td>
                        <td class="text-center">{{$data->tipe}}</td>
                        <td class="text-center">{{$data->nomor_rangka}}</td>
                        <td class="text-center">
                            @if($data->gender == 1)
                            Laki-laki
                            @else
                            Perempuan
                            @endif
                        </td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                        <td class="text-center">{{$data->umur}}</td>
                        <td class="text-center">{{$data->age_group}}</td>
                        <td class="text-center">{{$data->nama_pendidikan}}</td>
                        <td class="text-center">{{$data->nama_pekerjaan}}</td>
                        <td class="text-center">{{$data->pengeluaran}}</td>
                        <td class="text-center">{{$data->a1}}</td>
                        <td class="text-center">{{$data->a2}}</td>
                        <td class="text-center">{{$data->a3_1}}</td>
                        <td class="text-center">{{$data->a3_2}}</td>
                        <td class="text-center">{{$data->a3_3}}</td>
                        <td class="text-center">{{$data->a3_4}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->b1_3}}</td>
                        <td class="text-center">{{$data->b1_3a}}</td>
                        <td class="text-center">{{$data->b1_4}}</td>
                        <td class="text-center">{{$data->b1_5}}</td>
                        <td class="text-center">{{$data->b1_5a}}</td>
                        <td class="text-center">{{$data->b3}}</td>
                        <td class="text-center">{{$data->b4}}</td>
                        <td class="text-center">{{$data->d15}}</td>
                        <td class="text-center">{{$data->e1_1}}</td>
                        <td class="text-center">{{$data->e1_2}}</td>
                        <td class="text-center">{{$data->e1_3}}</td>
                        <td class="text-center">{{$data->f2_1}}</td>
                        <td class="text-center">{{$data->f2_2}}</td>
                        <td class="text-center">{{$data->g1}}</td>
                        <td class="text-center">{{$data->h1_1}}</td>
                        <td class="text-center">{{$data->h1_2}}</td>
                        <td class="text-center">{{$data->h1_3}}</td>
                        <td class="text-center">{{$data->h1_4}}</td>
                        <td class="text-center">{{$data->h1_5}}</td>
                        <td class="text-center">{{$data->h1_5a}}</td>
                        <td class="text-center">{{$data->h1_5b}}</td>
                        <td class="text-center">{{$data->h1_5c}}</td>
                        <td class="text-center">{{$data->h1_5d}}</td>
                        <td class="text-center">{{$data->h1_5d_note}}</td>
                        <td class="text-center">{{$data->h1_6}}</td>
                        <td class="text-center">{{$data->h2}}</td>
                        <td class="text-center">{{$data->h3_a1}}</td>
                        <td class="text-center">{{$data->h3_a2}}</td>
                        <td class="text-center">{{$data->h3_a3}}</td>
                        <td class="text-center">{{$data->h3_a4}}</td>
                        <td class="text-center">{{$data->h3_a5}}</td>
                        <td class="text-center">{{$data->i1_1}}</td>
                        <td class="text-center">{{$data->i1_2}}</td>
                        <td class="text-center">{{$data->j1_1}}</td>
                        <td class="text-center">{{$data->j1_2}}</td>
                        <td class="text-center">{{$data->n1}}</td>
                        <td class="text-center">{{$data->n2_month}}</td>
                        <td class="text-center">{{$data->n2_years}}</td>
                        <td class="text-center">{{$data->n2_note}}</td>
                        <td class="text-center">{{$data->n3}}</td>
                        <td class="text-center">{{$data->n3_note}}</td>
                        <td class="text-center">{{$data->n4}}</td>
                        <td class="text-center">{{$data->n4_note}}</td>
                        <td class="text-center">{{$data->n5}}</td>
                        <td class="text-center">{{$data->n5_note}}</td>
                        <td class="text-center">{{$data->n7}}</td>
                        <td class="text-center">{{$data->n7_note}}</td>
                        <td class="text-center">{{$data->n8}}</td>
                        <td class="text-center">{{$data->n8_note}}</td>
                        <td class="text-center">{{$data->n9}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        document.getElementById("id_kabupaten").disabled = true;
        document.getElementById("id_dealer").disabled = true;
        $('#id_kares').change(function() {
            if ($(this).val() != '') {
                var idk = $(this).val();
                document.getElementById("id_dealer").disabled = true;
                document.getElementById("id_kabupaten").disabled = true;
                $.ajax({
                    url: '/tersurvey/fetch_krs/' + idk,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_kabupaten").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            $('#id_kabupaten').empty();
                            $('#id_kabupaten').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_kabupaten').append('<option value="' + result[i].id_kab + '">' + result[i].nama_kab + '</option>');
                            }
                        }
                    }
                });
            }
        });

        $('#id_kabupaten').change(function() {
            if ($(this).val() != '') {
                var idkab = $(this).val();
                document.getElementById("id_dealer").disabled = true;
                $.ajax({
                    url: '/tersurvey/fetch_kab/' + idkab,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_dealer").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_dealer').append('<option value="' + result[i].id_dlr + '">' + result[i].nama_dlr + '</option>');
                            }
                        }
                    }
                });
            }
        });
    });
</script>
<script>
   $(document).ready(function () {
         $(".custom-select2").select2({
         });
   });
</script>
@endsection
