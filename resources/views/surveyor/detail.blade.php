@extends('layouts.sidebar')

@section('title-tab')
Detail Surveyor
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('surveyor.index')}}">Report Surveyor</a></li>
    <li class="breadcrumb-item active">Detail Surveyor</li>
</ol>
@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Detail Surveyor') }}</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">
                            <form>
                                <table cellpadding="3" cellspacing="0" class="">
                                    <tbody>
                                        <tr>
                                            <td class="text-left">Nama</td>
                                            <td class="text-left">
                                                <input id="name" required type="text" class="form-control" name="name" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Terhubung</td>
                                            <td class="text-left">
                                                <input id="Terhubung" required type="text" class="form-control" name="Terhubung" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Terhubung Tapi Pernah Survey</td>
                                            <td class="text-left">
                                                <input id="Terhubung Tapi Pernah Survey" required type="text" class="form-control" name="Terhubung Tapi Pernah Survey" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Terhubung Konsumen Tapi Tidak Seusai Data</td>
                                            <td class="text-left">
                                                <input id="Terhubung Konsumen Tapi Tidak Seusai Data" required type="text" class="form-control" name="Terhubung Konsumen Tapi Tidak Seusai Data" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Tidak Diangkat</td>
                                            <td class="text-left">
                                                <input id="Tidak Diangkat" required type="text" class="form-control" name="Tidak Diangkat" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Tidak Aktif/Mailbox</td>
                                            <td class="text-left">
                                                <input id="Tidak Aktif/Mailbox" required type="text" class="form-control" name="Tidak Aktif/Mailbox" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Di Luar Jangkauan</td>
                                            <td class="text-left">
                                                <input id="Di Luar Jangkauan" required type="text" class="form-control" name="Di Luar Jangkauan" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">No Telp Salah</td>
                                            <td class="text-left">
                                                <input id="No Telp Salah" required type="text" class="form-control" name="No Telp Salah" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Telp Dialihkan</td>
                                            <td class="text-left">
                                                <input id="Telp Dialihkan" required type="text" class="form-control" name="Telp Dialihkan" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">No Telp Sibuk</td>
                                            <td class="text-left">
                                                <input id="No Telp Sibuk" required type="text" class="form-control" name="No Telp Sibuk" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Reject</td>
                                            <td class="text-left">
                                                <input id="Reject" required type="text" class="form-control" name="Reject" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Telp Diangkat Tapi Ditolak Konsumen</td>
                                            <td class="text-left">
                                                <input id="Telp Diangkat Tapi Ditolak Konsumen" required type="text" class="form-control" name="Telp Diangkat Tapi Ditolak Konsumen" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Salah Sambung</td>
                                            <td class="text-left">
                                                <input id="Salah Sambung" required type="text" class="form-control" name="Salah Sambung" value="" required autofocus>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                        </div>
                        <div class="col-md-4">
                                <table cellpadding="3" cellspacing="0" class="">
                                    <tbody>
                                        <tr>
                                            <td class="text-left">Total</td>
                                            <td class="text-left">
                                                <input id="Total" required type="text" class="form-control" name="Total" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">%Terhubung</td>
                                            <td class="text-left">
                                                <input id="%Terhubung" required type="text" class="form-control" name="%Terhubung" value="" required autofocus>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Prospect</td>
                                            <td class="text-left">
                                                <input id="Prospect" required type="text" class="form-control" name="Prospect" value="" required autofocus>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection