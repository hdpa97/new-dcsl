@extends('layouts.sidebar')

@section('title-tab')
Surveyor
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET"
                action="{{ route('surveyor.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Periode</td>
                            <td class="text-left">
                                <select name="periode" class="custom-select" style="width:230px;">
                                    <option value="" disabled selected>- Pilih</option>
                                    <option value="H" @if($request->get('periode') == "H") selected @endif>Hari ini</option>
                                    <option value="H-7" @if($request->get('periode') == "H-7") selected @endif> 1 Minggu Terakhir</option>
                                    <option value="H-30" @if($request->get('periode') == "H-30") selected @endif> 1 Bulan Terakhir</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                                <button type="submit" class="btn btn-success btn-block submit-button">
                                    <i class='fa fa-search'></i>
                                    {{ __('Show') }}
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="col-md-4">
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 text-right">
            <a class="" href="{{route('surveyor.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
    </div>
</div>

{{--
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET" action="{{ route('cek.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Bulan</td>
                            <td class="text-left">
                                <select name="month" class="custom-select" style="width:230px;" disabled>
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach(range(1,12) as $month)

                                    <option value="{{$month}}">
                                        {{date("M", strtotime('2020-'.$month))}}
                                    </option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Status FU</td>
                            <td class="text-left">
                                <select name="statusfu" class="custom-select" style="width:230px;" disabled>
                                    <option disabled selected value="">- Pilih</option>
                                    <option>Terhubung</option>
                                    <option>Terhubung Tapi Pernah Survey</option>
                                    <option>Terhubung Konsumen Tapi Tidak Seusai Data</option>
                                    <option>Tidak Diangkat</option>
                                    <option>Tidak Aktif/Mailbox</option>
                                    <option>Di Luar Jangkauan</option>
                                    <option>No Telp Salah</option>
                                    <option>Telp Dialihkan</option>
                                    <option>No Telp Sibuk</option>
                                    <option>Reject</option>
                                    <option>Telp Diangkat Tapi Ditolak Konsumen</option>
                                    <option>Salah Sambung</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                                <button type="submit" class="btn btn-success btn-block submit-button">
                                    <i class='fa fa-search'></i>
                                    {{ __('Show') }}
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>
--}}
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableSurveyor" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">NPK</th>
                        <th class="text-center">Nama</th>
                        <th class="text-center">Survey Tersedia</th>
                        <th class="text-center">Survey Selesai</th>
                        <th class="text-center">Survey Reject</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                        <tr>
                            <td class="text-center">{{++$i}}.</td>
                            <td class="text-center">{{$data->npk}}</td>
                            <td class="text-center">{{$data->name}}</td>
                            <td class="text-center">{{$data->tersedia}}</td>
                            <td class="text-center">{{$data->selesai}}</td>
                            <td class="text-center">{{$data->reject}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
