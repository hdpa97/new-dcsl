@extends('layouts.sidebar')

@section('title-tab')
    Dashboard
@endsection

@section('breadcrumb')
<br/>
@endsection

@section('main-content')
<div class="content">  
    <div class="row" style="margin-top:-20px;">
        <div class="col-lg-4 col-md-6 col-sm-6 text-center">
          <div class="card border-0">
            <div class="card-category">
                <span style="color: #0067d6;">
                    <i class="fas fa-poll fa-5x"></i>
                </span>
            </div>
            <p class="card-category">Survey Tersedia</p>
                <h3 class="card-title">
                    @if($getAchPerson!=null)
                        {{$getAchPerson->tersedia}}
                    @else
                        0
                    @endif
                    <small>Survey</small>
                </h3>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 text-center">
          <div class="card border-0">
            <div class="card-category">
                <span style="color: #0067d6;">
                    <i class="fas fa-check-square fa-5x"></i>
                </span>
            </div>
            <p class="card-category">Survey Selesai</p>
                <h3 class="card-title">
                    @if($getAchPerson!=null)
                        {{$getAchPerson->selesai}}
                    @else
                        0
                    @endif
                    <small>Survey</small>
                </h3>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 text-center">
          <div class="card border-0">
            <div class="card-category">
                <span style="color: #0067d6;">
                    <i class="fas fa-window-close fa-5x"></i>
                </span>
            </div>
            <p class="card-category">Survey Reject</p>
                <h3 class="card-title">
                    @if($getAchPerson!=null)
                        {{$getAchPerson->reject}}
                    @else
                        0
                    @endif
                    <small>Survey</small>
                </h3>
          </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
@endsection