@extends('layouts.sidebar')

@section('title-tab')
    Survey Reject
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableSurvey" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kares</th>
                        <th class="text-center">Kabupaten/Kota</th>
                        <th class="text-center">Dealer</th>
                        <th class="text-center">Jaringan</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">H23</th>
                        {{-- 
                        <th class="text-center">H3</th>
                        --}}
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->nama_kares}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->nama_jaringan}}</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        @if($data->CT_H1!=0)
                        <td class="text-center">
                            <a href="#" style="color:black"> {{$data->CT_H1}}</a>
                        </td>
                        @else
                        <td class="text-center">
                            {{$data->CT_H1}}
                        </td>
                        @endif
                        @if($data->CT_H2!=0)
                        <td class="text-center">
                            <a href="#" style="color:black"> {{$data->CT_H2}}</a>
                        </td>
                        @else
                        <td class="text-center">
                            {{$data->CT_H2}}
                        </td>  
                        @endif   
                        {{-- 
                        @if($data->CT_H3!=0)                   
                        <td class="text-center">
                            <a href="#" style="color:black"> {{$data->CT_H3}}</a>
                        </td>
                        @else
                        <td class="text-center">
                            {{$data->CT_H3}}
                        </td>
                        @endif
                        --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection