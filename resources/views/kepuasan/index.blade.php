@extends('layouts.sidebar')

@section('title-tab')
Scoring
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET"
                action="{{ route('kepuasan.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Data</td>
                            <td class="text-left">
                                <select name="tipeData" class="custom-select" style="width:230px;">
                                    <option value="H1" @if($request->get('tipeData') == "H1") selected
                                        @elseif(!$request->get('tipeData')) selected @endif>H1</option>
                                    <option value="H2" @if($request->get('tipeData') == "H2") selected @endif>H2
                                    </option>
                                    <option value="H3" @if($request->get('tipeData') == "H3") selected @endif> H3
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">Point</td>
                            <td class="text-left">
                                <select name="point" class="custom-select" style="width:230px;">
                                    <option value="5" @if($request->get('point') == "5") selected
                                        @elseif(!$request->get('point')) selected @endif>5</option>
                                    <option value="4" @if($request->get('point') == "4") selected @endif>4</option>
                                    <option value="3" @if($request->get('point') == "3") selected @endif>3</option>
                                    <option value="2" @if($request->get('point') == "2") selected @endif>2</option>
                                    <option value="1" @if($request->get('point') == "1") selected @endif>1</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">Bulan</td>
                            <td class="text-left">
                                <select name="months[]" class="custom-select2" multiple="multiple" style="width:230px;">
                                @foreach(range(1,12) as $month)
                                <option value="{{$month}}"
                                @if($request->get('months') !== null)
                                    @if (in_array($month, $request->get('months'), FALSE))
                                        selected
                                    @endif
                                @endif
                                >
                                    {{date("M", strtotime('2020-'.$month))}}
                                </option>
                                @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Tahun</td>
                            <td class="text-left">
                                <select name="years[]" class="custom-select2" multiple="multiple" style="width:230px;">
                                    @php
                                        $firstYear = (int)date('Y') - 1;
                                        $lastYear = $firstYear + 6;
                                    @endphp
                                    @for($i=$firstYear;$i<=$lastYear;$i++)
                                        <option value="{{$i}}"
                                        @if($request->get('years') !== null)
                                            @if (in_array($i, $request->get('years'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                        >
                                            {{$i}}
                                        </option>
                                    @endfor
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                                <button type="submit" class="btn btn-success btn-block submit-button">
                                    <i class='fa fa-search'></i>
                                    {{ __('Show') }}
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 text-right">
            <a class="" href="{{route('kepuasan.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
    </div>
</div>

@if($request->get('tipeData') == 'H2')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableScoring" width="280%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center" width="10%">Dealer</th>
                        <th class="text-center">A3.1</th>
                        <th class="text-center">A3.2</th>
                        <th class="text-center">A3.3</th>
                        <th class="text-center">A3.4</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">C1.1</th>
                        <th class="text-center">C1.2</th>
                        <th class="text-center">C1.3A</th>
                        <th class="text-center">C1.4</th>
                        <th class="text-center">C1.5</th>
                        <th class="text-center">D3.1</th>
                        <th class="text-center">D3.2</th>
                        <th class="text-center">D3.3</th>
                        <th class="text-center">D4</th>
                        <th class="text-center">E1.1</th>
                        <th class="text-center">E1.2</th>
                        <th class="text-center">E1.3</th>
                        <th class="text-center">E1.4</th>
                        <th class="text-center">G3</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">I7</th>
                        <th class="text-center">K1</th>
                        <th class="text-center">N1</th>
                        <th class="text-center">N2</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getScoring as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center"><a
                                href="{{route('scoring.dealer',['kode_dealer' => $data->kode_dealer, 'tipe' => 'H2'])}}"
                                target="_blank">{{$data->kode_dealer}}</a></td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->a3_1}}</td>
                        <td class="text-center">{{$data->a3_2}}</td>
                        <td class="text-center">{{$data->a3_3}}</td>
                        <td class="text-center">{{$data->a3_4}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->c1_1}}</td>
                        <td class="text-center">{{$data->c1_2}}</td>
                        <td class="text-center">{{$data->c1_3a}}</td>
                        <td class="text-center">{{$data->c1_4}}</td>
                        <td class="text-center">{{$data->c1_5}}</td>
                        <td class="text-center">{{$data->d3_1}}</td>
                        <td class="text-center">{{$data->d3_2}}</td>
                        <td class="text-center">{{$data->d3_3}}</td>
                        <td class="text-center">{{$data->d4}}</td>
                        <td class="text-center">{{$data->e1_1}}</td>
                        <td class="text-center">{{$data->e1_2}}</td>
                        <td class="text-center">{{$data->e1_3}}</td>
                        <td class="text-center">{{$data->e1_4}}</td>
                        <td class="text-center">{{$data->g3}}</td>
                        <td class="text-center">{{$data->h1}}</td>
                        <td class="text-center">{{$data->i7}}</td>
                        <td class="text-center">{{$data->k1}}</td>
                        <td class="text-center">{{$data->n1}}</td>
                        <td class="text-center">{{$data->n2}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif($request->get('tipeData') == 'H3')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableScoring" width="160%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center" width="10%">Dealer</th>
                        <th class="text-center">A1.1</th>
                        <th class="text-center">A1.2</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">B1.3</th>
                        <th class="text-center">B1.4</th>
                        <th class="text-center">C1</th>
                        <th class="text-center">C1.B</th>
                        <th class="text-center">D2</th>
                        <th class="text-center">D3.1</th>
                        <th class="text-center">D3.2</th>
                        <th class="text-center">D3.2A</th>
                        <th class="text-center">D3.3</th>
                        <th class="text-center">E2</th>
                        <th class="text-center">F1</th>
                        <th class="text-center">G2</th>
                        <th class="text-center">G3</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getScoring as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center"><a
                                href="{{route('scoring.dealer',['kode_dealer' => $data->kode_dealer, 'tipe' => 'H3'])}}"
                                target="_blank">{{$data->kode_dealer}}</a></td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->a1_1}}</td>
                        <td class="text-center">{{$data->a1_2}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->b1_3}}</td>
                        <td class="text-center">{{$data->b1_4}}</td>
                        <td class="text-center">{{$data->c1}}</td>
                        <td class="text-center">{{$data->c1_b}}</td>
                        <td class="text-center">{{$data->d2}}</td>
                        <td class="text-center">{{$data->d3_1}}</td>
                        <td class="text-center">{{$data->d3_2}}</td>
                        <td class="text-center">{{$data->d3_2a}}</td>
                        <td class="text-center">{{$data->d3_3}}</td>
                        <td class="text-center">{{$data->e2}}</td>
                        <td class="text-center">{{$data->f1}}</td>
                        <td class="text-center">{{$data->g2}}</td>
                        <td class="text-center">{{$data->g3}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableScoring" width="200%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center" width="10%">Dealer</th>
                        <th class="text-center">A3.1</th>
                        <th class="text-center">A3.2</th>
                        <th class="text-center">A3.3</th>
                        <th class="text-center">A3.4</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">B1.3</th>
                        <th class="text-center">B1.4</th>
                        <th class="text-center">B1.5</th>
                        <th class="text-center">B1.5A</th>
                        <th class="text-center">E1.1</th>
                        <th class="text-center">E1.2</th>
                        <th class="text-center">E1.3</th>
                        <th class="text-center">F2.1</th>
                        <th class="text-center">F2.2</th>
                        <th class="text-center">G1</th>
                        <th class="text-center">H1.1</th>
                        <th class="text-center">H1.2</th>
                        <th class="text-center">H1.3</th>
                        <th class="text-center">H1.4</th>
                        <th class="text-center">H1.5</th>
                        <th class="text-center">H1.6</th>
                        <th class="text-center">I1.1</th>
                        <th class="text-center">I1.2</th>
                        <th class="text-center">J1.1</th>
                        <th class="text-center">J1.2</th>
                        <th class="text-center">N1</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($getScoring as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center"><a
                                href="{{route('scoring.dealer',['kode_dealer' => $data->kode_dealer, 'tipe' => 'H1'])}}"
                                target="_blank">{{$data->kode_dealer}}</a></td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->a3_1}}</td>
                        <td class="text-center">{{$data->a3_2}}</td>
                        <td class="text-center">{{$data->a3_3}}</td>
                        <td class="text-center">{{$data->a3_4}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->b1_3}}</td>
                        <td class="text-center">{{$data->b1_4}}</td>
                        <td class="text-center">{{$data->b1_5}}</td>
                        <td class="text-center">{{$data->b1_5a}}</td>
                        <td class="text-center">{{$data->e1_1}}</td>
                        <td class="text-center">{{$data->e1_2}}</td>
                        <td class="text-center">{{$data->e1_3}}</td>
                        <td class="text-center">{{$data->f2_1}}</td>
                        <td class="text-center">{{$data->f2_2}}</td>
                        <td class="text-center">{{$data->g1}}</td>
                        <td class="text-center">{{$data->h1_1}}</td>
                        <td class="text-center">{{$data->h1_2}}</td>
                        <td class="text-center">{{$data->h1_3}}</td>
                        <td class="text-center">{{$data->h1_4}}</td>
                        <td class="text-center">{{$data->h1_5}}</td>
                        <td class="text-center">{{$data->h1_6}}</td>
                        <td class="text-center">{{$data->i1_1}}</td>
                        <td class="text-center">{{$data->i1_2}}</td>
                        <td class="text-center">{{$data->j1_1}}</td>
                        <td class="text-center">{{$data->j1_2}}</td>
                        <td class="text-center">{{$data->n1}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<script>
    $(document).ready(function () {
     $(".custom-select2").select2({
     });
});
</script>
<script>
   $(document).ready(function () {
         $(".custom-select2").select2({
         });
   });
</script>
@endsection
