@extends('layouts.sidebar')

@section('title-tab')
    Dashboard <small>Kuartal 3 Bulan Terakhir</small>
@endsection

@section('breadcrumb')
<br/>
@endsection

@section('main-content')
<div class="content">  
    <div class="row" style="margin-top:-20px;">
        <div class="col-lg-4 col-md-6 col-sm-6 text-center">
          <div class="card border-0">
            <div class="card-category">
                <span style="color: #0067d6;">
                    <i class="fas fa-poll fa-5x"></i>
                </span>
            </div>
            <p class="card-category">Survey Tersedia</p>
            <a href="{{route('survey.index')}}" style="color:black; text-decoration: none;">
                <h3 class="card-title">
                    @if($getAch!=null)
                        {{$getAch->tersedia}}
                    @else
                        0
                    @endif
                    <small>Survey</small>
                </h3>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 text-center">
          <div class="card border-0">
            <div class="card-category">
                <span style="color: #0067d6;">
                    <i class="fas fa-check-square fa-5x"></i>
                </span>
            </div>
            <p class="card-category">Survey Selesai</p>
            <a href="{{route('survey.report')}}" style="color:black; text-decoration: none;">
                <h3 class="card-title">
                    @if($getAch!=null)
                        {{$getAch->selesai}}
                    @else
                        0
                    @endif
                    <small>Survey</small>
                </h3>
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 text-center">
          <div class="card border-0">
            <div class="card-category">
                <span style="color: #0067d6;">
                    <i class="fas fa-window-close fa-5x"></i>
                </span>
            </div>
            <p class="card-category">Survey Reject</p>
            <a href="{{route('survey-reject.index')}}" style="color:black; text-decoration: none;">
                <h3 class="card-title">
                    @if($getAch!=null)
                        {{$getAch->reject}}
                    @else
                        0
                    @endif
                    <small>Survey</small>
                </h3>
            </a>
          </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="card mt-4">
                <div class="card-header">
                    <i class="fas fa-chart-bar mr-1"></i>Achievements
                </div>
                <div class="card-body">
                    <canvas id="chartAch"></canvas>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card mt-4">
                <div class="card-header">
                    <i class="fas fa-chart-bar mr-1"></i>3 Top Dealer
                </div>
                <div class="card-body">
                    <canvas id="chartDealer"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    var ctx = document.getElementById("chartAch");
    var chartAch = new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: [ 
                'Selesai', 'Reject'
            ],
        datasets: [{
            data: [
            @if($getAch!=null)
                "{{str_replace("&","AND",($getAch->selesai))}}",
            @else
                "0",
            @endif
            @if($getAch!=null)
                "{{str_replace("&","AND",($getAch->reject))}}"
            @else
                "0"
            @endif
            ],
            backgroundColor: [
                '#007bff', '#de1d00',
            ]
        }],
    },
    options : {
        title: {
            display: false,
            text: ""
        },
        legend: {
            display: true
        },
        tooltips: {enabled: true},
    }
    });
</script>

<script>
    var ctx = document.getElementById("chartDealer");
    var chartDealer = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: [
            @if($get3TopDealer->isEmpty())
                "Tidak ada data"
            @else
                @foreach($get3TopDealer as $dealer)
                    "{{str_replace("&","AND",($dealer->nama_dealer))}}",
                @endforeach
            @endif
        ],
        datasets: [{
        label: "Total",
        backgroundColor: [
            @if($get3TopDealer->isEmpty())
                "#007bff"
            @else
                @foreach($get3TopDealer as $item)
                    "#007bff",
                @endforeach
            @endif
        ],
        data: [
            @if($get3TopDealer->isEmpty())
                "0"
            @else
                @foreach($get3TopDealer as $val)
                    "{{str_replace("&","AND",( $val->selesai))}}",
                @endforeach
            @endif
        ],
        }],
    },
    options: {
        title: {
            display: false,
            text: ""
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: true,
                    maxTicksLimit: 3
                },
                gridLines: {
                    display: false
                }
            }],
            xAxes : [{
                gridLines : {
                    display: false
                }
            }]
        },
        tooltips: {enabled: true},
        legend: {
        display: false
        }
    }
    });
</script>
@endsection
