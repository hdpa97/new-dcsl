@extends('layouts.sidebar')

@section('title-tab')
Detail Scoring Dealer
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('kepuasan.index')}}">Scoring</a></li>
    <li class="breadcrumb-item active">Detail Scoring Dealer {{$kode_dealer}}</li>
</ol>
@endsection

@section('main-content')

@if($tipe == 'H1')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableScoring" width="220%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">ID Data</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center" width="10%">Dealer</th>
                        <th class="text-center">A3.1</th>
                        <th class="text-center">A3.2</th>
                        <th class="text-center">A3.3</th>
                        <th class="text-center">A3.4</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">B1.3</th>
                        <th class="text-center">B1.3A</th>
                        <th class="text-center">B1.4</th>
                        <th class="text-center">B1.5</th>
                        <th class="text-center">B1.5A</th>
                        <th class="text-center">E1.1</th>
                        <th class="text-center">E1.2</th>
                        <th class="text-center">E1.3</th>
                        <th class="text-center">F2.1</th>
                        <th class="text-center">F2.2</th>
                        <th class="text-center">G1</th>
                        <th class="text-center">H1.1</th>
                        <th class="text-center">H1.2</th>
                        <th class="text-center">H1.3</th>
                        <th class="text-center">H1.4</th>
                        <th class="text-center">H1.5</th>
                        <th class="text-center">I1.1</th>
                        <th class="text-center">I1.2</th>
                        <th class="text-center">J1.2</th>
                        <th class="text-center">N1</th>
                        <th class="text-center">N9</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center"><a href="{{route('detail.data',['id' => $data->id_data, 'kode' => $data->kode_dealer])}}"
                            target="_blank">{{$data->id_data}}</a></td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->a3_1}}</td>
                        <td class="text-center">{{$data->a3_2}}</td>
                        <td class="text-center">{{$data->a3_3}}</td>
                        <td class="text-center">{{$data->a3_4}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->b1_3}}</td>
                        <td class="text-center">{{$data->b1_3a}}</td>
                        <td class="text-center">{{$data->b1_4}}</td>
                        <td class="text-center">{{$data->b1_5}}</td>
                        <td class="text-center">{{$data->b1_5a}}</td>
                        <td class="text-center">{{$data->e1_1}}</td>
                        <td class="text-center">{{$data->e1_2}}</td>
                        <td class="text-center">{{$data->e1_3}}</td>
                        <td class="text-center">{{$data->f2_1}}</td>
                        <td class="text-center">{{$data->f2_2}}</td>
                        <td class="text-center">{{$data->g1}}</td>
                        <td class="text-center">{{$data->h1_1}}</td>
                        <td class="text-center">{{$data->h1_2}}</td>
                        <td class="text-center">{{$data->h1_3}}</td>
                        <td class="text-center">{{$data->h1_4}}</td>
                        <td class="text-center">{{$data->h1_5}}</td>
                        <td class="text-center">{{$data->i1_1}}</td>
                        <td class="text-center">{{$data->i1_2}}</td>
                        <td class="text-center">{{$data->j1_2}}</td>
                        <td class="text-center">{{$data->n1}}</td>
                        <td class="text-center">{{$data->n9}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@elseif($tipe == 'H3')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableScoring" width="160%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">ID Data</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center" width="10%">Dealer</th>
                        <th class="text-center">A1.1</th>
                        <th class="text-center">A1.2</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">B1.3</th>
                        <th class="text-center">B1.4</th>
                        <th class="text-center">C1</th>
                        <th class="text-center">C1.B</th>
                        <th class="text-center">D2</th>
                        <th class="text-center">D3.1</th>
                        <th class="text-center">D3.2</th>
                        <th class="text-center">D3.2A</th>
                        <th class="text-center">D3.3</th>
                        <th class="text-center">E2</th>
                        <th class="text-center">F1</th>
                        <th class="text-center">G2</th>
                        <th class="text-center">G3</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center"><a href="{{route('detail.data',['id' => $data->id_data, 'kode' => $data->kode_dealer])}}"
                            target="_blank">{{$data->id_data}}</a></td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->a1_1}}</td>
                        <td class="text-center">{{$data->a1_2}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->b1_3}}</td>
                        <td class="text-center">{{$data->b1_4}}</td>
                        <td class="text-center">{{$data->c1}}</td>
                        <td class="text-center">{{$data->c1_b}}</td>
                        <td class="text-center">{{$data->d2}}</td>
                        <td class="text-center">{{$data->d3_1}}</td>
                        <td class="text-center">{{$data->d3_2}}</td>
                        <td class="text-center">{{$data->d3_2a}}</td>
                        <td class="text-center">{{$data->d3_3}}</td>
                        <td class="text-center">{{$data->e2}}</td>
                        <td class="text-center">{{$data->f1}}</td>
                        <td class="text-center">{{$data->g2}}</td>
                        <td class="text-center">{{$data->g3}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@else
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableScoring" width="280%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">ID Data</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center" width="10%">Dealer</th>
                        <th class="text-center">A3.1</th>
                        <th class="text-center">A3.2</th>
                        <th class="text-center">A3.3</th>
                        <th class="text-center">A3.4</th>
                        <th class="text-center">B1.1</th>
                        <th class="text-center">B1.2</th>
                        <th class="text-center">C1.1</th>
                        <th class="text-center">C1.2</th>
                        <th class="text-center">C1.3A</th>
                        <th class="text-center">C1.4</th>
                        <th class="text-center">C1.5</th>
                        <th class="text-center">D3.1</th>
                        <th class="text-center">D3.2</th>
                        <th class="text-center">D3.3</th>
                        <th class="text-center">D4</th>
                        <th class="text-center">E1.1</th>
                        <th class="text-center">E1.2</th>
                        <th class="text-center">E1.3</th>
                        <th class="text-center">E1.4</th>
                        <th class="text-center">G3</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">I7</th>
                        <th class="text-center">K1</th>
                        <th class="text-center">N1</th>
                        <th class="text-center">N2</th>
                        <th class="text-center">N8</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center"><a href="{{route('detail.data',['id' => $data->id_data, 'kode' => $data->kode_dealer])}}"
                            target="_blank">{{$data->id_data}}</a></td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->a3_1}}</td>
                        <td class="text-center">{{$data->a3_2}}</td>
                        <td class="text-center">{{$data->a3_3}}</td>
                        <td class="text-center">{{$data->a3_4}}</td>
                        <td class="text-center">{{$data->b1_1}}</td>
                        <td class="text-center">{{$data->b1_2}}</td>
                        <td class="text-center">{{$data->c1_1}}</td>
                        <td class="text-center">{{$data->c1_2}}</td>
                        <td class="text-center">{{$data->c1_3a}}</td>
                        <td class="text-center">{{$data->c1_4}}</td>
                        <td class="text-center">{{$data->c1_5}}</td>
                        <td class="text-center">{{$data->d3_1}}</td>
                        <td class="text-center">{{$data->d3_2}}</td>
                        <td class="text-center">{{$data->d3_3}}</td>
                        <td class="text-center">{{$data->d4}}</td>
                        <td class="text-center">{{$data->e1_1}}</td>
                        <td class="text-center">{{$data->e1_2}}</td>
                        <td class="text-center">{{$data->e1_3}}</td>
                        <td class="text-center">{{$data->e1_4}}</td>
                        <td class="text-center">{{$data->g3}}</td>
                        <td class="text-center">{{$data->h1}}</td>
                        <td class="text-center">{{$data->i7}}</td>
                        <td class="text-center">{{$data->k1}}</td>
                        <td class="text-center">{{$data->n1}}</td>
                        <td class="text-center">{{$data->n2}}</td>
                        <td class="text-center">{{$data->n8}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endif
@endsection
