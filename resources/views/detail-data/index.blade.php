@extends('layouts.sidebar')

@section('title-tab')
Detail Data
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('kepuasan.index')}}">Scoring</a></li>
    <li class="breadcrumb-item active">Detail Data Dealer {{$kode_dealer}}</li>
</ol>
@endsection

@section('main-content')
<div class="card-body">
    <div class="row mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Data Upload
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Nomor Rangka</td>
                                    <td>{{$data->nomor_rangka}}</td>
                                </tr>
                                <tr>
                                    <td>Kode Mesin</td>
                                    <td>{{$data->kode_mesin}}</td>
                                </tr>
                                <tr>
                                    <td>Nomor Mesin</td>
                                    <td>{{$data->nomor_mesin}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Cetak</td>
                                    <td>{{date('d-m-Y', strtotime($data->tgl_cetak))}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Mohon</td>
                                    <td>{{date('d-m-Y', strtotime($data->tgl_mohon))}}</td>
                                </tr>
                                <tr>
                                    <td>Nama Lengkap</td>
                                    <td>{{$data->nama_lengkap}}</td>
                                </tr>
                                <tr>
                                    <td>Kelurahan</td>
                                    <td>{{$data->kelurahan}}</td>
                                </tr>
                                <tr>
                                    <td>Kecamatan</td>
                                    <td>{{$data->kecamatan}}</td>
                                </tr>
                                <tr>
                                    <td>Kota</td>
                                    <td>{{$data->nama_kota}}</td>
                                </tr>
                                <tr>
                                    <td>Provinsi</td>
                                    <td>{{$data->nama_prov}}</td>
                                </tr>
                                <tr>
                                    <td>Cash / Credit</td>
                                    <td>
                                        @if($data->cash_credit == 1)
                                        Cash
                                        @else
                                        Credit
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Finance Company</td>
                                    <td>{{$data->nama_finance_company}}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Sales</td>
                                    <td>{{$data->jenis_sales}}</td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>
                                        @if($data->gender == 1)
                                        Laki-laki
                                        @else
                                        Perempuan
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td>Usia</td>
                                    <td>{{$data->umur}}</td>
                                </tr>
                                <tr>
                                    <td>Pendidikan</td>
                                    <td>{{$data->nama_pendidikan}}</td>
                                </tr>
                                <tr>
                                    <td>Pengeluaran</td>
                                    <td>{{$data->huruf_pengeluaran}} - {{$data->pengeluaran}}</td>
                                </tr>
                                <tr>
                                    <td>No. HP / No. Telp</td>
                                    <td>{{$data->no_hp}} / {{$data->no_telp}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    Data Surveyor
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>NPK Surveyor</td>
                                    <td>{{$data->npk}}</td>
                                </tr>
                                <tr>
                                    <td>Nama Surveyor</td>
                                    <td>{{$data->name}}</td>
                                </tr>
                                <tr>
                                    <td>Email Surveyor</td>
                                    <td>{{$data->email}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="card">
                <div class="card-header">
                    Data Dealer
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>Nama Kares</td>
                                    <td>{{$data->nama_kares}}</td>
                                </tr>
                                <tr>
                                    <td>Kabupaten</td>
                                    <td>{{$data->nama_kabupaten}}</td>
                                </tr>
                                <tr>
                                    <td>Kode Dealer</td>
                                    <td>{{$data->kode_dealer}}</td>
                                </tr>
                                <tr>
                                    <td>Nama Dealer</td>
                                    <td>{{$data->nama_dealer}}</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>{{$data->alamat}}</td>
                                </tr>
                                <tr>
                                    <td>Nama PIC</td>
                                    <td>{{$data->nama_pic}}</td>
                                </tr>
                                <tr>
                                    <td>No HP PIC</td>
                                    <td>{{$data->no_hp_pic}}</td>
                                </tr>
                                <tr>
                                    <td>Email PIC</td>
                                    <td>{{$data->email_pic}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
