@extends('layouts.front')

@section('front-content')
<div class="signin-box">
    <h2 class="signin-title-primary">Welcome back!</h2>
    <h3 class="signin-title-secondary">Sign in to continue.</h3>
    <form method="POST" action="{{ route('login') }}" autocomplete="off">
        @csrf
        <div class="form-group">
            <label for="email">{{ __('Email') }}</label>
            <input id="email" type="email" placeholder=""
                class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                placeholder="Email" required autocomplete="off" autofocus>
            @error('email')
            <span class="invalid-feedback text-center" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <label for="password">{{ __('Password') }}</label>
            <div class="input-group">
                <input id="password" type="password" placeholder=""
                    class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password"
                    required autocomplete="current-password">
                <div class="input-group-append">
                    <button tabindex="99" class="btn btn-info" onclick="intipPw()" type="button"><i class="fas fa-eye"></i></button>
                </div>
                @error('password')
                <span class="invalid-feedback text-center" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block btn-signin">Sign In</button>
    </form>
    <p class="mg-b-0 text-center">
        {{ __('Sign in') }}
    |
    @if (Route::has('password.request'))
    <a href="{{ route('password.request') }}">
        {{ __('Forgot Password? ') }}
    </a>
    @endif
</p>
</div>
@endsection

@section('script')
<script>
    function intipPw() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>
@endsection
