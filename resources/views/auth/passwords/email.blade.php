@extends('layouts.front')

@section('front-content')
<div class="signin-box">
    <h2 class="signin-title-primary">Reset password</h2>
    <h3 class="signin-title-secondary">Enter your email.</h3>
    @if (session('status'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {{ session('status') }}
    </div>
    @endif
    <form method="POST" action="{{ route('password.email') }}" autocomplete="off">
        @csrf
        <div class="form-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                value="{{ old('email') }}" placeholder="Email" required autocomplete="off" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary btn-block btn-signin">Reset</button>
    </form>
    <p class="mg-b-0 text-center"><a href="{{ route('login') }}">
        {{ __('Sign in') }}</a>
    |
    @if (Route::has('password.request'))
        {{ __('Forgot Password? ') }}
    @endif
</p>
</div>
@endsection
