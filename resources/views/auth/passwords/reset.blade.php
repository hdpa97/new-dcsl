@extends('layouts.front')

@section('front-content')
<div class="signin-box">
    <h2 class="signin-title-primary">Update Password</h2>
    <h5 class="signin-title-secondary">Strong passwords include numbers, letters, and punctuation marks.</h5>
    <form method="POST" action="{{ route('password.update') }}" autocomplete="off">
        @csrf
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="form-group">
            <label for="password">{{ __('Email Account') }}</label>
            <input id="email" type="email" readonly class="form-control @error('email') is-invalid @enderror"
                name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>

        <div class="form-group">
            <label for="password">{{ __('New Password') }}</label>
            <div class="input-group">
                <input id="password" type="password" placeholder="********"
                    class="form-control @error('password') is-invalid @enderror" name="password" required
                    autocomplete="new-password">
                <div class="input-group-append">
                    <button tabindex="98" class="btn btn-info" onclick="intipPw()" type="button"><i class="fas fa-eye"></i></button>
                </div>
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <label for="password">{{ __('Confirm Password') }}</label>

            <div class="input-group">
                <input id="password-confirm" type="password" placeholder="********" class="form-control"
                    name="password_confirmation" required autocomplete="new-password">
                <div class="input-group-append">
                    <button tabindex="99"class="btn btn-info" onclick="intipRePw()" type="button"><i class="fas fa-eye"></i></button>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block btn-signin">Change Password</button>
    </form>
    <p class="mg-b-0 text-center"><a href="{{ route('login') }}">
            {{ __('Sign in') }}</a>
        |
        @if (Route::has('password.request'))
        <a href="{{ route('password.request') }}">
            {{ __('Forgot Password? ') }}
        </a>
        @endif
    </p>
</div>
@endsection



@section('script')
<script>
    function intipPw() {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    function intipRePw() {
      var x = document.getElementById("password-confirm");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>
@endsection
