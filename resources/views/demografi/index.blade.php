@extends('layouts.sidebar')

@section('title-tab')
Demografi
@endsection

@section('breadcrumb')

@endsection

@section('main-content')

<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="#" class="needs-validation pull-left" method="GET"
                action="{{ route('demografi.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr id="filter_col4" data-column="3">
                            <td class="text-left">Data</td>
                            <td class="text-left">
                                <select name="tipeData" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    <option value="All" @if($request->get('tipeData') == "All") selected
                                        @elseif(!$request->get('tipeData')) selected @endif>All</option>
                                    <option value="H1" @if($request->get('tipeData') == "H1") selected @endif> H1
                                    </option>
                                    <option value="H2" @if($request->get('tipeData') == "H2") selected @endif> H2
                                    </option>
                                    <option value="H3" @if($request->get('tipeData') == "H3") selected @endif> H3
                                    </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-left">Kares</td>
                            <td class="text-left">
                                <select name="idKares" id="id_kares" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($dtKares as $kares)
                                    <option value="{{ $kares->id }}" @if ($request->get('idKares') == $kares->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kares->nama_kares }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Kab./Kota</td>
                            <td class="text-left">
                                <select name="idKab" id="id_kabupaten" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($dtKab as $kabupaten)
                                    <option value="{{ $kabupaten->id }}" @if ($request->get('idKab') == $kabupaten->id )
                                        selected="selected"
                                        @endif
                                        > {{ $kabupaten->nama_kabupaten }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Dealer</td>
                            <td class="text-left">
                                <select name="idDlr" id="id_dealer" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($dtDealer as $dealer)
                                    <option value="{{ $dealer->id }}" @if ($request->get('idDlr') == $dealer->id )
                                        selected="selected"
                                        @endif
                                        > {{ $dealer->nama_dealer }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
        <div class="col-md-4">
            <table cellpadding="3" cellspacing="0" class="">
                <tbody>
                    <tr>
                        <td class="text-left">Bulan</td>
                        <td class="text-left">
                            <select name="months[]" class="custom-select2" multiple="multiple" style="width:253px;">
                            @foreach(range(1,12) as $month)
                            <option value="{{$month}}"
                            @if($request->get('months') !== null)
                                @if (in_array($month, $request->get('months'), FALSE))
                                    selected
                                @endif
                            @endif
                            >
                                {{date("M", strtotime('2020-'.$month))}}
                            </option>
                            @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>Tahun</td>
                        <td class="text-left">
                            <select name="years[]" class="custom-select2" multiple="multiple" style="width:253px;">
                                @php
                                    $firstYear = (int)date('Y') - 1;
                                    $lastYear = $firstYear + 6;
                                @endphp
                                @for($i=$firstYear;$i<=$lastYear;$i++)
                                    <option value="{{$i}}"
                                    @if($request->get('years') !== null)
                                        @if (in_array($i, $request->get('years'), FALSE))
                                            selected
                                        @endif
                                    @endif
                                    >
                                        {{$i}}
                                    </option>
                                @endfor
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-left">
                            <button type="submit" class="btn btn-success btn-block submit-button">
                                <i class='fa fa-search'></i>
                                {{ __('Show') }}
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 text-right">
            <a class="" href="{{route('demografi.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
    </div>
</div>
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="row">
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-bar mr-1"></i>Occupation
                        <button class="btn btn-info btn-sm float-right" name="action" type="submit"
                            value="downloadOccupation">Download</button>
                    </div>
                    <div id="reportOccupation">
                        <div class="card-body">
                            <canvas id="chartOccupation"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-bar mr-1"></i>SES
                        <button class="btn btn-info btn-sm float-right" name="action" type="submit"
                            value="downloadSES">Download</button>
                    </div>
                    <div id="reportSES">
                        <div class="card-body">
                            <canvas id="chartSES"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-bar mr-1"></i>Education
                        <button class="btn btn-info btn-sm float-right" name="action" type="submit"
                            value="downloadEducation">Download</button>
                    </div>
                    <div id="reportEducation">
                        <div class="card-body">
                            <canvas id="chartEducation"></canvas>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-bar mr-1"></i>Gender
                        <button class="btn btn-info btn-sm float-right" name="action" type="submit"
                            value="downloadGender">Download</button>
                    </div>
                    <div id="reportGender">
                        <div class="card-body">
                            <canvas id="chartGender"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header">
                        <i class="fas fa-chart-bar mr-1"></i>Age Group
                        <button class="btn btn-info btn-sm float-right" name="action" type="submit"
                            value="downloadAgeGroup">Download</button>
                    </div>
                    <div id="reportAgeGroup">
                        <div class="card-body">
                            <canvas id="chartAgeGroup"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </form>

            <div class="col-lg-6 mt-2">
                <form id="formExport" class="needs-validation pull-left" method="GET"
                    action="{{ route('demografi.index') }}">
                    <div class="row">
                        <div class="col-md-8">
                            <select id="export_pdf" name="" class="custom-select" required>
                                <option disabled selected value="">- Pilih</option>
                                <option value="chartOccupation" @if($request->get('export_pdf') == "chartOccupation")
                                    selected @endif> Chart Occupation </option>
                                <option value="chartSES" @if($request->get('export_pdf') == "chartSES") selected @endif>
                                    Chart SES </option>
                                <option value="chartEducation" @if($request->get('export_pdf') == "chartEducation")
                                    selected @endif> Chart Education </option>
                                <option value="chartGender" @if($request->get('export_pdf') == "chartGender") selected
                                    @endif> Chart Gender </option>
                                <option value="chartAgeGroup" @if($request->get('export_pdf') == "chartAgeGroup")
                                    selected @endif> Chart Age Group </option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <button class="btn btn-md btn-success pull-right">
                                <i class="fas fa-file-pdf"></i>
                                Export PDF
                            </button>
                        </div>
                    </div>
                </form>
                <div class="mt-2">
                    Total Data :
                    <i>
                        @if($getSES->isEmpty())
                        0
                        @else
                        {{$getSES->sum('total')}} (100%)
                        @endif
                    </i>
                    <br />
                    Most of respondents are:
                    <ul>
                        <li>Gender:
                            <i>
                                @if($getGender->male > $getGender->woman)
                                Laki-Laki ({{round($getGender->male / ($getGender->male+$getGender->woman) * 100), 1}}%)
                                @elseif($getGender->male < $getGender->woman)
                                    Perempuan
                                    ({{round($getGender->woman / ($getGender->male+$getGender->woman) * 100), 1}}%)
                                    @elseif($getGender->male = $getGender->woman)
                                    Laki-Laki & Perempuan (50%)
                                    @elseif($getGender->male == 0 || $getGender->woman == 0)
                                    -
                                    @endif
                            </i>
                        </li>
                        <li>Age Group:
                            <i>
                                @if($getGroupAges->isEmpty())
                                -
                                @else
                                {{$getGroupAges->where('age_count',$getGroupAges->max('age_count'))->first()->age_group}}
                                ({{round($getGroupAges->where('age_count',$getGroupAges->max('age_count'))->first()->age_count / $getGroupAges->sum('age_count') * 100, 1)}}%)
                                @endif
                            </i>
                        </li>
                        <li>Occupation:
                            <i>
                                @if($getOccupation->isEmpty())
                                -
                                @else
                                {{$getOccupation->where('total',$getOccupation->max('total'))->first()->nama_pekerjaan}}
                                ({{round($getOccupation->where('total',$getOccupation->max('total'))->first()->total / $getOccupation->sum('total') * 100, 1)}}%)
                                @endif
                            </i>
                        </li>
                        <li>Education:
                            <i>
                                @if($getEducation->isEmpty())
                                -
                                @else
                                {{$getEducation->where('total',$getEducation->max('total'))->first()->nama_pendidikan}}
                                ({{round($getEducation->where('total',$getEducation->max('total'))->first()->total / $getEducation->sum('total') * 100, 1)}}%)
                                @endif
                            </i>
                        </li>
                        <li>SES:
                            <i>
                                @if($getSES->isEmpty())
                                -
                                @else
                                {{$getSES->where('total',$getSES->max('total'))->first()->pengeluaran}}
                                ({{round($getSES->where('total',$getSES->max('total'))->first()->total / $getSES->sum('total') * 100, 1)}}%)
                                @endif
                            </i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>



    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>

    <script>
        var ctx = document.getElementById("chartAgeGroup");
    var chartAgeGroup = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: [
            @if($getGroupAges->isEmpty())
                "Tidak ada data"
            @else
                @foreach($getGroupAges as $ages)
                    "{{$ages->age_group}}",
                @endforeach
            @endif
        ],
        datasets: [{
            label: "Total Data",
            backgroundColor: [
                @if($getGroupAges->isEmpty())
                    "#007bff"
                @else
                    @foreach($getGroupAges as $item)
                        "#007bff",
                    @endforeach
                @endif
            ],
            data: [
                @if($getGroupAges->isEmpty())
                    "0"
                @else
                    @foreach($getGroupAges as $val)
                    "{{round(str_replace("&","AND",( $val->age_count / $getGroupAges->sum('age_count') * 100 )), 1)}}",
                    @endforeach
                @endif
            ]
        }],
    },
    options : {
        title: {
            display: true,
            text: "Chart - Age Group"
        },
        scales: {
            yAxes : [{
                gridLines: {
                    display: false
                }
            }],
            xAxes : [{
                ticks: {
                    display: false,
                    max:
                    @if($getGroupAges->isEmpty())
                        1
                    @else
                        {{round(str_replace("&","AND",( $getGroupAges->max('age_count') / $getGroupAges->sum('age_count') * 100 )), 1)+5}}
                    @endif
                },
                gridLines: {
                    display: false
                },
            }]
        },
        legend: {
            display: false
        },
        animation: {
            duration: 1,
            onComplete: function() {
                var chartInstance = this.chart,
                ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                // ctx.textAlign = 'center';
                ctx.textBaseline = 'top';

                this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                    var data = " "+dataset.data[index]+"%";
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                });
            }
        },
        tooltips: {enabled: false},
    }
    });
    </script>

    <script>
        var ctx = document.getElementById("chartSES");
    var chartSES = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: [
            @if($getSES->isEmpty())
                "Tidak ada data"
            @else
                @foreach($getSES as $ses)
                    "{{str_replace("&","AND",($ses->huruf_pengeluaran))}}",
                @endforeach
            @endif
        ],
        datasets: [{
            label: "Total Data",
            backgroundColor: [
                @if($getSES->isEmpty())
                    "#007bff"
                @else
                    @foreach($getSES as $item)
                        "#007bff",
                    @endforeach
                @endif
            ],
            data: [
                @if($getSES->isEmpty())
                    "0"
                @else
                    @foreach($getSES as $val)
                        "{{round(str_replace("&","AND",( $val->total / $getSES->sum('total') * 100 )), 1)}}",
                    @endforeach
                @endif
            ]
        }],
    },
    options : {
        title: {
            display: true,
            text: "Chart - SES"
        },
        scales: {
            yAxes : [{
                gridLines: {
                    display: false
                }
            }],
            xAxes : [{
                ticks: {
                    display: false,
                    max:
                    @if($getSES->isEmpty())
                        1
                    @else
                        {{round($getSES->max('total') / $getSES->sum('total') * 100, 1 )+5}},
                    @endif
                },
                gridLines: {
                    display: false
                },
            }]
        },
        legend: {
            display: false
        },
        animation: {
            duration: 1,
            onComplete: function() {
                var chartInstance = this.chart,
                ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                // ctx.textAlign = 'center';
                ctx.textBaseline = 'top';

                this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                    var data = " "+dataset.data[index]+"%";
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                });
            }
        },
        tooltips: {enabled: false},
    }
    });
    </script>

    <script>
        var ctx = document.getElementById("chartOccupation");
    var chartOccupation = new Chart(ctx, {
    type: 'horizontalBar',
    data: {
        labels: [
            @if($getOccupation->isEmpty())
                "Tidak ada data"
            @else
                @foreach($getOccupation as $occupation)
                    "{{str_replace("&","AND",($occupation->nama_pekerjaan))}}",
                @endforeach
            @endif
        ],
        datasets: [{
            label: "Total",
            backgroundColor: [
                @if($getOccupation->isEmpty())
                    "#007bff"
                @else
                    @foreach($getOccupation as $item)
                        "#007bff",
                    @endforeach
                @endif
            ],
            data: [
                @if($getOccupation->isEmpty())
                    "0"
                @else
                    @foreach($getOccupation as $val)
                        "{{round(str_replace("&","AND",( $val->total / $getSES->sum('total') * 100 )), 1)}}",
                    @endforeach
                @endif
            ]
        }],
    },
    options : {
        title: {
            display: true,
            text: "Chart - Occupation"
        },
        scales: {
            yAxes : [{
                gridLines: {
                    display: false
                }
            }],
            xAxes : [{
                ticks: {
                    display: false,
                    max:
                    @if($getOccupation->isEmpty())
                        1
                    @else
                        {{round(str_replace("&","AND",( $getOccupation->max('total') / $getOccupation->sum('total') * 100 )), 1)+4}}
                    @endif
                },
                gridLines: {
                    display: false
                },
            }]
        },
        legend: {
            display: false
        },
        animation: {
            duration: 1,
            onComplete: function() {
                var chartInstance = this.chart,
                ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                // ctx.textAlign = 'center';
                ctx.textBaseline = 'top';

                this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                    var data = " "+dataset.data[index]+"%";
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                });
            }
        },
        tooltips: {enabled: false},
    }
    });
    </script>

    <script>
        var ctx = document.getElementById("chartGender");
    var chartGender = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: [
                'Laki-laki : @if($getGender->male != 0) {{round($getGender->male / ($getGender->male+$getGender->woman) * 100), 1}}% @else 0 @endif',
                'Perempuan : @if($getGender->woman != 0) {{round($getGender->woman / ($getGender->male+$getGender->woman) * 100), 1}}% @else 0 @endif'
            ],
        datasets: [{
            data: [
                @if($getGender->male != 0)
                    "{{round(str_replace("&","AND",($getGender->male / ($getGender->male+$getGender->woman) * 100) ), 1)}}",
                @else
                    "0",
                @endif
                @if($getGender->woman != 0)
                    "{{round(str_replace("&","AND",($getGender->woman / ($getGender->male+$getGender->woman) * 100)), 1)}}"
                @else
                    "0"
                @endif
            ],
            backgroundColor: [
                '#007bff', '#de1d00'
            ]
        }],
    },
    options : {
        title: {
            display: true,
            text: "Chart - Gender"
        },
        legend: {
            display: true
        },
        tooltips: {enabled: false},
    }
    });
    </script>

    <script>
        var ctx = document.getElementById("chartEducation");
    var chartEducation = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [
            @if($getEducation->isEmpty())
                "Tidak ada data"
            @else
                @foreach($getEducation as $education)
                    "{{str_replace("&","AND",($education->nama_pendidikan))}}",
                @endforeach
            @endif
        ],
        datasets: [{
        label: "Total",
        backgroundColor: [
            @if($getEducation->isEmpty())
                "#007bff"
            @else
                @foreach($getEducation as $item)
                    "#007bff",
                @endforeach
            @endif
        ],
        data: [
            @if($getEducation->isEmpty())
                "0"
            @else
                @foreach($getEducation as $val)
                    "{{round(str_replace("&","AND",( $val->total / $getSES->sum('total') * 100 )), 1)}}",
                @endforeach
            @endif
        ],
        }],
    },
    options: {
        title: {
            display: true,
            text: "Chart - Education"
        },
        scales: {
            yAxes: [{
                ticks: {
                    display: false,
                    maxTicksLimit: 5
                },
                gridLines: {
                    display: false
                }
            }],
            xAxes : [{
                gridLines : {
                    display: false
                }
            }]
        },
        animation: {
            duration: 1,
            onComplete: function() {
                var chartInstance = this.chart,
                ctx = chartInstance.ctx;
                ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, Chart.defaults.global.defaultFontStyle, Chart.defaults.global.defaultFontFamily);
                ctx.textAlign = 'center';
                ctx.textBaseline = 'center';

                this.data.datasets.forEach(function(dataset, i) {
                    var meta = chartInstance.controller.getDatasetMeta(i);
                    meta.data.forEach(function(bar, index) {
                    var data = " "+dataset.data[index]+"%";
                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                    });
                });
            }
        },
        tooltips: {enabled: false},
        legend: {
        display: false
        }
    }
    });
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
        document.getElementById("id_kabupaten").disabled = true;
        document.getElementById("id_dealer").disabled = true;
        $('#id_kares').change(function() {
            if ($(this).val() != '') {
                var idk = $(this).val();
                document.getElementById("id_dealer").disabled = true;
                document.getElementById("id_kabupaten").disabled = true;

                $.ajax({
                    url: '/tersurvey/fetch_krs/' + idk,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_dealer").disabled = false;
                            document.getElementById("id_kabupaten").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            $('#id_kabupaten').empty();
                            $('#id_kabupaten').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_kabupaten').append('<option value="' + result[i].id_kab + '">' + result[i].nama_kab + '</option>');
                            }
                        }
                    }
                });
                $.ajax({
                    url: '/tersurvey/fetch_dlr/' + idk,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_dealer").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_dealer').append('<option value="' + result[i].id_dlr + '">' + result[i].nama_dlr + '</option>');
                            }
                        }
                    }
                });
            }
        });

        $('#id_kabupaten').change(function() {
            if ($(this).val() != '') {
                var idkab = $(this).val();
                document.getElementById("id_dealer").disabled = true;
                $.ajax({
                    url: '/tersurvey/fetch_kab/' + idkab,
                    type: "GET",
                    dataType: "json",
                    success: function(result) {
                        if (result != "") {
                            document.getElementById("id_dealer").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            for (var i in result) {
                                $('#id_dealer').append('<option value="' + result[i].id_dlr + '">' + result[i].nama_dlr + '</option>');
                            }
                        }
                    }
                });
            }
        });
    });
    </script>

    <script>
        $('#formExport').submit(function(e) {
    e.preventDefault();
    var e = document.getElementById("export_pdf");
    var val = e.options[e.selectedIndex].value;
    var reportPageHeight = $('#'+val).innerHeight();
    var reportPageWidth = $('#'+val).innerWidth();

    var canvas = document.querySelector('#'+val);
    var canvasImg = canvas.toDataURL("image/png", 1.0);
    // var jsp = new jsPDF('l', 'pt','a4',true);
    var jsp = new jsPDF('l', 'pt', [reportPageWidth, reportPageHeight]);
    jsp.text(10, 20, 'Report Demografi');
    jsp.setFontSize(12);
    jsp.addImage(canvasImg, 'PNG', 10, 30, 280, 150 );
    jsp.save('report-demografi-'+val+'.pdf');

});
</script>
<script>
   $(document).ready(function () {
         $(".custom-select2").select2({
         });
   });
</script>
@endsection
