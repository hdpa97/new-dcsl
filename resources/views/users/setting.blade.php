@extends('layouts.sidebar')

@section('title-tab')
Settings
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('General') }}</div>
                <div class="card-body">
                    <form class="needs-validation" method="POST" action="{{ route('users.general') }}" autocomplete="off">
                        @csrf
                        <div class="form-group row">
                            <label for="npk" class="col-md-3 col-form-label text-md-left">{{ __('NPK') }}</label>
                            <div class="col-md-8">
                                <input id="npk" required type="number"
                                    class="form-control @error('npk') is-invalid @enderror" name="npk"
                                    value="{{$data['npk']}}" required autofocus @if($data['role']==0){ readonly }@endif>

                                @error('npk')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-md-left">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-8">
                                <input id="name" required type="text"
                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                    value="{{$data['name']}}" maxlength="32" required autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label text-md-left">{{ __('Email Address') }}</label>

                            <div class="col-md-8">
                                <input id="email" required type="text"
                                    class="form-control @error('email') is-invalid @enderror" name="email"
                                    value="{{$data['email']}}" required autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Ubah Password') }}</div>
                <div class="card-body">
                    <form class="needs-validation" method="POST" action="{{ route('users.password') }}" autocomplete="off">
                        @csrf
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left"
                                for="input-current-password">{{ __('Password Lama ') }}</label>
                            <div class="col-md-9">
                                <div class="input-group{{ $errors->has('old_password') ? ' has-danger' : '' }}">
                                    <input class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}"
                                        input type="password" name="old_password" id="input-current-password"
                                        placeholder="{{ __('Password Lama') }}" value="" required/>
                                    <div class="input-group-append">
                                        <button tabindex="97" class="btn btn-default" onclick="intipOldPassword()" type="button"><i class="fas fa-eye"></i></button>
                                    </div>
                                </div>
                                @if ($errors->has('old_password'))
                                <p id="name-error" class="error text-danger" for="input-name">
                                    {{ $errors->first('old_password') }}</p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left"
                                for="input-password">{{ __('Password Baru ') }}</label>
                            <div class="col-md-9">
                                <div class="input-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                        name="password" id="input-password" type="password"
                                        placeholder="{{ __('Password Baru') }}" value="" required minlength="8"/>
                                    <div class="input-group-append">
                                        <button tabindex="98" class="btn btn-default" onclick="intipNewPassword()" type="button"><i class="fas fa-eye"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left"
                                for="input-password-confirmation">{{ __('Ulangi Password ') }}</label>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input class="form-control" id="repw"
                                        name="password_confirmation" id="input-password-confirmation" type="password"
                                        placeholder="{{ __('Ulangi Password Baru') }}" value="" required minlength="8"/>
                                    <div class="input-group-append">
                                        <button tabindex="99" class="btn btn-default" onclick="intipRePassword()" type="button"><i class="fas fa-eye"></i></button>
                                    </div>
                                </div>
                                @if ($errors->has('password'))
                                <p id="password-error" class="error text-danger" for="input-password">
                                    {{ $errors->first('password') }}</p>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function intipOldPassword() {
      var x = document.getElementById("input-current-password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    function intipNewPassword() {
      var x = document.getElementById("input-password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    function intipRePassword() {
      var x = document.getElementById("repw");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>
@endsection
