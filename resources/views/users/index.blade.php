@extends('layouts.sidebar')

@section('title-tab')
    Data Users
@endsection
@section('button')
<a class="" href="{{route('register')}}">
    <button class="btn btn-md btn-primary pull-right">
        <i class="fas fa-plus-circle"></i>
        Tambah User
    </button>
</a>
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4">
    <div class="card-body">
        <div class="table-responsive">

            <table class="table table-bordered" id="dataTable">
                <thead>
                    <tr>
                        <th class="text-center" width="20px">No.</th>
                        <th class="text-center" width="80px">NPK</th>
                        <th class="text-center" width="100px">Name</th>
                        <th class="text-center" width="100px">Email</th>
                        <th class="text-center" width="100px">Role</th>
                        <th class="text-center" width="100px">Status</th>
                        <th class="text-center" width="100px">Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($users as $user)

                        <tr>
                            <td class="text-center">{{++$i}}.</td>
                            <td class="text-center">{{$user->npk}}</td>
                            <td class="text-center">{{$user->name}}</td>
                            <td class="text-center">{{$user->email}}</td>
                            <td class="text-center">
                                @if($user->role==1)
                                    SuperAdmin
                                @else
                                    Surveyor
                                @endif
                            </td>
                            <td class="text-center">
                                @if($user->flag)
                                    <label class="label label-success">Active</label>
                                @else
                                <label class="label label-danger">No</label>
                                @endif
                            </td>
                            <td class="text-center">
                            @if ($user->id == auth()->user()->id)
                            -
                            @else
                                @if ($user->flag == 1)
                                <a class="float-left ml-2" href="{{route('users.edit',['id' => $user->id])}}">
                                    <button class="btn btn-sm btn-info">
                                        <i>edit</i>
                                    </button>
                                </a>
                                    <form class="text-center ml-2 delete" action="{{route('users.disable',['id' => $user->id])}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('put') }}
                                        <button type="submit" class="btn btn-sm btn-danger">
                                            <i>disable</i>
                                        </button>
                                    </form>
                                @else
                                    <form class="text-center ml-2 delete" action="{{route('users.enable',['id' => $user->id])}}" method="POST">
                                        {{ csrf_field() }}
                                        {{ method_field('put') }}
                                        <button type="submit" class="btn btn-sm btn-success">
                                            <i>activate</i>
                                        </button>
                                    </form>
                                @endif
                            @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
