@extends('layouts.sidebar')

@section('title-tab')
Edit User
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('users.index')}}">Data Users</a></li>
    <li class="breadcrumb-item active">Edit User</li>
</ol>
@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Data') }}</div>

                <div class="card-body">
                    <form id="uploadForm" class="needs-validation" method="POST"
                        action="{{ route('users.update', ['id' => $data->id]) }}" autocomplete="off">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left">{{ __('NPK') }}</label>

                            <div class="col-md-8">
                                <input required type="number" class="form-control @error('npk') is-invalid @enderror"
                                    name="npk" value="{{  old('npk') ?? $data->npk}}" autofocus>

                                @error('npk')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left">{{ __('Nama Lengkap') }}</label>

                            <div class="col-md-8">
                                <input required type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{  old('name') ?? $data->name}}" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left">{{ __('Email') }}</label>

                            <div class="col-md-8">
                                <input required type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{  old('email') ?? $data->email}}" required autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left">{{ __('Password Baru') }}</label>

                            <div class="col-md-8">
                                <input type="password" minlength="8"
                                    class="form-control @error('pw') is-invalid @enderror" name="pw"
                                    placeholder="*******" autofocus>
                                <span style="margin-top:5px; font-size:12px; font-weight: bold;"
                                    class="label alert-danger">Kosongkan jika tidak ingin mengubah password</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left">{{ __('Role') }}</label>
                            <div class="col-md-8">
                                <select name="id_role" class="custom-select" disabled required="required">
                                    @if($data->role==0){
                                    <option value="1">SuperAdmin</option>
                                    <option value="{{ $data->role }}" selected>Surveyor</option>
                                    }@else{
                                    <option value="{{ $data->role }}" selected>SuperAdmin</option>
                                    <option value="0">Surveyor</option>
                                    }
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-md-3 col-form-label text-md-left">{{ __('Dealer')}}</label>
                            <div class="col-md-8 mt-2">
                                <select name="id_dealer[]" class="custom-select2" multiple="multiple"
                                    style="width:100%;" required="required">
                                    @foreach($allDealer as $dealer)
                                    <option value="{{ $dealer->id }}" @if($dealer->id_surveyor==$data->id){
                                        selected
                                        } @endif
                                        > {{ $dealer->nama_dealer }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="flag" class="col-md-3 col-form-label text-md-left">{{ __('Status') }}</label>

                            <div class="col-md-8 mt-2">
                                <input name="flag" id="f1" required type="radio" value="1" @if($data->flag == 1) checked
                                @endif><label for="f1">&nbsp;Enable</label>
                                &emsp;
                                <input name="flag" id="f2" type="radio" value="0" @if($data->flag == 0) checked
                                @endif><label for="f2">&nbsp;Disable</label>
                            </div>
                        </div>
                        <input type="hidden" name="id_user" class="form-control" value="{{$data->id}}">

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
          $(".custom-select2").select2({
          });
    });
</script>
@endsection
