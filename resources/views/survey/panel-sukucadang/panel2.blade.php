@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storeee.page.2') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> T. PURCHASE MOTIVES </b>
                </div>
                <div class="card-body">
                    <h6>T1</h6>
                    <label>
                        Selain oli, suku cadang apa sajakah yang terakhir kali Anda beli atau ganti di toko/bengkel resmi Honda?
                    </label>
                    <br>
                    <input required name="t1" id="t1v1" type="radio" value="Gear set (paket rantai roda)"><label for="t1v1">&nbsp;Gear set (paket rantai roda)</label>
                    <br>
                    <input required name="t1" id="t1v2" type="radio" value="Ban luar"><label for="t1v2">&nbsp;Ban luar</label>
                    <br>
                    <input required name="t1" id="t1v3" type="radio" value="Ban dalam"><label for="t1v3">&nbsp;Ban dalam</label>
                    <br>
                    <input required name="t1" id="t1v4" type="radio" value="Shock breaker (Shock absorbers)"><label for="t1v4">&nbsp;Shock breaker (Shock absorbers)</label>
                    <br>
                    <input required name="t1" id="t1v5" type="radio" value="Kampas rem (brake shoe)"><label for="t1v5">&nbsp;Kampas rem (brake shoe)</label>
                    <br>
                    <input required name="t1" id="t1v6" type="radio" value="Aki (baterei)"><label for="t1v6">&nbsp;Aki (baterei)</label>
                    <br>
                    <input required name="t1" id="t1v7" type="radio" value="Bearing (kolaher/bantalan)"><label for="t1v7">&nbsp;Bearing (kolaher/bantalan)</label>
                    <br>
                    <input required name="t1" id="t1v8" type="radio" value="Seher (piston)"><label for="t1v8">&nbsp;Seher (piston)</label>
                    <br>
                    <input required name="t1" id="t1v9" type="radio" value="Busi"><label for="t1v9">&nbsp;Busi</label>
                    <br>
                    <input required name="t1" id="t1v10" type="radio" value="Conn rod kit (stang seher)"><label for="t1v10">&nbsp;Conn rod kit (stang seher)</label>
                    <br>
                    <input required name="t1" id="t1v11" type="radio" value="Gear/spoket driven/gigi roda depan"><label for="t1v11">&nbsp;Gear/spoket driven/gigi roda depan</label>
                    <br>
                    <input required name="t1" id="t1v12" type="radio" value="Gear/spoket driven/gigi roda belakang"><label for="t1v12">&nbsp;Gear/spoket driven/gigi roda belakang</label>
                    <br>
                    <input required name="t1" id="t1v13" type="radio" value="Lampu halogen (halogen bulb)"><label for="t1v13">&nbsp;Lampu halogen (halogen bulb)</label>
                    <br>
                    <input required name="t1" id="t1v14" type="radio" value="Plat kopling (disc clutch)"><label for="t1v14">&nbsp;Plat kopling (disc clutch)</label>
                    <br>
                    <input required name="t1" id="t1v15" type="radio" value="Ring seher (ring set)"><label for="t1v15">&nbsp;Ring seher (ring set)</label>
                    <br>
                    <input required name="t1" id="t1v16" type="radio" value="Gasket (packing)"><label for="t1v16">&nbsp;Gasket (packing)</label>
                    <br>
                    <input required name="t1" id="t1v17" type="radio" value="Lainnya"><label for="t1v17">&nbsp;Lainnya, sebutkan</label>
                    <input type="text" name="t1_note">
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b>A. LOKASI & JAM PELAYANAN TOKO/BENGKEL RESMI PENJUAL SUKU CADANG ASLI</b>
                </div>
                <div class="card-body">
                    <h6>A1.1</h6>
                    <label>
                        Kemudahan menemukan toko/bengkel resmi yang menjual suku cadang asli Honda
                    </label>
                    <br>
                    <input required name="a1_1" id="a1_1v5" type="radio" value="5"><label for="a1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a1_1" id="a1_1v4" type="radio" value="4"><label for="a1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a1_1" id="a1_1v3" type="radio" value="3"><label for="a1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a1_1" id="a1_1v2" type="radio" value="2"><label for="a1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a1_1" id="a1_1v1" type="radio" value="1"><label for="a1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A1.2</h6>
                    <label>
                        Jam kerja jaringan/toko resmi yang menjual suku cadang asli Honda
                    </label>
                    <br>
                    <input required name="a1_2" id="a1_2v5" type="radio" value="5"><label for="a1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a1_2" id="a1_2v4" type="radio" value="4"><label for="a1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a1_2" id="a1_2v3" type="radio" value="3"><label for="a1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a1_2" id="a1_2v2" type="radio" value="2"><label for="a1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a1_2" id="a1_2v1" type="radio" value="1"><label for="a1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b>B. PETUGAS PENJUAL SUKU CADANG ASLI</b>
                </div>
                <div class="card-body">
                    <h6>B1.1</h6>
                    <label>
                        Kesigapan dan kecekatan petugas penjual suku cadang dalam melayani Anda
                    </label>
                    <br>
                    <input required name="b1_1" id="b1_1v5" type="radio" value="5"><label for="b1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v4" type="radio" value="4"><label for="b1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v3" type="radio" value="3"><label for="b1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_1" id="b1_1v2" type="radio" value="2"><label for="b1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v1" type="radio" value="1"><label for="b1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>B1.2</h6>
                    <label>
                        Keramahan dan kesopanan petugas (senyum, salam, sapa, sopan, santun)
                    </label>
                    <br>
                    <input required name="b1_2" id="b1_2v5" type="radio" value="5"><label for="b1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v4" type="radio" value="4"><label for="b1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v3" type="radio" value="3"><label for="b1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_2" id="b1_2v2" type="radio" value="2"><label for="b1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v1" type="radio" value="1"><label for="b1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>B1.3</h6>
                    <label>
                        Penampilan petugas penjual suku cadang (kerapian, kebersihan seragam)
                    </label>
                    <br>
                    <input required name="b1_3" id="b1_3v5" type="radio" value="5"><label for="b1_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_3" id="b1_3v4" type="radio" value="4"><label for="b1_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_3" id="b1_3v3" type="radio" value="3"><label for="b1_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_3" id="b1_3v2" type="radio" value="2"><label for="b1_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_3" id="b1_3v1" type="radio" value="1"><label for="b1_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>B1.4</h6>
                    <label>
                        Pengetahuan dan penjelasan petugas penjual suku cadang tentang berbagai jenis suku cadang asli Honda
                    </label>
                    <br>
                    <input required name="b1_4" id="b1_4v5" type="radio" value="5"><label for="b1_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_4" id="b1_4v4" type="radio" value="4"><label for="b1_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_4" id="b1_4v3" type="radio" value="3"><label for="b1_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_4" id="b1_4v2" type="radio" value="2"><label for="b1_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_4" id="b1_4v1" type="radio" value="1"><label for="b1_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>

            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">
            <button type="submit" class="btn btn-primary btn-block">
                {{ __('LANJUTKAN') }}
            </button>
        </div>
    </form>
</div>
@endsection