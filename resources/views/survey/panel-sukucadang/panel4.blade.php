@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storeee.page.4') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b>F. KUALITAS SUKU CADANG ASLI</b>
                </div>
                <div class="card-body">
                    <h6>F1</h6>
                    <label>
                        Seberapa puaskah Anda dengan kualitas suku cadang asli Honda?
                    </label>
                    <br>
                    <input required name="f1" id="f1v5" type="radio" value="5"><label for="f1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="f1" id="f1v4" type="radio" value="4"><label for="f1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="f1" id="f1v3" type="radio" value="3"><label for="f1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="f1" id="f1v2" type="radio" value="2"><label for="f1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="f1" id="f1v1" type="radio" value="1"><label for="f1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b>G. HARGA SUKU CADANG ASLI</b>
                </div>
                <div class="card-body">
                    <h6>G1</h6>
                    <label>
                        Apakah di toko/bengkel resmi Honda tersebut tersedia daftar harga suku cadang asli yang dapat dilihat konsumen?
                    </label>
                    <br>
                    <input required name="g1" id="g1v1" type="radio" value="Ya"><label for="g1v1">&nbsp;Ya</label>
                    <br>
                    <input required name="g1" id="g1v2" type="radio" value="Tidak"><label for="g1v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body g1" id="OpenYa" style="display: none;">
                    <h6>G2</h6>
                    <label>
                        Seberapa puaskah Anda dengan kesamaan harga suku cadang asli dengan daftar harga yang disediakan?
                    </label>
                    <br>
                    <input name="g2" id="g2v5" type="radio" value="5"><label for="g2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="g2" id="g2v4" type="radio" value="4"><label for="g2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="g2" id="g2v3" type="radio" value="3"><label for="g2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="g2" id="g2v2" type="radio" value="2"><label for="g2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="g2" id="g2v1" type="radio" value="1"><label for="g2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>G3</h6>
                    <label>
                        Menurut Anda, seberapa wajar harga suku cadang asli tersebut dibandingkan dengan kualitas yang diperoleh?
                    </label>
                    <br>
                    <input required name="g3" id="g3v5" type="radio" value="5"><label for="g3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="g3" id="g3v4" type="radio" value="4"><label for="g3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="g3" id="g3v3" type="radio" value="3"><label for="g3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="g3" id="g3v2" type="radio" value="2"><label for="g3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="g3" id="g3v1" type="radio" value="1"><label for="g3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> H. TINDAK LANJUT DAN PENANGANAN KELUHAN </b>
                </div>
                <div class="card-body">
                    <h6>H1</h6>
                    <label>
                        Apakah Anda pernah mempunyai keluhan di toko/bengkel resmi Honda <b>{{$dataDealer->nama_dealer}}</b> saat membeli suku cadang terakhir kalinya?
                    </label>
                    <br>
                    <input required name="h1" id="h1v1" type="radio" value="Ya"><label for="h1v1">&nbsp;Ya</label>
                    <br>
                    <input required name="h1" id="h1v2" type="radio" value="Tidak"><label for="h1v2">&nbsp;Tidak</label>
                    <br>
                    <label>Notes: (Tuliskan keluhannya.)</label>
                    <div class="row col-md-4">
                        <textarea class="form-control" rows="4" , cols="90" id="#" name="h1_note" style="resize:none"></textarea>
                    </div>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.sukucadang.page.3',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection