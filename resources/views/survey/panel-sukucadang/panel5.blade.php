@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storeee.page.5') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> I. TINDAK LANJUT DAN PENANGANAN KELUHAN </b>
                </div>
                <div class="card-body">
                    <h6>I4</h6>
                    <label>
                        Apakah Anda pernah mempunyai keluhan di bengkel resmi Honda saat melakukan servis sepeda motor Anda ini?
                    </label>
                    <br>
                    <input name="i4" id="i4v1" type="radio" value="Ya"><label for="i4v1">&nbsp;Ya <font color='#ff0000'><b>(GO TO I5)</b></font></label>
                    <br>
                    <input name="i4" id="i4v2" type="radio" value="Tidak"><label for="i4v2">&nbsp;Tidak <font color='#ff0000'><b>(GO TO I9)</b></font></label>
                    <br>
                    <label>Notes: (Tuliskan keluhannya.)</label>
                    <div class="row col-md-4">
                        <textarea class="form-control" rows="4" , cols="90" id="#" name="i4_note" style="resize:none"></textarea>
                    </div>
                </div>
                <div class="i4" id="OpenYa" style="display: none;">
                    <div class="card-body">
                        <h6>I5</h6>
                        <label>
                            Apakah Anda menyampaikan keluhan tersebut kepada pihak bengkel resmi Honda?
                        </label>
                        <br>
                        <input name="i5" id="i5v1" type="radio" value="Ya"><label for="i5v1">&nbsp;Ya <font color='#ff0000'><b>(GO TO I6)</b></font></label>
                        <br>
                        <input name="i5" id="i5v2" type="radio" value="Tidak"><label for="i5v2">&nbsp;Tidak <font color='#ff0000'><b>(GO TO I9)</b></font></label>
                    </div>
                </div>

                <div class="i5" id="BukaYa" style="display: none;">
                    <div class="card-body">
                        <h6>I6</h6>
                        <label>
                            Apakah pihak bengkel resmi Honda (AHASS) langsung menyelesaikan masalah tersebut?
                        </label>
                        <br>
                        <input name="i6" id="i6v1" type="radio" value="Ya"><label for="i6v1">&nbsp;Ya</label>
                        <br>
                        <input name="i6" id="i6v2" type="radio" value="Tidak"><label for="i6v2">&nbsp;Tidak</label>
                    </div>
                    <div class="card-body">
                        <h6>I7</h6>
                        <label>
                            Seberapa puaskah Anda dengan proses penanganan keluhan oleh petugas bengkel resmi Honda (AHASS)?
                        </label>
                        <br>
                        <input name="i7" id="i7v5" type="radio" value="5"><label for="i7v5">&nbsp;(5) Sangat Puas</label>
                        <br>
                        <input name="i7" id="i7v4" type="radio" value="4"><label for="i7v4">&nbsp;(4) Puas</label>
                        <br>
                        <input name="i7" id="i7v3" type="radio" value="3"><label for="i7v3">&nbsp;(3) Antara Puas dan Tidak <font color='#ff0000'><b>(GO TO I8)</b></font></label>
                        <br>
                        <input name="i7" id="i7v2" type="radio" value="2"><label for="i7v2">&nbsp;(2) Tidak Puas <font color='#ff0000'><b>(GO TO I8)</b></font></label>
                        <br>
                        <input name="i7" id="i7v1" type="radio" value="1"><label for="i7v1">&nbsp;(1) Sangat Tidak Puas <font color='#ff0000'><b>(GO TO I8)</b></font></label>
                    </div>
                    <div class="card-body i7" id="Check" style="display: none;">
                        <h6>I8 <font color='#ff0000'><b>(ASK IF CODED 1 OR 2 OR 3 IN I7)</b></font>
                        </h6>
                        <label>
                            Apa yang membuat Anda tidak puas terhadap proses penanganan keluhan oleh petugas bengkel resmi Honda (AHASS)?
                        </label>
                        <br>
                        <input name="i8" id="i8v1" type="radio" value="Masalah terselesaikan namun cara menjawabnya kurang sopan">
                        <label for="i8v1">&nbsp;Masalah terselesaikan namun cara menjawabnya kurang sopan</label>
                        <br>
                        <input name="i8" id="i8v2" type="radio" value="Cara menjawab sudah cukup sopan namun masalah saya tidak terselesaikan">
                        <label for="i8v2">&nbsp;Cara menjawab sudah cukup sopan namun masalah saya tidak terselesaikan </label>
                        <br>
                        <input name="i8" id="i8v3" type="radio" value="Lainnya"><label for="i8v3">&nbsp;Lainnya, sebutkan</label>
                        <input type="text" name="i8_note">
                    </div>
                </div>
                <div class="card-body">
                    <h6>I9</h6>
                    <label>
                        Berdasarkan pengalaman tersebut, bagaimanakah tingkat kemungkinan Anda untuk servis motor lagi di bengkel resmi Honda (AHASS) yang sama di masa mendatang?
                    </label>
                    <br>
                    <input name="i9" id="i9v1" type="radio" value="Saya akan tetap servis di bengkel resmi Honda (AHASS) yang sama">
                    <label for="i9v1">&nbsp;Saya akan tetap servis di bengkel resmi Honda (AHASS) yang sama</label>
                    <br>
                    <input name="i9" id="i9v2" type="radio" value="Saya akan tetap servis di bengkel resmi Honda (AHASS) namun ke cabang lainnya">
                    <label for="i9v2">&nbsp;Saya akan tetap servis di bengkel resmi Honda (AHASS) namun ke cabang lainnya </label>
                    <br>
                    <input name="i9" id="i9v3" type="radio" value="Saya tidak akan servis di bengkel resmi Honda (AHASS) ini lagi di masa yang akan datang">
                    <label for="i9v3">&nbsp;Saya tidak akan servis di bengkel resmi Honda (AHASS) ini lagi di masa yang akan datang </label>
                    <br>
                    <label>Notes: (Tuliskan cabang lainnya itu yang dimana?)</label>
                    <div class="row col-md-4">
                        <textarea class="form-control" rows="4" , cols="90" id="#" name="i9_note" style="resize:none"></textarea>
                    </div>
                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> K. OVERALL SATISFACTION </b>
                </div>
                <div class="card-body">
                    <h6>K1</h6>
                    <label>
                        <b><u>SHOWCARD</u></b>
                        <br>
                        Secara keseluruhan, seberapa puas Anda terhadap bengkel resmi Honda (AHASS) pada saat Anda menyervis sepeda motor? (SA)
                    </label>
                    <br>
                    <input name="k1" id="k1v5" type="radio" value="Ya"><label for="k1v5">&nbsp;Sangat Puas</label>
                    <br>
                    <input name="k1" id="k1v4" type="radio" value="Tidak"><label for="k1v4">&nbsp;Puas</label>
                    <br>
                    <input name="k1" id="k1v3" type="radio" value="Ya"><label for="k1v3">&nbsp;Antara Puas dan Tidak</label>
                    <br>
                    <input name="k1" id="k1v2" type="radio" value="Tidak"><label for="k1v2">&nbsp;Tidak Puas</label>
                    <br>
                    <input name="k1" id="k1v1" type="radio" value="Tidak"><label for="k1v1">&nbsp;Sangat Tidak Puas</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.sukucadang.page.4',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection