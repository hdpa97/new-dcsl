@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storeee.page.3') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b>C. KETERSEDIAAN SUKU CADANG ASLI</b>
                </div>
                <div class="card-body">
                    <h6>C1</h6>
                    <label>
                        Selanjutnya, kami ingin mengetahui tingkat kepuasan Anda terhadap ketersediaan suku cadang asli Honda di <b>{{$dataDealer->nama_dealer}}</b>. Seberapa puaskah Anda dengan ketersediaan suku cadang asli Honda?
                    </label>
                    <br>
                    <input required name="c1" id="c1v5" type="radio" value="5"><label for="c1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="c1" id="c1v4" type="radio" value="4"><label for="c1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="c1" id="c1v3" type="radio" value="3"><label for="c1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="c1" id="c1v2" type="radio" value="2"><label for="c1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="c1" id="c1v1" type="radio" value="1"><label for="c1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>C1.a</h6>
                    <label>
                        Apakah anda pernah membeli oli asli Honda di <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input required name="c1_a" id="c1_av1" type="radio" value="Ya"><label for="c1_av1">&nbsp;Ya</label>
                    <br>
                    <input required name="c1_a" id="c1_av2" type="radio" value="Tidak"><label for="c1_av2">&nbsp;Tidak</label>
                </div>
                <div class="card-body c1_a" id="BukaYa" style="display: none;">
                    <h6>C1.b</h6>
                    <label>
                        Selanjutnya, kami ingin mengetahui tingkat kepuasan Anda terhadap ketersediaan oli asli Honda di <b>{{$dataDealer->nama_dealer}}</b>.
                        Seberapa puaskah Anda dengan ketersediaan oli asli Honda?
                    </label>
                    <br>
                    <input name="c1_b" id="c1_bv5" type="radio" value="5"><label for="c1_bv5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="c1_b" id="c1_bv4" type="radio" value="4"><label for="c1_bv4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="c1_b" id="c1_bv3" type="radio" value="3"><label for="c1_bv3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="c1_b" id="c1_bv2" type="radio" value="2"><label for="c1_bv2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="c1_b" id="c1_bv1" type="radio" value="1"><label for="c1_bv1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>C2</h6>
                    <label>
                        Apakah Anda pernah tidak mendapatkan suku cadang asli Honda yang dicari saat membeli di toko/bengkel
                        resmi Honda <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input required name="c2" id="c2v1" type="radio" value="Ya"><label for="c2v1">&nbsp;Ya</label>
                    <br>
                    <input required name="c2" id="c2v2" type="radio" value="Tidak"><label for="c2v2">&nbsp;Tidak</label>
                </div>
                <div class="c2" id="BukaaYa" style="display: none;">
                    <div class="card-body">
                        <h6>C3</h6>
                        <label>
                            Suku cadang asli Honda apakah yang tidak tersedia tersebut?
                            <br>
                            <b>Probe.</b> Apalagi?
                        </label>
                        <br>
                        <input name="c3" id="c3v1" type="radio" value="Gear set (paket rantai roda)"><label for="c3v1">&nbsp;Gear set (paket rantai roda)</label>
                        <br>
                        <input name="c3" id="c3v2" type="radio" value="Ban luar"><label for="c3v2">&nbsp;Ban luar</label>
                        <br>
                        <input name="c3" id="c3v3" type="radio" value="Ban dalam"><label for="c3v3">&nbsp;Ban dalam</label>
                        <br>
                        <input name="c3" id="c3v4" type="radio" value="Shock breaker (Shock absorbers)"><label for="c3v4">&nbsp;Shock breaker (Shock absorbers)</label>
                        <br>
                        <input name="c3" id="c3v5" type="radio" value="Kampas rem (brake shoe)"><label for="c3v5">&nbsp;Kampas rem (brake shoe)</label>
                        <br>
                        <input name="c3" id="c3v6" type="radio" value="Aki (baterei)"><label for="c3v6">&nbsp;Aki (baterei)</label>
                        <br>
                        <input name="c3" id="c3v7" type="radio" value="Bearing (kolaher/bantalan)"><label for="c3v7">&nbsp;Bearing (kolaher/bantalan)</label>
                        <br>
                        <input name="c3" id="c3v8" type="radio" value="Seher (piston)"><label for="c3v8">&nbsp;Seher (piston)</label>
                        <br>
                        <input name="c3" id="c3v9" type="radio" value="Busi"><label for="c3v9">&nbsp;Busi</label>
                        <br>
                        <input name="c3" id="c3v10" type="radio" value="Conn rod kit (stang seher)"><label for="c3v10">&nbsp;Conn rod kit (stang seher)</label>
                        <br>
                        <input name="c3" id="c3v11" type="radio" value="Gear/spoket driven/gigi roda depan"><label for="c3v11">&nbsp;Gear/spoket driven/gigi roda depan</label>
                        <br>
                        <input name="c3" id="c3v12" type="radio" value="Gear/spoket driven/gigi roda belakang"><label for="c3v12">&nbsp;Gear/spoket driven/gigi roda belakang</label>
                        <br>
                        <input name="c3" id="c3v13" type="radio" value="Lampu halogen (halogen bulb)"><label for="c3v13">&nbsp;Lampu halogen (halogen bulb)</label>
                        <br>
                        <input name="c3" id="c3v14" type="radio" value="Plat kopling (disc clutch)"><label for="c3v14">&nbsp;Plat kopling (disc clutch)</label>
                        <br>
                        <input name="c3" id="c3v15" type="radio" value="Ring seher (ring set)"><label for="c3v15">&nbsp;Ring seher (ring set)</label>
                        <br>
                        <input name="c3" id="c3v16" type="radio" value="Gasket (packing)"><label for="c3v16">&nbsp;Gasket (packing)</label>
                        <br>
                        <input name="c3" id="c3v17" type="radio" value="Lainnya"><label for="c3v17">&nbsp;Lainnya, sebutkan</label>
                        <input type="text" name="c3_note">
                    </div>
                    <div class="card-body">
                        <h6>C4</h6>
                        <label>
                            Kapankah Anda mengalami kejadian saat suku cadang asli Honda yang dicari tidak ada di toko/bengkel resmi
                            Honda <b>{{$dataDealer->nama_dealer}}</b>?
                        </label>
                        <br>
                        <input name="c4" id="c4v1" type="radio" value="1"><label for="c4v1">&nbsp;Kurang dari 1 bulan yang lalu</label>
                        <br>
                        <input name="c4" id="c4v2" type="radio" value="2"><label for="c4v2">&nbsp;2-3 bulan yang lalu</label>
                        <br>
                        <input name="c4" id="c4v3" type="radio" value="3"><label for="c4v3">&nbsp;4-6 bulan yang lalu</label>
                        <br>
                        <input name="c4" id="c4v4" type="radio" value="4"><label for="c4v4">&nbsp;7-12 bulan yang lalu</label>
                        <br>
                        <input name="c4" id="c4v5" type="radio" value="5"><label for="c4v5">&nbsp;Lebih dari 1 tahun yang lalu</label>
                    </div>
                    <div class="card-body">
                        <h6>C5</h6>
                        <label>
                            <br>
                            Ketika suku cadang asli Honda yang Anda cari tidak ada, apakah petugas menawarkan kepada Anda untuk
                            mengisi form pemesanan suku cadang tersebut?
                        </label>
                        <br>
                        <input name="c5" id="c5v1" type="radio" value="Ya"><label for="c5v1">&nbsp;Ya</label>
                        <br>
                        <input name="c5" id="c5v2" type="radio" value="Tidak"><label for="c5v2">&nbsp;Tidak</label>
                    </div>
                </div>
            </div>

            <div class="c5" id="OpenYa" style="display: none;">
                <div class="card shadow mb-4">
                    <div class="card-header text-center">
                        <b>D. PEMESANAN SUKU CADANG ASLI</b>
                    </div>
                    <div class="collapse show">
                        <div class="card-body">
                            <h6>D1</h6>
                            <label>
                                Berapa lama waktu yang dijanjikan petugas untuk mendapatkan suku cadang yang Anda pesan?
                            </label>
                            <br>
                            <input name="d1" id="d1v1" type="radio" value="1"><label for="d1v1">&nbsp;Kurang dari 1 minggu</label>
                            <br>
                            <input name="d1" id="d1v2" type="radio" value="2"><label for="d1v2">&nbsp;1-2 minggu</label>
                            <br>
                            <input name="d1" id="d1v3" type="radio" value="3"><label for="d1v3">&nbsp;3-4 minggu</label>
                            <br>
                            <input name="d1" id="d1v4" type="radio" value="4"><label for="d1v4">&nbsp;5-6 minggu</label>
                            <br>
                            <input name="d1" id="d1v5" type="radio" value="5"><label for="d1v5">&nbsp;7-8 minggu</label>
                            <br>
                            <input name="d1" id="d1v6" type="radio" value="6"><label for="d1v6">&nbsp;Lebih dari 8 minggu</label>
                        </div>
                        <div class="card-body">
                            <h6>D2</h6>
                            <label>
                                Menurut Anda, seberapa wajar lamanya waktu menunggu pesanan suku cadang tersebut, jika dinilai dengan
                                kartu bantu berikut?
                            </label>
                            <br>
                            <input name="d2" id="d2v5" type="radio" value="5"><label for="d2v5">&nbsp;(5) Sangat Wajar</label>
                            <br>
                            <input name="d2" id="d2v4" type="radio" value="4"><label for="d2v4">&nbsp;(4) Wajar</label>
                            <br>
                            <input name="d2" id="d2v3" type="radio" value="3"><label for="d2v3">&nbsp;(3) Antara Wajar dan Tidak</label>
                            <br>
                            <input name="d2" id="d2v2" type="radio" value="2"><label for="d2v2">&nbsp;(2) Tidak Wajar</label>
                            <br>
                            <input name="d2" id="d2v1" type="radio" value="1"><label for="d2v1">&nbsp;(1) Sangat Tidak Wajar</label>
                        </div>
                        <div class="card-body">
                            <h6>D3.1</h6>
                            <label>
                                Kemudahan cara dan syarat pemesanan suku cadang tersebut di toko/bengkel resmi Honda
                            </label>
                            <br>
                            <input name="d3_1" id="d3_1v5" type="radio" value="5"><label for="d3_1v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="d3_1" id="d3_1v4" type="radio" value="4"><label for="d3_1v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="d3_1" id="d3_1v3" type="radio" value="3"><label for="d3_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="d3_1" id="d3_1v2" type="radio" value="2"><label for="d3_1v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="d3_1" id="d3_1v1" type="radio" value="1"><label for="d3_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>D3.2</h6>
                            <label>
                                Informasi perkiraan kedatangan pemesanan suku cadang
                            </label>
                            <br>
                            <input name="d3_2" id="d3_2v5" type="radio" value="5"><label for="d3_2v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="d3_2" id="d3_2v4" type="radio" value="4"><label for="d3_2v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="d3_2" id="d3_2v3" type="radio" value="3"><label for="d3_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="d3_2" id="d3_2v2" type="radio" value="2"><label for="d3_2v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="d3_2" id="d3_2v1" type="radio" value="1"><label for="d3_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>D3.2a</h6>
                            <label>
                                Ketepatan perkiraan waktu tunggu dibandingkan dengan informasi kedatangan pesanan suku cadang oleh bengkel resmi Honda
                            </label>
                            <br>
                            <input name="d3_2a" id="d3_2av5" type="radio" value="5"><label for="d3_2av5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="d3_2a" id="d3_2av4" type="radio" value="4"><label for="d3_2av4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="d3_2a" id="d3_2av3" type="radio" value="3"><label for="d3_2av3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="d3_2a" id="d3_2av2" type="radio" value="2"><label for="d3_2av2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="d3_2a" id="d3_2av1" type="radio" value="1"><label for="d3_2av1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>D3.3</h6>
                            <label>
                                Ketepatan perkiraan waktu tunggu dibandingkan dengan datangnya pesanan suku cadang
                            </label>
                            <br>
                            <input name="d3_3" id="d3_3v5" type="radio" value="5"><label for="d3_3v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="d3_3" id="d3_3v4" type="radio" value="4"><label for="d3_3v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="d3_3" id="d3_3v3" type="radio" value="3"><label for="d3_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="d3_3" id="d3_3v2" type="radio" value="2"><label for="d3_3v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="d3_3" id="d3_3v1" type="radio" value="1"><label for="d3_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>D3.3a</h6>
                            <label>
                                Apakah anda telah mendapatkan suku cadang yang dipesan tersebut?
                            </label>
                            <br>
                            <input name="d3_3a" id="d3_3av1" type="radio" value="Ya"><label for="d3_3av1">&nbsp;Ya</label>
                            <br>
                            <input name="d3_3a" id="d3_3av2" type="radio" value="Tidak"><label for="d3_3av2">&nbsp;Tidak</label>
                        </div>

                        <div class="d3_3a" id="BukaaaYa" style="display: none;">
                            <div class="card-body">
                                <h6>D3.3b</h6>
                                <label>
                                    Berapa lama waktu kedatangan suku cadang yang anda pesan tersebut?
                                </label>
                                <br>
                                <input name="d3_3b" id="d3_3bv1" type="radio" value="1"><label for="d3_3bv1">&nbsp;(1) Kurang dari 1 minggu</label>
                                <br>
                                <input name="d3_3b" id="d3_3bv2" type="radio" value="2"><label for="d3_3bv2">&nbsp;(2) 1 – 2 minggu</label>
                                <br>
                                <input name="d3_3b" id="d3_3bv3" type="radio" value="3"><label for="d3_3bv3">&nbsp;(3) 3 – 4 minggu</label>
                                <br>
                                <input name="d3_3b" id="d3_3bv4" type="radio" value="4"><label for="d3_3bv4">&nbsp;(4) 5 – 6 minggu</label>
                                <br>
                                <input name="d3_3b" id="d3_3bv5" type="radio" value="5"><label for="d3_3bv5">&nbsp;(5) 7 – 8 minggu</label>
                                <br>
                                <input name="d3_3b" id="d3_3bv6" type="radio" value="6"><label for="d3_3bv6">&nbsp;(6) Lebih dari 8 minggu</label>
                            </div>
                        </div>
                        <div class="card-body">
                            <h6>D4</h6>
                            <label>
                                Apakah suku cadang yang Anda pesan di toko/bengkel resmi Honda <b>{{$dataDealer->nama_dealer}}</b> mengalami keterlambatan pengiriman?
                            </label>
                            <br>
                            <input name="d4" id="d4v1" type="radio" value="Ya"><label for="d4v1">&nbsp;Ya</label>
                            <br>
                            <input name="d4" id="d4v2" type="radio" value="Tidak"><label for="d4v2">&nbsp;Tidak</label>
                        </div>
                        <div class="card-body d4" id="BukaaaaYa" style="display: none;">
                            <h6>D5</h6>
                            <label>
                                Apakah petugas menginformasikan apabila ada keterlambatan waktu pemenuhan suku cadang tersebut?
                            </label>
                            <br>
                            <input name="d5" id="d5v1" type="radio" value="Ya"><label for="d5v1">&nbsp;Ya</label>
                            <br>
                            <input name="d5" id="d5v2" type="radio" value="Tidak"><label for="d5v2">&nbsp;Tidak</label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="c5" id="OpenTidak" style="display: none;">
                <div class="card shadow mb-4">
                    <div class="card-header text-center">
                        <b>E. BANTUAN PETUGAS DALAM MENCARIKAN SUKU CADANG ASLI</b>
                    </div>
                    <div class="collapse show">
                        <div class="card-body">
                            <h6>E1</h6>
                            <label>
                                Apabila suku cadang yang Anda butuhkan tidak tersedia dan Anda tidak melakukan pengisian form pemesanan, apakah petugas membantu Anda mencarikan suku cadang yang Anda butuhkan?
                            </label>
                            <br>
                            <input name="e1" id="e1v1" type="radio" value="Ya"><label for="e1v1">&nbsp;Ya</label>
                            <br>
                            <input name="e1" id="e1v2" type="radio" value="Tidak"><label for="e1v2">&nbsp;Tidak</label>
                        </div>
                        <div class="card-body e1" id="CheckkYa" style="display: none;">
                            <h6>E2</h6>
                            <label>
                                Selanjutnya, kami ingin mengetahui tingkat kepuasan Anda terhadap sikap petugas toko/bengkel resmi Honda dalam membantu mencarikan suku cadang.
                                Seberapa puaskah Anda dengan sikap petugas dalam membantu mencarikan suku cadang asli Honda yang Anda cari namun tidak tersedia?
                            </label>
                            <br>
                            <input name="e2" id="e2v5" type="radio" value="5"><label for="e2v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="e2" id="e2v4" type="radio" value="4"><label for="e2v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="e2" id="e2v3" type="radio" value="3"><label for="e2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="e2" id="e2v2" type="radio" value="2"><label for="e2v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="e2" id="e2v1" type="radio" value="1"><label for="e2v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.sukucadang.page.2',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
