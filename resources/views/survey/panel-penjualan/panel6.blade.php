@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('store.page.6') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> H. PENGIRIMAN DAN PENYERAHAN SEPEDA MOTOR </b>
                </div>
                <div class="card-body">
                    <h6>H3</h6>
                    <label>
                        Setelah menerima motor, apakah Anda dihubungi kembali melalui telepon oleh petugas dealer?
                    </label>
                    <br>
                    <input required name="h3" id="h3v1" type="radio" value="Ya"><label for="h3v1">&nbsp;Ya</label>
                    <br>
                    <input required name="h3" id="h3v2" type="radio" value="Tidak"><label for="h3v2">&nbsp;Tidak</label>
                </div>
                <div class="h3_v12345" id="OpenYa" style="display: none;">
                    <div class="card-body">
                        <h6>H3.a1</h6>
                        <label>
                            Apakah pada saat menelepon, petugas dealer memberikan ucapan terima kasih kepada Anda?
                        </label>
                        <br>
                        <input name="h3_a1" id="h3_a1v1" type="radio" value="Ya"><label for="h3_a1v1">&nbsp;Ya</label>
                        <br>
                        <input name="h3_a1" id="h3_a1v2" type="radio" value="Tidak"><label for="h3_a1v2">&nbsp;Tidak</label>
                    </div>
                    <div class="card-body">
                        <h6>H3.a2</h6>
                        <label>
                            Apakah pada saat menelepon, petugas dealer meminta kirik dan saran kepada Anda?
                        </label>
                        <br>
                        <input name="h3_a2" id="h3_a2v1" type="radio" value="Ya"><label for="h3_a2v1">&nbsp;Ya</label>
                        <br>
                        <input name="h3_a2" id="h3_a2v2" type="radio" value="Tidak"><label for="h3_a2v2">&nbsp;Tidak</label>
                    </div>
                    <div class="card-body">
                        <h6>H3.a3</h6>
                        <label>
                            Apakah pada saat menelepon, Petugas Dealer melakukan pengecekan data (seperti nama, alamat surat menyurat, nomor telepon, agama, dan tanggal lahir) kepada Anda?
                        </label>
                        <br>
                        <input name="h3_a3" id="h3_a3v1" type="radio" value="Ya"><label for="h3_a3v1">&nbsp;Ya</label>
                        <br>
                        <input name="h3_a3" id="h3_a3v2" type="radio" value="Tidak"><label for="h3_a3v2">&nbsp;Tidak</label>
                    </div>
                    <div class="card-body">
                        <h6>H3.a4</h6>
                        <label>
                            Apakah pada saat menelepon, Petugas Dealer mengingatkan jadwal servis pertama kepada Anda?
                        </label>
                        <br>
                        <input name="h3_a4" id="h3_a4v1" type="radio" value="Ya"><label for="h3_a4v1">&nbsp;Ya</label>
                        <br>
                        <input name="h3_a4" id="h3_a4v2" type="radio" value="Tidak"><label for="h3_a4v2">&nbsp;Tidak</label>
                    </div>
                    <div class="card-body">
                        <h6>H3.a5</h6>
                        <label>
                            Berapa hari setelah menerima motor Anda dihubungi kembali melalui telepon oleh petugas dealer?
                        </label>
                        <br>
                        <input name="h3_a5" id="h3_a5v1" type="radio" value="Kurang dari 1 minggu"><label for="h3_a5v1">&nbsp;Kurang dari 1 minggu</label>
                        <br>
                        <input name="h3_a5" id="h3_a5v2" type="radio" value="1 – 2 minggu"><label for="h3_a5v2">&nbsp;1 – 2 minggu</label>
                        <br>
                        <input name="h3_a5" id="h3_a5v3" type="radio" value="Lebih dari 2 minggu"><label for="h3_a5v3">&nbsp;Lebih dari 2 minggu</label>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> I. PENGURUSAN STNK </b>
                </div>
                <div class="card-body">
                    <h6>I1.1</h6>
                    <label>
                        Ketepatan waktu penyerahan STNK sesuai yang dijanjikan
                    </label>
                    <br>
                    <input required name="i1_1" id="i1_1v5" type="radio" value="5"><label for="i1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="i1_1" id="i1_1v4" type="radio" value="4"><label for="i1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="i1_1" id="i1_1v3" type="radio" value="3"><label for="i1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="i1_1" id="i1_1v2" type="radio" value="2"><label for="i1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="i1_1" id="i1_1v1" type="radio" value="1"><label for="i1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>I1.2</h6>
                    <label>
                        Kemudahan dalam proses (tata cara/syarat-syarat) pengambilan STNK
                    </label>
                    <br>
                    <input required name="i1_2" id="i1_2v5" type="radio" value="5"><label for="i1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="i1_2" id="i1_2v4" type="radio" value="4"><label for="i1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="i1_2" id="i1_2v3" type="radio" value="3"><label for="i1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="i1_2" id="i1_2v2" type="radio" value="2"><label for="i1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="i1_2" id="i1_2v1" type="radio" value="1"><label for="i1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> INTERVIEWER </b>
                </div>
                <div class="card-body">
                    <b><u>KERAMAHAN DAN PENAMPILAN SELURUH PETUGAS</u></b>
                    <br>
                    Selanjutnya, kami ingin mengetahui tingkat kepuasan Anda terhadap keramahan dan penampilan seluruh petugas dishowroom/dealer resmi <b>{{$dataDealer->nama_dealer}}</b>.
                    Seberapa puskah Anda dengan keramahan dan penampilan seluruh petugas dalam hal ______________ [BACAKAN ATRIBUT]
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> J. KERAMAHAN DAN PENAMPILAN SELURUH PETUGAS </b>
                </div>
                <div class="card-body">
                    <h6>J1.1</h6>
                    <label>
                        Keramahan dan kesopanan (senyum, salam, sapa, sopan, santun) petugas dealer baik salesman/SPG, kasir, dan petugas pengiriman
                    </label>
                    <br>
                    <input required name="j1_1" id="j1_1v5" type="radio" value="5"><label for="j1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="j1_1" id="j1_1v4" type="radio" value="4"><label for="j1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="j1_1" id="j1_1v3" type="radio" value="3"><label for="j1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="j1_1" id="j1_1v2" type="radio" value="2"><label for="j1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="j1_1" id="j1_1v1" type="radio" value="1"><label for="j1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>J1.2</h6>
                    <label>
                        Penampilan (kerapian, kebersihan seragam) petugas dealer baik salesman/SPG, kasir, dan petugas pengiriman
                    </label>
                    <br>
                    <input required name="j1_2" id="j1_2v5" type="radio" value="5"><label for="j1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="j1_2" id="j1_2v4" type="radio" value="4"><label for="j1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="j1_2" id="j1_2v3" type="radio" value="3"><label for="j1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="j1_2" id="j1_2v2" type="radio" value="2"><label for="j1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="j1_2" id="j1_2v1" type="radio" value="1"><label for="j1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.penjualan.page.5',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection