@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('store.page.4') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> D. PROSES PEMBAYARAN </b>
                </div>
                <div class="card-body">
                    <h6>D15</h6>
                    <label>
                        Apakah Anda membayar tunai atau kredit untuk motor <b>{{$datas->tipe}}</b>?
                    </label>
                    <br>
                    <input required name="d15" id="d15v1" type="radio" value="Tunai"><label for="d15v1">&nbsp;Tunai</label>
                    <br>
                    <input required name="d15" id="d15v2" type="radio" value="Kredit"><label for="d15v2">&nbsp;Kredit</label>
                </div>
            </div>
            <div class="d15" id="OpenTunai" style="display: none;">
                <div class="card shadow mb-4">
                    <div class="card-header text-center">
                        <a href="#tabBeliTunai" style="color:transparent" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                            <h6 class="m-0 font-weight-bold text-primary">
                                E. PEMBELIAN SECARA TUNAI
                            </h6>
                        </a>
                    </div>

                    <div class="collapse show">
                        <div class="card-body">
                            <h6>E1.1</h6>
                            <label>
                                Kemudahan pengurusan administrasi dan proses pembayaran secara tunai
                            </label>
                            <br>
                            <input name="e1_1" id="e1_1v5" type="radio" value="5"><label for="e1_1v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="e1_1" id="e1_1v4" type="radio" value="4"><label for="e1_1v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="e1_1" id="e1_1v3" type="radio" value="3"><label for="e1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="e1_1" id="e1_1v2" type="radio" value="2"><label for="e1_1v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="e1_1" id="e1_1v1" type="radio" value="1"><label for="e1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>E1.2</h6>
                            <label>
                                Kesamaan harga yang ditawarkan petugas showroom/dealer dengan daftar harga yang ada di showroom/dealer
                            </label>
                            <br>
                            <input name="e1_2" id="e1_2v5" type="radio" value="5"><label for="e1_2v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="e1_2" id="e1_2v4" type="radio" value="4"><label for="e1_2v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="e1_2" id="e1_2v3" type="radio" value="3"><label for="e1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="e1_2" id="e1_2v2" type="radio" value="2"><label for="e1_2v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="e1_2" id="e1_2v1" type="radio" value="1"><label for="e1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>E1.3</h6>
                            <label>
                                Kewajaran harga sepeda motor dibandingkan dengan kualitasnya
                            </label>
                            <br>
                            <input name="e1_3" id="e1_3v5" type="radio" value="5"><label for="e1_3v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="e1_3" id="e1_3v4" type="radio" value="4"><label for="e1_3v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="e1_3" id="e1_3v3" type="radio" value="3"><label for="e1_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="e1_3" id="e1_3v2" type="radio" value="2"><label for="e1_3v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="e1_3" id="e1_3v1" type="radio" value="1"><label for="e1_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d15" id="OpenKredit" style="display: none;">
                <div class="card shadow mb-4">
                    <div class="card-header text-center">
                        <a href="#tabBeliKredit" style="color:transparent" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
                            <h6 class="m-0 font-weight-bold text-primary">
                                F. PEMBELIAN SECARA KREDIT
                            </h6>
                        </a>
                    </div>
                    <div class="collapse show">
                        <div class="card-body">
                            <h6>F2.1</h6>
                            <label>
                                Kemudahan proses administrasi pembelian secara kredit (termasuk waktu tunggu survei)
                            </label>
                            <br>
                            <input name="f2_1" id="f2_1v5" type="radio" value="5"><label for="f2_1v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="f2_1" id="f2_1v4" type="radio" value="4"><label for="f2_1v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="f2_1" id="f2_1v3" type="radio" value="3"><label for="f2_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="f2_1" id="f2_1v2" type="radio" value="2"><label for="f2_1v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="f2_1" id="f2_1v1" type="radio" value="1"><label for="f2_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                        <div class="card-body">
                            <h6>F2.2</h6>
                            <label>
                                Kepuasan terhadap kemudahan pembayaran dalam pembelian motor (dapat lewat ATM, Bank, dll)
                            </label>
                            <br>
                            <input name="f2_2" id="f2_2v5" type="radio" value="5"><label for="f2_2v5">&nbsp;(5) Sangat Puas</label>
                            <br>
                            <input name="f2_2" id="f2_2v4" type="radio" value="4"><label for="f2_2v4">&nbsp;(4) Puas</label>
                            <br>
                            <input name="f2_2" id="f2_2v3" type="radio" value="3"><label for="f2_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                            <br>
                            <input name="f2_2" id="f2_2v2" type="radio" value="2"><label for="f2_2v2">&nbsp;(2) Tidak Puas</label>
                            <br>
                            <input name="f2_2" id="f2_2v1" type="radio" value="1"><label for="f2_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> G. PETUGAS KASIR </b>
                </div>
                <div class="card-body">
                    <h6>G1</h6>
                    <label>
                        Selanjutnya, kami ingin mengetahui tingkat kepuasan Anda terhadap petugas kasir di dealer <b>{{$dataDealer->nama_dealer}}</b>. Seberapa puaskah Anda dengan ketelitian petugas kasirdalam melayani pembayaran tunai, kredit atau uang muka?
                    </label>
                    <br>
                    <input required name="g1" id="g1v5" type="radio" value="5"><label for="g1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="g1" id="g1v4" type="radio" value="4"><label for="g1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="g1" id="g1v3" type="radio" value="3"><label for="g1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="g1" id="g1v2" type="radio" value="2"><label for="g1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="g1" id="g1v1" type="radio" value="1"><label for="g1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.penjualan.page.3',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection