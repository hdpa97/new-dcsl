@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('store.page.3') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> B. PELAYANAN PETUGAS PENJUALAN </b>
                </div>
                <div class="card-body">
                    <h6>B1.1</h6>
                    <label>
                        Kemampuan petugas penjualan dalam menggali kebutuhan, menjelaskan fitur dan keunggulan serta menyarankan produk yang sesuai dengan kebutuhan Anda
                    </label>
                    <br>
                    <input required name="b1_1" id="b1_1v5" type="radio" value="5"><label for="b1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v4" type="radio" value="4"><label for="b1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v3" type="radio" value="3"><label for="b1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_1" id="b1_1v2" type="radio" value="2"><label for="b1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v1" type="radio" value="1"><label for="b1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>B1.2</h6>
                    <label>
                        Penjelasan petugas penjualan mengenai bentuk serta paket pembayaran sepeda motor (tunai/kredit, tempat pembayaran)
                    </label>
                    <br>
                    <input required name="b1_2" id="b1_2v5" type="radio" value="5"><label for="b1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v4" type="radio" value="4"><label for="b1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v3" type="radio" value="3"><label for="b1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_2" id="b1_2v2" type="radio" value="2"><label for="b1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v1" type="radio" value="1"><label for="b1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                @if($dataDealer->id_layer != 2 || $motors==true)
                <div class="card-body">
                    <h6>B1.3</h6>
                    <label>
                        Penjelasan petugas penjualan mengenai aksesoris dan apparel (baju/jaket) sepeda motor honda
                    </label>
                    <br>
                    <input required name="b1_3" id="b1_3v5" type="radio" value="5"><label for="b1_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_3" id="b1_3v4" type="radio" value="4"><label for="b1_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_3" id="b1_3v3" type="radio" value="3"><label for="b1_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_3" id="b1_3v2" type="radio" value="2"><label for="b1_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_3" id="b1_3v1" type="radio" value="1"><label for="b1_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>B1.3a</h6>
                    <label>
                        Apakah petugas penjualan menjelaskan tentang kegiatan komunitas Honda?
                    </label>
                    <br>
                    <input required name="b1_3a" id="b1_3av1" type="radio" value="Ya"><label for="b1_3av1">&nbsp;Ya</label>
                    <br>
                    <input required name="b1_3a" id="b1_3av2" type="radio" value="Tidak"><label for="b1_3av2">&nbsp;Tidak</label>
                </div>
                <div class="card-body b1_3a" id="OpenYa" style="display: none;">
                    <h6>B1.4</h6>
                    <label>
                        Penjelasan petugas mengenai kegiatan komunitas Honda
                    </label>
                    <br>
                    <input name="b1_4" id="b1_4v5" type="radio" value="5"><label for="b1_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="b1_4" id="b1_4v4" type="radio" value="4"><label for="b1_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="b1_4" id="b1_4v3" type="radio" value="3"><label for="b1_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="b1_4" id="b1_4v2" type="radio" value="2"><label for="b1_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="b1_4" id="b1_4v1" type="radio" value="1"><label for="b1_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                @endif
                <div class="card-body">
                    <h6>B1.5</h6>
                    <label>
                        Penjelasan petugas penjualan mengenai bengkel resmi Honda tempat penjualan motor tersebut atau (AHASS) terdekat, jadwal servis sepeda motor dan garansi pembelian
                    </label>
                    <br>
                    <input required name="b1_5" id="b1_5v5" type="radio" value="5"><label for="b1_5v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_5" id="b1_5v4" type="radio" value="4"><label for="b1_5v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_5" id="b1_5v3" type="radio" value="3"><label for="b1_5v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_5" id="b1_5v2" type="radio" value="2"><label for="b1_5v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_5" id="b1_5v1" type="radio" value="1"><label for="b1_5v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                @if($dataDealer->id_layer != 2 || $motors==true)
                <div class="card-body">
                    <h6>B1.5a</h6>
                    <label>
                        Penjelasan petugas penjualan mengenai bengkel resmi Honda tempat penjualan motor tersebut atau (AHASS) terdekat, jadwal servis sepeda motor, garansi pembelian dan mengenalkan kepada petugas bengkel
                    </label>
                    <br>
                    <input required name="b1_5a" id="b1_5av5" type="radio" value="5"><label for="b1_5av5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_5a" id="b1_5av4" type="radio" value="4"><label for="b1_5av4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_5a" id="b1_5av3" type="radio" value="3"><label for="b1_5av3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_5a" id="b1_5av2" type="radio" value="2"><label for="b1_5av2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_5a" id="b1_5av1" type="radio" value="1"><label for="b1_5av1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                @endif
                @if($dataDealer->id_layer != 2)
                <div class="card-body">
                    <h6>B3</h6>
                    <label>
                        Ketika Anda masuk ke dalam ruang showroom/dealer resmi Honda, apakah ada petugas yang datang menghampiri Anda?
                    </label>
                    <br>
                    <input required name="b3" id="b3v1" type="radio" value="Ya"><label for="b3v1">&nbsp;Ya</label>
                    <br>
                    <input required name="b3" id="b3v2" type="radio" value="Tidak"><label for="b3v2">&nbsp;Tidak</label>
                </div>
                @endif
                @if($dataDealer->id_layer == 2)
                <div class="card-body">
                    <h6>B4</h6>
                    <label>
                        Ketika Anda masuk ke dalam ruang showroom/dealer resmi Honda, apakah ada petugas yang datang menghampiri Anda?
                    </label>
                    <br>
                    <input required name="b4" id="b4v1" type="radio" value="Ya"><label for="b4v1">&nbsp;Ya</label>
                    <br>
                    <input required name="b4" id="b4v2" type="radio" value="Tidak"><label for="b4v2">&nbsp;Tidak</label>
                </div>
                @endif
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.penjualan.page.2',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection