@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('store.page.2') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header">
                    INTERVIEWER
                </div>
                <div class="card-body">
                    Selanjutnya, kami meminta Anda menjawab pertanyaan berikut berdasarkan pengalaman pada saat Anda membeli motor.
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> A. SHOWROOM/DEALER RESMI HONDA </b>
                </div>
                @if($dataDealer->id_layer != 2)
                <div class="card-body">
                    <h6>A1</h6>
                    <label>
                        Apakah Anda membawa kendaraan pada saat berkunjung ke showroom/dealer resmi <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input required name="a1" id="a1v1" type="radio" value="Ya"><label for="a1v1">&nbsp;Ya</label>
                    <br>
                    <input required name="a1" id="a1v2" type="radio" value="Tidak"><label for="a1v2">&nbsp;Tidak</label>
                </div>
                @endif
                @if($dataDealer->id_layer != 2)
                <div class="card-body a1" id="OpenYa" style="display: none;">
                    <h6>A2</h6>
                    <label>
                        Apakah ada petugas keamanan (satpam) yang berjaga di pintu masuk area showroom/dealer resmi <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input name="a2" id="a2v1" type="radio" value="Ya"><label for="a2v1">&nbsp;Ya</label>
                    <br>
                    <input name="a2" id="a2v2" type="radio" value="Tidak"><label for="a2v2">&nbsp;Tidak</label>
                </div>
                @endif
                <div class="card-body">
                    <h6>A3.1</h6>
                    <label>
                        Kemudahan dalam menemukan dan menghubungi showroom/dealer resmi Honda (lokasi, telepon, jam kerja)
                    </label>
                    <br>
                    <input required name="a3_1" id="a3_1v5" type="radio" value="5"><label for="a3_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_1" id="a3_1v4" type="radio" value="4"><label for="a3_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_1" id="a3_1v3" type="radio" value="3"><label for="a3_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_1" id="a3_1v2" type="radio" value="2"><label for="a3_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_1" id="a3_1v1" type="radio" value="1"><label for="a3_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A3.2</h6>
                    <label>
                        Bantuan petugas keamanaan untuk mengarahkan ke lokasi parkir yang tersedia
                    </label>
                    <br>
                    <input required name="a3_2" id="a3_2v5" type="radio" value="5"><label for="a3_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_2" id="a3_2v4" type="radio" value="4"><label for="a3_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_2" id="a3_2v3" type="radio" value="3"><label for="a3_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_2" id="a3_2v2" type="radio" value="2"><label for="a3_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_2" id="a3_2v1" type="radio" value="1"><label for="a3_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A3.3</h6>
                    <label>
                        Kebersihan dan kerapian di dalam showroom/dealer resmi Honda
                    </label>
                    <br>
                    <input required name="a3_3" id="a3_3v5" type="radio" value="5"><label for="a3_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_3" id="a3_3v4" type="radio" value="4"><label for="a3_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_3" id="a3_3v3" type="radio" value="3"><label for="a3_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_3" id="a3_3v2" type="radio" value="2"><label for="a3_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_3" id="a3_3v1" type="radio" value="1"><label for="a3_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A3.4</h6>
                    <label>
                        Ketersediaan informasi produk (brosur, daftar harga, leaflet, spanduk)
                    </label>
                    <br>
                    <input required name="a3_4" id="a3_4v5" type="radio" value="5"><label for="a3_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_4" id="a3_4v4" type="radio" value="4"><label for="a3_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_4" id="a3_4v3" type="radio" value="3"><label for="a3_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_4" id="a3_4v2" type="radio" value="2"><label for="a3_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_4" id="a3_4v1" type="radio" value="1"><label for="a3_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.penjualan.page.1',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection