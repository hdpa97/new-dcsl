@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('store.page.5') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> H. PENGIRIMAN DAN PENYERAHAN SEPEDA MOTOR </b>
                </div>
                <div class="card-body">
                    <h6>H1.1</h6>
                    <label>
                        Ketepatan waktu pengiriman/ penyerahan sepeda motor sesuai dengan yang dijanjikan
                    </label>
                    <br>
                    <input required name="h1_1" id="h1_1v5" type="radio" value="5"><label for="h1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="h1_1" id="h1_1v4" type="radio" value="4"><label for="h1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="h1_1" id="h1_1v3" type="radio" value="3"><label for="h1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="h1_1" id="h1_1v2" type="radio" value="2"><label for="h1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="h1_1" id="h1_1v1" type="radio" value="1"><label for="h1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>H1.2</h6>
                    <label>
                        Kesesuaian antara pesanan dengan sepeda motor yang dikirim baik dari tipe dan warnanya
                    </label>
                    <br>
                    <input required name="h1_2" id="h1_2v5" type="radio" value="5"><label for="h1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="h1_2" id="h1_2v4" type="radio" value="4"><label for="h1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="h1_2" id="h1_2v3" type="radio" value="3"><label for="h1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="h1_2" id="h1_2v2" type="radio" value="2"><label for="h1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="h1_2" id="h1_2v1" type="radio" value="1"><label for="h1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>H1.3</h6>
                    <label>
                        Kelengkapan dan kebersihan sepeda motor yang dikirim
                    </label>
                    <br>
                    <input required name="h1_3" id="h1_3v5" type="radio" value="5"><label for="h1_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="h1_3" id="h1_3v4" type="radio" value="4"><label for="h1_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="h1_3" id="h1_3v3" type="radio" value="3"><label for="h1_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="h1_3" id="h1_3v2" type="radio" value="2"><label for="h1_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="h1_3" id="h1_3v1" type="radio" value="1"><label for="h1_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>H1.4</h6>
                    <label>
                        Kualitas sepeda motor pada saat diterima oleh konsumen (tidak lecet, tidak berkarat, dll)
                    </label>
                    <br>
                    <input required name="h1_4" id="h1_4v5" type="radio" value="5"><label for="h1_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="h1_4" id="h1_4v4" type="radio" value="4"><label for="h1_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="h1_4" id="h1_4v3" type="radio" value="3"><label for="h1_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="h1_4" id="h1_4v2" type="radio" value="2"><label for="h1_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="h1_4" id="h1_4v1" type="radio" value="1"><label for="h1_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>H1.5</h6>
                    <label>
                        Kemampuan petugas pengiriman dalam menjelaskan dan memperagakan fungsi fitur sepeda motor,garansi motor (kelistrikan, mesin dan rangka),buku servis berkala dan buku pedoman perawatan motor.
                    </label>
                    <br>
                    <input required name="h1_5" id="h1_5v5" type="radio" value="5"><label for="h1_5v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="h1_5" id="h1_5v4" type="radio" value="4"><label for="h1_5v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="h1_5" id="h1_5v3" type="radio" value="3"><label for="h1_5v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="h1_5" id="h1_5v2" type="radio" value="2"><label for="h1_5v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="h1_5" id="h1_5v1" type="radio" value="1"><label for="h1_5v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>H1.5a</h6>
                    <label>
                        Apakah petugas pengantaran memberikan kartu/surat apresiasi dari dealer kepada anda?
                    </label>
                    <br>
                    <input required name="h1_5a" id="h1_5av1" type="radio" value="Ya"><label for="h1_5av1">&nbsp;Ya</label>
                    <br>
                    <input required name="h1_5a" id="h1_5av2" type="radio" value="Tidak"><label for="h1_5av2">&nbsp;Tidak</label>
                </div>
                @if($dataDealer->id_layer != 2 || $motors==true)
                <div class="card-body h1_5a" id="OpenTidak" style="display: none;">
                    <h6>H1.5b</h6>
                    <label>
                        Apakah yang mengantarkan motor adalah petugas penjualan yang melayani Anda pada saat pembelian?
                    </label>
                    <br>
                    <input name="h1_5b" id="h1_5bv1" type="radio" value="Ya"><label for="h1_5bv1">&nbsp;Ya</label>
                    <br>
                    <input name="h1_5b" id="h1_5bv2" type="radio" value="Tidak"><label for="h1_5bv2">&nbsp;Tidak</label>
                </div>
                <div class="card-body h1_5b" id="BukaYa" style="display: none;">
                    <h6>H1.5c</h6>
                    <label>
                        Apakah petugas menawarkan Anda untuk foto bersama motor dan menyampaikan hasil foto akan digunakan sebagai keperluan promosi?
                    </label>
                    <br>
                    <input name="h1_5c" id="h1_5cv1" type="radio" value="Ya"><label for="h1_5cv1">&nbsp;Ya</label>
                    <br>
                    <input name="h1_5c" id="h1_5cv2" type="radio" value="Tidak"><label for="h1_5cv2">&nbsp;Tidak</label>
                </div>
                @endif
                <div class="card-body h1_5" id="Check" style="display: none;">
                    <h6>H1.5d</h6>
                    <label>
                        <b>ALASAN TIDAK PUAS TERHADAPKEMAMPUAN PETUGAS PENGIRIMAN DALAM MENJELASKAN DAN MEMPERAGAKAN FUNGSI FITUR SEPEDA MOTOR,
                            BUKU TERKAIT GARANSI SERVIS, BUKU PEDOMAN PERAWATAN MOTOR</b>
                        <br>
                        Anda mengatakan bahwa anda tidak puas terhadapkemampuan petugas dealer Honda <b>{{$dataDealer->nama_dealer}}</b> pada saat pengiriman dalam menjelaskan dan
                        memperagakan fungsi fitur sepeda motor, buku terkait garansi servis, buku pedoman perawatan motor.
                        Mohon katakan kepada kami bagian penjelasan manakah yang anda merasa tidak puas dari petugas di dealer Honda <b>{{$dataDealer->nama_dealer}}</b> pada saat pengiriman?
                    </label>
                    <br>
                    <input name="h1_5d" id="h1_5dv1" type="radio" value="Penjelasan dan peragaan fungsi fitur sepeda motor"><label for="h1_5dv1">&nbsp;Penjelasan dan peragaan fungsi fitur sepeda motor</label>
                    <br>
                    <input name="h1_5d" id="h1_5dv2" type="radio" value="Penjelasan mengenai buku terkait garansi servis"><label for="h1_5dv2">&nbsp;Penjelasan mengenai buku terkait garansi servis</label>
                    <br>
                    <input name="h1_5d" id="h1_5dv3" type="radio" value="Penjelasan mengenaibuku pedoman perawatan motor"><label for="h1_5dv3">&nbsp;Penjelasan mengenaibuku pedoman perawatan motor</label>
                    <br>
                    <input name="h1_5d" id="h1_5dv4" type="radio" value="Lainnya"><label for="h1_5dv4">&nbsp;Lainnya, sebutkan</label>
                    <input type="text" name="h1_5d_note">
                </div>
                <div class="card-body h1_5a" id="OpenYa" style="display: none;">
                    <h6>H1.6</h6>
                    <label>
                        Apresiasi yang dealer berikan pada saat pengiriman/penyerahan motor
                    </label>
                    <br>
                    <input name="h1_6" id="h1_6v5" type="radio" value="5"><label for="h1_6v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="h1_6" id="h1_6v4" type="radio" value="4"><label for="h1_6v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="h1_6" id="h1_6v3" type="radio" value="3"><label for="h1_6v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="h1_6" id="h1_6v2" type="radio" value="2"><label for="h1_6v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="h1_6" id="h1_6v1" type="radio" value="1"><label for="h1_6v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>H2</h6>
                    <label>
                        Apakah petugas pengantar memberikan peragaan penggunaan dari sepeda motor ini (seperti lampu, klakson, dll) pada saat penyerahan?
                    </label>
                    <br>
                    <input required name="h2" id="h2v1" type="radio" value="Ya"><label for="h2v1">&nbsp;Ya</label>
                    <br>
                    <input required name="h2" id="h2v2" type="radio" value="Tidak"><label for="h2v2">&nbsp;Tidak</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.penjualan.page.4',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection