@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.4') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> D. KEMAMPUAN ANALISA PETUGAS BENGKEL RESMI HONDA (AHASS) </b>
                </div>
                <div class="card-body">
                    <h6>D1</h6>
                    <label>
                        Apakah petugas bengkel melakukan pemeriksaan pada motor anda?
                    </label>
                    <br>
                    <input required name="d1" id="d1v1" type="radio" value="Ya"><label for="d1v1">&nbsp;Ya</label>
                    <br>
                    <input required name="d1" id="d1v2" type="radio" value="Tidak"><label for="d1v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>D2</h6>
                    <label>
                        Apakah petugas bengkel mengkonfirmasi ulang keluhan yang anda sampaikan?
                    </label>
                    <br>
                    <input required name="d2" id="d2v1" type="radio" value="Ya"><label for="d2v1">&nbsp;Ya</label>
                    <br>
                    <input required name="d2" id="d2v2" type="radio" value="Tidak"><label for="d2v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>D3.1</h6>
                    <label>
                        Kemampuan petugas bengkel dalam mendengarkan, memahami dan mengkonfirmasi keluhan Anda
                    </label>
                    <br>
                    <input required name="d3_1" id="d3_1v5" type="radio" value="5"><label for="d3_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="d3_1" id="d3_1v4" type="radio" value="4"><label for="d3_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="d3_1" id="d3_1v3" type="radio" value="3"><label for="d3_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="d3_1" id="d3_1v2" type="radio" value="2"><label for="d3_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="d3_1" id="d3_1v1" type="radio" value="1"><label for="d3_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>D3.2</h6>
                    <label>
                        Kemampuan petugas bengkel dalam menjelaskan permasalahan sepeda motor kepada Anda
                    </label>
                    <br>
                    <input required name="d3_2" id="d3_2v5" type="radio" value="5"><label for="d3_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="d3_2" id="d3_2v4" type="radio" value="4"><label for="d3_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="d3_2" id="d3_2v3" type="radio" value="3"><label for="d3_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="d3_2" id="d3_2v2" type="radio" value="2"><label for="d3_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="d3_2" id="d3_2v1" type="radio" value="1"><label for="d3_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>D3.3</h6>
                    <label>
                        Kemampuan petugas bengkel dalam menentukan penyebab dan solusi permasalahan motor anda
                    </label>
                    <br>
                    <input required name="d3_3" id="d3_3v5" type="radio" value="5"><label for="d3_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="d3_3" id="d3_3v4" type="radio" value="4"><label for="d3_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="d3_3" id="d3_3v3" type="radio" value="3"><label for="d3_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="d3_3" id="d3_3v2" type="radio" value="2"><label for="d3_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="d3_3" id="d3_3v1" type="radio" value="1"><label for="d3_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                @if($dataDealer->id_layer != 2 || $motors==true)
                <div class="card-body">
                    <h6>D3.3a</h6>
                    <label>
                        Apakah petugas bengkel menjelaskan informasi mengenai aksesoris dan apparel (baju/jaket) sepeda motor honda?
                    </label>
                    <br>
                    <input required name="d3_3a" id="d3_3av1" type="radio" value="Ya"><label for="d3_3av1">&nbsp;Ya</label>
                    <br>
                    <input required name="d3_3a" id="d3_3av2" type="radio" value="Tidak"><label for="d3_3av2">&nbsp;Tidak</label>
                </div>
                <div class="card-body d3_3a" id="BukaYa" style="display: none;">
                    <h6>D4</h6>
                    <label>
                        Kemampuan petugas bengkel dalam menjelaskan informasi mengenai aksesoris dan apparel (baju/jaket) sepeda motor honda
                    </label>
                    <br>
                    <input name="d4" id="d4v5" type="radio" value="5"><label for="d4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="d4" id="d4v4" type="radio" value="4"><label for="d4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="d4" id="d4v3" type="radio" value="3"><label for="d4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="d4" id="d4v2" type="radio" value="2"><label for="d4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="d4" id="d4v1" type="radio" value="1"><label for="d4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>D5</h6>
                    <label>
                        Apakah petugas bengkel menjelaskan part untuk modifikasi sepeda motor Honda dan informasi terkait gugurnya garansi apabila memasang part modifikasi
                    </label>
                    <br>
                    <input required name="d5" id="d5v1" type="radio" value="Ya"><label for="d5v1">&nbsp;Ya</label>
                    <br>
                    <input required name="d5" id="d5v2" type="radio" value="Tidak"><label for="d5v2">&nbsp;Tidak</label>
                </div>
                @endif
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.pemeliharaan.page.3',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection