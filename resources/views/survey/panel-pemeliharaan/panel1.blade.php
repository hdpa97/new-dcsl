@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> PANEL PEMELIHARAAN </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.1') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header">
                    DATA RESPONDEN
                </div>
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-3">{{ __('NAMA DEALER') }}</label>
                        <div class="col-sm-8">
                            {{$dataDealer->nama_dealer }} ({{$dataDealer->kode_dealer}})
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3">{{ __('LAYER DEALER') }}</label>
                        <div class="col-sm-6">
                            @if($dataDealer->id_layer == 1)
                            BIG WING
                            @elseif($dataDealer->id_layer == 2)
                            REGULER
                            @elseif($dataDealer->id_layer == 3)
                            WING
                            @else
                            WING SATELITE
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3">{{ __('NAMA RESPONDEN') }}</label>
                        <div class="col-sm-6">
                            {{$datas->nama_lengkap}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3">{{ __('TYPE SEPEDA MOTOR') }}</label>
                        <div class="col-sm-6">
                            {{$datas->tipe}}
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3">{{ __('JENIS PEMBELIAN') }}</label>
                        <div class="col-sm-6">
                            @if($datas->cash_credit == 1)
                            Cash
                            @else
                            Credit
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-3">{{ __('STATUS') }}</label>
                        <div class="col-sm-4">
                            <select required class="custom-select" name="status_survey">
                                <option disabled selected value="">- Pilih</option>
                                <option value="Terhubung">Terhubung</option>
                                <option value="Terhubung Tapi Pernah Survey">Terhubung Tapi Pernah Survey</option>
                                <option value="Terhubung Konsumen Tapi Tidak Sesuai Data">Terhubung Konsumen Tapi Tidak Sesuai Data</option>
                                <option value="Tidak Diangkat">Tidak Diangkat</option>
                                <option value="Tidak Aktif/Mailbox">Tidak Aktif/Mailbox</option>
                                <option value="Di Luar Jangkauan">Di Luar Jangkauan</option>
                                <option value="No Telp Salah">No Telp Salah</option>
                                <option value="Telp Dialihkan">Telp Dialihkan</option>
                                <option value="No Telp Sibuk">No Telp Sibuk</option>
                                <option value="Reject">Reject</option>
                                <option value="Telp Diangkat Tapi Ditolak Konsumen">Telp Diangkat Tapi Ditolak Konsumen</option>
                                <option value="Salah Sambung">Salah Sambung</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card shadow mb-4">
                <div class="card-header">
                    PERKENALAN
                </div>
                <div class="card-body" style="text-align: justify;">&emsp;&emsp;
                    Selamat pagi / siang / sore, perkenalkan (Bapak/Ibu) saya <b>{{ Auth::user()->name }}</b>, petugas wawancara dari kantor pusat Astra Motor Yogyakarta.
                    Benar ini dengan Bapak/Ibu <b>{{$datas->nama_lengkap}}</b>.
                    Sebelumnya Bapak/Ibu pernah membeli sepeda motor Honda di dealer <b>{{$dataDealer->nama_dealer}}</b>, untuk unit sepeda motor <b>{{$datas->tipe}}</b>.
                    Boleh kami minta waktunya sebentar sekitar 10 menit untuk mengetahui tingkat kepuasan bapak/ibu.
                    Sebelumnya, terima kasih sekali untuk kerjasamanya.
                </div>
            </div>

            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">
            <button type="submit" class="btn btn-primary btn-block">
                {{ __('LANJUTKAN') }}
            </button>
        </div>
    </form>
</div>

@endsection