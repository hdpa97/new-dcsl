@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> SCREENING SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.2') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                @if($dataDealer->id_layer != 2)
                <div class="card-header text-center">
                    <b>A. AKSES KE BENGKEL RESMI HONDA (AHASS)</b>
                </div>
                <div class="card-body">
                    <h6>A1</h6>
                    <label>
                        Apakah ada petugas keamanan (satpam) yang berjaga di pintu masuk area AHASS <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input required name="a1" id="a1v1" type="radio" value="Ya"><label for="a1v1">&nbsp;Ya</label>
                    <br>
                    <input required name="a1" id="a1v2" type="radio" value="Tidak"><label for="a1v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body a1" id="OpenYa" style="display: none;">
                    <h6>A2</h6>
                    <label>
                        Apakah petugas keamanan (satpam) memberikan nomer antrian pada saat Anda masuk area AHASS <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input name="a2" id="a2v1" type="radio" value="Ya"><label for="a2v1">&nbsp;Ya</label>
                    <br>
                    <input name="a2" id="a2v2" type="radio" value="Tidak"><label for="a2v2">&nbsp;Tidak</label>
                </div>
                @endif
                <div class="card-body">
                    <h6>A3.1</h6>
                    <label>
                        Kemudahan menemukan bengkel resmi Honda
                    </label>
                    <br>
                    <input required name="a3_1" id="a3_1v5" type="radio" value="5"><label for="a3_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_1" id="a3_1v4" type="radio" value="4"><label for="a3_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_1" id="a3_1v3" type="radio" value="3"><label for="a3_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_1" id="a3_1v2" type="radio" value="2"><label for="a3_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_1" id="a3_1v1" type="radio" value="1"><label for="a3_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A3.2</h6>
                    <label>
                        Bantuan petugas keamanaan untuk mengarahkan ke area parkir untuk servis dan memberikan nomer antrian
                    </label>
                    <br>
                    <input required name="a3_2" id="a3_2v5" type="radio" value="5"><label for="a3_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_2" id="a3_2v4" type="radio" value="4"><label for="a3_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_2" id="a3_2v3" type="radio" value="3"><label for="a3_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_2" id="a3_2v2" type="radio" value="2"><label for="a3_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_2" id="a3_2v1" type="radio" value="1"><label for="a3_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A3.3</h6>
                    <label>
                        Informasi jam kerja bengkel resmi Honda
                    </label>
                    <br>
                    <input required name="a3_3" id="a3_3v5" type="radio" value="5"><label for="a3_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="a3_3" id="a3_3v4" type="radio" value="4"><label for="a3_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="a3_3" id="a3_3v3" type="radio" value="3"><label for="a3_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="a3_3" id="a3_3v2" type="radio" value="2"><label for="a3_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="a3_3" id="a3_3v1" type="radio" value="1"><label for="a3_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>A3.3a</h6>
                    <label>
                        Apakah anda mengetahui nomor telepon AHASS <b>{{$dataDealer->nama_dealer}}</b> ini?
                    </label>
                    <br>
                    <input required name="a3_3a" id="a3_3av1" type="radio" value="Ya"><label for="a3_3av1">&nbsp;Ya</label>
                    <br>
                    <input required name="a3_3a" id="a3_3av2" type="radio" value="Tidak"><label for="a3_3av2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>A3.3b</h6>
                    <label>
                        Apakah anda pernah menghubungi AHASS <b>{{$dataDealer->nama_dealer}}</b> melalui telepon dalam setahun terakhir?
                    </label>
                    <br>
                    <input required name="a3_3b" id="a3_3bv1" type="radio" value="Ya"><label for="a3_3bv1">&nbsp;Ya</label>
                    <br>
                    <input required name="a3_3b" id="a3_3bv2" type="radio" value="Tidak"><label for="a3_3bv2">&nbsp;Tidak</label>
                </div>
                <div class="card-body a3_3" id="Check" style="display: none;">
                    <h6>A3.2b</h6>
                    <label>
                        <b>ALASAN TIDAK PUAS DENGAN JAM KERJA BENGKEL RESMI HONDA
                        </b>
                        <br>
                        Anda mengatakan bahwa anda tidak puas dengan jam kerja bengkel resmi Honda(AHASS) <b>{{$dataDealer->nama_dealer}}</b>.
                        Mohon katakan kepada kami mengapa anda mengatakan tidak puas terhadapjam kerja jaringan / toko/bengkel resmi <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input name="a3_2b" id="a3_2bv1" type="radio" value="Jam bukanya terlalu siang"><label for="a3_2bv1">&nbsp;Jam bukanya terlalu siang</label>
                    <br>
                    <input name="a3_2b" id="a3_2bv2" type="radio" value="Jam tutupnya terlalu awal"><label for="a3_2bv2">&nbsp;Jam tutupnya terlalu awal</label>
                    <br>
                    <input name="a3_2b" id="a3_2bv3" type="radio" value="Jam operasional toko tersebut tidak konsisten"><label for="a3_2bv3">&nbsp;Jam operasional toko tersebut tidak konsisten</label>
                    <br>
                    <input name="a3_2b" id="a3_2bv4" type="radio" value="Lainnya"><label for="a3_2bv4">&nbsp;Lainnya, sebutkan</label>
                    <input type="text" name="a3_2b_note">
                </div>
                <div class="card-body a3_4" id="Open34" style="display: none;">
                    <h6>A3.3c</h6>
                    <label>
                        <b>ALASAN TIDAK PUAS DENGAN KEMUDAHAN MENDAPATKAN INFORMASI MELALUI TELEPON DARIBENGKEL RESMI HONDA
                        </b>
                        <br>
                        Anda mengatakan bahwa anda tidak puas dengan Kemudahan mendapatkan informasi melalui telepon dari bengkel resmi Honda(AHASS) <b>{{$dataDealer->nama_dealer}}</b>.
                        Mohon katakan kepada kami informasi apa yang tidak mudah anda dapatkan dari toko/bengkel resmi <b>{{$dataDealer->nama_dealer}}</b>?
                    </label>
                    <br>
                    <input name="a3_3c" id="a3_3cv1" type="radio" value="Informasi harga suku  cadang"><label for="a3_3cv1">&nbsp;Informasi harga suku cadang</label>
                    <br>
                    <input name="a3_3c" id="a3_3cv2" type="radio" value="Informasi mengenai perkembangan servis kendaraan yang ditinggal"><label for="a3_3cv2">&nbsp;Informasi mengenai perkembangan servis kendaraan yang ditinggal</label>
                    <br>
                    <input name="a3_3c" id="a3_3cv3" type="radio" value="Informasi mengenai booking service"><label for="a3_3cv3">&nbsp;Informasi mengenai booking service</label>
                    <br>
                    <input name="a3_3c" id="a3_3cv4" type="radio" value="Informasi mengenai service reminder"><label for="a3_3cv4">&nbsp;Informasi mengenai service reminder</label>
                    <br>
                    <input name="a3_3c" id="a3_3cv5" type="radio" value="Lainnya"><label for="a3_3cv5">&nbsp;Lainnya, sebutkan</label>
                    <input type="text" name="a3_3c_note">
                </div>
                <div class="card-body a3_3b" id="BukaYa" style="display: none;">
                    <h6>A3.4</h6>
                    <label>
                        Kemudahan mendapatkan informasi melalui telepon (informasi harga suku cadang, perkembangan servis kendaraan yang ditinggal, termasuk mengenai Booking Service)
                    </label>
                    <br>
                    <input name="a3_4" id="a3_4v5" type="radio" value="5"><label for="a3_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="a3_4" id="a3_4v4" type="radio" value="4"><label for="a3_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="a3_4" id="a3_4v3" type="radio" value="3"><label for="a3_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="a3_4" id="a3_4v2" type="radio" value="2"><label for="a3_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="a3_4" id="a3_4v1" type="radio" value="1"><label for="a3_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.pemeliharaan.page.1',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
