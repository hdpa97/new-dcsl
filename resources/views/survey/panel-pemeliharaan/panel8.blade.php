@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.8') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> N. RETENTION, RECOMMENDATION & OWNERSHIP </b>
                </div>
                <div class="card-body">
                    <h6>N1</h6>
                    <label>
                        Bagaimanakah tingkat kemungkinan Anda untuk tetap servis di bengkel resmi Honda (AHASS) lagi di masa yang akan datang?
                    </label>
                    <br>
                    <input required name="n1" id="n1v5" type="radio" value="5"><label for="n1v5">&nbsp;(5) Sangat Mungkin Service Lagi</label>
                    <br>
                    <input required name="n1" id="n1v4" type="radio" value="4"><label for="n1v4">&nbsp;(4) Mungkin Service Lagi</label>
                    <br>
                    <input required name="n1" id="n1v3" type="radio" value="3"><label for="n1v3">&nbsp;(3) Antara Mungkin dan Tidak Service Lagi</label>
                    <br>
                    <input required name="n1" id="n1v2" type="radio" value="2"><label for="n1v2">&nbsp;(2) Tidak Mungkin Service Lagi</label>
                    <br>
                    <input required name="n1" id="n1v1" type="radio" value="1"><label for="n1v1">&nbsp;(1) Sangat Tidak Mungkin Service Lagi</label>
                </div>
                <div class="card-body">
                    <h6>N2</h6>
                    <label>
                        Bagaimanakan tingkat kemungkinan Anda untuk membeli sepeda motor baru lagi di masa yang akan datang?
                    </label>
                    <br>
                    <input required name="n2" id="n2v5" type="radio" value="5"><label for="n2v5">&nbsp;(5) Sangat Mungkin Membeli</label>
                    <br>
                    <input required name="n2" id="n2v4" type="radio" value="4"><label for="n2v4">&nbsp;(4) Mungkin Membeli</label>
                    <br>
                    <input required name="n2" id="n2v3" type="radio" value="3"><label for="n2v3">&nbsp;(3) Antara Mungkin dan Tidak Membeli</label>
                    <br>
                    <input required name="n2" id="n2v2" type="radio" value="2"><label for="n2v2">&nbsp;(2) Tidak Mungkin Membeli</label>
                    <br>
                    <input required name="n2" id="n2v1" type="radio" value="1"><label for="n2v1">&nbsp;(1) Sangat Tidak Mungkin Membeli</label>
                </div>
                <div class="n2" id="CheckNew" style="display: none;">
                    <div class="card-body">
                        <h6>N3</h6>
                        <label>
                            Kapan rencana pembelian sepeda motor tersebut?
                        </label>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Bulan : </label>
                            <div class="col-md-2">
                                <select name="n3_month" class="custom-select">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach(range(1,12) as $month)

                                    <option value="{{$month}}">
                                        {{date("M", strtotime('2020-'.$month))}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-1 col-form-label">Tahun : </label>
                            <div class="col-md-2">
                                <select name="n3_years" class="custom-select">
                                    @php
                                        $firstYear = (int)date('Y');
                                        $lastYear = $firstYear + 5;
                                    @endphp
                                    <option disabled selected value="">- Pilih</option>
                                    @for($i=$firstYear;$i<=$lastYear;$i++)
                                        <option value="{{$i}}">
                                            {{$i}}
                                        </option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <label>Notes: (Tuliskan detil kapan mau beli, misal 2 minggu lagi.)</label>
                        <div class="row col-md-4">
                            <textarea class="form-control" rows="4" , cols="90" id="#" name="n3_note" style="resize:none"></textarea>
                        </div>
                    </div>
                    <div class="card-body">
                        <h6>N4</h6>
                        <label>
                            Tipe motor apakah yang ingin Anda beli tersebut?
                        </label>
                        <br>
                        <input name="n4" id="n4v1" type="radio" value="Cub/Bebek"><label for="n4v1">&nbsp;Cub/Bebek</label>
                        <br>
                        <input name="n4" id="n4v2" type="radio" value="Sport"><label for="n4v2">&nbsp;Sport</label>
                        <br>
                        <input name="n4" id="n4v3" type="radio" value="Matic"><label for="n4v3">&nbsp;Matic</label>
                        <br>
                        <input name="n4" id="n4v4" type="radio" value="Lainnya"><label for="n4v4">&nbsp;Lainnya, sebutkan</label>
                        <input type="text" name="n4_note">
                    </div>
                    <div class="card-body">
                        <h6>N5</h6>
                        <label>
                            Untuk siapa sepeda motor tersebut?
                        </label>
                        <br>
                        <input name="n5" id="n5v1" type="radio" value="Saya Sendiri"><label for="n5v1">&nbsp;Saya Sendiri</label>
                        <br>
                        <input name="n5" id="n5v2" type="radio" value="Anak"><label for="n5v2">&nbsp;Anak</label>
                        <br>
                        <input name="n5" id="n5v3" type="radio" value="Pasangan(Suami/Istri)"><label for="n5v3">&nbsp;Pasangan(Suami/Istri)</label>
                        <br>
                        <input name="n5" id="n5v4" type="radio" value="Lainnya"><label for="n5v4">&nbsp;Lainnya, sebutkan</label>
                        <input type="text" name="n5_note">
                    </div>
                    <div class="card-body">
                        <h6>N6</h6>
                        <label>
                            Merek motor apakah yang ingin Anda beli tersebut?
                        </label>
                        <br>
                        <input name="n6" id="n6v1" type="radio" value="Honda"><label for="n6v1">&nbsp;Honda</label>
                        <br>
                        <input name="n6" id="n6v2" type="radio" value="Yamaha"><label for="n6v2">&nbsp;Yamaha</label>
                        <br>
                        <input name="n6" id="n6v3" type="radio" value="Suzuki"><label for="n6v3">&nbsp;Suzuki</label>
                        <br>
                        <input name="n6" id="n6v4" type="radio" value="Kawasaki"><label for="n6v4">&nbsp;Kawasaki</label>
                        <br>
                        <input name="n6" id="n6v5" type="radio" value="Lainnya"><label for="n6v5">&nbsp;Lainnya, sebutkan</label>
                        <input type="text" name="n6_note">
                    </div>
                    <div class="card-body n6" id="OpenSeri" style="display: none;">
                        <h6>N7</h6>
                        <label>
                            Type series apa yang anda inginkan?
                        </label>
                        <br>
                        <label>Jawab </label>
                        <input type="text" name="n7_note">
                    </div>
                </div>
                <div class="card-body">
                    <h6>N8</h6>
                    <label>
                        Dalam skala 0 sampai 10, seberapa besar kemungkinan Anda akan merekomendasikan sepeda motor merek HONDA kepada orang lain dimana 0 pasti tidak akan merekomendasikan dan 10 pasti akan merekomendasikan?
                    </label>
                    <br>
                    <input required name="n8" id="n8v1" type="radio" value="0"><label for="n8v1">&nbsp;0</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v2" type="radio" value="1"><label for="n8v2">&nbsp;1</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v3" type="radio" value="2"><label for="n8v3">&nbsp;2</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v4" type="radio" value="3"><label for="n8v4">&nbsp;3</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v5" type="radio" value="4"><label for="n8v5">&nbsp;4</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v6" type="radio" value="5"><label for="n8v6">&nbsp;5</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v7" type="radio" value="6"><label for="n8v7">&nbsp;6</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v8" type="radio" value="7"><label for="n8v8">&nbsp;7</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v9" type="radio" value="8"><label for="n8v9">&nbsp;8</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v10" type="radio" value="9"><label for="n8v10">&nbsp;9</label>&nbsp;&nbsp;&nbsp;
                    <input required name="n8" id="n8v11" type="radio" value="10"><label for="n8v11">&nbsp;10</label>
                </div>
                <div class="card-header text-center">
                    <b> WAWANCARA SELESAI & UCAPKAN TERIMA KASIH KEPADA RESPONDEN </b>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.pemeliharaan.page.7',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('SUBMIT') }} &nbsp;<i class="fas fa-save"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
