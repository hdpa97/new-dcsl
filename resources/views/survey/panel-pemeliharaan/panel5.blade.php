@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.5') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> E. WAKTU MENUNGGU </b>
                </div>
                <div class="card-body">
                    <h6>E1.1</h6>
                    <label>
                        Waktu menunggu dari Anda datang ke bengkel sampai dengan Anda dilayani pertama kali oleh petugas
                    </label>
                    <br>
                    <input required name="e1_1" id="e1_1v5" type="radio" value="5"><label for="e1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="e1_1" id="e1_1v4" type="radio" value="4"><label for="e1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="e1_1" id="e1_1v3" type="radio" value="3"><label for="e1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="e1_1" id="e1_1v2" type="radio" value="2"><label for="e1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="e1_1" id="e1_1v1" type="radio" value="1"><label for="e1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>E1.1a</h6>
                    <label>
                        Apakah Anda mendapatkan nomer antrian untuk bertemu dengan petugas bengkel?
                    </label>
                    <br>
                    <input required name="e1_1a" id="e1_1av1" type="radio" value="Ya"><label for="e1_1av1">&nbsp;Ya</label>
                    <br>
                    <input required name="e1_1a" id="e1_1av2" type="radio" value="Tidak"><label for="e1_1av2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>E1.2</h6>
                    <label>
                        Kejelasan sistem antrian yang diberikan
                    </label>
                    <br>
                    <input required name="e1_2" id="e1_2v5" type="radio" value="5"><label for="e1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="e1_2" id="e1_2v4" type="radio" value="4"><label for="e1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="e1_2" id="e1_2v3" type="radio" value="3"><label for="e1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="e1_2" id="e1_2v2" type="radio" value="2"><label for="e1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="e1_2" id="e1_2v1" type="radio" value="1"><label for="e1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>E1.3</h6>
                    <label>
                        Waktu menunggu dari Anda selesai mendaftar sampai dengan motor Anda masuk ke dalam pit servis (mulai diservis mekanik)
                    </label>
                    <br>
                    <input required name="e1_3" id="e1_3v5" type="radio" value="5"><label for="e1_3v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="e1_3" id="e1_3v4" type="radio" value="4"><label for="e1_3v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="e1_3" id="e1_3v3" type="radio" value="3"><label for="e1_3v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="e1_3" id="e1_3v2" type="radio" value="2"><label for="e1_3v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="e1_3" id="e1_3v1" type="radio" value="1"><label for="e1_3v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>E1.4</h6>
                    <label>
                        Waktu menunggu dari motor Anda masuk ke dalam pit servis sampai motor Anda selesai dikerjakan
                    </label>
                    <br>
                    <input required name="e1_4" id="e1_4v5" type="radio" value="5"><label for="e1_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="e1_4" id="e1_4v4" type="radio" value="4"><label for="e1_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="e1_4" id="e1_4v3" type="radio" value="3"><label for="e1_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="e1_4" id="e1_4v2" type="radio" value="2"><label for="e1_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="e1_4" id="e1_4v1" type="radio" value="1"><label for="e1_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> G. BIAYA SERVIS </b>
                </div>
                <div class="card-body">
                    <h6>G1</h6>
                    <label>
                        Apakah di bengkel resmi Honda (AHASS) tersebut tersedia daftar harga yang dapat dilihat konsumen untuk setiap jenis jasa?
                    </label>
                    <br>
                    <input required name="g1" id="g1v1" type="radio" value="Ya"><label for="g1v1">&nbsp;Ya</label>
                    <br>
                    <input required name="g1" id="g1v2" type="radio" value="Tidak"><label for="g1v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>G2</h6>
                    <label>
                        Apakah petugas bengkel resmi Honda (AHASS) tersebut memberikan perkiraan biaya servis dan pergantian suku cadang sebelum motor Anda dikerjakan?
                    </label>
                    <br>
                    <input required name="g2" id="g2v1" type="radio" value="Ya"><label for="g2v1">&nbsp;Ya</label>
                    <br>
                    <input required name="g2" id="g2v2" type="radio" value="Tidak"><label for="g2v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>G3</h6>
                    <label>
                        Menurut Anda, seberapa wajar biaya yang dikeluarkan dengan kualitas pekerjaan, jika dinilai dengan kartu bantu berikut?
                    </label>
                    <br>
                    <input required name="g3" id="g3v5" type="radio" value="5"><label for="g3v5">&nbsp;(5) Sangat Wajar</label>
                    <br>
                    <input required name="g3" id="g3v4" type="radio" value="4"><label for="g3v4">&nbsp;(4) Wajar</label>
                    <br>
                    <input required name="g3" id="g3v3" type="radio" value="3"><label for="g3v3">&nbsp;(3) Antara Wajar dan Tidak</label>
                    <br>
                    <input required name="g3" id="g3v2" type="radio" value="2"><label for="g3v2">&nbsp;(2) Tidak Wajar</label>
                    <br>
                    <input required name="g3" id="g3v1" type="radio" value="1"><label for="g3v1">&nbsp;(1) Sangat Tidak Wajar</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.pemeliharaan.page.4',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
