@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.3') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> B. KERAMAHAN DAN PENAMPILAN PETUGAS ADMINISTRASI </b>
                </div>
                <div class="card-body">
                    <h6>B1.1</h6>
                    <label>
                        Penampilan petugas Registrasi bengkel resmi (kerapian, kebersihan seragam)
                    </label>
                    <br>
                    <input required name="b1_1" id="b1_1v5" type="radio" value="5"><label for="b1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v4" type="radio" value="4"><label for="b1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v3" type="radio" value="3"><label for="b1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_1" id="b1_1v2" type="radio" value="2"><label for="b1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_1" id="b1_1v1" type="radio" value="1"><label for="b1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>B1.2</h6>
                    <label>
                        Keramahan petugas Registrasi bengkel resmi (senyum, salam, sapa, sopan santun)
                    </label>
                    <br>
                    <input required name="b1_2" id="b1_2v5" type="radio" value="5"><label for="b1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v4" type="radio" value="4"><label for="b1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v3" type="radio" value="3"><label for="b1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="b1_2" id="b1_2v2" type="radio" value="2"><label for="b1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="b1_2" id="b1_2v1" type="radio" value="1"><label for="b1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> C. FASILITAS DAN JENIS JASA DI BENGKEL RESMI HONDA (AHASS) </b>
                </div>
                <div class="card-body">
                    <h6>C1.1</h6>
                    <label>
                        Kebersihan dan kerapihan bengkel resmi di area kerja mekanik dan area tunggu konsumen
                    </label>
                    <br>
                    <input required name="c1_1" id="c1_1v5" type="radio" value="5"><label for="c1_1v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="c1_1" id="c1_1v4" type="radio" value="4"><label for="c1_1v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="c1_1" id="c1_1v3" type="radio" value="3"><label for="c1_1v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="c1_1" id="c1_1v2" type="radio" value="2"><label for="c1_1v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="c1_1" id="c1_1v1" type="radio" value="1"><label for="c1_1v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>C1.2</h6>
                    <label>
                        Kelengkapan jenis jasa pekerjaan di bengkel resmi (servis ringan/berkala, servis berat, ganti oli, klaim garansi, servis gratis setelah pembelian, penggantian spare part)
                    </label>
                    <br>
                    <input required name="c1_2" id="c1_2v5" type="radio" value="5"><label for="c1_2v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input required name="c1_2" id="c1_2v4" type="radio" value="4"><label for="c1_2v4">&nbsp;(4) Puas</label>
                    <br>
                    <input required name="c1_2" id="c1_2v3" type="radio" value="3"><label for="c1_2v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input required name="c1_2" id="c1_2v2" type="radio" value="2"><label for="c1_2v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input required name="c1_2" id="c1_2v1" type="radio" value="1"><label for="c1_2v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>C1.3</h6>
                    <label>
                        Apakah anda pernah mendapatkan sms/telepon yang mengingatkan jadwal perawatan berkala untuk sepeda Motor Honda anda?
                    </label>
                    <br>
                    <input required name="c1_3" id="c1_3v1" type="radio" value="Ya"><label for="c1_3v1">&nbsp;Ya</label>
                    <br>
                    <input required name="c1_3" id="c1_3v2" type="radio" value="Tidak"><label for="c1_3v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body c1_3" id="OpenYa" style="display: none;">
                    <h6>C1.3a</h6>
                    <label>
                        Fasilitas sms/telepon yang mengingatkan jadwal perawatan berkala
                    </label>
                    <br>
                    <input name="c1_3a" id="c1_3av5" type="radio" value="5"><label for="c1_3av5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="c1_3a" id="c1_3av4" type="radio" value="4"><label for="c1_3av4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="c1_3a" id="c1_3av3" type="radio" value="3"><label for="c1_3av3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="c1_3a" id="c1_3av2" type="radio" value="2"><label for="c1_3av2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="c1_3a" id="c1_3av1" type="radio" value="1"><label for="c1_3av1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body c1_3" id="OpenTidak" style="display: none;">
                    <h6>C1.4</h6>
                    <label>
                        Kenyamanan serta fasilitas yang tersedia saat menunggu (jumlah tempat duduk yang memadai, toilet, ada media hiburan seperti TV, koran dan majalah, air minum dalam kemasan atau lainnya)
                    </label>
                    <br>
                    <input name="c1_4" id="c1_4v5" type="radio" value="5"><label for="c1_4v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="c1_4" id="c1_4v4" type="radio" value="4"><label for="c1_4v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="c1_4" id="c1_4v3" type="radio" value="3"><label for="c1_4v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="c1_4" id="c1_4v2" type="radio" value="2"><label for="c1_4v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="c1_4" id="c1_4v1" type="radio" value="1"><label for="c1_4v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
                <div class="card-body">
                    <h6>C1.4a</h6>
                    <label>
                        Apakah Anda pernah melakukan Booking Service?
                    </label>
                    <br>
                    <input required name="c1_4a" id="c1_4av1" type="radio" value="Ya"><label for="c1_4av1">&nbsp;Ya</label>
                    <br>
                    <input required name="c1_4a" id="c1_4av2" type="radio" value="Tidak"><label for="c1_4av2">&nbsp;Tidak</label>
                </div>

                <div class="c1_2" id="Check" style="display: none;">
                    <div class="collapse show">
                        <div class="card-body">
                            <h6>C1.2a</h6>
                            <label>
                                <b>ALASAN TIDAK PUAS DENGAN KELENGKAPAN JENIS JASA PEKERJAAN DI BENGKEL RESMI HONDA
                                </b>
                                <br>
                                Anda mengatakan bahwa anda tidak puas dengan kelengkapan jenis jasa pekerjaan dibengkel resmi Honda(AHASS) <b>{{$dataDealer->nama_dealer}}</b>.
                                Mohon katakan kepada kami jenis jasa pekerjaan apakah yang tidak tersedia di bengkel resmi Honda <b>{{$dataDealer->nama_dealer}}</b> sehingga anda mengatakan tidak puas dengan kelengkapan jenis jasa pekerjaan nya?
                            </label>
                            <br>
                            <input name="c1_2a" id="c1_2av1" type="radio" value="Servis ringan/berkala"><label for="c1_2av1">&nbsp;Servis ringan/berkala</label>
                            <br>
                            <input name="c1_2a" id="c1_2av2" type="radio" value="Servis berat"><label for="c1_2av2">&nbsp;Servis berat</label>
                            <br>
                            <input name="c1_2a" id="c1_2av3" type="radio" value="Ganti oli"><label for="c1_2av3">&nbsp;Ganti oli</label>
                            <br>
                            <input name="c1_2a" id="c1_2av4" type="radio" value="Klaim garansi"><label for="c1_2av4">&nbsp;Klaim garansi</label>
                            <br>
                            <input name="c1_2a" id="c1_2av5" type="radio" value="Service gratis setelah pembelian"><label for="c1_2av5">&nbsp;Service gratis setelah pembelian</label>
                            <br>
                            <input name="c1_2a" id="c1_2av6" type="radio" value="Lainnya"><label for="c1_2av6">&nbsp;Lainnya, sebutkan</label>
                            <input type="text" name="c1_2a_note">
                        </div>
                        <div class="card-body">
                            <h6>C2</h6>
                            <label>
                                <b>ALASAN TIDAK PUAS DENGAN KELENGKAPAN JENIS JASA PEKERJAAN DI BENGKEL RESMI HONDA
                                </b>
                                <br>
                                Mengapa Anda mengatakan tidak puas terhadap kenyamanan fasilitas yang tersedia saat menunggu di bengkel resmi Honda (AHASS) <b>{{$dataDealer->nama_dealer}}</b>?
                                <br>
                                <b>Probe.</b> Apalagi?
                            </label>
                            <br>
                            <input name="c2" id="c2v1" type="radio" value="Kursi ruang tunggu tidak memiliki sandaran"><label for="c2v1">&nbsp;Kursi ruang tunggu tidak memiliki sandaran</label>
                            <br>
                            <input name="c2" id="c2v2" type="radio" value="Kursi di ruang tunggu sedikit"><label for="c2v2">&nbsp;Kursi di ruang tunggu sedikit</label>
                            <br>
                            <input name="c2" id="c2v3" type="radio" value="Penataan ruang tunggu tidak rapi"><label for="c2v3">&nbsp;Penataan ruang tunggu tidak rapi</label>
                            <br>
                            <input name="c2" id="c2v4" type="radio" value="Ruang tunggu banyak asap rokok"><label for="c2v4">&nbsp;Ruang tunggu banyak asap rokok</label>
                            <br>
                            <input name="c2" id="c2v5" type="radio" value="Ruang tunggu berisik"><label for="c2v5">&nbsp;Ruang tunggu berisik</label>
                            <br>
                            <input name="c2" id="c2v6" type="radio" value="Ruang tunggu digabung dengan pit servis"><label for="c2v6">&nbsp;Ruang tunggu digabung dengan pit servis</label>
                            <br>
                            <input name="c2" id="c2v7" type="radio" value="Ruang tunggu kotor"><label for="c2v7">&nbsp;Ruang tunggu kotor</label>
                            <br>
                            <input name="c2" id="c2v8" type="radio" value="Ruang tunggu sempit"><label for="c2v8">&nbsp;Ruang tunggu sempit</label>
                            <br>
                            <input name="c2" id="c2v9" type="radio" value="Ruang tunggu tidak ada pendingin udara "><label for="c2v9">&nbsp;Ruang tunggu tidak ada pendingin udara </label>
                            <br>
                            <input name="c2" id="c2v10" type="radio" value="Ruang tunggu tidak ada bacaan"><label for="c2v10">&nbsp;Ruang tunggu tidak ada bacaan</label>
                            <br>
                            <input name="c2" id="c2v11" type="radio" value="Ruang tunggu tidak ada TV"><label for="c2v11">&nbsp;Ruang tunggu tidak ada TV</label>
                            <br>
                            <input name="c2" id="c2v12" type="radio" value="Ruang tunggu tidak tersedia minuman"><label for="c2v12">&nbsp;Ruang tunggu tidak tersedia minuman</label>
                            <br>
                            <input name="c2" id="c2v13" type="radio" value="Tidak ada toilet"><label for="c2v13">&nbsp;Tidak ada toilet</label>
                            <br>
                            <input name="c2" id="c2v14" type="radio" value="Toilet Kotor"><label for="c2v14">&nbsp;Toilet Kotor</label>
                            <br>
                            <input name="c2" id="c2v15" type="radio" value="Lainnya"><label for="c2v15">&nbsp;Lainnya, sebutkan</label>
                            <input type="text" name="c2_note">
                        </div>
                        <div class="card-body">
                            <h6>C3</h6>
                            <label>
                                <b>ALASAN TIDAK PUAS DENGAN KELENGKAPAN JENIS JASA PEKERJAAN DI BENGKEL RESMI HONDA
                                </b>
                                <br>
                                Menurut Anda, fasilitas apa lagi yang perlu ada di ruang tunggu bengkel resmi Honda (AHASS) <b>{{$dataDealer->nama_dealer}}</b>
                                sehingga Anda merasa nyaman dan puas saat menunggu sepeda motor yang sedang diservis di bengkel resmi Honda?
                                <br>
                                <b>Probe.</b> Apalagi?
                            </label>
                            <br>
                            <input name="c3" id="c3v1" type="radio" value="AC"><label for="c3v1">&nbsp;AC </label>
                            <br>
                            <input name="c3" id="c3v2" type="radio" value="Kursi di ruang tunggu sedikit"><label for="c3v2">&nbsp;Kursi di ruang tunggu sedikit</label>
                            <br>
                            <input name="c3" id="c3v3" type="radio" value="Koran atau Majalah"><label for="c3v3">&nbsp;Koran atau Majalah</label>
                            <br>
                            <input name="c3" id="c3v4" type="radio" value="Internet atau wifi"><label for="c3v4">&nbsp;Internet atau wifi</label>
                            <br>
                            <input name="c3" id="c3v5" type="radio" value="Colokan Listrik"><label for="c3v5">&nbsp;Colokan Listrik</label>
                            <br>
                            <input name="c3" id="c3v6" type="radio" value="Kopi, teh, minuman berwarna"><label for="c3v6">&nbsp;Kopi, teh, minuman berwarna</label>
                            <br>
                            <input name="c3" id="c3v7" type="radio" value="Televisi"><label for="c3v7">&nbsp;Televisi</label>
                            <br>
                            <input name="c3" id="c3v8" type="radio" value="Lainnya"><label for="c3v8">&nbsp;Lainnya, sebutkan</label>
                            <input type="text" name="c3_note">
                        </div>
                    </div>
                </div>

                <div class="card-body c1_4a" id="IntipYa" style="display: none;">
                    <h6>C1.5</h6>
                    <label>
                        Fasilitas sms/telepon yang mengingatkan untuk datang sesuai dengan waktu booking service
                    </label>
                    <br>
                    <input name="c1_5" id="c1_5v5" type="radio" value="5"><label for="c1_5v5">&nbsp;(5) Sangat Puas</label>
                    <br>
                    <input name="c1_5" id="c1_5v4" type="radio" value="4"><label for="c1_5v4">&nbsp;(4) Puas</label>
                    <br>
                    <input name="c1_5" id="c1_5v3" type="radio" value="3"><label for="c1_5v3">&nbsp;(3) Antara Puas dan Tidak</label>
                    <br>
                    <input name="c1_5" id="c1_5v2" type="radio" value="2"><label for="c1_5v2">&nbsp;(2) Tidak Puas</label>
                    <br>
                    <input name="c1_5" id="c1_5v1" type="radio" value="1"><label for="c1_5v1">&nbsp;(1) Sangat Tidak Puas</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.pemeliharaan.page.2',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection