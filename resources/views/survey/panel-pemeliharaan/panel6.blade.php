@extends('layouts.sidebar')

@section('title-tab')

@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header text-center">
        <b> MAIN SECTION </b>
    </div>
    <form class="needs-validation" method="POST" action="{{ route('storee.page.6') }}" autocomplete="off">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="card shadow mb-4">
                <div class="card-header text-center">
                    <b> H. PENJELASAN AKHIR DARI PETUGAS BENGKEL RESMI (AHASS) </b>
                </div>
                <div class="card-body">
                    <h6>H1</h6>
                    <label>
                        Selanjutnya, kami ingin mengetahui tingkat kepuasan Anda terhadap penjelasan akhir yang disampaikan petugas bengkel resmi Honda (AHASS) <b>{{$dataDealer->nama_dealer}}</b>.
                        Seberapa puaskah Anda dengan penjelasan akhir petugas bengkel resmi Honda mengenai perbaikan yang telah dilakukan,
                        biaya yang terperinci, serta garansi servis sesuai ketentuan ATPM Honda Motor?
                    </label>
                    <br>
                    <input required name="h1" id="h1v5" type="radio" value="5"><label for="h1v5">&nbsp;(5) Sangat Wajar</label>
                    <br>
                    <input required name="h1" id="h1v4" type="radio" value="4"><label for="h1v4">&nbsp;(4) Wajar</label>
                    <br>
                    <input required name="h1" id="h1v3" type="radio" value="3"><label for="h1v3">&nbsp;(3) Antara Wajar dan Tidak</label>
                    <br>
                    <input required name="h1" id="h1v2" type="radio" value="2"><label for="h1v2">&nbsp;(2) Tidak Wajar</label>
                    <br>
                    <input required name="h1" id="h1v1" type="radio" value="1"><label for="h1v1">&nbsp;(1) Sangat Tidak Wajar</label>
                </div>
                <div class="card-body">
                    <h6>H2</h6>
                    <label>
                        Apakah petugas bengkel resmi Honda mengingatkan waktu servis yang berikutnya?
                    </label>
                    <br>
                    <input required name="h2" id="h2v1" type="radio" value="Ya"><label for="h2v1">&nbsp;Ya</label>
                    <br>
                    <input required name="h2" id="h2v2" type="radio" value="Tidak"><label for="h2v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>H3</h6>
                    <label>
                        Apakah saat ini motor <b>{{$datas->tipe}}</b> ini masih dalam masa servis gratis (KPB)?
                    </label>
                    <br>
                    <input required name="h3" id="h3v1" type="radio" value="Ya"><label for="h3v1">&nbsp;Ya</label>
                    <br>
                    <input required name="h3" id="h3v2" type="radio" value="Tidak"><label for="h3v2">&nbsp;Tidak</label>
                </div>
                <div class="card-body">
                    <h6>H4</h6>
                    <label>
                        Apakah pernah melakukan pembelian/penggantian sparepart?
                    </label>
                    <br>
                    <input required name="h4" required id="h4v1" type="radio" value="Ya"><label for="h4v1">&nbsp;Ya</label>
                    <br>
                    <input required name="h4" id="h4v2" type="radio" value="Tidak"><label for="h4v2">&nbsp;Tidak</label>
                </div>
            </div>
            <input type="hidden" name="kodeDealer" value="{{$datas->kode_dealer}}">
            <input type="hidden" name="idData" value="{{$datas->id}}">

            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-info btn-block" href="{{route('panel.pemeliharaan.page.5',['kodeDealer' => $datas->kode_dealer, 'idData' => $datas->id])}}">
                        <i class="fas fa-angle-left"></i>&nbsp; KEMBALI
                    </a>
                </div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ __('LANJUTKAN') }} &nbsp;<i class="fas fa-angle-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
