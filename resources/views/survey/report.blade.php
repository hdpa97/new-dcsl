@extends('layouts.sidebar')

@section('title-tab')
    Survey Selesai
@endsection

@section('breadcrumb')

@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableReportSurvey" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kares</th>
                        <th class="text-center">Kabupaten/Kota</th>
                        <th class="text-center">Dealer</th>
                        <th class="text-center">Jaringan</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">H23</th>
                        {{-- 
                        <th class="text-center">H3</th>
                        --}}
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->nama_kares}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->nama_jaringan}}</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        {{-- 
                            <td class="text-center" @if($data->CT_H1 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>
                            <a href="{{route('detail.h1',['kodeDealer' => $data->kode_dealer])}}"> {{$data->CT_H1}}</a></td>
                            <td class="text-center" @if($data->CT_H2 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>
                            <a href="{{route('detail.h2',['kodeDealer' => $data->kode_dealer])}}"> {{$data->CT_H2}}</a></td>
                            <td class="text-center" @if($data->CT_H3 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>
                            <a href="{{route('detail.h3',['kodeDealer' => $data->kode_dealer])}}"> {{$data->CT_H3}}</a></td>
                        --}}
                        <td class="text-center" @if($data->CT_H1 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>{{$data->CT_H1}}</a></td>
                        <td class="text-center" @if($data->CT_H2 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>{{$data->CT_H2}}</a></td>
                        {{-- 
                        <td class="text-center" @if($data->CT_H3 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>{{$data->CT_H3}}</a></td>
                        --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection