@extends('layouts.sidebar')

@section('title-tab')
Edit Jaringan
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('jaringan.index')}}">Data Jaringan</a></li>
    <li class="breadcrumb-item active">Edit Jaringan</li>
</ol>
@endsection

@section('main-content')
<div class="card-body">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Data') }}</div>

                <div class="card-body">
                    <form class="needs-validation" method="POST" action="{{ route('jaringan.update', ['id' => $data->id]) }}" autocomplete="off">
                        {{method_field('PUT')}}
                        {{ csrf_field() }}

                        <div class="form-group row">
                            <label for="nama_jaringan" class="col-md-3 col-form-label text-md-left">{{ __('Nama Jaringan') }}</label>

                            <div class="col-md-8">
                                <input id="nama_jaringan" required type="text" class="form-control @error('nama_jaringan') is-invalid @enderror" name="nama_jaringan" value="{{  old('nama_jaringan') ?? $data->nama_jaringan}}" required autofocus>

                                @error('nama_jaringan')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="flag" class="col-md-3 col-form-label text-md-left">{{ __('Status') }}</label>

                            <div class="col-md-8 mt-2">
                                <input name="flag" id="f1" required type="radio" value="1" @if($data->flag == 1) checked @endif><label for="f1">&nbsp;Enable</label>
                                &emsp;
                                <input name="flag" id="f2" type="radio" value="0" @if($data->flag == 0) checked @endif><label for="f2">&nbsp;Disable</label>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 mt-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection