@extends('layouts.sidebar')

@section('title-tab')
Cek Data
@endsection
@section('button')

@endsection
@section('breadcrumb')

@endsection

@section('main-content')
<div class="card border-0">
    <div class="row">
        <div class="col-md-4">
            <form id="uploadForm" class="needs-validation pull-left" method="GET" action="{{ route('cek.index') }}">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Kares</td>
                            <td class="text-left">
                                <select name="idKar" id="id_kares" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($karess as $kares)
                                    <option value="{{ $kares->id }}"
                                    @if ($request->get('kar') == $kares->id )
                                        selected="selected"
                                    @endif
                                    > {{ $kares->nama_kares }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Kab. / Kota</td>
                            <td class="text-left">
                                <select name="idKab" id="id_kabupaten" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($kabs as $kabupaten)
                                    <option value="{{ $kabupaten->id }}"
                                    @if ($request->get('kab') == $kabupaten->id )
                                        selected="selected"
                                    @endif
                                    > {{ $kabupaten->nama_kabupaten }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Dealer</td>
                            <td class="text-left">
                                <select name="idDlr" id="id_dealer" class="custom-select" style="width:230px;">
                                    <option disabled selected value="">- Pilih</option>
                                    @foreach($dealers as $dealer)
                                    <option value="{{ $dealer->id }}"
                                    @if ($request->get('dlr') == $dealer->id )
                                        selected="selected"
                                    @endif
                                    > {{ $dealer->nama_dealer }}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </tbody>
                </table>
        </div>
        <div class="col-md-4">
                <table cellpadding="3" cellspacing="0" class="">
                    <tbody>
                        <tr>
                            <td class="text-left">Bulan</td>
                            <td class="text-left">
                                <select name="months[]" class="custom-select2" multiple="multiple" style="width:300px;">
                                @foreach(range(1,12) as $month)
                                <option value="{{$month}}"
                                @if($request->get('months') !== null)
                                    @if (in_array($month, $request->get('months'), FALSE))
                                        selected
                                    @endif
                                @endif
                                >
                                    {{date("M", strtotime('2020-'.$month))}}
                                </option>
                                @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Tahun</td>
                            <td class="text-left">
                                <select name="years[]" class="custom-select2" multiple="multiple" style="width:300px;">
                                    @php
                                        $firstYear = (int)date('Y') - 1;
                                        $lastYear = $firstYear + 6;
                                    @endphp
                                    @for($i=$firstYear;$i<=$lastYear;$i++)
                                        <option value="{{$i}}"
                                        @if($request->get('years') !== null)
                                            @if (in_array($i, $request->get('years'), FALSE))
                                                selected
                                            @endif
                                        @endif
                                        >
                                            {{$i}}
                                        </option>
                                    @endfor
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-left">
                            <button type="submit" class="btn btn-success btn-block submit-button">
                                <i class='fa fa-search'></i>
                                {{ __('Show') }}
                            </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 text-right">
            <a class="" href="{{route('cek.index')}}">
                <button class="btn btn-md btn-danger">
                    <i class="fas fa-times"></i>
                    Reset Field
                </button>
            </a>
        </div>
    </div>
</div>
<div class="card mb-4 mt-3">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableCek" width="100%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Kares</th>
                        <th class="text-center">Kabupaten/Kota</th>
                        <th class="text-center">Dealer</th>
                        <th class="text-center">Kode Dealer</th>
                        <th class="text-center">H1</th>
                        <th class="text-center">H2</th>
                        <th class="text-center">H3</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->nama_kares}}</td>
                        <td class="text-center">{{$data->nama_kabupaten}}</td>
                        <td class="text-center">{{$data->nama_dealer}}</td>
                        <td class="text-center">{{$data->kode_dealer}}</td>
                        <td class="text-center" @if($data->CT_H1 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>
                        <a href="{{route('detail.h1',['kodeDealer' => $data->kode_dealer])}}"> {{$data->CT_H1}}</a></td>
                        <td class="text-center" @if($data->CT_H2 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>
                        <a href="{{route('detail.h2',['kodeDealer' => $data->kode_dealer])}}"> {{$data->CT_H2}}</a></td>
                        <td class="text-center" @if($data->CT_H3 >= 30) style="background-color:#71c765" @else style="background-color:#eba98d" @endif>
                        <a href="{{route('detail.h3',['kodeDealer' => $data->kode_dealer])}}"> {{$data->CT_H3}}</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        document.getElementById("id_kabupaten").disabled = true;
        document.getElementById("id_dealer").disabled = true;

        $('#id_kares').change(function(){
            if($(this).val()!='')
            {
                var idk = $(this).val();
                $.ajax({
                    url:'/cek/fetch_krs/'+idk,
                    type:"GET",
                    dataType:"json",
                    success:function(result)
                    {
                        if(result != "")
                        {
                            document.getElementById("id_kabupaten").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            $('#id_kabupaten').empty();
                            $('#id_kabupaten').append('<option value="" selected>- Pilih</option>');
                            for(var i in result){
                                $('#id_kabupaten').append('<option value="'+result[i].id_kab+'">'+result[i].nama_kab+'</option>');
                            }
                        }
                    }
                });
            }
        });

        $('#id_kabupaten').change(function(){
            if($(this).val()!='')
            {
                var idkab = $(this).val();
                $.ajax({
                    url:'/cek/fetch_kab/'+idkab,
                    type:"GET",
                    dataType:"json",
                    success:function(result)
                    {
                        if(result != "")
                        {
                            document.getElementById("id_dealer").disabled = false;
                            $('#id_dealer').empty();
                            $('#id_dealer').append('<option value="" selected>- Pilih</option>');
                            for(var i in result){
                                $('#id_dealer').append('<option value="'+result[i].id_dlr+'">'+result[i].nama_dlr+'</option>');
                            }
                        }
                    }
                });
            }
        });
    });
</script>
<script>
   $(document).ready(function () {
         $(".custom-select2").select2({
         });
   });
</script>
@endsection
