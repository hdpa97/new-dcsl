@extends('layouts.sidebar')

@section('title-tab')
Detail Data {{$kodeData}}
@endsection

@section('breadcrumb')
<ol class="breadcrumb mb-4">
    <li class="breadcrumb-item"><a href="{{route('cek.index')}}">Cek Data</a></li>
    <li class="breadcrumb-item active">Detail Data</li>
</ol>
@endsection

@section('main-content')
<div class="card mb-4 mt-3">
    <div class="card-header">
        Dealer : {{$dataDealer->nama_dealer }} ({{$dataDealer->kode_dealer}})
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTableDetailCek" width="450%">
                <thead>
                    <tr>
                        <th class="text-center" width="2%">No.</th>
                        <th class="text-center">Sales Person</th>
                        <th class="text-center">No. Rangka</th>
                        <th class="text-center">Kode Mesin</th>
                        <th class="text-center">No. Mesin</th>
                        <th class="text-center">Tgl Cetak</th>
                        <th class="text-center">Tgl Mohon</th>
                        <th class="text-center" width="10%">Nama</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center">Kel</th>
                        <th class="text-center">Kec</th>
                        <th class="text-center">Kota</th>
                        <th class="text-center">Provinsi</th>
                        <th class="text-center">Cash/Credit</th>
                        <th class="text-center">Finance Company</th>
                        <th class="text-center">Down Payment</th>
                        <th class="text-center">Tenor</th>
                        <th class="text-center">Jenis Sales</th>
                        <th class="text-center">Gender</th>
                        <th class="text-center">Tgl Lahir</th>
                        <th class="text-center">Agama</th>
                        <th class="text-center" width="8%">Pekerjaan</th>
                        <th class="text-center">No. HP</th>
                        <th class="text-center">No. Telp</th>
                        <th class="text-center">Umur</th>
                        <th class="text-center">Tipe</th>
                        <th class="text-center">3 Jenis</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr>
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">{{$data->sales_person}}</td>
                        <td class="text-center">{{$data->nomor_rangka}}</td>
                        <td class="text-center">{{$data->kode_mesin}}</td>
                        <td class="text-center">{{$data->nomor_mesin}}</td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_cetak))}}</td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_mohon))}}</td>
                        <td class="text-center">{{$data->nama_lengkap}}</td>
                        <td class="text-center">{{$data->alamat}}</td>
                        <td class="text-center">{{$data->kelurahan}}</td>
                        <td class="text-center">{{$data->kecamatan}}</td>
                        <td class="text-center">{{$data->nama_kota}}</td>
                        <td class="text-center">{{$data->nama_prov}}</td>
                        <td class="text-center">
                            @if($data->cash_credit == 1)
                            Cash
                            @else
                            Credit
                            @endif
                        </td>
                        <td class="text-center">{{$data->nama_finance_company}}</td>
                        <td class="text-center">{{$data->down_payment}}</td>
                        <td class="text-center">{{$data->tenor}}</td>
                        <td class="text-center">{{$data->jenis_sales}}</td>
                        <td class="text-center">
                            @if($data->gender == 1)
                            Laki-laki
                            @else
                            Perempuan
                            @endif
                        </td>
                        <td class="text-center">{{date('d-m-Y', strtotime($data->tgl_lahir))}}</td>
                        <td class="text-center">{{$data->nama_agama}}</td>
                        <td class="text-center">
                            @if($data->kode_pekerjaan == 11)
                            Lain-lain
                            @else
                            {{$data->nama_pekerjaan}}
                            @endif
                        </td>
                        <td class="text-center">{{$data->no_hp}}</td>
                        <td class="text-center">{{$data->no_telp}}</td>
                        <td class="text-center">{{$data->umur}}</td>
                        <td class="text-center">{{$data->tipe}}</td>
                        <td class="text-center">{{$data->tiga_jenis}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection