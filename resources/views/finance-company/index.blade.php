@extends('layouts.sidebar')

@section('title-tab')
Data Finance Company
@endsection
@section('button')
<a class="" onClick="return fetchData()">
    <button class="btn btn-md btn-primary pull-right">
        <i class="fas fa-plus-circle"></i>
        Tambah Data
    </button>
</a>
@endsection
@section('main-content')
<div class="card mb-4">
    <div class="card-body">
        <div id="message"></div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="dataTableFinanceCompany" width="100%">
                <thead>
                    <th class="text-center" width="5%">No.</th>
                    <th class="text-center">Kode Finance Company</th>
                    <th class="text-center">Nama Finance Company</th>
                    <th class="text-center" width="15%">Opsi</th>
                </thead>
                <tbody>
                    @php $i = 0; @endphp
                    @foreach($datas as $data)
                    <tr id="{{$data->id}}">
                        <td class="text-center">{{++$i}}.</td>
                        <td class="text-center">
                            <span class="editSpan kd">{{$data->kode_finance_company}}</span>
                            <input class="editInput kd form-control input-sm" type="text" id="kode_finance_company{{$data->id}}" value="{{$data->kode_finance_company}}" style="display: none;">
                        </td>
                        <td class="text-center">
                            <span class="editSpan nm">{{$data->nama_finance_company}}</span>
                            <input class="editInput nm form-control input-sm" type="text" id="nama_finance_company{{$data->id}}" value="{{$data->nama_finance_company}}" style="display: none;">
                        </td>
                        <td class="text-center">
                            <div class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-sm btn-info mr-2 editBtn" style="float: none;">edit</button>
                                <button type="button" class="btn btn-sm btn-danger deleteBtn" style="float: none;">delete</button>
                            </div>
                            <button type="button" class="btn btn-sm btn-success saveBtn" style="float: none; display: none;">Save</button>
                            <button type="button" class="btn btn-sm btn-success confirmBtn" style="float: none; display: none;">Confirm</button>
                            <button type="button" class="btn btn-sm btn-warning cancelBtn" style="float: none; display: none;">Cancel</button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ csrf_field() }}
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>
    function fetchData() {
        $.ajax({
            url: "{{ route('finance.company.fetch') }}",
            dataType: "json",
            success: function(data) {
                var html = '';
                html += '<tr>';
                html += '<td></td>';
                html += '<td> <input id="addKodeFinanceCompany" class="form-control input-sm addInput"/></td>';
                html += '<td> <input id="addNamaFinanceCompany" class="form-control input-sm addInput"/></td>';
                html += '<td class="text-center"><button type="button" class="btn btn-sm btn-success" id="add">Add</button>';
                html += '<button type="button" class="btn btn-sm btn-warning cancelAdd ml-2" style="float: none;">Cancel</button></td></tr>';
                for (var count = 0; count < data.length; count++) {
                    var ct = count + 1;
                    html += '<tr id="' + data[count].id + '">';
                    html += '<td class="text-center">' + ct + '</td>';
                    html += '<td class="text-center">';
                    html += '<span class="editSpan kd">' + data[count].kode_finance_company + '</span>';
                    html += '<input class="editInput kd form-control input-sm" type="text" id="kode_finance_company' + data[count].id + '" value="' + data[count].kode_finance_company + '" style="display: none;">'
                    html += '</td>';
                    html += '<td class="text-center">';
                    html += '<span class="editSpan nm">' + data[count].nama_finance_company + '</span>';
                    html += '<input class="editInput nm form-control input-sm" type="text" id="nama_finance_company' + data[count].id + '" value="' + data[count].nama_finance_company + '" style="display: none;">'
                    html += '</td>';
                    html += '<td class="text-center">';
                    html += '<div class="btn-group btn-group-sm">';
                    html += '<button type="button" class="btn btn-sm btn-info mr-2 editBtn" style="float: none;">edit</button>';
                    html += '<button type="button" class="btn btn-sm btn-danger deleteBtn" style="float: none;">delete</button>'
                    html += '</div>';
                    html += '<button type="button" class="btn btn-sm btn-success saveBtn" style="float: none; display: none;">Save</button>';
                    html += '<button type="button" class="btn btn-sm btn-success confirmBtn" style="float: none; display: none;">Confirm</button>';
                    html += '<button type="button" class="btn btn-sm btn-warning cancelBtn ml-2" style="float: none; display: none;">Cancel</button>';
                    html += '</td>';
                }
                $('tbody').html(html);
            }
        });
    }

    $(document).ready(function() {

        // fetchData();

        var _token = $('input[name="_token"]').val();

        $(document).on('click', '#add', function() {
            var kode_finance_company = $('#addKodeFinanceCompany').val();
            var nama_finance_company = $('#addNamaFinanceCompany').val();
            if (kode_finance_company || nama_finance_company != '') {
                if (confirm("Apakah ingin menambah data?")) {
                    $.ajax({
                        url: "{{ route('finance.company.add') }}",
                        method: "POST",
                        data: {
                            kode_finance_company: kode_finance_company,
                            nama_finance_company: nama_finance_company,
                            _token: _token
                        },
                        success: function(data) {
                            $('#message').html(data);
                            $('#addKodeFinanceCompany').hide();
                            $('#addNamaFinanceCompany').hide();
                            window.location.href = "{{route('finance.company.index')}}";
                        }
                    });
                } else {
                    window.location.href = "{{route('finance.company.index')}}";
                }
            } else {
                $('#message').html("<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button>Lengkapi kolom.</div>");
            }
        });

        $(document).on('click', '.editBtn', function() {
            $(this).closest("tr").find(".editSpan").hide();
            $(this).closest("tr").find(".editInput").show();
            $(this).closest("tr").find(".editBtn").hide();
            $(this).closest("tr").find(".saveBtn").show();
            $(this).closest("tr").find(".deleteBtn").hide();
            $(this).closest("tr").find(".cancelBtn").show();
        });

        $(document).on('click', '.saveBtn', function() {
            var trObj = $(this).closest("tr");
            var id = $(this).closest("tr").attr('id');
            var kode_finance_company = $("#kode_finance_company" + id).val();
            var nama_finance_company = $("#nama_finance_company" + id).val();

            $.ajax({
                url: "{{ route('finance.company.update') }}",
                method: "POST",
                data: {
                    kode_finance_company: kode_finance_company,
                    nama_finance_company: nama_finance_company,
                    id: id,
                    _token: _token
                },
                success: function(data) {
                    $('#message').html(data);
                    trObj.find(".editSpan.kd").text(kode_finance_company);
                    trObj.find(".editInput.kd").text(kode_finance_company);
                    trObj.find(".editSpan.nm").text(nama_finance_company);
                    trObj.find(".editInput.nm").text(nama_finance_company);
                    trObj.find(".editInput").hide();
                    trObj.find(".saveBtn").hide();
                    trObj.find(".editSpan").show();
                    trObj.find(".editBtn").show();
                    window.location.href = "{{route('finance.company.index')}}";
                }
            })

        });

        $(document).on('click', '.deleteBtn', function() {
            $(this).closest("tr").find(".deleteBtn").hide();
            $(this).closest("tr").find(".editBtn").hide();
            $(this).closest("tr").find(".confirmBtn").show();
            $(this).closest("tr").find(".cancelBtn").show();
        });

        $(document).on('click', '.confirmBtn', function() {
            var trObj = $(this).closest("tr");
            var id = $(this).closest("tr").attr('id');
            $.ajax({
                type: "POST",
                url: "{{ route('finance.company.delete') }}",
                dataType: "json",
                data: {
                    id: id,
                    _token: _token
                },
                success: function(res) {
                    if (res.status == "ok") {
                        $('#message').html(res.alert);
                        trObj.remove();
                        window.location.href = "{{route('finance.company.index')}}";
                    } else {
                        trObj.find(".confirmBtn").hide();
                        trObj.find(".deleteBtn").show();
                        $('#message').html(res.alert);
                        window.location.href = "{{route('finance.company.index')}}";
                    }
                },
                error: function(err) {
                    trObj.find(".confirmBtn").hide();
                    trObj.find(".deleteBtn").show();
                    $('#message').html(
                        "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert'>×</button>" + err.status + " " + err.statusText + "</div>"
                    );
                }
            });
        });

        $(document).on('click', '.cancelBtn', function() {
            $(this).closest("tr").find(".editInput").hide();
            $(this).closest("tr").find(".editSpan").show();
            $(this).closest("tr").find(".confirmBtn").hide();
            $(this).closest("tr").find(".cancelBtn").hide();
            $(this).closest("tr").find(".saveBtn").hide();
            $(this).closest("tr").find(".deleteBtn").show();
            $(this).closest("tr").find(".editBtn").show();
        });

        $(document).on('click', '.cancelAdd', function() {
            var trObj = $(this).closest("tr");
            trObj.remove();
        });

    });
</script>
@endsection
