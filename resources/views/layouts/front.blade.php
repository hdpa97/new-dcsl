<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Dealer Customer Satisfaction Level (DCSL) Online - Astra Motor" />
    <meta name="author" content="hhhdpa@gmail.com,dakapradana.dp@gmail.com" />
    <link href="{{URL::asset('img/favicon.png')}}" rel="icon" type="image/x-icon">
    <title>DCSL Online | Astra Motor</title>

    <!-- Vendor css -->
    <link href="{{URL::asset('css/slim-clean/font-awesome.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('css/slim-clean/ionicons.css')}}" rel="stylesheet" />
    <link href="{{URL::asset('css/slim-clean/slim.css')}}" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous">
    </script>

</head>

<body>
    <div class="signin-wrapper">
        @yield('front-content')
    </div>
</body>
    @yield('script')
</html>
