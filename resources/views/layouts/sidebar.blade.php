<?php

use App\Http\Controllers\UserController;

$userData = UserController::getUserData();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="Dealer Customer Satisfaction Level (DCSL) Online - Astra Motor" />
    <meta name="author" content="hhhdpa@gmail.com,dakapradana.dp@gmail.com" />
    <link href="{{URL::asset('img/favicon.png')}}" rel="icon" type="image/x-icon">
    <title>DCSL Online | Astra Motor</title>
    <link href="{{URL::asset('css/styles.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{URL::asset('dataTable/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('dataTable/buttons.dataTables.min.css')}}">
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>

    <style>
        /* Center the loader */
        #loader {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border: 16px solid #de1d00;
            border-radius: 50%;
            border-top: 16px solid #134F97;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }

        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }

        /* Add animation to "page content" */
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0
            }

            to {
                bottom: 0px;
                opacity: 1
            }
        }

        @keyframes animatebottom {
            from {
                bottom: -100px;
                opacity: 0
            }

            to {
                bottom: 0;
                opacity: 1
            }
        }

        #myDiv {
            display: none;
        }
    </style>
</head>

<body onload="startPage()">
    <div id="loader"></div>
    <nav class="sb-topnav navbar navbar-expand navbar-white">
        <center>
            <a class="navbar-brand" href="index.html"><img src="{{asset('img/img_logo_honda.png')}}" width="70%" /></a>
        </center>
        <button class="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" href="#"><i class="fas fa-bars" style="color:black"></i></button><!-- Navbar Search-->
        <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
            {{--
                    <div class="input-group">
                        <input class="form-control" type="text" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="button"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                --}}
        </form>
        <!-- Navbar-->
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="userDropdown" href="#" style="color:black" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi, {{$userData['name']}} <i class="fas fa-user fa-fw" style="color:black"></i></a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="{{ route('users.setting')}}">Settings</a>
                    <div class="dropdown-divider"></div>

                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>&nbsp;Log Out
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        @if($userData['role']=='1')
                        <div class="sb-sidenav-menu-heading">Home</div>
                        <a class="nav-link" href="{{route('home')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-chart-line"></i></div>
                            Dashboard
                        </a>
                        <div class="sb-sidenav-menu-heading">Master Data</div>
                        <a class="nav-link" href="{{route('users.index')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Users
                        </a>
                        <a class="nav-link" href="{{route('upload-data.index')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-file-upload"></i></div>
                            Upload Data
                        </a>
                        <a class="nav-link" href="{{route('cek.index')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-info-circle"></i></div>
                            Cek Data
                        </a>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                            <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                            Data Dealer
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('dealer.index')}}">
                                    Dealer
                                </a>
                                <a class="nav-link" href="{{route('kares.index')}}">
                                    Kares
                                </a>
                                <a class="nav-link" href="{{route('jaringan.index')}}">
                                    Jaringan
                                </a>
                                <a class="nav-link" href="{{route('layer.index')}}">
                                    Layer
                                </a>
                                <a class="nav-link" href="{{route('kabupaten.index')}}">
                                    Kabupaten
                                </a>
                            </nav>
                        </div>

                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseDataMaster" aria-expanded="false" aria-controls="collapseLayouts">
                            <div class="sb-nav-link-icon"><i class="fas fa-database"></i></div>
                            Data Master
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseDataMaster" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('motor.premium.index')}}">
                                    Motor Premium
                                </a>
                                <a class="nav-link" href="{{route('finance.company.index')}}">
                                    Finance Company
                                </a>
                                <a class="nav-link" href="{{route('agama.index')}}">
                                    Agama
                                </a>
                                <a class="nav-link" href="{{route('pekerjaan.index')}}">
                                    Pekerjaan
                                </a>
                                <a class="nav-link" href="{{route('pendidikan.index')}}">
                                    Pendidikan
                                </a>
                                <a class="nav-link" href="{{route('pengeluaran.index')}}">
                                    Pengeluaran
                                </a>
                            </nav>
                        </div>

                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts2" aria-expanded="false" aria-controls="collapseLayouts2">
                            <div class="sb-nav-link-icon"><i class="fas fa-archive"></i></div>
                            Report
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                        </a>
                        <div class="collapse" id="collapseLayouts2" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('demografi.index')}}">
                                    Demografi
                                </a>
                            </nav>
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('results.index')}}">
                                    Results
                                </a>
                            </nav>
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('surveyor.index')}}">
                                    Surveyor
                                </a>
                            </nav>
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('kepuasan.index')}}">
                                    Scoring
                                </a>
                            </nav>
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{route('tersurvey.index')}}">
                                    Tersurvey
                                </a>
                            </nav>
                        </div>
                        <a class="nav-link" href="{{route('ro-data.index')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-redo"></i></div>
                            Data RO
                        </a>
                        @else
                        <div class="sb-sidenav-menu-heading">Home</div>
                        <a class="nav-link" href="{{route('home.surveyor')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-chart-line"></i></div>
                            Dashboard
                        </a>
                        <div class="sb-sidenav-menu-heading">Master Data</div>
                        <a class="nav-link" href="{{route('survey.index')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-file-upload"></i></div>
                            Survey
                        </a>
                        <a class="nav-link" href="{{route('survey-reject.index')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-file-upload"></i></div>
                            Survey Reject
                        </a>
                        <a class="nav-link" href="{{route('survey.report')}}">
                            <div class="sb-nav-link-icon"><i class="fas fa-file-upload"></i></div>
                            Report Survey
                        </a>
                        @endif
                    </div>

                </div>
                <div class="sb-sidenav-footer py-3" style="background-color:#134F97">
                    <div class="small ml-4" id="sTime"></div>
                </div>
            </nav>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid" style="display:none;" id="myDiv" class="animate-bottom">
                    <h3 class="mt-4 pull-left">@yield('title-tab')</h3>
                    @yield('breadcrumb')
                    <div class="text-right mb-3">@yield('button') </div>
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif

                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                    @endif
                    @yield('main-content')
                </div>
            </main>
            <footer class="py-3 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        {{--
                            <div class="text-muted">Made with ❤ in Kos-Kosan</div>
                        --}}
                        <div class="text-muted"></div>
                        <div class="text-muted">
                            Copyright &copy; ASTRA-MOTOR 2020
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    @yield('script')
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script> -->
    <!-- <script src="{{URL::asset('js/tableedit.min.js')}}"></script> -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="{{URL::asset('js/scripts.js')}}"></script>
    <!-- DataTable-->
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="{{URL::asset('assets/demo/datatables-demo.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="{{URL::asset('dataTable/jquery.dataTables.min.js')}}"></script>
    <script src="{{URL::asset('dataTable/dataTables.buttons.min.js')}}"></script>
    <script src="{{URL::asset('dataTable/buttons.flash.min.js')}}"></script>
    <script src="{{URL::asset('dataTable/jszip.min.js')}}"></script>
    <script src="{{URL::asset('dataTable/pdfmake.min.js')}}"></script>
    <script src="{{URL::asset('dataTable/vfs_fonts.js')}}"></script>
    <script src="{{URL::asset('dataTable/buttons.html5.min.js')}}"></script>
    <script src="{{URL::asset('dataTable/buttons.print.min.js')}}"></script>
    <!-- ChartJS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <!-- <script src="{{URL::asset('assets/demo/chart-area-demo.js')}}"></script>
    <script src="{{URL::asset('assets/demo/chart-bar-demo.js')}}"></script>
    <script src="{{URL::asset('assets/demo/chart-pie-demo.js')}}"></script> -->
    <!-- Load file CSS Bootstrap dan Select2 melalui CDN -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <!-- Load file JS untuk JQuery dan Selec2.js melalui CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
</body>

</html>
<script>
    var myVar;

    function startPage() {
        myVar = setTimeout(showPage, 5);
        startTime();
    }

    function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
    }

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        document.getElementById('sTime').innerHTML = "Server Time : " +
            h + ":" + m + ":" + s;
        var t = setTimeout(startTime, 500);
    }

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i
        }; // add zero in front of numbers < 10
        return i;
    }
</script>
