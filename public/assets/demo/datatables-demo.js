$(document).ready(function () {
  $('#dataTable').DataTable({
    buttons: []
  });

  $('#dataTableMotor').DataTable({
    dom: 'Bfrtip',
    buttons: []
  });

  $('#dataTableAgama').DataTable({
    dom: 'Bfrtip',
    buttons: []
  });

  $('#dataTablePekerjaan').DataTable({
    dom: 'Bfrtip',
    buttons: []
  });

  $('#dataTablePendidikan').DataTable({
    dom: 'Bfrtip',
    buttons: []
  });

  $('#dataTablePengeluaran').DataTable({
    dom: 'Bfrtip',
    buttons: []
  });

  $('#dataTableFinanceCompany').DataTable({
    dom: 'Bfrtip',
    buttons: []
  });

  $('#dataTableKares').DataTable({
    buttons: []
  });

  $('#dataTableKabupaten').DataTable({
    buttons: []
  });

  $('#dataTableJaringan').DataTable({
    buttons: []
  });

  $('#dataTableLayer').DataTable({
    buttons: []
  });

  $('#dataTableDealer').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Dealer ASTRA MOTOR - JOMBOR 2020',
        exportOptions: {
          columns: [1, 2, 3, 4, 5, 6]
        }
      },
      {
        extend: 'pdf',
        messageTop: 'Data Dealer ASTRA MOTOR - JOMBOR 2020',
        exportOptions: {
          columns: [1, 2, 3, 4, 5, 6]
        }
      }]
  });

  $('#dataTableTersurvey').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Tersurvey ASTRA MOTOR - JOMBOR 2020'
      },
      {
        extend: 'pdf',
        messageTop: 'Data Tersurvey ASTRA MOTOR - JOMBOR 2020'
      }]
  });

  $('#dataTableSurveyor').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Surveyor ASTRA MOTOR - JOMBOR 2020'
      },
      {
        extend: 'pdf',
        messageTop: 'Data Surveyor ASTRA MOTOR - JOMBOR 2020'
      }]
  });

  $('#dataTableRepeatOrder').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Repeat Order ASTRA MOTOR - JOMBOR 2020'
      }]
  });

  $('#dataTableScoring').DataTable({
    dom: 'Bfrtip',
    pageLength: 20,
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Scoring ASTRA MOTOR - JOMBOR 2020'
      }]
  });

 $('#dataTableResults').DataTable({
    dom: 'Bfrtip',
    pageLength: 20,
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Results ASTRA MOTOR - JOMBOR 2020'
      }]
  });


  $('input.column_filter').on('keyup click', function () {
    filterColumn($(this).parents('tr').attr('data-column'));
  });

  // FORM H1
  $('#uploadForm').submit(function (e) {
    $('.submit-button').html("<i class='fa fa-spinner fa-spin'></i> Process");
    $('.submit-button').attr("disabled", true);
  });

  $('#dataTableUploadData').DataTable({
    buttons: [],
    columnDefs: [{
      "orderable": false,
      "targets": [5, 6]
    }]
  });

  $('#dataTableCek').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Cek Data ASTRA MOTOR - JOMBOR 2020',
        exportOptions: {
          columns: [1, 2, 3, 4, 5, 6, 7]
        }
      },
      {
        extend: 'pdf',
        messageTop: 'Cek Data ASTRA MOTOR - JOMBOR 2020',
        exportOptions: {
          columns: [1, 2, 3, 4, 5, 6, 7]
        }
      }
    ]
  });

  $('#dataTableDetailCek').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Detail Cek Data ASTRA MOTOR - JOMBOR 2020',
      }
    ]
  });

  $('#dataTableReportSurvey').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Report Survey ASTRA MOTOR - JOMBOR 2020',
      }
    ]
  });

  $('#dataTableSurvey').DataTable({
    dom: 'Bfrtip',
    buttons: [
      {
        extend: 'excel',
        messageTop: 'Data Report Survey ASTRA MOTOR - JOMBOR 2020',
      }
    ]
  });

  // START PANEL PENJUALAN //
  // PANEL 2
  $("input[name$='a1']").click(function () {
    var val = $(this).val();

    $("div.a1").hide();
    $("#Open" + val).show();

    document.getElementById("a2v1").checked = false;
    document.getElementById("a2v2").checked = false;
  });

  // PANEL 3
  $("input[name$='b1_3a']").click(function () {
    var val = $(this).val();

    $("div.b1_3a").hide();
    $("#Open" + val).show();

    document.getElementById("b1_4v1").checked = false;
    document.getElementById("b1_4v2").checked = false;
  });

  // PANEL 4
  $("input[name$='d15']").click(function () {
    var val = $(this).val();

    $("div.d15").hide();
    $("#Open" + val).show();
  });

  // PANEL 5
  $("input[name$='h1_5a']").click(function () {
    var val = $(this).val();

    $("div.h1_5a").hide();
    $("#Open" + val).show();

    document.getElementById("h1_5bv1").checked = false;
    document.getElementById("h1_5bv2").checked = false;
    document.getElementById("h1_5cv1").checked = false;
    document.getElementById("h1_5cv2").checked = false;
  });
  $("input[name$='h1_5b']").click(function () {
    var val = $(this).val();

    $("div.h1_5b").hide();
    $("#Buka" + val).show();

    document.getElementById("h1_5cv1").checked = false;
    document.getElementById("h1_5cv2").checked = false;
  });
  $("input[name$='h1_5']").click(function () {
    var val = $(this).val();
    var arr = ["1", "2", "3"];

    if (arr.includes(val)) {
      $("#Check").show();
    }
    else {
      $("div.h1_5").hide();
    }
  });

  //PAGE 6
  $("input[name$='h3']").click(function () {
    var val = $(this).val();

    $("div.h3_v12345").hide();
    $("#Open" + val).show();
  });

  //PAGE7
  $("input[name$='n1']").click(function () {
    var val = $(this).val();
    var arr = ["4", "5"];

    if (arr.includes(val)) {
      $("#Open45").show();
    }
    else {
      $("div.n1").hide();
    }
  });
  $("input[name$='n5']").click(function () {
    var val = $(this).val();
    var arr = ["Honda"];

    if (arr.includes(val)) {
      $("#OpenHonda").show();
    } else {
      $("div.n5").hide();
    }
  });

  // END PANEL PENJUALAN //

  // START PANEL PEMELIHARAAN //
  // PAGE 2
  $("input[name$='a1']").click(function () {
    var val = $(this).val();

    $("div.a1").hide();
    $("#Open" + val).show();

    document.getElementById("a2v1").checked = false;
    document.getElementById("a2v2").checked = false;
  });
  $("input[name$='a3_3b']").click(function () {
    var val = $(this).val();

    $("div.a3_3b").hide();
    $("#Buka" + val).show();
  });
  $("input[name$='a3_3']").click(function () {
    var val = $(this).val();
    var arr = ["1", "2", "3"];

    if (arr.includes(val)) {
      $("#Check").show();
    }
    else {
      $("div.a3_3").hide();
    }
  });
  $("input[name$='a3_4']").click(function () {
    var val = $(this).val();
    var arr = ["1", "2", "3"];

    if (arr.includes(val)) {
      $("#Open34").show();
    }
    else {
      $("div.a3_4").hide();
    }
  });

  // PAGE 3
  $("input[name$='c1_3']").click(function () {
    var val = $(this).val();

    $("div.c1_3").hide();
    $("#Open" + val).show();

    document.getElementById("c1_3av1").checked = false;
    document.getElementById("c1_3av2").checked = false;
  });
  $("input[name$='c1_2']").click(function () {
    var val = $(this).val();
    var arr = ["1", "2", "3"];

    if (arr.includes(val)) {
      $("#Check").show();
    }
    else {
      $("div.c1_2").hide();
    }
  });
  $("input[name$='c1_4a']").click(function () {
    var val = $(this).val();

    $("div.c1_4a").hide();
    $("#Intip" + val).show();
  });

  // PAGE 4
  $("input[name$='d3_3a']").click(function () {
    var val = $(this).val();

    $("div.d3_3a").hide();
    $("#Buka" + val).show();

    document.getElementById("d3_3bv1").checked = false;
    document.getElementById("d3_3bv2").checked = false;
  });

  // PAGE 7
  $("input[name$='i4']").click(function () {
    var val = $(this).val();

    $("div.i4").hide();
    $("#Open" + val).show();

    document.getElementById("i5v1").checked = false;
    document.getElementById("i5v2").checked = false;
    document.getElementById("i6v1").checked = false;
    document.getElementById("i6v2").checked = false;
  });
  $("input[name$='i5']").click(function () {
    var val = $(this).val();

    $("div.i5").hide();
    $("#Buka" + val).show();

    document.getElementById("i6v1").checked = false;
    document.getElementById("i6v2").checked = false;
  });
  $("input[name$='i7']").click(function () {
    var val = $(this).val();
    var arr = ["1", "2", "3"];

    if (arr.includes(val)) {
      $("#Check").show();
    }
    else {
      $("div.i7").hide();
    }
  });

  // PAGE 8
  $("input[name$='n2']").click(function () {
    var val = $(this).val();
    var arr = ["4", "5"];

    if (arr.includes(val)) {
      $("#CheckNew").show();
    }
    else {
      $("div.n2").hide();
    }
  });
  $("input[name$='n6']").click(function () {
    var val = $(this).val();
    var arr = ["Honda"];

    if (arr.includes(val)) {
      $("#OpenSeri").show();
    } else {
      $("div.n6").hide();
    }
  });
  // END PANEL PEMELIHARAAN //

  // START PANEL SUKU CADANG //
  // PAGE 3
  $("input[name$='c1_a']").click(function () {
    var val = $(this).val();

    $("div.c1_a").hide();
    $("#Buka" + val).show();

    document.getElementById("c1_bv1").checked = false;
    document.getElementById("c1_bv2").checked = false;
  });
  $("input[name$='c2']").click(function () {
    var val = $(this).val();

    $("div.c2").hide();
    $("#Bukaa" + val).show();

    document.getElementById("c3v1").checked = false;
    document.getElementById("c3v2").checked = false;
  });

  $("input[name$='c5']").click(function () {
    var val = $(this).val();

    $("div.c5").hide();
    $("#Open" + val).show();
  });

  $("input[name$='d3_3a']").click(function () {
    var val = $(this).val();

    $("div.d3_3a").hide();
    $("#Bukaaa" + val).show();

    document.getElementById("d3_3bv1").checked = false;
    document.getElementById("d3_3bv2").checked = false;
  });
  $("input[name$='d4']").click(function () {
    var val = $(this).val();

    $("div.d4").hide();
    $("#Bukaaaa" + val).show();

    document.getElementById("d5bv1").checked = false;
    document.getElementById("d5bv2").checked = false;
  });
  $("input[name$='e1']").click(function () {
    var val = $(this).val();

    $("div.e1").hide();
    $("#Checkk" + val).show();

    document.getElementById("e2v1").checked = false;
    document.getElementById("e2v2").checked = false;
  });

  // PAGE 4
  $("input[name$='g1']").click(function () {
    var val = $(this).val();

    $("div.g1").hide();
    $("#Open" + val).show();

    document.getElementById("g2v1").checked = false;
    document.getElementById("g2v2").checked = false;
  });

  // END PANEL SUKU CADANG //

});
