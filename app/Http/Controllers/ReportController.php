<?php

namespace App\Http\Controllers;

use App\Serialiser\Serialiser_Occupation;
use App\Serialiser\Serialiser_SES;
use App\Serialiser\Serialiser_Education;
use App\Serialiser\Serialiser_GroupAges;
use App\Serialiser\Serialiser_Gender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelDemografi;
use App\Model\ModelSurveyor;
use App\Model\ModelTersurvey;
use App\Model\ModelJaringan;
use App\Model\ModelKares;
use App\Model\ModelKabupaten;
use App\Model\ModelDealer;
use App\Model\ModelPendidikan;
use App\Model\ModelPengeluaran;
use App\Model\ModelPekerjaan;
use App\Model\ModelScoringKepuasan;
use App\Model\ModelResults;
use Auth;
use Carbon\Carbon;
use Exporter;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    private $ModelDemografi;
    private $ModelSurveyor;
    private $ModelTersurvey;
    private $ModelPengeluaran;
    private $ModelPekerjaan;
    private $ModelScoringKepuasan;
    private $ModelResults;

    public function __construct(
        ModelDemografi $ModelDemografi,
        ModelSurveyor $ModelSurveyor,
        ModelTersurvey $ModelTersurvey,
        ModelJaringan $ModelJaringan,
        ModelKares $ModelKares,
        ModelKabupaten $ModelKabupaten,
        ModelDealer $ModelDealer,
        ModelPendidikan $ModelPendidikan,
        ModelPengeluaran $ModelPengeluaran,
        ModelPekerjaan $ModelPekerjaan,
        ModelScoringKepuasan $ModelScoringKepuasan,
        ModelResults $ModelResults
    ) {
        $this->ModelDemografi = $ModelDemografi;
        $this->ModelSurveyor = $ModelSurveyor;
        $this->ModelTersurvey = $ModelTersurvey;
        $this->ModelJaringan = $ModelJaringan;
        $this->ModelKares = $ModelKares;
        $this->ModelKabupaten = $ModelKabupaten;
        $this->ModelDealer = $ModelDealer;
        $this->ModelPendidikan = $ModelPendidikan;
        $this->ModelPengeluaran = $ModelPengeluaran;
        $this->ModelPekerjaan = $ModelPekerjaan;
        $this->ModelScoringKepuasan = $ModelScoringKepuasan;
        $this->ModelResults = $ModelResults;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        if($request->action == 'downloadOccupation'){
            return $this->downloadOccupation($request);
        }
        if($request->action == 'downloadSES'){
            return $this->downloadSES($request);
        }
        if($request->action == 'downloadEducation'){
            return $this->downloadEducation($request);
        }
        if($request->action == 'downloadAgeGroup'){
            return $this->downloadGroupAges($request);
        }
        if($request->action == 'downloadGender'){
            return $this->downloadGender($request);
        }

        $getGender = $this->ModelTersurvey->getReportDemografiGender($request);
        $getEducation = $this->ModelPendidikan->getReportDemografi($request);
        $getSES = $this->ModelPengeluaran->getReportDemografi($request);
        $getOccupation = $this->ModelPekerjaan->getReportDemografi($request);
        $getGroupAges = $this->ModelTersurvey->getRportAgeGroup($request);

        $dtKares = $this->ModelKares->getAllData();
        $dtKab = $this->ModelKabupaten->getAllData();
        $dtDealer = $this->ModelDealer->getData();

        return view('demografi.index', compact('getGroupAges', 'getGender', 'getEducation', 'getSES', 'getOccupation', 'request', 'dtKares', 'dtKab', 'dtDealer'));
    }

    public function downloadOccupation(Request $request)
    {
        $getOccupation = $this->ModelPekerjaan->getReportDemografi($request);
        $serialiser = new Serialiser_Occupation();
        $excel = Exporter::make('Excel');
        $excel->load($getOccupation)->setSerialiser($serialiser);
        return $excel->stream('dataDemografiOccupation.xlsx');
    }

    public function downloadSES(Request $request)
    {
        $getSES = $this->ModelPengeluaran->getReportDemografi($request);
        $serialiser = new Serialiser_SES();
        $excel = Exporter::make('Excel');
        $excel->load($getSES)->setSerialiser($serialiser);
        return $excel->stream('dataDemografiSES.xlsx');
    }

    public function downloadEducation(Request $request)
    {
        $getEducation = $this->ModelPendidikan->getReportDemografi($request);
        $serialiser = new Serialiser_Education();
        $excel = Exporter::make('Excel');
        $excel->load($getEducation)->setSerialiser($serialiser);
        return $excel->stream('dataDemografiEducation.xlsx');
    }

    public function downloadGroupAges(Request $request)
    {
        $getGroupAges = $this->ModelTersurvey->getRportAgeGroup($request);
        $serialiser = new Serialiser_GroupAges();
        $excel = Exporter::make('Excel');
        $excel->load($getGroupAges)->setSerialiser($serialiser);
        return $excel->stream('dataDemografiAgeGroup.xlsx');
    }

    public function downloadGender(Request $request)
    {
        $getGender = $this->ModelTersurvey->getReportDemografiGender2($request);
        $serialiser = new Serialiser_Gender();
        $excel = Exporter::make('Excel');
        $excel->load($getGender)->setSerialiser($serialiser);
        return $excel->stream('dataDemografiGender.xlsx');
    }

    public function indexx(Request $request)
    {
        $datas = $this->ModelTersurvey->getAchPerson($request);
        return view('surveyor.index', compact('datas','request'));
    }

    public function detail()
    {
        return view('surveyor.detail');
    }

    public function indexxx(Request $request)
    {
        $jars = $this->ModelJaringan->getAllData();
        $karess = $this->ModelKares->getAllData();
        $kabs = $this->ModelKabupaten->getAllData();
        $dealers = $this->ModelDealer->getData();
        $getTersurvey = $this->ModelTersurvey->getReportTersurvey($request);

        return view('tersurvey.index', compact('jars', 'karess', 'kabs', 'dealers', 'getTersurvey', 'request'));
    }

    public function detailData($kode_dealer,$id)
    {
        $data = $this->ModelScoringKepuasan->getDetailData($id);
        if(!isset($data)){
            return 404;
        }
        return view('detail-data.index', compact('data','kode_dealer'));
    }

    public function scoringDealer($kode_dealer,$tipe,Request $request)
    {
        if($tipe=='H3'){
            $datas = $this->ModelScoringKepuasan->getScoringDealerH3($kode_dealer);
        }elseif($tipe=='H2'){
            $datas = $this->ModelScoringKepuasan->getScoringDealerH2($kode_dealer);
        }else
        {
           $datas = $this->ModelScoringKepuasan->getScoringDealerH1($kode_dealer);
        }
        if(!isset($datas)){
            return 404;
        }
        return view('detail-data.dealer', compact('request','datas','tipe','kode_dealer'));
    }

    public function kepuasan(Request $request)
    {
        $jars = $this->ModelJaringan->getAllData();
        $karess = $this->ModelKares->getAllData();
        $kabs = $this->ModelKabupaten->getAllData();
        $dealers = $this->ModelDealer->getData();
        if($request->get('tipeData') == 'H2') {
            $getScoring = $this->ModelScoringKepuasan->getScoringKepuasanH2($request);
        }elseif($request->get('tipeData') == 'H3') {
            $getScoring = $this->ModelScoringKepuasan->getScoringKepuasanH3($request);
        }else{
            $getScoring = $this->ModelScoringKepuasan->getScoringKepuasanH1($request);
        }

        return view('kepuasan.index', compact('jars', 'karess', 'kabs', 'dealers', 'request', 'getScoring'));
    }

    public function results(Request $request)
    {
        $jars = $this->ModelJaringan->getAllData();
        $karess = $this->ModelKares->getAllData();
        $kabs = $this->ModelKabupaten->getAllData();
        $dealers = $this->ModelDealer->getData();
        if($request->get('tipeData') == 'H2') {
            $getResults = $this->ModelResults->getResultsH2($request);
        }elseif($request->get('tipeData') == 'H3') {
            $getResults = $this->ModelResults->getResultsH3($request);
        }else{
            $getResults = $this->ModelResults->getResultsH1($request);
        }
        return view('results.index', compact('jars','karess','kabs','dealers','request', 'getResults'));
    }

    public function fetch_jaringan($idJaringan)
    {
        $data = DB::select(
            'SELECT DISTINCT tbl_jaringan.id AS id_jar, tbl_jaringan.nama_jaringan AS nama_jar
        FROM tbl_jaringan
        WHERE tbl_jaringan.id = :idjar
        ORDER BY tbl_jaringan.nama_jaringan ASC',
            ['idjar' => $idJaringan]
        );

        return json_encode($data);
    }

    public function fetch_kab($idKares)
    {
        $data = DB::select(
            'SELECT DISTINCT tbl_kabupaten.id AS id_kab, tbl_kabupaten.nama_kabupaten AS nama_kab
        FROM tbl_kabupaten
        INNER JOIN tbl_dealer on tbl_dealer.id_kabupaten = tbl_kabupaten.id
        INNER JOIN tbl_kares on tbl_dealer.id_kares = tbl_kares.id
        WHERE tbl_kares.id = :idkrs
        ORDER BY tbl_kabupaten.nama_kabupaten ASC',
            ['idkrs' => $idKares]
        );

        return json_encode($data);
    }

    public function fetch_dlr($idKares)
    {
        $data = DB::select(
            'SELECT DISTINCT tbl_dealer.id AS id_dlr, tbl_dealer.nama_dealer AS nama_dlr
        FROM tbl_dealer
        INNER JOIN tbl_kabupaten on tbl_dealer.id_kabupaten = tbl_kabupaten.id
        INNER JOIN tbl_kares on tbl_dealer.id_kares = tbl_kares.id
        WHERE tbl_kares.id = :idkrs
        ORDER BY tbl_dealer.nama_dealer ASC',
            ['idkrs' => $idKares]
        );

        return json_encode($data);
    }

    public function fetch_dealer($idKabupaten)
    {
        $data = DB::select(
            'SELECT DISTINCT tbl_dealer.id AS id_dlr, tbl_dealer.nama_dealer AS nama_dlr
        FROM tbl_dealer
        INNER JOIN tbl_kabupaten on tbl_dealer.id_kabupaten = tbl_kabupaten.id
        INNER JOIN tbl_kares on tbl_dealer.id_kares = tbl_kares.id
        WHERE tbl_kabupaten.id = :idkab
        ORDER BY tbl_dealer.nama_dealer ASC',
            ['idkab' => $idKabupaten]
        );

        return json_encode($data);
    }
}
