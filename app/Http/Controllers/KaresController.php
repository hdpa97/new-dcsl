<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelKares;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class KaresController extends Controller
{
    private $ModelKares;

    public function __construct(ModelKares $ModelKares)
    {
        $this->ModelKares = $ModelKares;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelKares->getAllData();
        return view('kares.index', compact('datas'));
    }

    public function create()
    {
        return view('kares.create');
    }

    public function edit($id)
    {
        $data = $this->ModelKares->where('id',$id)->first();

        if(!isset($data))
        {
            return 404;
        }
        return view('kares.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $this->ModelKares->updateData($request,$id);
        return redirect()->route('kares.index')->with('success','Kares berhasil diupdate.');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kares' => 'required|string|max:250',
            'flag' => 'required',
        ]);

        if ($validator->fails())
        {
            $request->flash();
            return redirect()->route('kares.create')->withErrors($validator->messages())->withInput();
        }

        $this->ModelKares->createData($request);
        return redirect()->route('kares.index')->with('success','Kares berhasil ditambahkan.');
    }

    public function disable($id)
    {
        $this->ModelKares->disableData($id);
        return redirect()->route('kares.index')->with('success','Kares berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->ModelKares->enableData($id);
        return redirect()->route('kares.index')->with('success','Kares berhasil dinonaktifkan.');
    }

}
