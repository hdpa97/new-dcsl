<?php

namespace App\Http\Controllers;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelJaringan;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class JaringanController extends Controller
{
    private $ModelJaringan;

    public function __construct(ModelJaringan $ModelJaringan)
    {
        $this->ModelJaringan = $ModelJaringan;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelJaringan->getAllData();
        return view('jaringan.index', compact('datas'));
    }

    public function create()
    {
        return view('jaringan.create');
    }

    public function edit($id)
    {
        $data = $this->ModelJaringan->where('id', $id)->first();

        if (!isset($data)) {
            return 404;
        }
        return view('jaringan.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $this->ModelJaringan->updateData($request, $id);
        return redirect()->route('jaringan.index')->with('success', 'Jaringan berhasil diupdate.');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_jaringan' => 'required|string|max:250',
            'flag' => 'required',
        ]);

        if ($validator->fails()) {
            $request->flash();
            return redirect()->route('jaringan.create')->withErrors($validator->messages())->withInput();
        }

        $this->ModelJaringan->createData($request);
        return redirect()->route('jaringan.index')->with('success', 'Jaringan berhasil ditambahkan.');
    }

    public function disable($id)
    {
        $this->ModelJaringan->disableData($id);
        return redirect()->route('jaringan.index')->with('success', 'Jaringan berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->ModelJaringan->enableData($id);
        return redirect()->route('jaringan.index')->with('success', 'Jaringan berhasil dinonaktifkan.');
    }
}
