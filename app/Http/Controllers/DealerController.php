<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelDealer;
use App\Model\ModelJaringan;
use App\Model\ModelKares;
use App\Model\ModelKabupaten;
use App\Model\ModelLayer;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DealerController extends Controller
{
    private $ModelDealer;
    private $ModelJaringan;
    private $ModelKares;
    private $ModelKabupaten;
    private $ModelLayer;

    public function __construct(ModelDealer $ModelDealer, ModelJaringan $ModelJaringan, ModelKares $ModelKares, ModelKabupaten $ModelKabupaten, ModelLayer $ModelLayer)
    {
        $this->ModelDealer = $ModelDealer;
        $this->ModelJaringan = $ModelJaringan;
        $this->ModelKares = $ModelKares;
        $this->ModelKabupaten = $ModelKabupaten;
        $this->ModelLayer = $ModelLayer;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $karess = $this->ModelKares->getAllData();
        $kabs = $this->ModelKabupaten->getAllData();
        $datas = $this->ModelDealer->getAllData($request);
        return view('dealer.index', compact('datas','kabs','karess','request'));
    }

    public function create()
    {
        $jaringans = $this->ModelJaringan->getAllActiveData();
        $karess = $this->ModelKares->getAllActiveData();
        $kabupatens = $this->ModelKabupaten->getAllActiveData();
        $layers = $this->ModelLayer->getAllActiveData();

        return view('dealer.create', compact('jaringans','karess','kabupatens','layers'));
    }

    public function edit($id)
    {
        $data = $this->ModelDealer->where('id',$id)->first();
        $jaringans = $this->ModelJaringan->getAllActiveData();
        $karess = $this->ModelKares->getAllActiveData();
        $kabupatens = $this->ModelKabupaten->getAllActiveData();
        $layers = $this->ModelLayer->getAllActiveData();

        if(!isset($data))
        {
            return 404;
        }
        return view('dealer.edit', compact('data','jaringans','layers','kabupatens','karess'));
    }

    public function update($id, Request $request)
    {
        $this->ModelDealer->updateData($request,$id);
        return redirect()->route('dealer.index')->with('success','Dealer berhasil diupdate.');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_dealer' => 'required|string|max:250',
            'kode_dealer' => 'required|string|max:12',
            'flag' => 'required',
        ]);

        if ($validator->fails())
        {
            $request->flash();
            return redirect()->route('dealer.create')->withErrors($validator->messages())->withInput();
        }

        $this->ModelDealer->createData($request);
        return redirect()->route('dealer.index')->with('success','Dealer berhasil ditambahkan.');
    }

    public function disable($id)
    {
        $this->ModelDealer->disableData($id);
        return redirect()->route('dealer.index')->with('success','Dealer berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->ModelDealer->enableData($id);
        return redirect()->route('dealer.index')->with('success','Dealer berhasil dinonaktifkan.');
    }

    public function fetch_kab($idKares)
    {
        $data = DB::select(
            'SELECT DISTINCT tbl_kabupaten.id AS id_kab, tbl_kabupaten.nama_kabupaten AS nama_kab
        FROM tbl_kabupaten
        INNER JOIN tbl_dealer on tbl_dealer.id_kabupaten = tbl_kabupaten.id
        INNER JOIN tbl_kares on tbl_dealer.id_kares = tbl_kares.id
        WHERE tbl_kares.id = :idkrs
        ORDER BY tbl_kabupaten.nama_kabupaten ASC',
            ['idkrs' => $idKares]
        );

        return json_encode($data);
    }
}
