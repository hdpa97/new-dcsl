<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UserController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelUploadData;
use App\Model\ModelSurvey;
use App\Model\ModelDealer;
use App\Model\ModelHasilSurveyH1;
use App\Model\ModelHasilSurveyH2;
use App\Model\ModelHasilSurveyH3;
use App\Model\ModelRescheduleData;
use App\Model\ModelMotorPremium;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SurveyController extends Controller
{
    private $ModelUploadData;
    private $ModelSurvey;
    private $ModelDealer;
    private $ModelHasilSurveyH1;
    private $ModelHasilSurveyH2;
    private $ModelHasilSurveyH3;
    private $ModelRescheduleData;
    private $ModelMotorPremium;

    public function __construct(ModelUploadData $ModelUploadData, ModelSurvey $ModelSurvey, ModelDealer $ModelDealer, ModelHasilSurveyH1 $ModelHasilSurveyH1, ModelHasilSurveyH2 $ModelHasilSurveyH2, ModelHasilSurveyH3 $ModelHasilSurveyH3, ModelRescheduleData $ModelRescheduleData, ModelMotorPremium $ModelMotorPremium)
    {
        $this->ModelUploadData = $ModelUploadData;
        $this->ModelSurvey = $ModelSurvey;
        $this->ModelDealer = $ModelDealer;
        $this->ModelHasilSurveyH1 = $ModelHasilSurveyH1;
        $this->ModelHasilSurveyH2 = $ModelHasilSurveyH2;
        $this->ModelHasilSurveyH3 = $ModelHasilSurveyH3;
        $this->ModelRescheduleData = $ModelRescheduleData;
        $this->ModelMotorPremium = $ModelMotorPremium;
        $this->middleware('auth');
        $this->middleware('isSurveyor');
    }

    public function index(Request $request)
    {
        $userData = UserController::getUserData();
        $idSurveyor = $userData['id'];
        $datas = $this->ModelSurvey->getSurveyTersedia($request, $idSurveyor);
        return view('survey.index', compact('datas'));
    }

    public function detail_h1($kodeDealer)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSpesificDataH1($kodeDealer);
        $reschedule = $this->ModelSurvey->getRescheduleDataH1($kodeDealer);
        $kodeData = 'H1';
        return view('survey.detail', compact('datas', 'dataDealer', 'kodeData', 'reschedule'));
    }

    public function detail_h2_h3($kodeDealer)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSpesificDataH2H3($kodeDealer);
        $reschedule = $this->ModelSurvey->getRescheduleDataH23($kodeDealer);
        $kodeData = 'H23';
        return view('survey.detail', compact('datas', 'dataDealer', 'kodeData', 'reschedule'));
    }

    public function panel_penjualan_page_1(Request $request, $kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        return view('survey.panel-penjualan.panel1', compact('datas', 'dataDealer'));
    }

    public function store_page_1(Request $request)
    {
        if ($request->input('kepemilikan') == 'Perusahaan') {
            $this->ModelUploadData->salahSambungSurvey($request->input('idData'));
            $this->ModelHasilSurveyH1->createData($request);
            return redirect()->route('detail.survey.h1', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        } else if ($request->input('status_survey') == 'Tidak Diangkat' || $request->input('status_survey') == 'Tidak Aktif/Mailbox' || $request->input('status_survey') == 'Di Luar Jangkauan' || $request->input('status_survey') == 'No Telp Sibuk') {
            $isEmpty = $this->ModelRescheduleData->where('kode_data', '=', 'H1')->where('id_data', '=', $request->input('idData'))->first();
            if ($isEmpty == null) {
                $this->ModelRescheduleData->createRescheduleData($request, 'H1');
                $this->ModelUploadData->updateRescheduleData($request->input('idData'));
            } else {
                $this->ModelUploadData->updateRescheduleData($request->input('idData'));
            }
            // return view reschedule survey
            return redirect()->route('reschedule.survey', ['kodeDealer' => $request->input('kodeDealer'), 'kodeData' => 'H1', 'idData' => $request->input('idData')]);
        }else if ($request->input('status_survey') == 'No Telp Salah' || $request->input('status_survey') =='Reject' || $request->input('status_survey') =='Telp Diangkat Tapi Ditolak Konsumen' || $request->input('status_survey') =='Salah Sambung' || $request->input('status_survey') =='Terhubung Tapi Pernah Survey'){
            $this->ModelUploadData->salahSambungSurvey($request->input('idData'));
            return redirect()->route('detail.survey.h1', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        }else if ($request->input('status_survey') == 'Telp Dialihkan' || $request->input('status_survey') =='Terhubung Konsumen Tapi Tidak Sesuai Data'){
            $this->ModelUploadData->RejectSurvey($request->input('idData'));
            return redirect()->route('detail.survey.h1', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        }else{
            $checkData = $this->ModelHasilSurveyH1->checkData($request->input('idData'));
            if ($checkData == null) {
                $this->ModelHasilSurveyH1->createData($request);
            } else {
                // $this->ModelHasilSurveyH1->updateData($request, $request->input('idData'));
            }
            return redirect()->route('panel.penjualan.page.2', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
        }
    }

    public function panel_penjualan_page_2($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        return view('survey.panel-penjualan.panel2', compact('datas', 'dataDealer'));
    }

    public function store_page_2(Request $request)
    {
        $this->ModelHasilSurveyH1->updateDataPage2($request, $request->input('idData'));
        return redirect()->route('panel.penjualan.page.3', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_penjualan_page_3($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        $motors = $this->ModelMotorPremium->checkDataMotor($datas->tipe);
        return view('survey.panel-penjualan.panel3', compact('datas', 'dataDealer', 'motors'));
    }

    public function store_page_3(Request $request)
    {
        $this->ModelHasilSurveyH1->updateDataPage3($request, $request->input('idData'));
        return redirect()->route('panel.penjualan.page.4', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_penjualan_page_4($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        return view('survey.panel-penjualan.panel4', compact('datas', 'dataDealer'));
    }

    public function store_page_4(Request $request)
    {
        $this->ModelHasilSurveyH1->updateDataPage4($request, $request->input('idData'));
        return redirect()->route('panel.penjualan.page.5', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_penjualan_page_5($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        $motors = $this->ModelMotorPremium->checkDataMotor($datas->tipe);
        return view('survey.panel-penjualan.panel5', compact('datas', 'dataDealer', 'motors'));
    }

    public function store_page_5(Request $request)
    {
        $this->ModelHasilSurveyH1->updateDataPage5($request, $request->input('idData'));
        return redirect()->route('panel.penjualan.page.6', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_penjualan_page_6($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        return view('survey.panel-penjualan.panel6', compact('datas', 'dataDealer'));
    }

    public function store_page_6(Request $request)
    {
        $this->ModelHasilSurveyH1->updateDataPage6($request, $request->input('idData'));
        return redirect()->route('panel.penjualan.page.7', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_penjualan_page_7($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleData($kodeDealer, $idData);
        return view('survey.panel-penjualan.panel7', compact('datas', 'dataDealer'));
    }

    public function store_page_7(Request $request)
    {
        $this->ModelHasilSurveyH1->updateDataPage7($request, $request->input('idData'));
        $this->ModelUploadData->suksesSurvey($request->input('idData'));
        return redirect()->route('detail.survey.h1', ['kodeDealer' => $request->input('kodeDealer')])->with('success', 'Survey Telah Selesai.');
    }

    //PEMELIHARAAN
    public function panel_pemeliharaan_page_1(Request $request, $kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);

        return view('survey.panel-pemeliharaan.panel1', compact('datas', 'dataDealer'));
    }

    public function storee_page_1(Request $request)
    {
        if ($request->input('status_survey') == 'Terhubung') {
            $checkData = $this->ModelHasilSurveyH2->checkData($request->input('idData'));
            if ($checkData == null) {
                $this->ModelHasilSurveyH2->createData($request);
            } else {
                $this->ModelHasilSurveyH2->updateData($request, $request->input('idData'));
            }
            return redirect()->route('panel.pemeliharaan.page.2', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
        } else if ($request->input('status_survey') == 'Tidak Diangkat' || $request->input('status_survey') == 'Tidak Aktif/Mailbox' || $request->input('status_survey') == 'Di Luar Jangkauan' || $request->input('status_survey') == 'No Telp Sibuk') {
            $isEmpty = $this->ModelRescheduleData->where('kode_data', '=', 'H2')->where('id_data', '=', $request->input('idData'))->first();
            if ($isEmpty == null) {
                $this->ModelRescheduleData->createRescheduleData($request, 'H2');
                $this->ModelUploadData->updateRescheduleData($request->input('idData'));
            } else {
                $this->ModelUploadData->updateRescheduleData($request->input('idData'));
            }
            // return view reschedule survey
            return redirect()->route('reschedule.survey', ['kodeDealer' => $request->input('kodeDealer'), 'kodeData' => 'H2', 'idData' => $request->input('idData')]);
        }else if ($request->input('status_survey') == 'No Telp Salah' || $request->input('status_survey') =='Reject' || $request->input('status_survey') =='Telp Diangkat Tapi Ditolak Konsumen' || $request->input('status_survey') =='Salah Sambung' || $request->input('status_survey') =='Terhubung Tapi Pernah Survey'){
            $this->ModelUploadData->salahSambungSurvey($request->input('idData'));
            return redirect()->route('detail.survey.h23', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        }else if ($request->input('status_survey') == 'Telp Dialihkan' || $request->input('status_survey') =='Terhubung Konsumen Tapi Tidak Sesuai Data'){
            $this->ModelUploadData->RejectSurvey($request->input('idData'));
            return redirect()->route('detail.survey.h23', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        } else {
            $this->ModelUploadData->salahSambungSurvey($request->input('idData'));
            return redirect()->route('detail.survey.h23', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        }
    }

    public function panel_pemeliharaan_page_2($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-pemeliharaan.panel2', compact('datas', 'dataDealer'));
    }

    public function storee_page_2(Request $request)
    {
        $this->ModelHasilSurveyH2->updateDataPage2($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.3', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_pemeliharaan_page_3($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-pemeliharaan.panel3', compact('datas', 'dataDealer'));
    }

    public function storee_page_3(Request $request)
    {
        $this->ModelHasilSurveyH2->updateDataPage3($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.4', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_pemeliharaan_page_4($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        $motors = $this->ModelMotorPremium->checkDataMotor($datas->tipe);
        return view('survey.panel-pemeliharaan.panel4', compact('datas', 'dataDealer', 'motors'));
    }

    public function storee_page_4(Request $request)
    {
        $this->ModelHasilSurveyH2->updateDataPage4($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.5', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_pemeliharaan_page_5($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-pemeliharaan.panel5', compact('datas', 'dataDealer'));
    }

    public function storee_page_5(Request $request)
    {
        $this->ModelHasilSurveyH2->updateDataPage5($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.6', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_pemeliharaan_page_6($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-pemeliharaan.panel6', compact('datas', 'dataDealer'));
    }

    public function storee_page_6(Request $request)
    {
        if ($request->input('h4') == 'Ya') {
            $this->ModelHasilSurveyH3->createData($request);
            return redirect()->route('panel.sukucadang.page.2', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
        }
        $this->ModelHasilSurveyH2->updateDataPage6($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.7', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_pemeliharaan_page_7($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-pemeliharaan.panel7', compact('datas', 'dataDealer'));
    }

    public function storee_page_7(Request $request)
    {
        $this->ModelHasilSurveyH2->updateDataPage7($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.8', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_pemeliharaan_page_8($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-pemeliharaan.panel8', compact('datas', 'dataDealer'));
    }

    public function storee_page_8(Request $request)
    {
        $this->ModelHasilSurveyH2->updateDataPage8($request, $request->input('idData'));
        $this->ModelUploadData->suksesSurvey($request->input('idData'));
        return redirect()->route('detail.survey.h23', ['kodeDealer' => $request->input('kodeDealer')])->with('success', 'Survey Telah Selesai.');
    }

    //SUKU CADANG
    public function panel_sukucadang_page_1($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-sukucadang.panel1', compact('datas', 'dataDealer'));
    }

    public function storeee_page_1(Request $request)
    {
        if ($request->input('status_survey') != 'Terhubung') {
            $checkData = $this->ModelHasilSurveyH2->checkData($request->input('idData'));
            if ($checkData == null) {
                $this->ModelHasilSurveyH2->createData($request);
            } else {
                $this->ModelHasilSurveyH2->updateData($request, $request->input('idData'));
            }
            return redirect()->route('panel.sukucadang.page.2', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
        } else if ($request->input('status_survey') == 'Tidak Diangkat' || $request->input('status_survey') == 'Tidak Aktif/Mailbox' || $request->input('status_survey') == 'Di Luar Jangkauan' || $request->input('status_survey') == 'No Telp Sibuk') {
            $isEmpty = $this->ModelRescheduleData->where('kode_data', '=', 'H3')->where('id_data', '=', $request->input('idData'))->first();
            if ($isEmpty == null) {
                $this->ModelRescheduleData->createRescheduleData($request, 'H3');
                $this->ModelUploadData->updateRescheduleData($request->input('idData'));
            } else {
                $this->ModelUploadData->updateRescheduleData($request->input('idData'));
            }
            // return view reschedule survey
            return redirect()->route('reschedule.survey', ['kodeDealer' => $request->input('kodeDealer'), 'kodeData' => 'H3', 'idData' => $request->input('idData')]);
        } else {
            $this->ModelUploadData->salahSambungSurvey($request->input('idData'));
            return redirect()->route('detail.survey.h23', ['kodeDealer' => $request->input('kodeDealer')])->with('error', 'Survey Dihentikan.');
        }
    }

    public function panel_sukucadang_page_2($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-sukucadang.panel2', compact('datas', 'dataDealer'));
    }

    public function storeee_page_2(Request $request)
    {
        $this->ModelHasilSurveyH3->updateDataPage2($request, $request->input('idData'));
        return redirect()->route('panel.sukucadang.page.3', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_sukucadang_page_3($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-sukucadang.panel3', compact('datas', 'dataDealer'));
    }

    public function storeee_page_3(Request $request)
    {
        $this->ModelHasilSurveyH3->updateDataPage3($request, $request->input('idData'));
        return redirect()->route('panel.sukucadang.page.4', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    public function panel_sukucadang_page_4($kodeDealer, $idData)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleDataH2H3($kodeDealer, $idData);
        return view('survey.panel-sukucadang.panel4', compact('datas', 'dataDealer'));
    }

    public function storeee_page_4(Request $request)
    {
        $this->ModelHasilSurveyH3->updateDataPage4($request, $request->input('idData'));
        return redirect()->route('panel.pemeliharaan.page.7', ['kodeDealer' => $request->input('kodeDealer'), 'idData' => $request->input('idData')]);
    }

    //REPORT
    public function report(Request $request)
    {
        $userData = UserController::getUserData();
        $idSurveyor = $userData['id'];
        $datas = $this->ModelSurvey->getSurveyReport($request, $idSurveyor);
        return view('survey.report', compact('datas'));
    }
}
