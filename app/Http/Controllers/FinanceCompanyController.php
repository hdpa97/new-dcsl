<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelFinanceCompany;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FinanceCompanyController extends Controller
{
    private $ModelFinanceCompany;

    public function __construct(ModelFinanceCompany $ModelFinanceCompany)
    {
        $this->ModelFinanceCompany = $ModelFinanceCompany;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelFinanceCompany->getAllData();
        return view('finance-company.index', compact('datas'));
    }


    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $datas = $this->ModelFinanceCompany->getAllData();
            echo json_encode($datas);
        }
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $data = array(
                'kode_finance_company'    =>  $request->kode_finance_company,
                'nama_finance_company'    =>  $request->nama_finance_company
            );
            $id = DB::table('tbl_finance_company')->insert($data);
            if ($id > 0) {
                echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil ditambahkan.</div>';
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::table('tbl_finance_company')
                ->where('id', $request->id)
                ->update([
                    "kode_finance_company" => $request->kode_finance_company,
                    "nama_finance_company" => $request->nama_finance_company
                ]);
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil diupdate.</div>';
        }
    }

    function delete(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('tbl_finance_company')
                ->where('id', $request->id)
                ->update([
                    'flag' => 0
                ]);

            if ($query) {
                $returnData = array(
                    'status' => 'ok',
                    'alert' => '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil dihapus.</div>'
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'alert' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Proses gagal.</div>'
                );
            }

            echo json_encode($returnData);
        }
    }
}
