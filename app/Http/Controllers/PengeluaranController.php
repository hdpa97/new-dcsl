<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelPengeluaran;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PengeluaranController extends Controller
{
    private $ModelPengeluaran;

    public function __construct(ModelPengeluaran $ModelPengeluaran)
    {
        $this->ModelPengeluaran = $ModelPengeluaran;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelPengeluaran->getAllData();
        return view('Pengeluaran.index', compact('datas'));
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $datas = $this->ModelPengeluaran->getAllData();
            echo json_encode($datas);
        }
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $data = array(
                'kode_pengeluaran'    =>  $request->kode_pengeluaran,
                'pengeluaran'    =>  $request->pengeluaran,
                'huruf_pengeluaran'    =>  $request->huruf_pengeluaran
            );
            $id = DB::table('tbl_kode_pengeluaran')->insert($data);
            if ($id > 0) {
                echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil ditambahkan.</div>';
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::table('tbl_kode_pengeluaran')
                ->where('id', $request->id)
                ->update([
                    "kode_pengeluaran" =>  $request->kode_pengeluaran,
                    "pengeluaran" => $request->pengeluaran,
                    "huruf_pengeluaran" =>  $request->huruf_pengeluaran
                ]);
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil diupdate.</div>';
        }
    }

    function delete(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('tbl_kode_pengeluaran')
                ->where('id', $request->id)
                ->update([
                    'flag' => 0
                ]);

            if ($query) {
                $returnData = array(
                    'status' => 'ok',
                    'alert' => '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil dihapus.</div>'
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'alert' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Proses gagal.</div>'
                );
            }

            echo json_encode($returnData);
        }
    }
}
