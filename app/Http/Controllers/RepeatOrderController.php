<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelRepeatOrder;
use App\Model\ModelJaringan;
use App\Model\ModelKares;
use App\Model\ModelKabupaten;
use App\Model\ModelDealer;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RepeatOrderController extends Controller
{
    private $ModelRepeatOrder;
    private $ModelJaringan;
    private $ModelKares;
    private $ModelKabupaten;
    private $ModelDealer;

    public function __construct(ModelRepeatOrder $ModelRepeatOrder, ModelJaringan $ModelJaringan, ModelKares $ModelKares, ModelKabupaten $ModelKabupaten, ModelDealer $ModelDealer)
    {
        $this->ModelRepeatOrder = $ModelRepeatOrder;
        $this->ModelJaringan = $ModelJaringan;
        $this->ModelKares = $ModelKares;
        $this->ModelKabupaten = $ModelKabupaten;
        $this->ModelDealer = $ModelDealer;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $jars = $this->ModelJaringan->getAllData();
        $karess = $this->ModelKares->getAllData();
        $kabs = $this->ModelKabupaten->getAllData();
        $dealers = $this->ModelDealer->getData();
        if($request->get('tipeData') == 'H23') {
            $datas = $this->ModelRepeatOrder->getSpesificDataH23($request);
        }else{
            $datas = $this->ModelRepeatOrder->getSpesificDataH1($request);
        }
        return view('ro-data.index', compact('jars','karess','kabs','dealers','datas','request'));
    }
}
