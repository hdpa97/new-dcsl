<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelMotorPremium;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MotorPremiumController extends Controller
{
    private $ModelMotorPremium;

    public function __construct(ModelMotorPremium $ModelMotorPremium)
    {
        $this->ModelMotorPremium = $ModelMotorPremium;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelMotorPremium->getAllData();
        return view('motor-premium.index', compact('datas'));
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $datas = $this->ModelMotorPremium->getAllData();
            echo json_encode($datas);
        }
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $data = array(
                'nama_motor'    =>  $request->nama_motor
            );
            $id = DB::table('tbl_motor_premium')->insert($data);
            if ($id > 0) {
                echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil ditambahkan.</div>';
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::table('tbl_motor_premium')
                ->where('id', $request->id)
                ->update([
                    "nama_motor" => $request->nama_motor
                ]);
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil diupdate.</div>';
        }
    }

    function delete(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('tbl_motor_premium')
                ->where('id', $request->id)
                ->update([
                    'flag' => 0
                ]);

            if ($query) {
                $returnData = array(
                    'status' => 'ok',
                    'alert' => '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil dihapus.</div>'
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'alert' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Proses gagal.</div>'
                );
            }

            echo json_encode($returnData);
        }
    }
}
