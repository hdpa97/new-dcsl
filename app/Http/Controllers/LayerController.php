<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelLayer;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class LayerController extends Controller
{
    private $ModelLayer;

    public function __construct(ModelLayer $ModelLayer)
    {
        $this->ModelLayer = $ModelLayer;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelLayer->getAllData();
        return view('layer.index', compact('datas'));
    }

    public function create()
    {
        return view('layer.create');
    }

    public function edit($id)
    {
        $data = $this->ModelLayer->where('id', $id)->first();

        if (!isset($data)) {
            return 404;
        }
        return view('layer.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $this->ModelLayer->updateData($request, $id);
        return redirect()->route('layer.index')->with('success', 'Layer berhasil diupdate.');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_layer' => 'required|string|max:250',
            'flag' => 'required',
        ]);

        if ($validator->fails()) 
        {   
            $request->flash();
            return redirect()->route('layer.create')->withErrors($validator->messages())->withInput();
        }

        $this->ModelLayer->createData($request);
        return redirect()->route('layer.index')->with('success', 'Layer berhasil ditambahkan.');
    }

    public function disable($id)
    {
        $this->ModelLayer->disableData($id);
        return redirect()->route('layer.index')->with('success', 'Layer berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->ModelLayer->enableData($id);
        return redirect()->route('layer.index')->with('success', 'Layer berhasil dinonaktifkan.');
    }
}
