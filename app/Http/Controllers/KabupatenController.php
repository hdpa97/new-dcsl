<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelKabupaten;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class KabupatenController extends Controller
{
    private $ModelKabupaten;

    public function __construct(ModelKabupaten $ModelKabupaten)
    {
        $this->ModelKabupaten = $ModelKabupaten;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelKabupaten->getAllData();
        return view('kabupaten.index', compact('datas'));
    }

    public function create()
    {
        return view('kabupaten.create');
    }

    public function edit($id)
    {
        $data = $this->ModelKabupaten->where('id',$id)->first();

        if(!isset($data))
        {
            return 404;
        }
        return view('kabupaten.edit', compact('data'));
    }

    public function update($id, Request $request)
    {
        $this->ModelKabupaten->updateData($request,$id);
        return redirect()->route('kabupaten.index')->with('success','Kabupaten berhasil diupdate.');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kabupaten' => 'required|string|max:250',
            'id_kares' => 'required|min:1',
            'flag' => 'required',
        ]);

        if ($validator->fails()) 
        {   
            $request->flash();
            return redirect()->route('kabupaten.create')->withErrors($validator->messages())->withInput();
        }

        $this->ModelKabupaten->createData($request);
        return redirect()->route('kabupaten.index')->with('success','Kabupaten berhasil ditambahkan.');
    }

    public function disable($id)
    {
        $this->ModelKabupaten->disableData($id);
        return redirect()->route('kabupaten.index')->with('success','Kabupaten berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->ModelKabupaten->enableData($id);
        return redirect()->route('kabupaten.index')->with('success','Kabupaten berhasil dinonaktifkan.');
    }

}
