<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelRescheduleData;
use App\Model\ModelDealer;
use App\Model\ModelSurvey;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class RescheduleController extends Controller
{
    private $ModelRescheduleData;
    private $ModelDealer;
    private $ModelSurvey;

    public function __construct(ModelRescheduleData $ModelRescheduleData, ModelDealer $ModelDealer, ModelSurvey $ModelSurvey)
    {
        $this->ModelRescheduleData = $ModelRescheduleData;
        $this->ModelDealer = $ModelDealer;
        $this->ModelSurvey = $ModelSurvey;
        $this->middleware('auth');
        $this->middleware('isSurveyor');
    }

    public function create($kodeDealer,$kodeData,$idData)
    {
        $data = $this->ModelRescheduleData->where('id_data',$idData)->where('kode_data',$kodeData)->first();
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelSurvey->getSingleResheduleData($kodeDealer, $idData);
        return view('reschedule.create', compact('data', 'dataDealer', 'datas'));
    }

    public function update($id, Request $request)
    {
        $this->ModelRescheduleData->updateData($request,$id);
        if($request->kode_data=='H1'){
            return redirect()->route('detail.survey.h1',['kodeDealer' => $request->kode_dealer])->with('success','Data berhasil direschedule.');
        }else{
            return redirect()->route('detail.survey.h23',['kodeDealer' => $request->kode_dealer])->with('success','Data berhasil direschedule.');
        }
        
    }

}
