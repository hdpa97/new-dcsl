<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelPekerjaan;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PekerjaanController extends Controller
{
    private $ModelPekerjaan;

    public function __construct(ModelPekerjaan $ModelPekerjaan)
    {
        $this->ModelPekerjaan = $ModelPekerjaan;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelPekerjaan->getAllData();
        return view('Pekerjaan.index', compact('datas'));
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $datas = $this->ModelPekerjaan->getAllData();
            echo json_encode($datas);
        }
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $data = array(
                'kode_pekerjaan'    =>  $request->kode_pekerjaan,
                'nama_pekerjaan'    =>  $request->nama_pekerjaan
            );
            $id = DB::table('tbl_kode_pekerjaan')->insert($data);
            if ($id > 0) {
                echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil ditambahkan.</div>';
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::table('tbl_kode_pekerjaan')
                ->where('id', $request->id)
                ->update([
                    "kode_pekerjaan" =>  $request->kode_pekerjaan,
                    "nama_pekerjaan" => $request->nama_pekerjaan
                ]);
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil diupdate.</div>';
        }
    }

    function delete(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('tbl_kode_pekerjaan')
                ->where('id', $request->id)
                ->update([
                    'flag' => 0
                ]);

            if ($query) {
                $returnData = array(
                    'status' => 'ok',
                    'alert' => '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil dihapus.</div>'
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'alert' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Proses gagal.</div>'
                );
            }

            echo json_encode($returnData);
        }
    }
}
