<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\ModelDealer;
use DB;
use Auth;
use Carbon\Carbon;
use App\Rules\CurrentPasswordCheckRule;

class UserController extends Controller
{
    private $User;
    private $ModelDealer;

    public function __construct(User $User, ModelDealer $ModelDealer)
    {
        $this->User = $User;
        $this->ModelDealer = $ModelDealer;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin')->only(['edit',]);
    }


        /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function index(Request $request)
    {
        $users = $this->User->getData();
        return view('users.index', compact('users'));
    }

    // SETTING PROFILE //

    public function setting()
    {
        $data = $this->getUserData();
        return view('users.setting', compact('data'));
    }

    public function updateProfile(Request $request)
    {
        $data = $this->getUserData();
        $id = $data['id'];

        $attributes = [
            'email' => 'Email Address',
            'npk' => 'NPK',
            'name' => 'Nama Lengkap'
        ];

        $request->validate([
            'email' => ['required', 'string', 'max:50', \Illuminate\Validation\Rule::unique('users')->ignore($id)],
            'npk' => ['required', 'min:1', \Illuminate\Validation\Rule::unique('users')->ignore($id)],
            'name' => ['required', 'min:3']
        ], [], $attributes);

        $this->User->updateProfile($request, $id);
        return redirect()->route('users.setting')->with('success','Profile berhasil diupdate.');
    }

    public function updatePassword(Request $request)
    {
        $data = $this->getUserData();
        $id = $data['id'];

        $attributes = [
            'old_password' => 'Current Password',
            'password' => 'New Password'
        ];

        $request->validate([
            'old_password' => ['required', 'min:8', new CurrentPasswordCheckRule],
            'password' => ['required', 'min:8', 'confirmed', 'different:old_password'],
            'password_confirmation' => ['required', 'min:8'],
        ], [], $attributes);

        $this->User->updatePassword($request, $id);
        return redirect()->route('users.setting')->with('success','Password berhasil diupdate.');
    }

    // END SETTING PROFILE //

    public function edit($id)
    {
        $data = $this->User->where('id',$id)->first();
        $allDealer = $this->ModelDealer->getData();
        $dataDealer = $this->ModelDealer->getDealerFromSurveyor($id);

        if(!isset($data))
        {
            return 404;
        }
        return view('users.edit', compact('data','dataDealer','allDealer'));
    }

    public function update($id, Request $request)
    {
        $attributes = [
            'email' => 'Email Address',
            'npk' => 'NPK'
        ];

        $request->validate([
            'email' => ['required', 'string', 'max:50', \Illuminate\Validation\Rule::unique('users')->ignore($id)],
            'npk' => ['required', 'min:1', \Illuminate\Validation\Rule::unique('users')->ignore($id)]
        ], [], $attributes);

        $this->User->updateData($request,$id);
        return redirect()->route('users.index')->with('success','User berhasil diupdate.');
    }

    public function disable($id)
    {
        $this->User->disableData($id);
        return redirect()->route('users.index')->with('success', 'User berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->User->enableData($id);
        return redirect()->route('users.index')->with('success', 'User berhasil diaktifkan.');
    }

    public static function getUserData()
    {
        $userData = array(
            "name" => Auth::user()->name,
            "email" => Auth::user()->email,
            "id" => Auth::user()->id,
            "role" => Auth::user()->role,
            "npk" => Auth::user()->npk,
            "flag" => Auth::user()->flag
        );
        return $userData;
    }
}
