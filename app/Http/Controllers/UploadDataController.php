<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Model\ModelUploadData;
use DB;
use Auth;
use Importer;
use Exporter;
use File;

class UploadDataController extends Controller
{
    private $ModelUploadData;

    public function __construct(ModelUploadData $ModelUploadData)
    {
        $this->ModelUploadData = $ModelUploadData;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelUploadData->getAllData();
        return view('upload-data.index', compact('datas'));
    }

    public function create(Request $request)
    {
        return view('upload-data.create');
    }

    public function disable($id)
    {
        $this->ModelUploadData->disableData($id);
        return redirect()->route('upload-data.index')->with('success','Customer berhasil dinonaktifkan.');
    }

    public function enable($id)
    {
        $this->ModelUploadData->enableData($id);
        return redirect()->route('upload-data.index')->with('success','Customer berhasil diaktifkan.');
    }

    public function importExcel(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:5000|mimes:xlsx,xls',
            'kode_data' => 'required'
        ]);

        if ($validator->fails())
        {
            $request->flash();
            return redirect()->back()->with(['error' => $validator->messages()]);
        }
        else
        {
            $dataTime = date('Ymd_His');
            $file = $request->file('file');
            $fileName = $dataTime . '-' . $file->getClientOriginalName();
            $savePath = public_path('/upload/');
            $file->move($savePath, $fileName);

            $excel = Importer::make('Excel');
            $excel->load($savePath.$fileName);
            $excel->setSheet(1);
            $collection = $excel->getCollection();
            $countColl = count($collection[0]);

            if($countColl == 102){
                for($row=1; $row<sizeof($collection); $row++)
                {
                    $newNoHP = str_replace("'","",($collection[$row][37]));
                    $setNoHP = $this->setNoHP($newNoHP);
                    // $cekDuplikatData = $this->ModelUploadData->cekDuplikatData($collection[$row][6],$collection[$row][14],$setNoHP);
                    $cekDuplikatData = $this->ModelUploadData->cekDuplikatNoRangka($collection[$row][1]);
                    $newKodeDealer = str_replace("'","",($collection[$row][14]));
                    $cekKodeDealer = $this->ModelUploadData->cekKodeDealer($newKodeDealer);
                    $newKodePekerjaan = str_replace("'","",($collection[$row][33]));
                    $cekKodePekerjaan = $this->ModelUploadData->cekKodePekerjaan($newKodePekerjaan);
                    $newKodeFinance = str_replace("'","",($collection[$row][16]));
                    $cekKodeFinance = $this->ModelUploadData->cekKodeFinance($newKodeFinance);
                    $rowExcel = $row+1;

                    if($cekDuplikatData!=null) //true
                    {
                        File::delete($savePath.'/'.$fileName);
                        // return redirect()->back()->with(['error'=>'Proses Berhenti. Data: '.$cekDuplikatData->nama_lengkap.'|'.$cekDuplikatData->kode_dealer.'|'.$cekDuplikatData->no_hp.' sudah ada pada baris Excel ke-'.$rowExcel]);
                        return redirect()->back()->with(['error'=>'Proses Berhenti. Nomor Rangka : '.$cekDuplikatData->nomor_rangka.' sudah ada pada baris Excel ke-'.$rowExcel]);
                    }
                    if($cekKodeDealer==null)
                    {
                        File::delete($savePath.'/'.$fileName);
                        return redirect()->back()->with(['error'=>'Proses Berhenti. Kode Dealer : '.$collection[$row][14].' tidak ditemukan pada baris Excel ke-'.$rowExcel]);
                    }
                    if($cekKodePekerjaan==null)
                    {
                        File::delete($savePath.'/'.$fileName);
                        return redirect()->back()->with(['error'=>'Proses Berhenti. Kode Pekerjaan : '.$collection[$row][33].' tidak ditemukan pada baris Excel ke-'.$rowExcel]);
                    }
                    if($cekKodeFinance==null)
                    {
                        File::delete($savePath.'/'.$fileName);
                        return redirect()->back()->with(['error'=>'Proses Berhenti. Kode Finance Company : '.$collection[$row][15].' tidak ditemukan pada baris Excel ke-'.$rowExcel]);
                    }

                }
                for($row=1; $row<sizeof($collection); $row++)
                {
                    $oldDate = str_replace("'","", ($collection[$row][25]));
                    $newDate = $oldDate[4].$oldDate[5].$oldDate[6].$oldDate[7].'-'.$oldDate[2].$oldDate[3].'-'.$oldDate[0].$oldDate[1];
                    $dt = \Carbon\Carbon::parse($newDate)->toDateTimeString();
                    $umurSekarang = $this->getLiveUmur($dt);

                    $oldTglCetak = str_replace("'","", ($collection[$row][4]));

                    if($oldTglCetak==""){
                        $newTglCetak = "0000-00-00";
                    }else{
                        $newTglCetak = $oldTglCetak[4].$oldTglCetak[5].$oldTglCetak[6].$oldTglCetak[7].'-'.$oldTglCetak[2].$oldTglCetak[3].'-'.$oldTglCetak[0].$oldTglCetak[1];
                    }
                    $oldTglMohon = str_replace("'","", ($collection[$row][5]));
                    if($oldTglMohon==""){
                        $newTglMohon = "0000-00-00";
                    }else{
                        $newTglMohon = $oldTglMohon[4].$oldTglMohon[5].$oldTglMohon[6].$oldTglMohon[7].'-'.$oldTglMohon[2].$oldTglMohon[3].'-'.$oldTglMohon[0].$oldTglMohon[1];
                    }

                    $newNoHP = str_replace("'","",($collection[$row][37]));
                    $setNoHP = $this->setNoHP($newNoHP);
                    $newNoTelp = str_replace("'","",($collection[$row][38]));
                    $setNoTelp = $this->setNoHP($newNoTelp);

                    ModelUploadData::create([
                        'kode_dealer' => $collection[$row][14],
                        'sales_person' => $collection[$row][44],
                        'nomor_rangka' => $collection[$row][1],
                        'kode_mesin' => $collection[$row][2],
                        'nomor_mesin' => $collection[$row][3],
                        'tgl_cetak' => $newTglCetak,
                        'tgl_mohon' => $newTglMohon,
                        'nama_lengkap' => $collection[$row][6],
                        'alamat' => $collection[$row][7],
                        'kelurahan' => $collection[$row][8],
                        'kecamatan' => $collection[$row][9],
                        'kode_kota' => str_replace("'","",($collection[$row][10])),
                        'kode_prov' => str_replace("'","",($collection[$row][12])),
                        'cash_credit' => $collection[$row][13],
                        'finance_company' => $collection[$row][16],
                        'jenis_sales' => $collection[$row][23],
                        'gender' => $collection[$row][24],
                        'tgl_lahir' => $newDate,
                        'kode_agama' => $collection[$row][32],
                        'kode_pekerjaan' => $collection[$row][33],
                        'kode_pendidikan' => $collection[$row][35],
                        'kode_pengeluaran' => $collection[$row][34],
                        'no_hp' => $setNoHP,
                        'no_telp' => $setNoTelp,
                        'umur' => $umurSekarang,
                        'tipe' => $collection[$row][48],
                        'tiga_jenis'=> $collection[$row][50],
                        'down_payment' => $collection[$row][17],
                        'tenor' => $collection[$row][18],
                        'flag' => 1,
                        'kode_data' => $request->input('kode_data'),
                        'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                        'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                    ]);
                }

                // File::delete($savePath.'/'.$fileName);
                return redirect()->route('upload-data.index')->with(['success'=>'File Uploaded Successfully.']);
            }else{
                // File::delete($savePath.'/'.$fileName);
                return redirect()->back()->with(['error'=>'Error Data. Template does not match.']);
            }
        }
    }

    public function getLiveUmur($tglLahir)
    {
        $now = strtotime(date("Y-m-d"));
        $tgl_lahir = strtotime($tglLahir);
        $diff = abs($now - $tgl_lahir);
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        $usia = $years.' tahun '.$months.' bulan';

        return $usia;
    }

    public function setNoHP($nohp)
    {
        if(!preg_match('/[^+0-9]/',trim($nohp))){
            if(substr(trim($nohp), 0, 2)=='62'){
                return $hp = '08'.substr(trim($nohp), 1);
            }
            elseif(substr(trim($nohp), 0, 1)=='8'){
                return $hp = '08'.substr(trim($nohp), 1);
            }
            else
            {
                return $nohp;
            }
        }
    }
}
