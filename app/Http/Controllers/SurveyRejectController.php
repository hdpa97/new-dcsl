<?php

namespace App\Http\Controllers;

use App\Http\Controllers\UserController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelUploadData;
use App\Model\ModelSurvey;
use App\Model\ModelDealer;
use App\Model\ModelHasilSurveyH1;
use App\Model\ModelHasilSurveyH2;
use App\Model\ModelHasilSurveyH3;
use App\Model\ModelRescheduleData;
use App\Model\ModelMotorPremium;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class SurveyRejectController extends Controller
{
    private $ModelUploadData;
    private $ModelSurvey;
    private $ModelDealer;
    private $ModelHasilSurveyH1;
    private $ModelHasilSurveyH2;
    private $ModelHasilSurveyH3;
    private $ModelRescheduleData;
    private $ModelMotorPremium;

    public function __construct(ModelUploadData $ModelUploadData, ModelSurvey $ModelSurvey, ModelDealer $ModelDealer, ModelHasilSurveyH1 $ModelHasilSurveyH1, ModelHasilSurveyH2 $ModelHasilSurveyH2, ModelHasilSurveyH3 $ModelHasilSurveyH3, ModelRescheduleData $ModelRescheduleData, ModelMotorPremium $ModelMotorPremium)
    {
        $this->ModelUploadData = $ModelUploadData;
        $this->ModelSurvey = $ModelSurvey;
        $this->ModelDealer = $ModelDealer;
        $this->ModelHasilSurveyH1 = $ModelHasilSurveyH1;
        $this->ModelHasilSurveyH2 = $ModelHasilSurveyH2;
        $this->ModelHasilSurveyH3 = $ModelHasilSurveyH3;
        $this->ModelRescheduleData = $ModelRescheduleData;
        $this->ModelMotorPremium = $ModelMotorPremium;
        $this->middleware('auth');
        $this->middleware('isSurveyor');
    }

    public function index(Request $request)
    {
        $userData = UserController::getUserData();
        $idSurveyor = $userData['id'];
        $datas = $this->ModelSurvey->getSurveyReject($request, $idSurveyor);
        return view('survey-reject.index', compact('datas'));
    }
}