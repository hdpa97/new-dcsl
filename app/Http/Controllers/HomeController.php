<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use Auth;
use DB;
use Hash;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isSuperAdmin')->only(['index']);
        $this->middleware('isSurveyor')->only(['indexSurveyor']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->updateLiveUmur();
        //$getAchPerson = $this->getAchPerson($request);
        //return view('home-admin');
        $getAchPerson = $this->getAchPerson($request);
        $get3TopDealer = $this->get3TopDealer($request);
        return view('home-admin', compact('getAchPerson'));
    }

    public function indexSurveyor(Request $request)
    {
        $this->updateLiveUmur();
        $getAch = $this->getAch($request);
        $get3TopDealer = $this->get3TopDealer($request);
        return view('home-surveyor', compact('getAch','get3TopDealer'));
    }

    // Admin Function
    public function getAchPerson(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('users','users.id','tbl_dealer.id_surveyor')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        return $data
        ->where('tbl_upload_data.updated_at', '>=', Carbon::now()->subMonth(3)->toDateTimeString())
        ->groupby('tbl_dealer.id_surveyor')
        ->first(['tbl_dealer.id_surveyor','users.name',DB::raw('count(IF(tbl_upload_data.flag = 1,1,NULL))+count(IF(tbl_upload_data.flag = 4,1,NULL)) tersedia'),
        DB::raw('count(IF(tbl_upload_data.flag = 3,1,NULL)) selesai'),DB::raw('count(IF(tbl_upload_data.flag = 5,1,NULL))+count(IF(tbl_upload_data.flag = 2,1,NULL)) reject')]);
}

    // Surveyor Function
    public function getAch(Request $request)
    {
        $getUser = UserController::getUserData();

        $data = DB::table('tbl_dealer')
        ->join('users','users.id','tbl_dealer.id_surveyor')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        return $data
        ->where('tbl_dealer.id_surveyor','=', $getUser['id'])
        ->where('tbl_upload_data.updated_at', '>=', Carbon::now()->subMonth(3)->toDateTimeString())
        ->groupby('tbl_dealer.id_surveyor')
        ->first(['tbl_dealer.id_surveyor','users.name',DB::raw('count(IF(tbl_upload_data.flag = 1,1,NULL))+count(IF(tbl_upload_data.flag = 4,1,NULL)) tersedia'),
        DB::raw('count(IF(tbl_upload_data.flag = 3,1,NULL)) selesai'),DB::raw('count(IF(tbl_upload_data.flag = 5,1,NULL))+count(IF(tbl_upload_data.flag = 2,1,NULL)) reject')]);
    }

    public function get3TopDealer(Request $request)
    {
        $getUser = UserController::getUserData();

        $data = DB::table('tbl_dealer')
        ->join('users','users.id','tbl_dealer.id_surveyor')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        return $data
        ->where('tbl_dealer.id_surveyor','=', $getUser['id'])
        ->where('tbl_upload_data.updated_at', '>=', Carbon::now()->subMonth(3)->toDateTimeString())
        ->groupby('tbl_dealer.kode_dealer')
        ->orderby('selesai','desc')
        ->skip(0)->take(3)
        ->get(['tbl_dealer.id_surveyor','users.name', 'tbl_dealer.nama_dealer', DB::raw('count(IF(tbl_upload_data.flag = 1,1,NULL))+count(IF(tbl_upload_data.flag = 4,1,NULL)) tersedia'),
        DB::raw('count(IF(tbl_upload_data.flag = 3,1,NULL)) selesai'),DB::raw('count(IF(tbl_upload_data.flag = 5,1,NULL))+count(IF(tbl_upload_data.flag = 2,1,NULL)) reject')]);
    }

    public function updateLiveUmur()
    {
        DB::raw('UPDATE tbl_upload_data SET umur = DATE_FORMAT( FROM_DAYS( DATEDIFF( CURRENT_DATE, tgl_lahir ) ), "%y Tahun %m Bulan %d Hari" )');
        // DB::select('UPDATE tbl_upload_data SET umur = DATE_FORMAT( FROM_DAYS( DATEDIFF( CURRENT_DATE, tgl_lahir ) ), "%y Tahun %m Bulan %d Hari" )');
    }
}
