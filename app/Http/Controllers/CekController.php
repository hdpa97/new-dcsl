<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelCek;
use App\Model\ModelKares;
use App\Model\ModelKabupaten;
use App\Model\ModelDealer;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CekController extends Controller
{
    private $ModelCek;

    public function __construct(ModelCek $ModelCek, ModelKares $ModelKares, ModelKabupaten $ModelKabupaten, ModelDealer $ModelDealer)
    {
        $this->ModelCek = $ModelCek;
        $this->ModelKares = $ModelKares;
        $this->ModelKabupaten = $ModelKabupaten;
        $this->ModelDealer = $ModelDealer;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $karess = $this->ModelKares->getAllData();
        $kabs = $this->ModelKabupaten->getAllData();
        $dealers = $this->ModelDealer->getData();
        $datas = $this->ModelCek->getAllData($request);
        return view('cek.index', compact('karess','kabs','dealers','datas','request'));
    }

    public function fetch_kab($idKares)
    {
        $data = DB::select('SELECT DISTINCT tbl_kabupaten.id AS id_kab, tbl_kabupaten.nama_kabupaten AS nama_kab
        FROM tbl_kabupaten
        INNER JOIN tbl_dealer on tbl_dealer.id_kabupaten = tbl_kabupaten.id
        INNER JOIN tbl_kares on tbl_dealer.id_kares = tbl_kares.id
        WHERE tbl_kares.id = :idkrs
        ORDER BY tbl_kabupaten.nama_kabupaten ASC',
        ['idkrs' => $idKares]);

        return json_encode($data);
    }

    public function fetch_dealer($idKabupaten)
    {
        $data = DB::select('SELECT DISTINCT tbl_dealer.id AS id_dlr, tbl_dealer.nama_dealer AS nama_dlr
        FROM tbl_dealer
        INNER JOIN tbl_kabupaten on tbl_dealer.id_kabupaten = tbl_kabupaten.id
        INNER JOIN tbl_kares on tbl_dealer.id_kares = tbl_kares.id
        WHERE tbl_kabupaten.id = :idkab
        ORDER BY tbl_dealer.nama_dealer ASC',
        ['idkab' => $idKabupaten]);

        return json_encode($data);
    }

    public function detail_h1($kodeDealer)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelCek->getSpesificDataH1($kodeDealer);
        $kodeData = 'H1';
        return view('cek.detail', compact('datas','dataDealer','kodeData'));
    }

    public function detail_h2($kodeDealer)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelCek->getSpesificDataH2($kodeDealer);
        $kodeData = 'H2';
        return view('cek.detail', compact('datas','dataDealer','kodeData'));
    }

    public function detail_h3($kodeDealer)
    {
        $dataDealer = $this->ModelDealer->getSingleData($kodeDealer);
        $datas = $this->ModelCek->getSpesificDataH3($kodeDealer);
        $kodeData = 'H3';
        return view('cek.detail', compact('datas','dataDealer','kodeData'));
    }
}
