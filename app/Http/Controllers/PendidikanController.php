<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelPendidikan;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PendidikanController extends Controller
{
    private $ModelPendidikan;

    public function __construct(ModelPendidikan $ModelPendidikan)
    {
        $this->ModelPendidikan = $ModelPendidikan;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelPendidikan->getAllData();
        return view('pendidikan.index', compact('datas'));
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $datas = $this->ModelPendidikan->getAllData();
            echo json_encode($datas);
        }
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $data = array(
                'kode_pendidikan'    =>  $request->kode_pendidikan,
                'nama_pendidikan'    =>  $request->nama_pendidikan
            );
            $id = DB::table('tbl_kode_pendidikan')->insert($data);
            if ($id > 0) {
                echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil ditambahkan.</div>';
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::table('tbl_kode_pendidikan')
                ->where('id', $request->id)
                ->update([
                    "kode_pendidikan" =>  $request->kode_pendidikan,
                    "nama_pendidikan" => $request->nama_pendidikan
                ]);
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil diupdate.</div>';
        }
    }

    function delete(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('tbl_kode_pendidikan')
                ->where('id', $request->id)
                ->update([
                    'flag' => 0
                ]);

            if ($query) {
                $returnData = array(
                    'status' => 'ok',
                    'alert' => '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil dihapus.</div>'
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'alert' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Proses gagal.</div>'
                );
            }

            echo json_encode($returnData);
        }
    }
}
