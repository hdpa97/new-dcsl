<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Model\ModelAgama;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AgamaController extends Controller
{
    private $ModelAgama;

    public function __construct(ModelAgama $ModelAgama)
    {
        $this->ModelAgama = $ModelAgama;
        $this->middleware('auth');
        $this->middleware('isSuperAdmin');
    }

    public function index(Request $request)
    {
        $datas = $this->ModelAgama->getAllData();
        return view('agama.index', compact('datas'));
    }

    public function fetch(Request $request)
    {
        if ($request->ajax()) {
            $datas = $this->ModelAgama->getAllData();
            echo json_encode($datas);
        }
    }

    public function add(Request $request)
    {
        if ($request->ajax()) {
            $data = array(
                'kode_agama'    =>  $request->kode_agama,
                'nama_agama'    =>  $request->nama_agama
            );
            $id = DB::table('tbl_kode_agama')->insert($data);
            if ($id > 0) {
                echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil ditambahkan.</div>';
            }
        }
    }

    public function update(Request $request)
    {
        if ($request->ajax()) {
            DB::table('tbl_kode_agama')
                ->where('id', $request->id)
                ->update([
                    "kode_agama" =>  $request->kode_agama,
                    "nama_agama" => $request->nama_agama
                ]);
            echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil diupdate.</div>';
        }
    }

    function delete(Request $request)
    {
        if ($request->ajax()) {
            $query = DB::table('tbl_kode_agama')
                ->where('id', $request->id)
                ->update([
                    'flag' => 0
                ]);

            if ($query) {
                $returnData = array(
                    'status' => 'ok',
                    'alert' => '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button>Data berhasil dihapus.</div>'
                );
            } else {
                $returnData = array(
                    'status' => 'error',
                    'alert' => '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>Proses gagal.</div>'
                );
            }

            echo json_encode($returnData);
        }
    }
}
