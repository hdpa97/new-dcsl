<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'npk', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getData()
    {
        return $this->get(['id', 'name', 'email', 'npk', 'flag', 'role']);
    }

    public function disableData($id)
    {
        if ($id != 1) {
            $this->where('id', $id)->update([
                'flag' => 0,
            ]);
        }
    }

    public function enableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 1,
        ]);
    }

    public function updateData(Request $request, $id)
    {
        $tempPw = Hash::make($request->pw);

        if ($request->pw != "" || $request->pw != null) {
            $this->where('id', $id)
                ->update([
                    'password' => $tempPw
                ]);
        }

        $this->where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'npk' => $request->input('npk'),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]);

        // set flag surveyor 0 to all dealer
        DB::table('tbl_dealer')->where('id_surveyor', $request->input('id_user'))->update(array('id_surveyor' => 0));

        // update surveyor
        DB::table('tbl_dealer')->whereIn('id', $request->input('id_dealer'))->update(array('id_surveyor' => $request->input('id_user')));
    }

    public function updateProfile(Request $request, $id)
    {
        $this->where('id', $id)
            ->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'npk' => $request->input('npk'),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]);
    }

    public function updatePassword(Request $request, $id)
    {
        $this->where('id', $id)
            ->update([
                'password' => Hash::make($request->input('password_confirmation')),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]);
    }
}
