<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelRepeatOrder extends Authenticatable
{
    public function getSpesificDataH1(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->join('tbl_hasil_survey_h1','tbl_hasil_survey_h1.id_data','tbl_upload_data.id');

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)->where('tbl_hasil_survey_h1.n2_years','!=',null)
        ->get(['tbl_upload_data.kode_dealer','tbl_dealer.nama_dealer','tbl_upload_data.nama_lengkap','tbl_upload_data.no_telp','tbl_upload_data.no_hp','tbl_upload_data.umur','tbl_kode_pekerjaan.nama_pekerjaan','tbl_kode_pengeluaran.kode_pengeluaran',
        'tbl_hasil_survey_h1.n2_month as bulan','tbl_hasil_survey_h1.n2_years as tahun',
        'tbl_hasil_survey_h1.n3 as tipe','tbl_hasil_survey_h1.n4 as untuk','tbl_hasil_survey_h1.n5 as merk',
        'tbl_kode_pekerjaan.kode_pekerjaan','tbl_kode_pengeluaran.pengeluaran']);
    }

    public function getSpesificDataH23(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->join('tbl_hasil_survey_h2','tbl_hasil_survey_h2.id_data','tbl_upload_data.id');

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)->where('tbl_hasil_survey_h2.n3_years','!=',null)
        ->get(['tbl_upload_data.kode_dealer','tbl_dealer.nama_dealer','tbl_upload_data.nama_lengkap','tbl_upload_data.no_telp','tbl_upload_data.no_hp','tbl_upload_data.umur','tbl_kode_pekerjaan.nama_pekerjaan','tbl_kode_pengeluaran.kode_pengeluaran',
        'tbl_hasil_survey_h2.n3_month as bulan','tbl_hasil_survey_h2.n3_years as tahun',
        'tbl_hasil_survey_h2.n4 as tipe','tbl_hasil_survey_h2.n5 as untuk','tbl_hasil_survey_h2.n6 as merk',
        'tbl_kode_pekerjaan.kode_pekerjaan','tbl_kode_pengeluaran.pengeluaran']);
    }

    public function getAllData()
    {
        return $this->get();
    }

    public function getAllActiveData()
    {
        return $this->where('flag', 1)->get();
    }

    public function createData(Request $request)
    {
        $this->create(
            []
        );
    }
}
