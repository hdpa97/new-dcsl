<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelHasilSurveyH3 extends Authenticatable
{
    protected $table = "tbl_hasil_survey_h3";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = [
        'id_data', 'status_survey', 'kode_data',
        'T1', 'T1_NOTE', 'A1_1', 'A1_2', 'B1_1', 'B1_2', 'B1_3', 'B1_4',
        'C1', 'C1_A', 'C1_B', 'C2', 'C3', 'C4', 'C5', 'D1', 'D2', 'D3_1', 'D3_2', 'D3_2A', 'D3_3', 'D3_3A', 'D3_3B', 'D4', 'D5', 'E1', 'E2',
        'F1', 'G1', 'G2', 'G3', 'H1', 'H1_NOTE',
        'I4', 'I4_NOTE', 'I5', 'I6', 'I7', 'I8', 'I8_NOTE', 'I9', 'I9_NOTE', 'K1',
        'N1', 'N2', 'N3_MONTH', 'N3_YEARS', 'N3_NOTE', 'N4', 'N4_NOTE', 'N5', 'N5_NOTE', 'N6', 'N6_NOTE', 'N7', 'N8'
    ];

    public function checkData($idData)
    {
        return $this->select('*')->where('id_data', $idData)->first();
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'id_data' => $request->input('idData'),
                'status_survey' => "Terhubung",
                'kode_data' => 'H3',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]
        );
    }

    public function updateData(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'id_data' => $request->input('idData'),
                    'status_survey' => $request->input('status_survey'),
                    'kode_data' => 'H3',
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    // SAVE PER PAGE
    public function updateDataPage2(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'T1' => $request->input('t1'),
                    'T1_NOTE' => $request->input('t1_note'),
                    'A1_1' => $request->input('a1_1'),
                    'A1_2' => $request->input('a1_2'),
                    'B1_1' => $request->input('b1_1'),
                    'B1_2' => $request->input('b1_2'),
                    'B1_3' => $request->input('b1_3'),
                    'B1_4' => $request->input('b1_4'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage3(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'C1' => $request->input('c1'),
                    'C1_A' => $request->input('c1_a'),
                    'C1_B' => $request->input('c1_b'),
                    'C2' => $request->input('c2'),
                    'C3' => $request->input('c3'),
                    'C4' => $request->input('c4'),
                    'C5' => $request->input('c5'),
                    'D1' => $request->input('d1'),
                    'D2' => $request->input('d2'),
                    'D3_1' => $request->input('d3_1'),
                    'D3_2' => $request->input('d3_2'),
                    'D3_2A' => $request->input('d3_2a'),
                    'D3_3' => $request->input('d3_3'),
                    'D3_3A' => $request->input('d3_3a'),
                    'D3_3B' => $request->input('d3_3b'),
                    'D4' => $request->input('d4'),
                    'D5' => $request->input('d5'),
                    'E1' => $request->input('e1'),
                    'E2' => $request->input('e2'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage4(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'F1' => $request->input('f1'),
                    'G1' => $request->input('g1'),
                    'G2' => $request->input('g2'),
                    'G3' => $request->input('g3'),
                    'H1' => $request->input('h1'),
                    'H1_NOTE' => $request->input('h1_note'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }
}
