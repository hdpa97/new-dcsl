<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelCek extends Authenticatable
{
    public function getAllData(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if ($request->get('idKar')) {
            $data = $data->where('tbl_kares.id',$request->get('idKar'));
        }

        if ($request->get('idKab')) {
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if ($request->get('idDlr')) {
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
        ->groupby('tbl_dealer.nama_dealer')
        ->orderby('tbl_dealer.id','asc')
        ->get(['tbl_dealer.id','tbl_kares.nama_kares','tbl_kabupaten.nama_kabupaten','tbl_dealer.nama_dealer','tbl_dealer.kode_dealer',
        DB::raw('count(if(tbl_upload_data.kode_data="H1",1,null)) CT_H1'), DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H2'), DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H3')]);
    }

    public function getSpesificDataH1($kodeDealer)
    {
        $data = DB::select(
            'SELECT tbl_upload_data.*, tbl_kode_agama.nama_agama, tbl_kode_kota.nama_kota, tbl_kode_prov.nama_prov, tbl_kode_pekerjaan.nama_pekerjaan, tbl_finance_company.nama_finance_company
            FROM tbl_upload_data
            INNER JOIN tbl_dealer on tbl_dealer.kode_dealer = tbl_upload_data.kode_dealer
            INNER JOIN tbl_kode_agama on tbl_kode_agama.kode_agama = tbl_upload_data.kode_agama
            INNER JOIN tbl_kode_pekerjaan on tbl_kode_pekerjaan.kode_pekerjaan = tbl_upload_data.kode_pekerjaan
            INNER JOIN tbl_kode_kota on tbl_kode_kota.kode_kota = tbl_upload_data.kode_kota
            INNER JOIN tbl_kode_prov on tbl_kode_prov.kode_prov = tbl_upload_data.kode_prov
            INNER JOIN tbl_finance_company on tbl_finance_company.kode_finance_company = tbl_upload_data.finance_company
            INNER JOIN tbl_kode_pendidikan on tbl_kode_pendidikan.kode_pendidikan = tbl_upload_data.kode_pendidikan
            INNER JOIN tbl_kode_pengeluaran on tbl_kode_pengeluaran.kode_pengeluaran = tbl_upload_data.kode_pengeluaran
            WHERE tbl_dealer.kode_dealer = :kodeDealer
            AND tbl_upload_data.kode_data = "H1"',
            ['kodeDealer' => $kodeDealer]
        );

        return $data;
    }

    public function getSpesificDataH2($kodeDealer)
    {
        $data = DB::select(
            'SELECT tbl_upload_data.*, tbl_kode_agama.nama_agama, tbl_kode_kota.nama_kota, tbl_kode_prov.nama_prov, tbl_kode_pekerjaan.nama_pekerjaan, tbl_finance_company.nama_finance_company
            FROM tbl_upload_data
            INNER JOIN tbl_dealer on tbl_dealer.kode_dealer = tbl_upload_data.kode_dealer
            INNER JOIN tbl_kode_agama on tbl_kode_agama.kode_agama = tbl_upload_data.kode_agama
            INNER JOIN tbl_kode_pekerjaan on tbl_kode_pekerjaan.kode_pekerjaan = tbl_upload_data.kode_pekerjaan
            INNER JOIN tbl_kode_kota on tbl_kode_kota.kode_kota = tbl_upload_data.kode_kota
            INNER JOIN tbl_kode_prov on tbl_kode_prov.kode_prov = tbl_upload_data.kode_prov
            INNER JOIN tbl_finance_company on tbl_finance_company.kode_finance_company = tbl_upload_data.finance_company
            INNER JOIN tbl_kode_pendidikan on tbl_kode_pendidikan.kode_pendidikan = tbl_upload_data.kode_pendidikan
            INNER JOIN tbl_kode_pengeluaran on tbl_kode_pengeluaran.kode_pengeluaran = tbl_upload_data.kode_pengeluaran
            WHERE tbl_dealer.kode_dealer = :kodeDealer
            AND tbl_upload_data.kode_data = "H23"',
            ['kodeDealer' => $kodeDealer]
        );

        return $data;
    }

    public function getSpesificDataH3($kodeDealer)
    {
        $data = DB::select(
            'SELECT tbl_upload_data.*, tbl_kode_agama.nama_agama, tbl_kode_kota.nama_kota, tbl_kode_prov.nama_prov, tbl_kode_pekerjaan.nama_pekerjaan, tbl_finance_company.nama_finance_company
            FROM tbl_upload_data
            INNER JOIN tbl_dealer on tbl_dealer.kode_dealer = tbl_upload_data.kode_dealer
            INNER JOIN tbl_kode_agama on tbl_kode_agama.kode_agama = tbl_upload_data.kode_agama
            INNER JOIN tbl_kode_pekerjaan on tbl_kode_pekerjaan.kode_pekerjaan = tbl_upload_data.kode_pekerjaan
            INNER JOIN tbl_kode_kota on tbl_kode_kota.kode_kota = tbl_upload_data.kode_kota
            INNER JOIN tbl_kode_prov on tbl_kode_prov.kode_prov = tbl_upload_data.kode_prov
            INNER JOIN tbl_finance_company on tbl_finance_company.kode_finance_company = tbl_upload_data.finance_company
            INNER JOIN tbl_kode_pendidikan on tbl_kode_pendidikan.kode_pendidikan = tbl_upload_data.kode_pendidikan
            INNER JOIN tbl_kode_pengeluaran on tbl_kode_pengeluaran.kode_pengeluaran = tbl_upload_data.kode_pengeluaran
            WHERE tbl_dealer.kode_dealer = :kodeDealer
            AND tbl_upload_data.kode_data = "H23"',
            ['kodeDealer' => $kodeDealer]
        );

        return $data;
    }
}
