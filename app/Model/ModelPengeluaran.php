<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelPengeluaran extends Authenticatable
{
    protected $table = "tbl_kode_pengeluaran";
    protected $primaryKey = "id";
    protected $fillable = ['kode_pengeluaran', 'pengeluaran', 'huruf_pengeluaran', 'flag'];


    public function getAllData()
    {
        return $this->where('flag', '=', '1')->get();
    }

    public function getReportDemografi(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if($request->get('tipeData') == 'H1') {
            $data = $data->join('tbl_hasil_survey_h1','tbl_hasil_survey_h1.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H2'){
            $data = $data->join('tbl_hasil_survey_h2','tbl_hasil_survey_h2.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H3'){
            $data = $data->join('tbl_hasil_survey_h3','tbl_hasil_survey_h3.id_data','tbl_upload_data.id');
        }else{
            $data = $data;
        }

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)
        ->groupby('tbl_upload_data.kode_pengeluaran')
        ->orderby('tbl_kode_pengeluaran.huruf_pengeluaran','asc')
        ->get(['tbl_kode_pengeluaran.huruf_pengeluaran', 'tbl_kode_pengeluaran.pengeluaran', DB::raw('count(*) as total')]);
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'kode_pengeluaran'    =>  $request->kode_pengeluaran,
                'pengeluaran'    =>  $request->pengeluaran,
                'huruf_pengeluaran'    =>  $request->huruf_pengeluaran,
                'flag' => $request->input('flag')
            ]
        );
    }

    public function updateData(Request $request, $id)
    {
        $this->where('id', $id)
            ->update([
                'kode_pengeluaran'    =>  $request->kode_pengeluaran,
                'pengeluaran'    =>  $request->pengeluaran,
                'huruf_pengeluaran'    =>  $request->huruf_pengeluaran,
                'flag' => $request->input('flag')
            ]);
    }

    public function disableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 0,
        ]);
    }

    public function enableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 1,
        ]);
    }
}
