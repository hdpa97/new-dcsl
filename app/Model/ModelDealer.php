<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelDealer extends Authenticatable
{
    protected $table = "tbl_dealer";
    protected $primaryKey = "id";
    protected $guarded = ['created_at','updated_at'];
    protected $fillable = ['nama_dealer','kode_dealer','flag','created_by','last_update_by','id_kares','id_jaringan','id_kabupaten','id_layer','nama_pic','email_pic','no_hp_pic','alamat'];

    public function getData()
    {
        return $this
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_layer','tbl_layer.id','tbl_dealer.id_layer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        // ->where('tbl_kares.flag',1)
        // ->where('tbl_kabupaten.flag',1)
        // ->where('tbl_layer.flag',1)
        // ->where('tbl_jaringan.flag',1)
        ->select('tbl_dealer.*','tbl_kares.nama_kares','tbl_layer.nama_layer','tbl_kabupaten.nama_kabupaten','tbl_jaringan.nama_jaringan')
        ->orderby('tbl_dealer.nama_dealer','asc')
        ->get();
    }

    public function getAllData(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_layer','tbl_layer.id','tbl_dealer.id_layer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan');
        // ->where('tbl_kares.flag',1)
        // ->where('tbl_kabupaten.flag',1)
        // ->where('tbl_layer.flag',1)
        // ->where('tbl_jaringan.flag',1)

        if ($request->get('idKar')) {
            $data = $data->where('tbl_kares.id',$request->get('idKar'));
        }

        if ($request->get('idKab')) {
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        return $data->select('tbl_dealer.*','tbl_kares.nama_kares','tbl_layer.nama_layer','tbl_kabupaten.nama_kabupaten','tbl_jaringan.nama_jaringan')
        ->orderby('tbl_dealer.nama_dealer','asc')
        ->get();
    }

    public function getDealerFromSurveyor($idSurveyor)
    {
        return $this->join('users','users.id','tbl_dealer.id_surveyor')
        ->where('tbl_dealer.id_surveyor',$idSurveyor)
        ->get([
            'tbl_dealer.nama_dealer as namaDealer','tbl_dealer.id as idDealer', 'users.id as idUser'
        ]);
    }

    public function getSingleData($kodeDealer)
    {
        return $this->select('tbl_dealer.*')->where('kode_dealer',$kodeDealer)->first();
    }
    public function createData(Request $request)
    {
        $this->create([
            'nama_dealer' => $request->input('nama_dealer'),
            'kode_dealer' => $request->input('kode_dealer'),
            'alamat' => $request->input('alamat'),
            'id_kares' => $request->input('id_kares'),
            'id_jaringan' => $request->input('id_jaringan'),
            'id_kabupaten' => $request->input('id_kabupaten'),
            'id_layer' => $request->input('id_layer'),
            'nama_pic' => $request->input('nama_pic'),
            'no_hp_pic' => $request->input('no_hp_pic'),
            'email_pic' => $request->input('email_pic'),
            'flag' => $request->input('flag'),
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'created_by' => Auth::user()->id,
            'last_update_by' => Auth::user()->id]
        );
    }

    public function updateData(Request $request,$id){
        $this->where('id',$id)
            ->update([
                'nama_dealer' => $request->input('nama_dealer'),
                'kode_dealer' => $request->input('kode_dealer'),
                'alamat' => $request->input('alamat'),
                'id_kares' => $request->input('id_kares'),
                'id_jaringan' => $request->input('id_jaringan'),
                'id_kabupaten' => $request->input('id_kabupaten'),
                'id_layer' => $request->input('id_layer'),
                'nama_pic' => $request->input('nama_pic'),
                'no_hp_pic' => $request->input('no_hp_pic'),
                'email_pic' => $request->input('email_pic'),
                'flag' => $request->input('flag'),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'last_update_by' => Auth::user()->id
                ]);
    }

    public function disableData($id)
    {
        $this->where('id',$id)->update([
            'flag' => 0,
        ]);
    }

    public function enableData($id)
    {
        $this->where('id',$id)->update([
            'flag' => 1,
        ]);
    }
}
