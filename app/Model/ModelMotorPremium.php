<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelMotorPremium extends Authenticatable
{
    protected $table = "tbl_motor_premium";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = ['nama_motor', 'flag', 'created_by', 'last_update_by'];


    public function getAllData()
    {
        return $this->where('flag','=','1')->get();
    }

    public function checkDataMotor($tipe)
    {
        $data = DB::table('tbl_motor_premium')->select('nama_motor')->where('nama_motor','like',$tipe)->first();
        if($data!=null){
            return true;    
        }else{
            return false;
        }
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'nama_motor' => $request->input('nama_motor'),
                'flag' => $request->input('flag'),
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'created_by' => Auth::user()->id,
                'last_update_by' => Auth::user()->id
            ]
        );
    }

    public function updateData(Request $request, $id)
    {
        $this->where('id', $id)
            ->update([
                'nama_motor' => $request->input('nama_motor'),
                'flag' => $request->input('flag'),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'last_update_by' => Auth::user()->id
            ]);
    }

    public function getMotor()
    {
        $motor = DB::select(
            'SELECT nama_motor
        FROM tbl_motor_premium'
        );

        return $motor;
    }

    public function disableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 0,
        ]);
    }

    public function enableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 1,
        ]);
    }
}
