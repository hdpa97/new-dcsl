<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelTersurvey extends Authenticatable
{

    public function getReportTersurvey(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if($request->get('tipeData') == 'H1') {
            $data = $data->join('tbl_hasil_survey_h1','tbl_hasil_survey_h1.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H2'){
            $data = $data->join('tbl_hasil_survey_h2','tbl_hasil_survey_h2.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H3'){
            $data = $data->join('tbl_hasil_survey_h3','tbl_hasil_survey_h3.id_data','tbl_upload_data.id');
        }else{
            $data = $data;
        }

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)
        ->groupby('tbl_dealer.nama_dealer')
        ->orderby('tbl_dealer.id')
        ->get(['tbl_dealer.id','tbl_kares.nama_kares','tbl_kabupaten.nama_kabupaten','tbl_dealer.nama_dealer','tbl_jaringan.nama_jaringan','tbl_dealer.kode_dealer',
        DB::raw('count(tbl_upload_data.kode_data) total')]);
    }

    public function getReportDemografiGender(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if($request->get('tipeData') == 'H1') {
            $data = $data->join('tbl_hasil_survey_h1','tbl_hasil_survey_h1.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H2'){
            $data = $data->join('tbl_hasil_survey_h2','tbl_hasil_survey_h2.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H3'){
            $data = $data->join('tbl_hasil_survey_h3','tbl_hasil_survey_h3.id_data','tbl_upload_data.id');
        }else{
            $data = $data;
        }

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)
        ->first([DB::raw('count(IF(tbl_upload_data.gender = 1,1,NULL)) male'),
        DB::raw('count(IF(tbl_upload_data.gender = 2,1,NULL)) woman')]);
    }

    public function getReportDemografiGender2(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if($request->get('tipeData') == 'H1') {
            $data = $data->join('tbl_hasil_survey_h1','tbl_hasil_survey_h1.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H2'){
            $data = $data->join('tbl_hasil_survey_h2','tbl_hasil_survey_h2.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H3'){
            $data = $data->join('tbl_hasil_survey_h3','tbl_hasil_survey_h3.id_data','tbl_upload_data.id');
        }else{
            $data = $data;
        }

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)
        ->get([DB::raw('count(IF(tbl_upload_data.gender = 1,1,NULL)) male'),
        DB::raw('count(IF(tbl_upload_data.gender = 2,1,NULL)) woman')]);
    }

    public function getRportAgeGroup(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if($request->get('tipeData') == 'H1') {
            $data = $data->join('tbl_hasil_survey_h1','tbl_hasil_survey_h1.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H2'){
            $data = $data->join('tbl_hasil_survey_h2','tbl_hasil_survey_h2.id_data','tbl_upload_data.id');
        }else if($request->get('tipeData') == 'H3'){
            $data = $data->join('tbl_hasil_survey_h3','tbl_hasil_survey_h3.id_data','tbl_upload_data.id');
        }else{
            $data = $data;
        }

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
        ->where('tbl_upload_data.flag','=',3)
        ->groupby('age_group')
        ->get([
            DB::raw('count(*) age_count'),
            DB::raw('
            (CASE WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 17 AND 20 THEN "17-20"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 21 AND 25 THEN "21-25"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 26 AND 30 THEN "26-30"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 31 AND 35 THEN "31-35"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 36 AND 40 THEN "36-40"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 41 AND 45 THEN "41-45"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 46 AND 50 THEN "46-50"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 51 AND 55 THEN "51-55"
            WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) > 56 THEN "56++"
            ELSE "Other" END) AS age_group')
        ]);
    }

    public function getAchPerson(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('users','users.id','tbl_dealer.id_surveyor')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran');

        if($request->get('periode')=="H"){
            $data = $data->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subDay(1)->toDateTimeString());
        }elseif($request->get('periode')=="H-7"){
            $data = $data->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subDay(7)->toDateTimeString());
        }elseif($request->get('periode')=="H-30"){
            $data = $data->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(30)->toDateTimeString());
        }


        return $data
        // ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString())
        ->groupby('tbl_dealer.id_surveyor')
        ->get(['tbl_dealer.id_surveyor','users.name', 'users.npk', DB::raw('count(IF(tbl_upload_data.flag = 1,1,NULL))+count(IF(tbl_upload_data.flag = 4,1,NULL)) tersedia'),
        DB::raw('count(IF(tbl_upload_data.flag = 3,1,NULL)) selesai'),DB::raw('count(IF(tbl_upload_data.flag = 5,1,NULL))+count(IF(tbl_upload_data.flag = 2,1,NULL)) reject'),
        'tbl_upload_data.updated_at']);
    }
}
