<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelScoringKepuasan extends Authenticatable
{

    public function getScoringKepuasanH1(Request $request)
    {

        $data = DB::table('tbl_dealer')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->join('tbl_hasil_survey_h1', 'tbl_hasil_survey_h1.id_data', 'tbl_upload_data.id');

        if ($request->get('point')){
            $pt = $request->get('point');
        }else {
            $pt = 5;
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
            ->where('tbl_upload_data.flag', '=', 3)
            ->orderby('tbl_dealer.nama_dealer', 'asc')
            ->groupby('tbl_dealer.nama_dealer')
            ->get([
                'tbl_upload_data.id as id_data', 'tbl_dealer.id', 'tbl_dealer.nama_dealer', 'tbl_dealer.kode_dealer',
                DB::raw('round(count(IF(tbl_hasil_survey_h1.a3_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.a3_1 ) / ' . $pt . ' * 100, 2) a3_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.a3_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.a3_2 ) / ' . $pt . ' * 100, 2) a3_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.a3_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.a3_3 ) / ' . $pt . ' * 100, 2) a3_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.a3_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.a3_4 ) / ' . $pt . ' * 100, 2) a3_4'),

                DB::raw('round(count(IF(tbl_hasil_survey_h1.b1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.b1_1 ) / ' . $pt . ' * 100, 2) b1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.b1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.b1_2 ) / ' . $pt . ' * 100, 2) b1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.b1_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.b1_3 ) / ' . $pt . ' * 100, 2) b1_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.b1_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.b1_4 ) / ' . $pt . ' * 100, 2) b1_4'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.b1_5 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.b1_5 ) / ' . $pt . ' * 100, 2) b1_5'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.b1_5a = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.b1_5a ) / ' . $pt . ' * 100, 2) b1_5a'),

                DB::raw('round(count(IF(tbl_hasil_survey_h1.e1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.e1_1 ) / ' . $pt . ' * 100, 2) e1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.e1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.e1_2 ) / ' . $pt . ' * 100, 2) e1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.e1_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.e1_3 ) / ' . $pt . ' * 100, 2) e1_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.f2_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.f2_1 ) / ' . $pt . ' * 100, 2) f2_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.f2_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.f2_2 ) / ' . $pt . ' * 100, 2) f2_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.g1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.g1 ) / ' . $pt . ' * 100, 2) g1'),

                DB::raw('round(count(IF(tbl_hasil_survey_h1.h1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.h1_1 ) / ' . $pt . ' * 100, 2) h1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.h1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.h1_2 ) / ' . $pt . ' * 100, 2) h1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.h1_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.h1_3 ) / ' . $pt . ' * 100, 2) h1_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.h1_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.h1_4 ) / ' . $pt . ' * 100, 2) h1_4'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.h1_5 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.h1_5 ) / ' . $pt . ' * 100, 2) h1_5'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.h1_6 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.h1_6 ) / ' . $pt . ' * 100, 2) h1_6'),

                DB::raw('round(count(IF(tbl_hasil_survey_h1.i1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.i1_1 ) / ' . $pt . ' * 100, 2) i1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.i1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.i1_2 ) / ' . $pt . ' * 100, 2) i1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.j1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.j1_1 ) / ' . $pt . ' * 100, 2) j1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h1.j1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.j1_2 ) / ' . $pt . ' * 100, 2) j1_2'),

                DB::raw('round(count(IF(tbl_hasil_survey_h1.n1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h1.n1 ) / ' . $pt . ' * 100, 2) n1'),
            ]);
    }

    public function getScoringKepuasanH2(Request $request)
    {

        $data = DB::table('tbl_dealer')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->join('tbl_hasil_survey_h2', 'tbl_hasil_survey_h2.id_data', 'tbl_upload_data.id');

        if ($request->get('point')){
            $pt = $request->get('point');
        }else {
            $pt = 5;
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }
        
        return $data
            ->where('tbl_upload_data.flag', '=', 3)
            ->orderby('tbl_dealer.nama_dealer', 'asc')
            ->groupby('tbl_dealer.nama_dealer')
            ->get([
                'tbl_upload_data.id as id_data', 'tbl_dealer.id', 'tbl_dealer.nama_dealer', 'tbl_dealer.kode_dealer',
                DB::raw('round(count(IF(tbl_hasil_survey_h2.a3_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.a3_1 ) / ' . $pt . ' * 100, 2) a3_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.a3_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.a3_2 ) / ' . $pt . ' * 100, 2) a3_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.a3_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.a3_3 ) / ' . $pt . ' * 100, 2) a3_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.a3_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.a3_4 ) / ' . $pt . ' * 100, 2) a3_4'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.b1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.b1_1 ) / ' . $pt . ' * 100, 2) b1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.b1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.b1_2 ) / ' . $pt . ' * 100, 2) b1_2'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.c1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.c1_1 ) / ' . $pt . ' * 100, 2) c1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.c1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.c1_2 ) / ' . $pt . ' * 100, 2) c1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.c1_3a = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.c1_3a ) / ' . $pt . ' * 100, 2) c1_3a'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.c1_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.c1_4 ) / ' . $pt . ' * 100, 2) c1_4'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.c1_5 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.c1_5 ) / ' . $pt . ' * 100, 2) c1_5'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.d3_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.d3_1 ) / ' . $pt . ' * 100, 2) d3_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.d3_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.d3_2 ) / ' . $pt . ' * 100, 2) d3_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.d3_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.d3_3 ) / ' . $pt . ' * 100, 2) d3_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.d4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.d4 ) / ' . $pt . ' * 100, 2) d4'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.e1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.e1_1 ) / ' . $pt . ' * 100, 2) e1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.e1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.e1_2 ) / ' . $pt . ' * 100, 2) e1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.e1_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.e1_3 ) / ' . $pt . ' * 100, 2) e1_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h2.e1_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.e1_4 ) / ' . $pt . ' * 100, 2) e1_4'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.g3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.g3 ) / ' . $pt . ' * 100, 2) g3'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.h1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.h1 ) / ' . $pt . ' * 100, 2) h1'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.i7 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.i7 ) / ' . $pt . ' * 100, 2) i7'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.k1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.k1 ) / ' . $pt . ' * 100, 2) k1'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.n1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.n1 ) / ' . $pt . ' * 100, 2) n1'),

                DB::raw('round(count(IF(tbl_hasil_survey_h2.n2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h2.n2 ) / ' . $pt . ' * 100, 2) n2')
            ]);
    }

    public function getScoringKepuasanH3(Request $request)
    {

        $data = DB::table('tbl_dealer')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->join('tbl_hasil_survey_h3', 'tbl_hasil_survey_h3.id_data', 'tbl_upload_data.id');

        if ($request->get('point')){
            $pt = $request->get('point');
        }else {
            $pt = 5;
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

        return $data
            ->where('tbl_upload_data.flag', '=', 3)
            ->orderby('tbl_dealer.nama_dealer', 'asc')
            ->groupby('tbl_dealer.nama_dealer')
            ->get([
                'tbl_upload_data.id as id_data', 'tbl_dealer.id', 'tbl_dealer.nama_dealer', 'tbl_dealer.kode_dealer',
                DB::raw('round(count(IF(tbl_hasil_survey_h3.a1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.a1_1 ) / ' . $pt . ' * 100, 2) a1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.a1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.a1_2 ) / ' . $pt . ' * 100, 2) a1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.b1_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.b1_1 ) / ' . $pt . ' * 100, 2) b1_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.b1_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.b1_2 ) / ' . $pt . ' * 100, 2) b1_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.b1_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.b1_3 ) / ' . $pt . ' * 100, 2) b1_3'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.b1_4 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.b1_4 ) / ' . $pt . ' * 100, 2) b1_4'),

                DB::raw('round(count(IF(tbl_hasil_survey_h3.c1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.c1 ) / ' . $pt . ' * 100, 2) c1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.c1_b = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.c1_b ) / ' . $pt . ' * 100, 2) c1_b'),

                DB::raw('round(count(IF(tbl_hasil_survey_h3.d2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.d2 ) / ' . $pt . ' * 100, 2) d2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.d3_1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.d3_1 ) / ' . $pt . ' * 100, 2) d3_1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.d3_2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.d3_2 ) / ' . $pt . ' * 100, 2) d3_2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.d3_2a = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.d3_2a ) / ' . $pt . ' * 100, 2) d3_2a'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.d3_3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.d3_3 ) / ' . $pt . ' * 100, 2) d3_3'),

                DB::raw('round(count(IF(tbl_hasil_survey_h3.e2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.e2 ) / ' . $pt . ' * 100, 2) e2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.f1 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.f1 ) / ' . $pt . ' * 100, 2) f1'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.g2 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.g2 ) / ' . $pt . ' * 100, 2) g2'),
                DB::raw('round(count(IF(tbl_hasil_survey_h3.g3 = ' . $pt . ',1,NULL)) * ' . $pt . ' / count( tbl_hasil_survey_h3.g3 ) / ' . $pt . ' * 100, 2) g3'),
            ]);
    }

    public function getScoringDealerH3($kode_dealer)
    {
        $data = DB::table('tbl_dealer')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->join('tbl_hasil_survey_h3', 'tbl_hasil_survey_h3.id_data', 'tbl_upload_data.id');

        return $data
            ->where('tbl_upload_data.flag', '=', 3)
            ->where('tbl_dealer.kode_dealer', '=', $kode_dealer)
            ->orderby('tbl_dealer.nama_dealer', 'asc')
            ->get([
                'tbl_upload_data.id as id_data', 'tbl_dealer.id', 'tbl_dealer.nama_dealer', 'tbl_dealer.kode_dealer',
                'tbl_hasil_survey_h3.a1_1', 'tbl_hasil_survey_h3.a1_2', 'tbl_hasil_survey_h3.b1_1', 'tbl_hasil_survey_h3.b1_2',
                'tbl_hasil_survey_h3.b1_3', 'tbl_hasil_survey_h3.b1_4',
                'tbl_hasil_survey_h3.c1', 'tbl_hasil_survey_h3.c1_b', 'tbl_hasil_survey_h3.d2', 'tbl_hasil_survey_h3.d3_1', 'tbl_hasil_survey_h3.d3_2',
                'tbl_hasil_survey_h3.d3_2a', 'tbl_hasil_survey_h3.d3_3', 'tbl_hasil_survey_h3.e2', 'tbl_hasil_survey_h3.f1',
                'tbl_hasil_survey_h3.g1', 'tbl_hasil_survey_h3.g2', 'tbl_hasil_survey_h3.g3'
            ]);
    }

    public function getScoringDealerH2($kode_dealer)
    {
        $data = DB::table('tbl_dealer')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->join('tbl_hasil_survey_h2', 'tbl_hasil_survey_h2.id_data', 'tbl_upload_data.id');

        return $data
            ->where('tbl_upload_data.flag', '=', 3)
            ->where('tbl_dealer.kode_dealer', '=', $kode_dealer)
            ->orderby('tbl_dealer.nama_dealer', 'asc')
            ->get([
                'tbl_upload_data.id as id_data', 'tbl_dealer.id', 'tbl_dealer.nama_dealer', 'tbl_dealer.kode_dealer',
                'tbl_hasil_survey_h2.a3_1', 'tbl_hasil_survey_h2.a3_2', 'tbl_hasil_survey_h2.a3_3', 'tbl_hasil_survey_h2.a3_4',
                'tbl_hasil_survey_h2.b1_1', 'tbl_hasil_survey_h2.b1_2',
                'tbl_hasil_survey_h2.c1_1', 'tbl_hasil_survey_h2.c1_2', 'tbl_hasil_survey_h2.c1_3a', 'tbl_hasil_survey_h2.c1_4', 'tbl_hasil_survey_h2.c1_5',
                'tbl_hasil_survey_h2.d3_1', 'tbl_hasil_survey_h2.d3_2', 'tbl_hasil_survey_h2.d3_3', 'tbl_hasil_survey_h2.d4',
                'tbl_hasil_survey_h2.e1_1', 'tbl_hasil_survey_h2.e1_2', 'tbl_hasil_survey_h2.e1_3', 'tbl_hasil_survey_h2.e1_4',
                'tbl_hasil_survey_h2.g3',
                'tbl_hasil_survey_h2.h1',
                'tbl_hasil_survey_h2.i7',
                'tbl_hasil_survey_h2.k1',
                'tbl_hasil_survey_h2.n1',
                'tbl_hasil_survey_h2.n2',
                'tbl_hasil_survey_h2.n8'
            ]);
    }

    public function getScoringDealerH1($kode_dealer)
    {
        $data = DB::table('tbl_dealer')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->join('tbl_hasil_survey_h1', 'tbl_hasil_survey_h1.id_data', 'tbl_upload_data.id');

        return $data
            ->where('tbl_upload_data.flag', '=', 3)
            ->where('tbl_dealer.kode_dealer', '=', $kode_dealer)
            ->orderby('tbl_dealer.nama_dealer', 'asc')
            ->get([
                'tbl_upload_data.id as id_data', 'tbl_dealer.id', 'tbl_dealer.nama_dealer', 'tbl_dealer.kode_dealer',
                'tbl_hasil_survey_h1.a3_1', 'tbl_hasil_survey_h1.a3_2', 'tbl_hasil_survey_h1.a3_3', 'tbl_hasil_survey_h1.a3_4',
                'tbl_hasil_survey_h1.b1_1', 'tbl_hasil_survey_h1.b1_2', 'tbl_hasil_survey_h1.b1_3', 'tbl_hasil_survey_h1.b1_3a', 'tbl_hasil_survey_h1.b1_4', 'tbl_hasil_survey_h1.b1_5', 'tbl_hasil_survey_h1.b1_5a',
                'tbl_hasil_survey_h1.e1_1', 'tbl_hasil_survey_h1.e1_2', 'tbl_hasil_survey_h1.e1_3',
                'tbl_hasil_survey_h1.f2_1', 'tbl_hasil_survey_h1.f2_2',
                'tbl_hasil_survey_h1.g1',
                'tbl_hasil_survey_h1.h1_1', 'tbl_hasil_survey_h1.h1_2', 'tbl_hasil_survey_h1.h1_3', 'tbl_hasil_survey_h1.h1_4', 'tbl_hasil_survey_h1.h1_5',
                'tbl_hasil_survey_h1.i1_1', 'tbl_hasil_survey_h1.i1_2',
                'tbl_hasil_survey_h1.h1_1', 'tbl_hasil_survey_h1.j1_2',
                'tbl_hasil_survey_h1.n1', 'tbl_hasil_survey_h1.n9'
            ]);
    }

    public function getDetailData($id)
    {
        return DB::table('tbl_dealer')
            ->join('users', 'users.id', 'tbl_dealer.id_surveyor')
            ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
            ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
            ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
            ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
            ->join('tbl_kode_kota', 'tbl_kode_kota.kode_kota', 'tbl_upload_data.kode_kota')
            ->join('tbl_kode_prov', 'tbl_kode_prov.kode_prov', 'tbl_upload_data.kode_prov')
            ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
            ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
            ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
            ->where('tbl_upload_data.flag', '=', 3)
            ->where('tbl_upload_data.id', '=', $id)
            ->first();
    }
}
