<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class ModelSurveyor extends Authenticatable
{
    protected $table = "tbl_surveyor";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = ['nama_surveyor', 'flag', 'created_by', 'last_update_by'];


    public function getAllData()
    {
        return $this->get();
    }

    public function getAllActiveData()
    {
        return $this->where('flag', 1)->get();
    }

    public static function getUserData()
    {
        $userData = array(
            "name" => Auth::user()->name,
            "email" => Auth::user()->email,
            "id" => Auth::user()->id,
            "role" => Auth::user()->role
        );
        return $userData;
    }
}
