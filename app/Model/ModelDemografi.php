<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class ModelDemografi extends Authenticatable
{
    protected $table = "tbl_demografi";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = ['nama_demografi', 'flag', 'created_by', 'last_update_by'];


    public function getAllData()
    {
        return $this->get();
    }

    public function getAllActiveData()
    {
        return $this->where('flag', 1)->get();
    }

    public function createData(Request $request)
    {
        $this->create(
            []
        );
    }
}
