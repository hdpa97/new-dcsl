<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelFinanceCompany extends Authenticatable
{
    protected $table = "tbl_finance_company";
    protected $primaryKey = "id";
    protected $fillable = ['kode_finance_company', 'nama_finance_company', 'flag'];

    public function getAllData()
    {
        return $this->where('flag', '=', '1')->get();
    }

    public function getSingleData($kodeFinanceCompany)
    {
        return $this->where('kode_finance_company', $kodeFinanceCompany)->first();
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'kode_finance_company' => $request->input('kode_finance_company'),
                'nama_finance_company' => $request->input('nama_finance_company'),
                'flag' => $request->input('flag')
            ]
        );
    }

    public function updateData(Request $request, $id)
    {
        $this->where('id', $id)
            ->update([
                'kode_finance_company' => $request->input('kode_finance_company'),
                'nama_finance_company' => $request->input('nama_finance_company'),
                'flag' => $request->input('flag')
            ]);
    }

    public function disableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 0,
        ]);
    }

    public function enableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 1,
        ]);
    }
}
