<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelHasilSurveyH2 extends Authenticatable
{
    protected $table = "tbl_hasil_survey_h2";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = [
        'id_data', 'status_survey', 'kode_data',
        'A1', 'A2', 'A3_1', 'A3_2', 'A3_3', 'A3_3A', 'A3_3B', 'A3_2B', 'A3_2B_NOTE', 'A3_3C', 'A3_3C_NOTE', 'A3_4',
        'B1_1', 'B1_2', 'C1_1', 'C1_2', 'C1_3', 'C1_3A', 'C1_4', 'C1_4A', 'C1_2A', 'C1_2A_NOTE', 'C2', 'C2_NOTE', 'C3', 'C3_NOTE', 'C1_5',
        'D1', 'D2', 'D3_1', 'D3_2', 'D3_3', 'D3_3A', 'D4', 'D5',
        'E1_1', 'E1_1A', 'E1_2', 'E1_3', 'E1_4', 'G1', 'G2', 'G3',
        'H1', 'H2', 'H3', 'H4',
        'I4', 'I4_NOTE', 'I5', 'I6', 'I7', 'I8', 'I8_NOTE', 'I9', 'I9_NOTE', 'K1',
        'N1', 'N2', 'N3_MONTH', 'N3_YEARS', 'N3_NOTE', 'N4', 'N4_NOTE', 'N5', 'N5_NOTE', 'N6', 'N6_NOTE', 'N7_NOTE', 'N8'
    ];

    public function checkData($idData)
    {
        return $this->select('*')->where('id_data', $idData)->first();
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'id_data' => $request->input('idData'),
                'status_survey' => $request->input('status_survey'),
                'kode_data' => 'H2',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]
        );
    }

    public function updateData(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'id_data' => $request->input('idData'),
                    'status_survey' => $request->input('status_survey'),
                    'kode_data' => 'H2',
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    // SAVE PER PAGE
    public function updateDataPage2(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'A1' => $request->input('a1'),
                    'A2' => $request->input('a2'),
                    'A3_1' => $request->input('a3_1'),
                    'A3_2' => $request->input('a3_2'),
                    'A3_3' => $request->input('a3_3'),
                    'A3_3A' => $request->input('a3_3a'),
                    'A3_3B' => $request->input('a3_3b'),
                    'A3_2B' => $request->input('a3_2b'),
                    'A3_2B_NOTE' => $request->input('a3_2b_note'),
                    'A3_3C' => $request->input('a3_3c'),
                    'A3_3C_NOTE' => $request->input('a3_3c_note'),
                    'A3_4' => $request->input('a3_4'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage3(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'B1_1' => $request->input('b1_1'),
                    'B1_2' => $request->input('b1_2'),
                    'C1_1' => $request->input('c1_1'),
                    'C1_2' => $request->input('c1_2'),
                    'C1_3' => $request->input('c1_3'),
                    'C1_3A' => $request->input('c1_3a'),
                    'C1_4' => $request->input('c1_4'),
                    'C1_4A' => $request->input('c1_4a'),
                    'C1_2A' => $request->input('c1_2a'),
                    'C1_2A_NOTE' => $request->input('c1_2a_note'),
                    'C2' => $request->input('c2'),
                    'C2_NOTE' => $request->input('c2_note'),
                    'C3' => $request->input('c3'),
                    'C3_NOTE' => $request->input('c3_note'),
                    'C1_5' => $request->input('c1_5'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage4(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'D1' => $request->input('d1'),
                    'D2' => $request->input('d2'),
                    'D3_1' => $request->input('d3_1'),
                    'D3_2' => $request->input('d3_2'),
                    'D3_3' => $request->input('d3_3'),
                    'D3_3A' => $request->input('d3_3a'),
                    'D4' => $request->input('d4'),
                    'D5' => $request->input('d5'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage5(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'E1_1' => $request->input('e1_1'),
                    'E1_1A' => $request->input('e1_1a'),
                    'E1_2' => $request->input('e1_2'),
                    'E1_3' => $request->input('e1_3'),
                    'E1_4' => $request->input('e1_4'),
                    'G1' => $request->input('g1'),
                    'G2' => $request->input('g2'),
                    'G3' => $request->input('g3'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage6(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'H1' => $request->input('h1'),
                    'H2' => $request->input('h2'),
                    'H3' => $request->input('h3'),
                    'H4' => $request->input('h4'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage7(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'I4' => $request->input('i4'),
                    'I4_NOTE' => $request->input('i4_note'),
                    'I5' => $request->input('i5'),
                    'I6' => $request->input('i6'),
                    'I7' => $request->input('i7'),
                    'I8' => $request->input('i8'),
                    'I8_NOTE' => $request->input('i8_note'),
                    'I9' => $request->input('i9'),
                    'I9_NOTE' => $request->input('i9_note'),
                    'K1' => $request->input('k1'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage8(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'N1' => $request->input('n1'),
                    'N2' => $request->input('n2'),
                    'N3_MONTH' => $request->input('n3_month'),
                    'N3_YEARS' => $request->input('n3_years'),
                    'N3_NOTE' => $request->input('n3_note'),
                    'N4' => $request->input('n4'),
                    'N4_NOTE' => $request->input('n4_note'),
                    'N5' => $request->input('n5'),
                    'N5_NOTE' => $request->input('n5_note'),
                    'N6' => $request->input('n6'),
                    'N6_NOTE' => $request->input('n6_note'),
                    'N7_NOTE' => $request->input('n7_note'),
                    'N8' => $request->input('n8'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

}
