<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;

class ModelRescheduleData extends Authenticatable
{
    protected $table = "tbl_data_reschedule";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = ['id_data', 'flag', 'status_survey', 'kode_data', 'tgl_reschedule'];

    public function createRescheduleData(Request $request, $kode_data)
    {
        $this->create(
            [
                'id_data' => $request->input('idData'),
                'status_survey' => $request->input('status_survey'),
                'kode_data' => $kode_data,
                'flag' => 1,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]
        );
    }

    public function updateData(Request $request,$id){
        $this->where('id',$id)
            ->update([
                'tgl_reschedule' => $request->input('tgl_reschedule'),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]);
    }
}
