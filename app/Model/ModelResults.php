<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelResults extends Authenticatable
{
    public function getResultsH1(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
        ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
        ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
        ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
        ->join('tbl_kode_pekerjaan', 'tbl_kode_pekerjaan.kode_pekerjaan', 'tbl_upload_data.kode_pekerjaan')
        ->join('tbl_hasil_survey_h1', 'tbl_hasil_survey_h1.id_data', 'tbl_upload_data.id');

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

    return $data
        ->where('tbl_upload_data.flag', '=', 3)
        ->orderby('tbl_dealer.nama_dealer', 'asc')
        ->get([
            'tbl_upload_data.id as id_data', 'tbl_dealer.id',
            'tbl_jaringan.nama_jaringan', 'tbl_dealer.kode_dealer', 'tbl_kabupaten.nama_kabupaten',
            'tbl_upload_data.nama_lengkap', 'tbl_upload_data.no_hp', 'tbl_upload_data.alamat', 'tbl_upload_data.tipe', 'tbl_upload_data.nomor_rangka', 'tbl_upload_data.gender', 'tbl_upload_data.tgl_lahir', 'tbl_upload_data.umur',
            DB::raw('
                (CASE WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 17 AND 20 THEN "17-20"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 21 AND 25 THEN "21-25"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 26 AND 30 THEN "26-30"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 31 AND 35 THEN "31-35"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 36 AND 40 THEN "36-40"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 41 AND 45 THEN "41-45"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 46 AND 50 THEN "46-50"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 51 AND 55 THEN "51-55"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) > 56 THEN "56++"
                ELSE "Other" END) AS age_group'
            ), 'tbl_kode_pendidikan.nama_pendidikan', 'tbl_kode_pekerjaan.nama_pekerjaan', 'tbl_kode_pengeluaran.pengeluaran',
            'tbl_hasil_survey_h1.a1', 'tbl_hasil_survey_h1.a2',
            'tbl_hasil_survey_h1.a3_1', 'tbl_hasil_survey_h1.a3_2', 'tbl_hasil_survey_h1.a3_3', 'tbl_hasil_survey_h1.a3_4',
            'tbl_hasil_survey_h1.b1_1', 'tbl_hasil_survey_h1.b1_2', 'tbl_hasil_survey_h1.b1_3', 'tbl_hasil_survey_h1.b1_3a', 'tbl_hasil_survey_h1.b1_4', 'tbl_hasil_survey_h1.b1_5', 'tbl_hasil_survey_h1.b1_5a', 'tbl_hasil_survey_h1.b3', 'tbl_hasil_survey_h1.b4',
            'tbl_hasil_survey_h1.d15',
            'tbl_hasil_survey_h1.e1_1', 'tbl_hasil_survey_h1.e1_2', 'tbl_hasil_survey_h1.e1_3',
            'tbl_hasil_survey_h1.f2_1', 'tbl_hasil_survey_h1.f2_2',
            'tbl_hasil_survey_h1.g1',
            'tbl_hasil_survey_h1.h1_1', 'tbl_hasil_survey_h1.h1_2', 'tbl_hasil_survey_h1.h1_3', 'tbl_hasil_survey_h1.h1_4', 'tbl_hasil_survey_h1.h1_5', 'tbl_hasil_survey_h1.h1_5a', 'tbl_hasil_survey_h1.h1_5b', 'tbl_hasil_survey_h1.h1_5c', 'tbl_hasil_survey_h1.h1_5d', 'tbl_hasil_survey_h1.h1_5d_note', 'tbl_hasil_survey_h1.h1_6', 'tbl_hasil_survey_h1.h2',
            'tbl_hasil_survey_h1.h3_a1', 'tbl_hasil_survey_h1.h3_a2', 'tbl_hasil_survey_h1.h3_a3', 'tbl_hasil_survey_h1.h3_a4', 'tbl_hasil_survey_h1.h3_a5',
            'tbl_hasil_survey_h1.i1_1', 'tbl_hasil_survey_h1.i1_2',
            'tbl_hasil_survey_h1.j1_1', 'tbl_hasil_survey_h1.j1_2',
            'tbl_hasil_survey_h1.n1', 'tbl_hasil_survey_h1.n2_month', 'tbl_hasil_survey_h1.n2_years', 'tbl_hasil_survey_h1.n2_note', 'tbl_hasil_survey_h1.n3', 'tbl_hasil_survey_h1.n3_note', 'tbl_hasil_survey_h1.n4', 'tbl_hasil_survey_h1.n4_note', 'tbl_hasil_survey_h1.n5', 'tbl_hasil_survey_h1.n5_note', 'tbl_hasil_survey_h1.n7', 'tbl_hasil_survey_h1.n7_note', 'tbl_hasil_survey_h1.n8', 'tbl_hasil_survey_h1.n8_note', 'tbl_hasil_survey_h1.n9'
        ]);
    }

    public function getResultsH2(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
        ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
        ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
        ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
        ->join('tbl_kode_pekerjaan', 'tbl_kode_pekerjaan.kode_pekerjaan', 'tbl_upload_data.kode_pekerjaan')
        ->join('tbl_hasil_survey_h2', 'tbl_hasil_survey_h2.id_data', 'tbl_upload_data.id');

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

    return $data
        ->where('tbl_upload_data.flag', '=', 3)
        ->orderby('tbl_dealer.nama_dealer', 'asc')
        ->get([
            'tbl_upload_data.id as id_data', 'tbl_dealer.id',
            'tbl_jaringan.nama_jaringan', 'tbl_dealer.kode_dealer', 'tbl_kabupaten.nama_kabupaten',
            'tbl_upload_data.nama_lengkap', 'tbl_upload_data.no_hp', 'tbl_upload_data.alamat', 'tbl_upload_data.tipe', 'tbl_upload_data.nomor_rangka', 'tbl_upload_data.gender', 'tbl_upload_data.tgl_lahir', 'tbl_upload_data.umur',
            DB::raw('
                (CASE WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 17 AND 20 THEN "17-20"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 21 AND 25 THEN "21-25"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 26 AND 30 THEN "26-30"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 31 AND 35 THEN "31-35"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 36 AND 40 THEN "36-40"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 41 AND 45 THEN "41-45"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 46 AND 50 THEN "46-50"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 51 AND 55 THEN "51-55"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) > 56 THEN "56++"
                ELSE "Other" END) AS age_group'
            ), 'tbl_kode_pendidikan.nama_pendidikan', 'tbl_kode_pekerjaan.nama_pekerjaan', 'tbl_kode_pengeluaran.pengeluaran',
            'tbl_hasil_survey_h2.a1', 'tbl_hasil_survey_h2.a2',
            'tbl_hasil_survey_h2.a3_1', 'tbl_hasil_survey_h2.a3_2', 'tbl_hasil_survey_h2.a3_3', 'tbl_hasil_survey_h2.a3_3a', 'tbl_hasil_survey_h2.a3_3b', 'tbl_hasil_survey_h2.a3_2b', 'tbl_hasil_survey_h2.a3_2b_note', 'tbl_hasil_survey_h2.a3_3c', 'tbl_hasil_survey_h2.a3_3c_note', 'tbl_hasil_survey_h2.a3_4',
            'tbl_hasil_survey_h2.b1_1', 'tbl_hasil_survey_h2.b1_2',
            'tbl_hasil_survey_h2.c1_1', 'tbl_hasil_survey_h2.c1_2', 'tbl_hasil_survey_h2.c1_3', 'tbl_hasil_survey_h2.c1_3a', 'tbl_hasil_survey_h2.c1_4', 'tbl_hasil_survey_h2.c1_4a', 'tbl_hasil_survey_h2.c1_2a', 'tbl_hasil_survey_h2.c1_2a_note', 'tbl_hasil_survey_h2.c2', 'tbl_hasil_survey_h2.c2_note', 'tbl_hasil_survey_h2.c3', 'tbl_hasil_survey_h2.c3_note', 'tbl_hasil_survey_h2.c1_5',
            'tbl_hasil_survey_h2.d1', 'tbl_hasil_survey_h2.d2', 'tbl_hasil_survey_h2.d3_1', 'tbl_hasil_survey_h2.d3_2', 'tbl_hasil_survey_h2.d3_3', 'tbl_hasil_survey_h2.d3_3a', 'tbl_hasil_survey_h2.d4', 'tbl_hasil_survey_h2.d5',
            'tbl_hasil_survey_h2.e1_1', 'tbl_hasil_survey_h2.e1_1a', 'tbl_hasil_survey_h2.e1_2', 'tbl_hasil_survey_h2.e1_3', 'tbl_hasil_survey_h2.e1_4',
            'tbl_hasil_survey_h2.g1', 'tbl_hasil_survey_h2.g2', 'tbl_hasil_survey_h2.g3',
            'tbl_hasil_survey_h2.h1', 'tbl_hasil_survey_h2.h2', 'tbl_hasil_survey_h2.h3', 'tbl_hasil_survey_h2.h4', 'tbl_hasil_survey_h2.i4', 'tbl_hasil_survey_h2.i4_note', 'tbl_hasil_survey_h2.i5', 'tbl_hasil_survey_h2.i6', 'tbl_hasil_survey_h2.i7', 'tbl_hasil_survey_h2.i8', 'tbl_hasil_survey_h2.i8_note', 'tbl_hasil_survey_h2.i9', 'tbl_hasil_survey_h2.i9_note',
            'tbl_hasil_survey_h2.k1',
            'tbl_hasil_survey_h2.n1', 'tbl_hasil_survey_h2.n2', 'tbl_hasil_survey_h2.n3_month', 'tbl_hasil_survey_h2.n3_years', 'tbl_hasil_survey_h2.n3_note', 'tbl_hasil_survey_h2.n4', 'tbl_hasil_survey_h2.n4_note', 'tbl_hasil_survey_h2.n5', 'tbl_hasil_survey_h2.n5_note', 'tbl_hasil_survey_h2.n6', 'tbl_hasil_survey_h2.n6_note', 'tbl_hasil_survey_h2.n7_note', 'tbl_hasil_survey_h2.n8'
        ]);
    }

    public function getResultsH3(Request $request)
    {
        $data = DB::table('tbl_dealer')
        ->join('tbl_jaringan', 'tbl_jaringan.id', 'tbl_dealer.id_jaringan')
        ->join('tbl_kares', 'tbl_kares.id', 'tbl_dealer.id_kares')
        ->join('tbl_kabupaten', 'tbl_kabupaten.id', 'tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data', 'tbl_upload_data.kode_dealer', 'tbl_dealer.kode_dealer')
        ->join('tbl_finance_company', 'tbl_finance_company.kode_finance_company', 'tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan', 'tbl_kode_pendidikan.kode_pendidikan', 'tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran', 'tbl_kode_pengeluaran.kode_pengeluaran', 'tbl_upload_data.kode_pengeluaran')
        ->join('tbl_kode_pekerjaan', 'tbl_kode_pekerjaan.kode_pekerjaan', 'tbl_upload_data.kode_pekerjaan')
        ->join('tbl_hasil_survey_h3', 'tbl_hasil_survey_h3.id_data', 'tbl_upload_data.id');

        if($request->get('idKares')){
            $data = $data->where('tbl_kares.id',$request->get('idKares'));
        }

        if($request->get('idKab')){
            $data = $data->where('tbl_kabupaten.id',$request->get('idKab'));
        }

        if($request->get('idDlr')){
            $data = $data->where('tbl_dealer.id',$request->get('idDlr'));
        }

        if ($request->get('months')){
            $data = $data->whereIn(DB::raw('MONTH(tbl_upload_data.tgl_cetak)'), $request->get('months'));
        }

        if ($request->get('years')){
            $data = $data->whereIn(DB::raw('YEAR(tbl_upload_data.tgl_cetak)'), $request->get('years'));
        }

    return $data
        ->where('tbl_upload_data.flag', '=', 3)
        ->orderby('tbl_dealer.nama_dealer', 'asc')
        ->get([
            'tbl_upload_data.id as id_data', 'tbl_dealer.id',
            'tbl_jaringan.nama_jaringan', 'tbl_dealer.kode_dealer', 'tbl_kabupaten.nama_kabupaten',
            'tbl_upload_data.nama_lengkap', 'tbl_upload_data.no_hp', 'tbl_upload_data.alamat', 'tbl_upload_data.tipe', 'tbl_upload_data.nomor_rangka', 'tbl_upload_data.gender', 'tbl_upload_data.tgl_lahir', 'tbl_upload_data.umur',
            DB::raw('
                (CASE WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 17 AND 20 THEN "17-20"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 21 AND 25 THEN "21-25"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 26 AND 30 THEN "26-30"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 31 AND 35 THEN "31-35"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 36 AND 40 THEN "36-40"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 41 AND 45 THEN "41-45"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 46 AND 50 THEN "46-50"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) BETWEEN 51 AND 55 THEN "51-55"
                WHEN TIMESTAMPDIFF(YEAR, tbl_upload_data.tgl_lahir, CURDATE()) > 56 THEN "56++"
                ELSE "Other" END) AS age_group'
            ), 'tbl_kode_pendidikan.nama_pendidikan', 'tbl_kode_pekerjaan.nama_pekerjaan', 'tbl_kode_pengeluaran.pengeluaran',
            'tbl_hasil_survey_h3.t1', 'tbl_hasil_survey_h3.t1_note',
            'tbl_hasil_survey_h3.a1_1', 'tbl_hasil_survey_h3.a1_2',
            'tbl_hasil_survey_h3.b1_1', 'tbl_hasil_survey_h3.b1_2', 'tbl_hasil_survey_h3.b1_3', 'tbl_hasil_survey_h3.b1_4',
            'tbl_hasil_survey_h3.c1', 'tbl_hasil_survey_h3.c1_a', 'tbl_hasil_survey_h3.c1_b', 'tbl_hasil_survey_h3.c2', 'tbl_hasil_survey_h3.c3', 'tbl_hasil_survey_h3.c4', 'tbl_hasil_survey_h3.c5',
            'tbl_hasil_survey_h3.d1', 'tbl_hasil_survey_h3.d2', 'tbl_hasil_survey_h3.d3_1', 'tbl_hasil_survey_h3.d3_2', 'tbl_hasil_survey_h3.d3_2a', 'tbl_hasil_survey_h3.d3_3', 'tbl_hasil_survey_h3.d3_3a', 'tbl_hasil_survey_h3.d3_3b', 'tbl_hasil_survey_h3.d4', 'tbl_hasil_survey_h3.d5',
            'tbl_hasil_survey_h3.e1', 'tbl_hasil_survey_h3.e2',
            'tbl_hasil_survey_h3.f1',
            'tbl_hasil_survey_h3.g1', 'tbl_hasil_survey_h3.g2', 'tbl_hasil_survey_h3.g3',
            'tbl_hasil_survey_h3.h1', 'tbl_hasil_survey_h3.h1_note'
        ]);
    }
}
