<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use DB;

class ModelUploadData extends Authenticatable
{
    protected $table = "tbl_upload_data";
    protected $primaryKey = "id";
    protected $guarded = ['created_at','updated_at'];
    protected $fillable = ['nomor_rangka','kode_mesin','nomor_mesin','tgl_cetak','tgl_mohon','nama_lengkap','alamat','kelurahan','kecamatan','kode_kota','kode_prov','cash_credit','kode_dealer','finance_company','down_payment','tenor','jenis_sales','gender','tgl_lahir','kode_agama','kode_pekerjaan','kode_pendidikan','kode_pengeluaran','sales_person','umur','tipe','tiga_jenis','flag','no_hp','no_telp','kode_data'];

    public function getAllData()
    {
        return $this->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->select('tbl_upload_data.*','tbl_finance_company.nama_finance_company','tbl_dealer.nama_dealer','tbl_kode_kota.nama_kota','tbl_kode_prov.nama_prov','tbl_kode_agama.nama_agama','tbl_kode_pekerjaan.nama_pekerjaan','tbl_kode_pendidikan.nama_pendidikan','tbl_kode_pengeluaran.pengeluaran')
        ->orderby('tbl_upload_data.kode_dealer')->get();
    }

    public function createData(Request $request)
    {
        $this->create([
            'nama_lengkap' => $request->input('nama_lengkap'),
            'flag' => $request->input('flag'),
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);
    }

    public function updateData(Request $request,$id){
        $this->where('id',$id)
            ->update([
                'nama_lengkap' => $request->input('nama_lengkap'),
                'flag' => $request->input('flag'),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]);
    }

    public function disableData($id)
    {
        $this->where('id',$id)->update([
            'flag' => 0,
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);
    }

    public function enableData($id)
    {
        $this->where('id',$id)->update([
            'flag' => 1,
        ]);
    }

    public function cekDuplikatData($nama,$kodeDealer,$no_hp)
    {
        return $this->select('nama_lengkap','kode_dealer','no_hp')
        ->where('nama_lengkap','=',$nama)
        ->where('kode_dealer','=',$kodeDealer)
        ->where('no_hp','=',$no_hp)
        ->where('flag','=','1')
        ->first();
    }

    public function cekDuplikatNoRangka($nomor_rangka)
    {
        return $this->select('nomor_rangka')
        ->where('nomor_rangka','=',$nomor_rangka)
        ->where('flag','=','1')
        ->first();
    }

    public function cekKodePekerjaan($kodePekerjaan)
    {
        return DB::table('tbl_kode_pekerjaan')->where('kode_pekerjaan','=',$kodePekerjaan)->first();
    }

    public function cekKodeDealer($kodeDealer)
    {
        return DB::table('tbl_dealer')->where('kode_dealer','=',$kodeDealer)->first();
    }

    public function cekKodeFinance($kodeFinance)
    {
        return DB::table('tbl_finance_company')->where('kode_finance_company','=',$kodeFinance)->first();
    }

    /* FLAG
        0 = NON AKTIF
        1 = AKTIF
        2 = SALAH SAMBUNG
        3 = TERSURVEY
        4 = RESCHEDULE

        lanjut survey 3 :
        Terhubung

        Reschedule 4 :
        Tidak Diangkat,
        Tidak Aktif/Mailbox,
        Di Luar Jangkauan,
        No Telp Sibuk

        Hapus 2 :
        No telp salah,
        reject,
        telp diangkat ditolak konsumen,
        salah sambung
        Terhubung tapi pernah survey,

        Temp 5 :
        Telpon dialihkan
        Terhubung Tapi tidak sesuai data
    */

    public function suksesSurvey($id)
    {
        return $this->where('id',$id)
            ->update(
            [
                'flag' => 3,
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]
        );
    }

    public function updateRescheduleData($id)
    {
        $this->where('id',$id)->update([
            'flag' => 4,
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);
    }

    public function salahSambungSurvey($id)
    {
        $this->where('id',$id)->update([
            'flag' => 2,
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);
    }

    public function RejectSurvey($id)
    {
        $this->where('id',$id)->update([
            'flag' => 5,
            'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
            'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
        ]);
    }
}
