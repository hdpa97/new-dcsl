<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelHasilSurveyH1 extends Authenticatable
{
    protected $table = "tbl_hasil_survey_h1";
    protected $primaryKey = "id";
    protected $guarded = ['created_at', 'updated_at'];
    protected $fillable = [
        'id_data', 'status_survey', 'tanda_kendaraan', 'kepemilikan', 'kode_data',
        'A1', 'A2', 'A3_1', 'A3_2', 'A3_3', 'A3_4',
        'B1_1', 'B1_2', 'B1_3', 'B1_3A', 'B1_4', 'B1_5', 'B1_5A', 'B3', 'B4',
        'D15', 'E1_1', 'E1_2', 'E1_3', 'F2_1', 'F2_2', 'G1',
        'H1_1', 'H1_2', 'H1_3', 'H1_4', 'H1_5', 'H1_5A', 'H1_5B', 'H1_5C', 'H1_5D', 'H1_5D_NOTE', 'H1_6', 'H2',
        'H3_A1', 'H3_A2', 'H3_A3', 'H3_A4', 'H3_A5', 'I1_1', 'I1_2', 'J1_1', 'J1_2',
        'N1', 'N2_MONTH', 'N2_YEARS', 'N2_NOTE', 'N3', 'N3_NOTE', 'N4', 'N4_NOTE', 'N5', 'N5_NOTE', 'N7', 'N7_NOTE', 'N8', 'N8_NOTE', 'N9'
    ];

    public function checkData($idData)
    {
        return $this->select('*')->where('id_data', $idData)->first();
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'id_data' => $request->input('idData'),
                'status_survey' => $request->input('status_survey'),
                'tanda_kendaraan' => $request->input('have_stnk'),
                'kepemilikan' => $request->input('kepemilikan'),
                'kode_data' => 'H1',
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]
        );
    }

    public function createDataH2H3(Request $request)
    {
        $this->create(
            [
                'id_data' => $request->input('idData'),
                'status_survey' => $request->input('status_survey'),
                'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
            ]
        );
    }

    // SAVE PER PAGE
    public function updateDataPage2(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'A1' => $request->input('a1'),
                    'A2' => $request->input('a2'),
                    'A3_1' => $request->input('a3_1'),
                    'A3_2' => $request->input('a3_2'),
                    'A3_3' => $request->input('a3_3'),
                    'A3_4' => $request->input('a3_4'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage3(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'B1_1' => $request->input('b1_1'),
                    'B1_2' => $request->input('b1_2'),
                    'B1_3' => $request->input('b1_3'),
                    'B1_3A' => $request->input('b1_3a'),
                    'B1_4' => $request->input('b1_4'),
                    'B1_5' => $request->input('b1_5'),
                    'B1_5A' => $request->input('b1_5a'),
                    'B3' => $request->input('b3'),
                    'B4' => $request->input('b4'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage4(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'D15' => $request->input('d15'),
                    'E1_1' => $request->input('e1_1'),
                    'E1_2' => $request->input('e1_2'),
                    'E1_3' => $request->input('e1_3'),
                    'F2_1' => $request->input('f2_1'),
                    'F2_2' => $request->input('f2_2'),
                    'G1' => $request->input('g1'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage5(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'H1_1' => $request->input('h1_1'),
                    'H1_2' => $request->input('h1_2'),
                    'H1_3' => $request->input('h1_3'),
                    'H1_4' => $request->input('h1_4'),
                    'H1_5' => $request->input('h1_5'),
                    'H1_5A' => $request->input('h1_5a'),
                    'H1_5B' => $request->input('h1_5b'),
                    'H1_5C' => $request->input('h1_5c'),
                    'H1_5D' => $request->input('h1_5d'),
                    'H1_5D_NOTE' => $request->input('h1_5d_note'),
                    'H1_6' => $request->input('h1_6'),
                    'H2' => $request->input('h2'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage6(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'H3_A1' => $request->input('h3_a1'),
                    'H3_A2' => $request->input('h3_a2'),
                    'H3_A3' => $request->input('h3_a3'),
                    'H3_A4' => $request->input('h3_a4'),
                    'H3_A5' => $request->input('h3_a5'),
                    'I1_1' => $request->input('i1_1'),
                    'I1_2' => $request->input('i1_2'),
                    'J1_1' => $request->input('j1_1'),
                    'J1_2' => $request->input('j1_2'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage7(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'N1' => $request->input('n1'),
                    'N2_MONTH' => $request->input('n2_month'),
                    'N2_YEARS' => $request->input('n2_years'),
                    'N2_NOTE' => $request->input('n2_note'),
                    'N3' => $request->input('n3'),
                    'N3_NOTE' => $request->input('n3_note'),
                    'N4' => $request->input('n4'),
                    'N4_NOTE' => $request->input('n4_note'),
                    'N5' => $request->input('n5'),
                    'N5_NOTE' => $request->input('n5_note'),
                    'N7' => $request->input('n7'),
                    'N7_NOTE' => $request->input('n7_note'),
                    'N8' => $request->input('n8'),
                    'N8_NOTE' => $request->input('n8_note'),
                    'N9' => $request->input('n9'),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage7H2(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    public function updateDataPage8H2(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    // SAVE DIAKHIR H1
    public function updateData(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'id_data' => $request->input('idData'),
                    'status_survey' => $request->input('status_survey'),
                    'tanda_kendaraan' => $request->input('have_stnk'),
                    'kepemilikan' => $request->input('kepemilikan'),
                    'A1' => $request->input('a1'),
                    'A2' => $request->input('a2'),
                    'A3_1' => $request->input('a3_1'),
                    'A3_2' => $request->input('a3_2'),
                    'A3_3' => $request->input('a3_3'),
                    'A3_4' => $request->input('a3_4'),
                    'B1_1' => $request->input('b1_1'),
                    'B1_2' => $request->input('b1_2'),
                    'B1_3' => $request->input('b1_3'),
                    'B1_3A' => $request->input('b1_3a'),
                    'B1_4' => $request->input('b1_4'),
                    'B1_5' => $request->input('b1_5'),
                    'B1_5A' => $request->input('b1_5a'),
                    'B3' => $request->input('b3'),
                    'B4' => $request->input('b4'),
                    'D15' => $request->input('d15'),
                    'E1_1' => $request->input('e1_1'),
                    'E1_2' => $request->input('e1_2'),
                    'E1_3' => $request->input('e1_3'),
                    'F2_1' => $request->input('f2_1'),
                    'F2_2' => $request->input('f2_2'),
                    'G1' => $request->input('g1'),
                    'H1_1' => $request->input('h1_1'),
                    'H1_2' => $request->input('h1_2'),
                    'H1_3' => $request->input('h1_3'),
                    'H1_4' => $request->input('h1_4'),
                    'H1_5' => $request->input('h1_5'),
                    'H1_5A' => $request->input('h1_5a'),
                    'H1_5B' => $request->input('h1_5b'),
                    'H1_5C' => $request->input('h1_5c'),
                    'H1_6' => $request->input('h1_6'),
                    'I1_1' => $request->input('i1_1'),
                    'I1_2' => $request->input('i1_2'),
                    'J1_1' => $request->input('j1_1'),
                    'J1_2' => $request->input('j1_2'),
                    'N1' => $request->input('n1'),
                    'N2' => $request->input('n2'),
                    'N3' => $request->input('n3'),
                    'N3_NOTE' => $request->input('n3_note'),
                    'N4' => $request->input('n4'),
                    'N4_NOTE' => $request->input('n4_note'),
                    'N5' => $request->input('n5'),
                    'N5_NOTE' => $request->input('n5_note'),
                    'N7' => $request->input('n7'),
                    'N7_NOTE' => $request->input('n7_note'),
                    'N8' => $request->input('n8'),
                    'N8_NOTE' => $request->input('n8_note'),
                    'N9' => $request->input('n9'),
                    'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    // SAVE DIAKHIR H2
    public function updateDataH2(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'id_data' => $request->input('idData'),
                    'status_survey' => $request->input('status_survey'),

                    'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }

    // SAVE DIAKHIR H3
    public function updateDataH3(Request $request, $id)
    {
        $this->where('id_data', $id)
            ->update(
                [
                    'id_data' => $request->input('idData'),
                    'status_survey' => $request->input('status_survey'),

                    'created_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now('Asia/Jakarta')->toDateTimeString()
                ]
            );
    }
}
