<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelAgama extends Authenticatable
{
    protected $table = "tbl_kode_agama";
    protected $primaryKey = "id";
    protected $fillable = ['kode_agama', 'nama_agama', 'flag'];


    public function getAllData()
    {
        return $this->where('flag', '=', '1')->get();
    }

    public function createData(Request $request)
    {
        $this->create(
            [
                'kode_agama' => $request->input('kode_agama'),
                'nama_agama' => $request->input('nama_agama'),
                'flag' => $request->input('flag')
            ]
        );
    }

    public function updateData(Request $request, $id)
    {
        $this->where('id', $id)
            ->update([
                'kode_agama' => $request->input('kode_agama'),
                'nama_agama' => $request->input('nama_agama'),
                'flag' => $request->input('flag')
            ]);
    }

    public function disableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 0,
        ]);
    }

    public function enableData($id)
    {
        $this->where('id', $id)->update([
            'flag' => 1,
        ]);
    }
}
