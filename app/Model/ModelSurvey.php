<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;

class ModelSurvey extends Authenticatable
{

    public function getSurveyReject(Request $request, $idSurveyor)
    {
        return DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where(function ($query) use ($idSurveyor) {
            $query->where('tbl_dealer.id_surveyor', '=', $idSurveyor)
                  ->Where('tbl_upload_data.flag', '=', 2)
                  ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString());
        })->orwhere(function ($query) use ($idSurveyor){
            $query->where('tbl_dealer.id_surveyor', '=', $idSurveyor)
                  ->Where('tbl_upload_data.flag', '=', 5)
                  ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString());
        })
        ->groupby('tbl_dealer.nama_dealer')
        ->orderby('tbl_dealer.id','asc')
        ->get(['tbl_dealer.id','tbl_jaringan.nama_jaringan','tbl_kares.nama_kares','tbl_kabupaten.nama_kabupaten','tbl_dealer.nama_dealer','tbl_dealer.kode_dealer',
        DB::raw('count(if(tbl_upload_data.kode_data="H1",1,null)) CT_H1'), DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H2'), 
        DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H3')]);
    }

    public function getSurveyTersedia(Request $request, $idSurveyor)
    {
        return DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where(function ($query) use ($idSurveyor) {
            $query->where('tbl_dealer.id_surveyor', '=', $idSurveyor)
                  ->Where('tbl_upload_data.flag', '=', 1)
                  ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString());
        })->orwhere(function ($query) use ($idSurveyor){
            $query->where('tbl_dealer.id_surveyor', '=', $idSurveyor)
                  ->Where('tbl_upload_data.flag', '=', 4)
                  ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString());
        })
        ->groupby('tbl_dealer.nama_dealer')
        ->orderby('tbl_dealer.id','asc')
        ->get(['tbl_dealer.id','tbl_jaringan.nama_jaringan','tbl_kares.nama_kares','tbl_kabupaten.nama_kabupaten','tbl_dealer.nama_dealer','tbl_dealer.kode_dealer',
        DB::raw('count(if(tbl_upload_data.kode_data="H1",1,null)) CT_H1'), DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H2'), 
        DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H3')]);
    }

    public function getSingleData($kodeDealer,$idData)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where('tbl_dealer.kode_dealer',$kodeDealer)
        ->where('tbl_upload_data.id',$idData)
        ->where('tbl_upload_data.kode_data','=','H1')
        ->first([
            'tbl_upload_data.id','tbl_upload_data.kode_dealer','tbl_upload_data.nama_lengkap','tbl_upload_data.tipe','tbl_upload_data.cash_credit'
        ]);
    }

    public function getSingleDataH2H3($kodeDealer, $idData)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where('tbl_dealer.kode_dealer',$kodeDealer)
        ->where('tbl_upload_data.id',$idData)
        ->where('tbl_upload_data.kode_data','=','H23')
        ->first([
            'tbl_upload_data.id','tbl_upload_data.kode_dealer','tbl_upload_data.nama_lengkap','tbl_upload_data.tipe','tbl_upload_data.cash_credit'
        ]);
    }

    public function getSpesificDataH1($kodeDealer)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->where(function ($query) use ($kodeDealer) {
            $query->where('tbl_upload_data.kode_dealer', '=', $kodeDealer)
                  ->where('tbl_upload_data.flag', '=', 1)
                  ->where('tbl_upload_data.kode_data',"H1")
                  ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString());
        })
        ->get([
            'tbl_upload_data.*','tbl_kode_agama.nama_agama','tbl_kode_kota.nama_kota','tbl_kode_prov.nama_prov','tbl_kode_pekerjaan.nama_pekerjaan','tbl_finance_company.nama_finance_company'
        ]);
    }

    public function getRescheduleDataH1($kodeDealer)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_data_reschedule','tbl_data_reschedule.id_data','tbl_upload_data.id')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where('tbl_dealer.kode_dealer', $kodeDealer)
        ->where('tbl_upload_data.flag',4)
        ->where('tbl_upload_data.kode_data','H1')
        ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString())
        ->orderby('tbl_data_reschedule.tgl_reschedule','asc')
        ->get([
            'tbl_data_reschedule.tgl_reschedule', 'tbl_upload_data.*', 'tbl_kode_agama.nama_agama', 'tbl_kode_kota.nama_kota', 
            'tbl_kode_prov.nama_prov', 'tbl_kode_pekerjaan.nama_pekerjaan', 'tbl_finance_company.nama_finance_company'
        ]);
    }

    public function getRescheduleDataH23($kodeDealer)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_data_reschedule','tbl_data_reschedule.id_data','tbl_upload_data.id')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where('tbl_dealer.kode_dealer', $kodeDealer)
        ->where('tbl_upload_data.flag',4)
        ->where('tbl_upload_data.kode_data','H23')
        ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString())
        ->orderby('tbl_data_reschedule.tgl_reschedule','asc')
        ->get([
            'tbl_data_reschedule.tgl_reschedule', 'tbl_upload_data.*', 'tbl_kode_agama.nama_agama', 'tbl_kode_kota.nama_kota', 
            'tbl_kode_prov.nama_prov', 'tbl_kode_pekerjaan.nama_pekerjaan', 'tbl_finance_company.nama_finance_company'
        ]);
    }

    public function getSpesificDataH2H3($kodeDealer)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->where(function ($query) use ($kodeDealer) {
            $query->where('tbl_upload_data.kode_dealer', '=', $kodeDealer)
                  ->where('tbl_upload_data.flag', '=', 1)
                  ->where('tbl_upload_data.kode_data',"H23")
                  ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString());
        })
        ->get([
            'tbl_upload_data.*','tbl_kode_agama.nama_agama','tbl_kode_kota.nama_kota','tbl_kode_prov.nama_prov','tbl_kode_pekerjaan.nama_pekerjaan','tbl_finance_company.nama_finance_company'
        ]);
    }

    
    public function getSingleResheduleData($kodeDealer, $idData)
    {
        return DB::table('tbl_upload_data')
        ->join('tbl_dealer','tbl_dealer.kode_dealer','tbl_upload_data.kode_dealer')
        ->join('tbl_kode_agama','tbl_kode_agama.kode_agama','tbl_upload_data.kode_agama')
        ->join('tbl_kode_pekerjaan','tbl_kode_pekerjaan.kode_pekerjaan','tbl_upload_data.kode_pekerjaan')
        ->join('tbl_kode_kota','tbl_kode_kota.kode_kota','tbl_upload_data.kode_kota')
        ->join('tbl_kode_prov','tbl_kode_prov.kode_prov','tbl_upload_data.kode_prov')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where('tbl_dealer.kode_dealer',$kodeDealer)
        ->where('tbl_upload_data.id',$idData)
        ->where('tbl_upload_data.flag',4)
        ->first([
            'tbl_upload_data.kode_data','tbl_upload_data.nama_lengkap'
        ]);
    }

    public function getSurveyReport(Request $request, $idSurveyor)
    {
        return DB::table('tbl_dealer')
        ->join('tbl_jaringan','tbl_jaringan.id','tbl_dealer.id_jaringan')
        ->join('tbl_kares','tbl_kares.id','tbl_dealer.id_kares')
        ->join('tbl_kabupaten','tbl_kabupaten.id','tbl_dealer.id_kabupaten')
        ->join('tbl_upload_data','tbl_upload_data.kode_dealer','tbl_dealer.kode_dealer')
        ->join('tbl_finance_company','tbl_finance_company.kode_finance_company','tbl_upload_data.finance_company')
        ->join('tbl_kode_pendidikan','tbl_kode_pendidikan.kode_pendidikan','tbl_upload_data.kode_pendidikan')
        ->join('tbl_kode_pengeluaran','tbl_kode_pengeluaran.kode_pengeluaran','tbl_upload_data.kode_pengeluaran')
        ->where('tbl_upload_data.updated_at', '>=', \Carbon\Carbon::now()->subMonth(3)->toDateTimeString())
        ->where('tbl_dealer.id_surveyor', $idSurveyor)
        ->where('tbl_upload_data.flag','=','3')
        ->groupby('tbl_dealer.nama_dealer')
        ->orderby('tbl_dealer.id','asc')
        ->get(['tbl_jaringan.nama_jaringan', 'tbl_dealer.id', 'tbl_kares.nama_kares', 'tbl_kabupaten.nama_kabupaten','tbl_dealer.nama_dealer','tbl_dealer.kode_dealer','tbl_finance_company.nama_finance_company',
        DB::raw('count(if(tbl_upload_data.kode_data="H1",1,null)) CT_H1'), DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H2'), DB::raw('count(if(tbl_upload_data.kode_data="H23",1,null)) CT_H3')]);
    }
}
