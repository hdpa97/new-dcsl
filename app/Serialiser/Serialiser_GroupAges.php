<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_GroupAges implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->age_group;
        $row[] = $data->age_count;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Age Group',
            'Jumlah Data'
        ];
    }
}

?>
