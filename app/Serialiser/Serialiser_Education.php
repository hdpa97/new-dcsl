<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_Education implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->nama_pendidikan;
        $row[] = $data->total;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Pendidikan',
            'Jumlah Data'
        ];
    }
}

?>
