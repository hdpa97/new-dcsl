<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_Gender implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->male;
        $row[] = $data->woman;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Pekerjaan',
            'Jumlah Data'
        ];
    }
}

?>
