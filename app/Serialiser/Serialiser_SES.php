<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_SES implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->huruf_pengeluaran;
        $row[] = $data->pengeluaran;
        $row[] = $data->total;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Kode',
            'Pengeluaran',
            'Jumlah Data'
        ];
    }
}

?>
