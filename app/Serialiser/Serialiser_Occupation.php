<?php

namespace App\Serialiser;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class Serialiser_Occupation implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->nama_pekerjaan;
        $row[] = $data->total;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Pekerjaan',
            'Jumlah Data'
        ];
    }
}

?>
